# coding: utf-8
import os
import numpy as np
import h5py
import scipy
import matplotlib
import time
from tqdm import tqdm
# from joblib import Parallel, delayed
from pathlib import Path

from IPython import get_ipython
if get_ipython() is not None:
    get_ipython().run_line_magic('load_ext', 'autoreload')
    get_ipython().run_line_magic('autoreload', '2')

    get_ipython().run_line_magic('matplotlib', 'inline')
    # matplotlib.use('Qt5Agg')

import matplotlib.pyplot as plt

from common import timer, encode_labels, decode_labels
from densityanalysis import DensityAnalysis, compare_density_analysis
import config

matplotlib.rcParams.update({'font.size': 22})

"""

conda install -c conda-forge numpy scipy matplotlib tqdm joblib pandas seaborn numba hashlib


conda install -c conda-forge ipykernel
python -m ipykernel install --user --name iwc

"""

# ------------------------------------------------------------------------------

if __name__ == '__main__':

    STRIDE = 1
    CATION = 'NA'

    # ARR = np.asarray(['*', 2, 2, '*', '*', '*', '*', '*'])
    # ARR = np.asarray(['*', 6, 0, '*', '*', '*', '*', '*'])
    # ARR = np.asarray(['*', 4, 0, '*', '*', '*', '*', '*'])
    # ARR = {'num_ow': 4, 'num_cl': 0, 'coord_num': 4}
    # ARR = {'num_ow': 3, 'num_cl': 1, 'coord_num': 4}
    # ARR = {'num_ow': 2, 'num_cl': 2, 'coord_num': 4}

    # ARR = {'num_ow': 5, 'num_cl': 0, 'coord_num': 5}
    # ARR = {'num_ow': 4, 'num_cl': 1, 'coord_num': 5}
    # ARR = {'num_ow': 5, 'num_cl': 2, 'coord_num': 5}
    # ARR = {'num_ow': 2, 'num_cl': 3, 'coord_num': 5}

    ARR = {'num_ow': 6, 'num_cl': 0, 'coord_num': 6}
    # ARR = {'num_ow': 5, 'num_cl': 1, 'coord_num': 6}
    # ARR = {'num_ow': 4, 'num_cl': 2, 'coord_num': 6}
    # ARR = {'num_ow': 3, 'num_cl': 3, 'coord_num': 6}
    # ARR = {'num_ow': 2, 'num_cl': 4, 'coord_num': 6}

    CONFIG = config.Config(
            CATION=CATION,
            stride=STRIDE,
            num_bins=(97, 99, 101,),
            # num_bins=(195, 197, 199,),
            # num_bins=(295, 297, 299,),
            hist_range=[(-9,9), (-9,9), (-9,9),],
            aligning_method='num_atoms',
            configuration_in_shells=ARR)

    # --------------------------------------------------------------------------

    DA = {}

    for atom_type in ['OW', 'HW']:
        DA[atom_type] = DensityAnalysis(
                CONFIG,
                atom_type=atom_type)

    # ==========================================================================

    range = {}
    xrange = {}

    SKIP_AT = set(['HW'])

    # --------------------------------------------------------------------------
    # --------------------------------------------------------------------------

    PAR = {
        'use_atom_weight': False,
        'normalization': 1e-3,
        'log_transform': False,
        }

    xrange['OW'] = np.linspace(1,8,90)
    xrange['HW'] = np.linspace(1,8,90)

    range['OW'] = [np.linspace(1,6,50), np.linspace(0,6,50)]
    range['HW'] = [np.linspace(1,6,50), np.linspace(0,4,50)]

    for atom_type in set(['OW', 'HW']) - SKIP_AT:
        # DA[atom_type].analyze_rdf(xrange=xrange[atom_type])

        # DA[atom_type].plot_rdf(fig=fig, ax=ax)
        # DA[atom_type].plot_cdf(fig=fig, ax=ax)

        DA[atom_type].analyze(**PAR, range=range[atom_type])
