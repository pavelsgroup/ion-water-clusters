#!/bin/bash

# cd ~/repo/ion-water-clusters/; goconda; conda activate iwc;
# USAGE: bash analysis_parallel.sh LI 1 analysis_parallel.input.4
# USAGE: bash analysis_parallel.sh NA 1 analysis_parallel.input.6


cd $(mktemp -d)
cp /home/ondrej/repo/ion-water-clusters/* .

WRKDIR=$(pwd)

printf "\n\
======================================================================\n\
#\n\
#  This is script:     analysis_parallel.sh\n\
#\n\
#  WRKDIR = $WRKDIR\n\
#\n\
======================================================================\n"

parallel -j 6 --bar --keep-order \
  python analysis.py --cation {1}  --stride 1 --nhood-distance 10 \
      --num-neighbors 10 \
      --do-neighborhoods \
      --do-shell-distances \
      ::: LI NA K \
      >> parallel.log

# ------------------------------------------------------------------------------
# LI

parallel -j 6 --bar --colsep ' ' --no-run-if-empty --keep-order \
  --delay 600 \
  python analysis.py --cation LI --stride 1 --nhood-distance 10 \
      --configuration {1} {2} {3} \
      --do-aligning \
      :::: analysis_parallel.input.4 \
      >> parallel.log

parallel -j 1 --bar --colsep ' ' --no-run-if-empty --keep-order \
  --delay 30 \
  python analysis.py  --cation LI --stride 1 --nhood-distance 10 \
      --configuration {1} {2} {3} \
      --hist-num-bins 128 128 128 \
      --do-histograms \
      :::: analysis_parallel.input.4 \
      >> parallel.log

# ------------------------------------------------------------------------------
# NA

parallel -j 6 --bar --colsep ' ' --no-run-if-empty --keep-order \
  --delay 600 \
  python analysis.py --cation NA --stride 1 --nhood-distance 10 \
      --configuration {1} {2} {3} \
      --do-aligning \
      :::: analysis_parallel.input.6 \
      >> parallel.log

parallel -j 1 --bar --colsep ' ' --no-run-if-empty --keep-order \
  --delay 30 \
  python analysis.py  --cation NA --stride 1 --nhood-distance 10 \
      --configuration {1} {2} {3} \
      --hist-num-bins 128 128 128 \
      --do-histograms \
      :::: analysis_parallel.input.6 \
      >> parallel.log

# ------------------------------------------------------------------------------
# K

parallel -j 6 --bar --colsep ' ' --no-run-if-empty --keep-order \
  --delay 600 \
  python analysis.py --cation K --stride 1 --nhood-distance 10 \
      --configuration {1} {2} {3} \
      --do-aligning \
      :::: analysis_parallel.input.8 \
      >> parallel.log

parallel -j 1 --bar --colsep ' ' --no-run-if-empty --keep-order \
  --delay 30 \
  python analysis.py  --cation K --stride 1 --nhood-distance 10 \
      --configuration {1} {2} {3} \
      --hist-num-bins 128 128 128 \
      --do-histograms \
      :::: analysis_parallel.input.8 \
      >> parallel.log


printf "\n\
======================================================================\n\
#\n\
#  Finished:     analysis_parallel.sh\n\
#\n\
#  You might want to delete the tmp directory\n\
#
#  $WRKDIR\n\
#\n\
======================================================================\n"


echo $WRKDIR
