# ion-water-clusters

### Configuration
```
cp config.ini.template config.ini
vim config.ini
```

## Usage

```
goconda; conda activate iwc
pip install -e .
```

### Batch jobs

 * submit one job `analysis_LI_sub.sh`, `analysis_NA_sub.sh`, `analysis_K_sub.sh`
 * submit multiple jobs `submit_multiple_data_range.sh`

### 3D Plotting


```
goconda; conda activate iwc-mayavi
python cluster_plot_3d_K.py --centroid-id 1
bash plot.sh --cation=K --id-range=10
pkill -f "python (cluster|centroid)_plot_3d_*"
```

### plot histograms of different configurations (occurences)
Edit `shell_groups.py` -- setup in __main__ (change cation)
```
goconda; conda activate iwc
python iwc/shell_groups.py
```

### analyse 3d density - 2d histogram
`densityanalysis.py`