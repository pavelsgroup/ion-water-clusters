#!/bin/bash
#SBATCH -J iwc-K
#SBATCH --time=12:00:00
# SBATCH --time=48:00:00
# SBATCH --nodes=2
#SBATCH --nodes=1
#SBATCH --exclusive
#SBATCH -p cpu
#SBATCH --mem=754G
# SBATCH -p mem
# SBATCH -p bigmem

# SBATCH --export=DISABLE_TQDM=1

# view the output file using
# col -b < $1 | less

set -e

source /opt/uochb/soft/anaconda3/2019.03/etc/profile.d/conda.sh
conda activate $HOME/conda-envs/iwc

#source /opt/uochb/soft/spack/20190608/share/spack/setup-env.sh
#spack load parallel

#cd $(mktemp -d)

WRKDIR=$HOME/iwc-run-K-$SLURM_JOB_ID
mkdir -p $WRKDIR
cd $WRKDIR

set +e
cp $HOME/ion-water-clusters/* .
cp -r $HOME/ion-water-clusters/iwc .
cp -r $HOME/ion-water-clusters/iwc.egg-info .
set -e

WRKDIR=$(pwd)

# export DISABLE_TQDM=1

STRIDE=1
CATION=K

printf "\n\
======================================================================\n\
#\n\
#  This is script:     analysis_parallel.sh\n\
#\n\
#  WRKDIR = $WRKDIR\n\
#  CATION = $CATION\n\
#  STRIDE = $STRIDE\n\
#\n\
======================================================================\n"

#python analysis.py \
#      --cation $CATION  \
#      --stride $STRIDE \
#      --nhood-distance 10 \
#      --num-neighbors 10 \
#      --do-neighborhoods \
#| col -b

#python analysis.py \
#      --cation $CATION \
#      --stride $STRIDE \
#      --nhood-distance 10 \
#      --do-shell-distances \
#      --num-neighbors 10 \
#| col -b

# --centroid-source expand_configuration \
# python analysis.py --n-cores 36 \
#       --cation $CATION \
#       --stride $STRIDE \
#       --nhood-distance 10 \
#       --do-centroid-perturbation \
#       --centroid-source folder \
#       --centroid-folder centroids \
#       --configuration-generator 3 9 9 \
# | col -b

# only run this once before submitting all
# rm -rf KCl_spce/aligned-KCL-D10-$STRIDE

python iwc/analysis.py --n-cores 36 \
      --cation $CATION \
      --stride $STRIDE \
      --nhood-distance 10 \
      --do-aligning \
      --centroid-source folder \
      --process-id $SLURM_JOB_ID \
      --data-range $DATA_START $DATA_END \
      --max-chunk-size 1000000 \
| col -b

# python analysis.py --n-cores 36 \
#       --cation $CATION \
#       --stride $STRIDE \
#       --nhood-distance 10 \
#       --do-histograms \
#       --configuration-generator 3 9 2 \
#       --hist-num-bins 128 128 128 \
# | col -b

# ------------------------------------------------------------------------------

printf "\n\
======================================================================\n\
#\n\
#  Finished:     analysis_parallel.sh\n\
#\n\
#  You might want to delete the tmp directory\n\
#
#  $WRKDIR\n\
#\n\
======================================================================\n"


echo $WRKDIR
