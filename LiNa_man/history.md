
```bash
cd $HOME/ion-water-clusters
goconda; conda activate $HOME/conda-envs/iwc/
```

K
```bash
python iwc/analysis.py --n-cores 36 --cation K --do-print-data-info --stride 100

python iwc/analysis.py --n-cores 36 --cation K --do-centroid-perturbation --stride 100 --centroid-source expand_configuration --centroid-folder centroids --configuration-generator 6 11 11 --num-data-to-load 802043 --max-chunk-size 802043

python iwc/analysis.py --n-cores 36 --cation K --do-aligning --stride 100 --centroid-source folder --max-chunk-size 1000000

python iwc/analysis.py --n-cores 36 --cation K --do-histograms --stride 100 --configuration-generator 6 11 11 --hist-num-bins 64 64 64

# analyse configurations by plotting.py
# remove excess configurations

python iwc/analysis.py --n-cores 36 --cation K --do-aligning --stride 1 --centroid-source folder --max-chunk-size 1000000
```

NA
```bash
python iwc/analysis.py --n-cores 36 --cation NA --do-print-data-info --stride 100

python iwc/analysis.py --n-cores 36 --cation NA --do-centroid-perturbation --stride 100 --centroid-source expand_configuration --centroid-folder centroids --configuration-generator 5 8 8 --num-data-to-load 802791 --max-chunk-size 802791

python iwc/analysis.py --n-cores 36 --cation NA --do-print-data-info --stride 1 --num-chunks 36

# edit submit_multiple_data_range.sh
# edit analysis_NA_sub.sh

bash ion-water-clusters/submit_multiple_data_range.sh

python iwc/analysis.py --n-cores 36 --cation NA --do-histograms --stride 1 --configuration-generator 3 9 9 --hist-num-bins 128 128 128
```

LI
```bash
python iwc/analysis.py --n-cores 36 --cation LI --do-print-data-info --stride 100

python iwc/analysis.py --n-cores 36 --cation LI --do-centroid-perturbation --stride 100 --centroid-source expand_configuration --centroid-folder centroids --configuration-generator 4 6 6 --num-data-to-load 802791 --max-chunk-size 802791

python iwc/analysis.py --n-cores 36 --cation LI --do-aligning --stride 100 --centroid-source folder --max-chunk-size 1000000

python iwc/analysis.py --n-cores 36 --cation LI --do-histograms --stride 100 --configuration-generator 4 6 6 --hist-num-bins 64 64 64

scp -r histograms-LICL-D10-100/ aligned-LICL-D10-100/ centroids-LICL-D10-100 hyd_shells-LICL-D10-100/ hongman@hoaxinh:/wrk/iwc3/LiCl_spce

# analyse configurations by plotting.py

cp -r centroids-LICL-D10-100/ centroids-LICL-D10-1

# remove excess configurations
# (we keep 7)

python iwc/analysis.py --n-cores 36 --cation LI --do-aligning --stride 1 --centroid-source folder --max-chunk-size 1000000

```