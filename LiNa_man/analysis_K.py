# coding: utf-8
import numpy as np

from IPython import get_ipython
if get_ipython() is not None:
    get_ipython().run_line_magic('load_ext', 'autoreload')
    get_ipython().run_line_magic('autoreload', '2')

import config
import neighborhoods
import hydration_shells
import shell_distances
import clustering
import histograms


# ==============================================================================
# LI

STRIDE = 10

for CATION in ['K']:

    CONFIG = config.Config(
            CATION=CATION,
            nhood_distance=10,
            stride=STRIDE)

    # neighborhoods.main(**vars(CONFIG))
    # hydration_shells.main(CONFIG)
    # shell_distances.main(CONFIG, num_neighbors=10)

    for ARR in [
            # {'num_ow': 4, 'num_cl': 0, 'coord_num': 4},
            # {'num_ow': 3, 'num_cl': 1, 'coord_num': 4},
            # {'num_ow': 2, 'num_cl': 2, 'coord_num': 4},

            # {'num_ow': 5, 'num_cl': 0, 'coord_num': 5},
            # {'num_ow': 4, 'num_cl': 1, 'coord_num': 5},
            # {'num_ow': 5, 'num_cl': 2, 'coord_num': 5},
            # {'num_ow': 2, 'num_cl': 3, 'coord_num': 5},
            #
            # {'num_ow': 6, 'num_cl': 0, 'coord_num': 6},
            # {'num_ow': 5, 'num_cl': 1, 'coord_num': 6},
            # {'num_ow': 4, 'num_cl': 2, 'coord_num': 6},
            # {'num_ow': 3, 'num_cl': 3, 'coord_num': 6},
            # {'num_ow': 2, 'num_cl': 4, 'coord_num': 6},

            # {'num_ow': 6, 'num_cl': 0, 'coord_num': 6},
            # {'num_ow': 7, 'num_cl': 0, 'coord_num': 7},
            {'num_ow': 8, 'num_cl': 0, 'coord_num': 8},
            {'num_ow': 9, 'num_cl': 0, 'coord_num': 9},
            {'num_ow': 10, 'num_cl': 0, 'coord_num': 10},

            # np.asarray(['*', 4, 0, '*', '*', '*', '*', '*']),
            # np.asarray(['*', 3, 1, '*', '*', '*', '*', '*']),
            # np.asarray(['*', 2, 2, '*', '*', '*', '*', '*']),
            ]:

        for NB in [ (97, 99, 101,),
                    # (195, 197, 199,),
                    # (295, 297, 299,),
                  ]:
            CONFIG = config.Config(
                    CATION=CATION,
                    stride=STRIDE,
                    num_bins=NB,
                    nhood_distance=10,
                    hist_range=[(-9,9), (-9,9), (-9,9),],
                    aligning_method='num_atoms',
                    configuration_in_shells=ARR)

            clustering.main(CONFIG)
            # histograms.main(CONFIG)
