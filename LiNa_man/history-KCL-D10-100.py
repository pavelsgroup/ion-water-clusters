import os
import numpy as np

# KCL-D10-100

keep = [
'82f0ac74',
'b22ebe05',
'0abb34e6',
'8450342e',
'0cb9d780',
'3215cc0e',
'da66eb8d',
'd95d386c',
'635fefce',
'8732c708',
'1ad417dc',
'ddf69a37',
'01bef921',
'0ef60b87',
'2914221e',
'2bbac21f',
'059fa85c',
'49c47c92',
'cc4c8363_1',
'c8be0523',
'b3fd23fe_1',
'4555dfd7_1',
'88527bd8',
'c44e4b39_0',
'5c089b16_0',
]

d_in = 'centroids-KCL-D10-100_old'
d_out = 'centroids-KCL-D10-100'

for f in keep:
    ff = f"centroid_perturbed-{f}*.npz"
    print(f"keeping {ff}")
    os.system(f"cp {d_in}/{ff} {d_out}")

# print(np.unique(to_be_removed).size)
# print(len(to_be_removed))

# for f in to_be_removed:
#     print(np.where(f == np.asarray(to_be_removed)))

# if np.unique(to_be_removed).size != len(to_be_removed):
#     raise ValueError("values need to be unique")

# print(to_be_removed)

# for f in to_be_removed:
#     ff = f"centroid_perturbed-{f}*.npz"
#     print(f"removing {ff}")
#     os.system(f"rm {ff}")