#!/bin/bash
#SBATCH -J iwc-LI
#SBATCH --time=48:00:00
#SBATCH --nodes=1
#SBATCH --exclusive
#SBATCH -p bigmem

# SBATCH --export=DISABLE_TQDM=1

# view the output file using
# col -b < $1 | less

set -e

source /opt/uochb/soft/anaconda3/2019.03/etc/profile.d/conda.sh
conda activate $HOME/conda-envs/iwc

# source /opt/uochb/soft/spack/20190608/share/spack/setup-env.sh
# spack load parallel

#cd $(mktemp -d)

WRKDIR=$HOME/iwc-run-LI-$SLURM_JOB_ID
mkdir -p $WRKDIR
cd $WRKDIR

set +e
cp $HOME/ion-water-clusters/* .
cp -r $HOME/ion-water-clusters/iwc .
cp -r $HOME/ion-water-clusters/iwc.egg-info .
set -e

WRKDIR=$(pwd)

# export DISABLE_TQDM=1

STRIDE=1
CATION=LI

printf "\n\
======================================================================\n\
#\n\
#  This is script:     analysis_parallel.sh\n\
#\n\
#  WRKDIR = $WRKDIR\n\
#  CATION = $CATION\n\
#  STRIDE = $STRIDE\n\
#\n\
======================================================================\n"

#python iwc/analysis.py \
#      --cation $CATION \
#      --stride $STRIDE \
#      --nhood-distance 10 \
#      --num-neighbors 10 \
#      --do-neighborhoods \
#| col -b

#python iwc/analysis.py \
#      --cation $CATION \
#      --stride $STRIDE \
#      --nhood-distance 10 \
#      --do-shell-distances \
#      --num-neighbors 10 \
#| col -b

# python iwc/analysis.py --n-cores 36 \
#       --cation $CATION \
#       --stride $STRIDE \
#       --nhood-distance 10 \
#       --do-aligning \
#       --centroid-source expand_configuration \
#       --centroid-folder centroids \
#       --configuration-generator 4 5 5 \
# | col -b

python iwc/analysis.py --n-cores 36 \
      --cation $CATION \
      --stride $STRIDE \
      --nhood-distance 10 \
      --do-aligning \
      --centroid-source folder \
      --centroid-folder centroids \
      --configuration-generator 4 5 5 \
| col -b

python iwc/analysis.py --n-cores 36 \
      --cation $CATION \
      --stride $STRIDE \
      --nhood-distance 10 \
      --do-histograms \
      --configuration-generator 4 5 5 \
      --hist-num-bins 128 128 128 \
| col -b

# ------------------------------------------------------------------------------

printf "\n\
======================================================================\n\
#\n\
#  Finished:     analysis_parallel.sh\n\
#\n\
#  You might want to delete the tmp directory\n\
#
#  $WRKDIR\n\
#\n\
======================================================================\n"


echo $WRKDIR
