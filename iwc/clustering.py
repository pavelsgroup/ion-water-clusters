# coding: utf-8
import os
import shutil
import contextlib
import warnings
import numpy as np
import scipy
import itertools
import matplotlib
# import gnuplotlib as gp
#import seaborn as sns
import time
import re
import pprint
from pathlib import Path
import numba
from numba import jit
from hashlib import blake2s

# np.seterr('raise')

import threading

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from iwc.aligning import main_align_core, main_align_core_parallel, check_permutation_set_sizes, align_through_permutation, IncompatiblePermutationError, calculate_invariant, normalized_permutations

from iwc.common.common import logger, notqdm, disp_title, timer, split, chunk_loader, encode_labels, decode_labels
from iwc.common.common_numba import is_sorted, is_all_zero
import iwc.config as config

# from IPython import get_ipython
# if get_ipython() is not None:
#     get_ipython().run_line_magic('load_ext', 'autoreload')
#     get_ipython().run_line_magic('autoreload', '2')

#     get_ipython().run_line_magic('matplotlib', 'inline')
#     # matplotlib.use('Qt5Agg')

try:
    import h5py
except ModuleNotFoundError:
    logger.warning("Module h5py not found, data loading will be disabled")

try:
    from joblib import Parallel, delayed
except ModuleNotFoundError:
    logger.warning("Module joblib not found, parallel processing will be disabled")

try:
    import pandas as pd
except ModuleNotFoundError:
    logger.warning("Module pandas not found, ...")

try:
    import tqdm
except ModuleNotFoundError:
    logger.warning("Module tqdm not found, progress bar will be disabled")
    tqdm = notqdm

disable_tqdm = bool(os.environ.get("DISABLE_TQDM"))
if disable_tqdm is True:
    logger.info("Encountered environment variable DISABLE_TQDM, disabling tqdm")
    tqdm = notqdm

pp = pprint.PrettyPrinter(indent=4)

"""

conda install -c conda-forge numpy scipy matplotlib tqdm joblib pandas seaborn numba hashlib


conda install -c conda-forge ipykernel
python -m ipykernel install --user --name iwc

Profiling:

python -m cProfile -o clustering.profile analysis.py -

snakeviz clustering.profile

"""

PLOT = {
    'distance_distribution':            False,
    'distance_distribution_shells':     False,
    '3d_scatter_data':                  False,
    'align_metric_distribution':        False,
    'pairwise_distance':                False,
}

# ------------------------------------------------------------------------------

def permutation_subsets_by_reference(positions, labels, ref_lab, exclude_atom_types=[], full_shell_check=True, eps=0.2):
    """
    Returns indices of atoms that should be used for aligning to a reference group.
    """

    assert positions.shape[1] == 3, f"positions.shape = {positions.shape}"
    assert labels.shape[0] == positions.shape[0], f"positions.shape = {positions.shape},  labels.shape = {labels.shape[0]}"

    sets = dict()
    num = 0

    num_atoms = ref_lab.size

    # note which reference atoms have been assigned one atom from the target group
    assigned = np.zeros(ref_lab.size, dtype=bool)

    # note which atoms in the targer group have been used
    #   1 ... used
    #   0 ... not used
    #  -1 ... not applicable
    used = np.zeros(positions.shape[0], dtype='int8')

    # setup not applicable atoms
    for i, l in enumerate(labels):
        if l in exclude_atom_types:
            used[i] = -1

    for i, l in enumerate(labels): # assume sorted by distance
        if num < num_atoms:
            if l not in exclude_atom_types:
                # import pdb; pdb.set_trace()
                ind = np.where(np.logical_and(assigned == False, ref_lab == l))[0]
                if ind.size > 0:
                    ind = ind[0]
                    assigned[ind] = True
                    num += 1
                    key = str(l)
                    used[i] = 1
                    if key not in sets:
                        sets[key] = [i]
                    else:
                        sets[key].append(i)
        else:
            break
    ss = [np.asarray(sets[key]) for key in sorted(sets.keys())]
    n = [s.shape[0] for s in ss]

    dist = np.linalg.norm(positions, axis=1)
    assert dist.size == positions.shape[0]

    # Perform a check of the shell - the assumption is that if an atom is used,
    # all atoms with approx the same distance from the center should be used as
    # well. If this is not true, raise.
    if full_shell_check is True:
        iT = np.where(used == 1)[0] # indices of used atoms
        iF = np.where(used == 0)[0] # indices of unused atoms
        
        if iF.size > 0:
            if np.max(dist[iT]) > np.min(dist[iF]) - eps:
                raise IncompatiblePermutationError()

    if num_atoms != np.sum(n):
        raise IncompatiblePermutationError(f'{num_atoms} ?== {np.sum(n)}\n')

    return ss, n


def permutation_subsets_num_atoms(positions, labels, num_atoms, exclude_atom_types=[]):

    assert positions.shape[1] == 3
    assert labels.shape[0] == positions.shape[0]

    sets = dict()
    num = 0
    for i, l in enumerate(labels): # assume sorted by distance
        if num < num_atoms:
            if l not in exclude_atom_types:
                num += 1
                key = str(l)
                if key not in sets:
                    sets[key] = [i]
                else:
                    sets[key].append(i)
        else:
            break
    ss = [np.asarray(sets[key]) for key in sorted(sets.keys())]
    n = [s.shape[0] for s in ss]

    assert num_atoms == np.sum(n), f'{num_atoms} ?== {np.sum(n)}\n'

    return ss, n


def permutation_subsets_distance_intervals(positions, labels, distance_intervals, separate_atom_types=True):

    assert positions.shape[1] == 3
    assert labels.shape[0] == positions.shape[0]

    # print(positions)
    # print(labels)

    sets = []
    set_atom_types = []
    if separate_atom_types:
        for lab in set(labels):
            # print(lab)
            sets.append(np.where(labels == lab))
            set_atom_types.append(lab)
    else:
        sets.append(np.arange(len(labels)))
        set_atom_types.append('all')

    ss = []
    for s, t in zip(sets, decode_labels(set_atom_types)):
        pos = positions[s[0], :]
        dist = np.linalg.norm(pos, axis=1)
        # print(dist)

        if t in distance_intervals.keys():
            for interval in distance_intervals[t]:
                ind = np.where((dist > interval[0]) & (dist <= interval[1]))[0]
                if ind.size > 0:
                    ss.append(s[0][ind])

    # print(ss)

    n = [s.shape[0] for s in ss]

    return ss, n


def plot_atoms_3d(positions, labels):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    for atom_group in set(labels):
        pos = positions[np.where((labels == atom_group))]
        ax.scatter(pos[:, 0], pos[:, 1], pos[:, 2], color=colors[atom_group])
    plt.show()


def plot_3d_marginal_histograms(H, edges, dim, **kwargs):

    assert dim in [0, 1, 2]

    fig = plt.figure()
    ax = fig.gca()
    # ax.pcolormesh(*np.meshgrid(edges[2], edges[1]), np.sum(H, axis=0))

    W = np.sum(H, axis=dim)

    j = np.min(np.setdiff1d([0, 1, 2], [dim]))
    i = np.max(np.setdiff1d([0, 1, 2], [dim]))

    assert edges[i].size == W.shape[1] + 1
    assert edges[j].size == W.shape[0] + 1

    ax.pcolormesh(*np.meshgrid(edges[i], edges[j]), W, **kwargs)
    plt.show()


def configure_distance_intervals(shell_intervals, CONFIG):

    print("Current distance intervals are:")
    print(CONFIG.distance_intervals)

    distance_intervals = {}
    for atom_type in CONFIG.distance_intervals:
        if atom_type in shell_intervals:
            conf = CONFIG.distance_intervals[atom_type]
            if conf is not None:
                distance_intervals[atom_type] = []
                for conf_string in conf:
                    m = re.match(r"shell\s+([0-9]+)", conf_string)
                    if m:
                        shell = int(m.group(1))
                        shell_int = shell_intervals[atom_type][shell - 1]
                        distance_intervals[atom_type].append(shell_int)

    print("New distance intervals are:")
    print(distance_intervals)

    return distance_intervals


def configuration_selector(confs, labels, requested_config):
    DF = pd.DataFrame(data=confs, columns=labels)

    query = []
    for l, r in zip(labels, requested_config):
        if r == '*':
            pass
        else:
            query.append(f"{l} == {r}")

    query = " and ".join(query)
    # print(query)

    df = DF.query(query)

    return df.index.tolist()


def pairwise_distance(pos, lab, distance_intervals):

    assert pos.shape[0] == lab.shape[0]

    N = pos.shape[0]

    dist = np.zeros((N, N,))

    for i in tqdm.tqdm(np.arange(N)):

        perm_sets, _ = permutation_subsets(
            pos[i], lab[i], distance_intervals, separate_atom_types=True)

        perm_A = np.asarray(np.concatenate(perm_sets))
        for j in np.arange(i + 1, N):

            perm_sets, perm_sets_sizes = permutation_subsets(
                pos[j], lab[j], distance_intervals, separate_atom_types=True)

            AA = pos[i][perm_A, :]
            _, _, _, D = align_through_permutation(
                AA, pos[j], perm_sets, perm_sets_sizes)

            dist[i,j] = D
            dist[j,i] = D

    return dist


def search_for_new_centroids(pos, lab, num_atoms, out, align_kwargs, ntries=5):

    ndata = pos.shape[0]

    distances = np.zeros((ndata, ntries,), dtype=float)

    j = np.random.randint(ndata)

    reference_positions = []
    reference_labels = []
    inter_centroid_distances = np.zeros((ntries, ntries), dtype=float)

    for i in np.arange(ntries):

        ref_pos = np.copy(pos[j])
        ref_lab = np.copy(lab[j])
        
        dist, _, _, _, _, centroid_pos, centroid_lab, _ = main_align_num_atoms(
            pos, lab, num_atoms, ref_pos, ref_lab, **align_kwargs)

        distances[:,i] = np.sqrt(dist)

        reference_positions.append(centroid_pos)
        reference_labels.append(centroid_lab)

        D = np.amax(distances)
        j = np.argmin(np.linalg.norm(D - distances, axis=1))

        inter_centroid_distances[i,0:i] =  distances[j,0:i]
        inter_centroid_distances[0:i,i] =  distances[j,0:i]

        #print(j)
        # print(np.mean(distances, axis=0))
        # print(distances[j,:])
    
    mean_distances = np.zeros((ntries,), dtype=float)
    std_distances = np.zeros((ntries,), dtype=float)
    occurence_frequency = np.zeros((ntries,), dtype=int)

    ind = np.argmin(distances, axis=1)
    for i in np.arange(ntries):
        mean_distances[i] = np.mean(distances[ind == i,i])
        std_distances[i] = np.std(distances[ind == i,i])
        occurence_frequency[i] = np.count_nonzero(ind == i)

    # sort by occurence frequency (descending)
    sort = np.flip(np.argsort(occurence_frequency))

    centroid_statistics = {
        'inter_centroid_distances': (inter_centroid_distances[sort,:])[:,sort],
        'mean_distances': mean_distances[sort],
        'std_distances': std_distances[sort],
        'occurence_frequency': occurence_frequency[sort],
    }
    
    reference_positions = [reference_positions[i] for i in sort]
    reference_labels = [reference_labels[i] for i in sort]

    return reference_positions, reference_labels, centroid_statistics


@timer
def main_align_num_atoms(pos, lab, num_atoms, ref_pos, ref_lab,
        exclude_atom_types=None, estimate_centroid=True,
        return_distance=False, return_positions=False, return_labels=False,
        return_permutations=False, return_rotations=False, return_centroid=False,
        force_non_heuristic_alignment=False,
        full_shell_check=True,
        perm_subsets_fun='num_atoms', n_cores=None, num_centroid_iterations=10):

    assert pos.shape[0] == lab.shape[0]

    if np.any(pos == np.NaN):
        raise(ValueError("NaN vlaue encountered in target structure"))
    if np.any(ref_pos == np.NaN):
        raise(ValueError("NaN vlaue encountered in reference structure"))

    N = pos.shape[0]
    
    assert N > 0, 'Zero data provided'

    # Generate permutation subsets
    if perm_subsets_fun == 'num_atoms':
        perm_subsets_fun = lambda g, l: permutation_subsets_num_atoms(g, l, num_atoms, exclude_atom_types=exclude_atom_types)
    elif perm_subsets_fun == 'reference':
        perm_subsets_fun = lambda g, l: permutation_subsets_by_reference(g, l, ref_lab, exclude_atom_types=exclude_atom_types, full_shell_check=full_shell_check)
    else:
        raise(ValueError("unknown perm_sets_fun"))

    perm_sets, perm_sets_sizes = perm_subsets_fun(ref_pos, ref_lab)
    check_permutation_set_sizes(perm_sets_sizes)

    perm_sets_labels = [ref_lab[ps] for ps in perm_sets]

    logger.info('Aligning to: ' + str([decode_labels(ref_lab[i]) for i in np.concatenate(perm_sets)]) + str(perm_sets_sizes))

    # print(perm_sets, perm_sets_sizes)

    perm_A = np.asarray(np.concatenate(perm_sets))

    print(perm_A)

    if return_positions is True:
        pos_new = np.copy(pos)
    else:
        pos_new = None
    if return_labels is True:
        lab_new = np.copy(lab)
    else:
        lab_new = None

    reference = ref_pos[perm_A, :]
    reference_lab = ref_lab[perm_A]
    # print('REF', reference)

    # print(centroid_invariant(reference))

    if len(pos) < n_cores:
        logger.warning(f"Number of cores reduced due to dataset size")
        n_cores = len(pos)

    if n_cores == 1:
        align = main_align_core

    else:
        logger.info(f"Aligning in parallel using {n_cores} processes")
        align = main_align_core_parallel

    norm_perms, norm_perm_sets = normalized_permutations(perm_sets_sizes)

    if estimate_centroid is True:
        for i in np.arange(num_centroid_iterations):
            if N <= 100:
                stride = 1
                offset = 0
            else:
                stride = 10
                offset = offset=np.random.randint(10)

            centroid, centroid_std, _, _, _, _, _ = \
                align(pos, lab, N, reference,
                    perm_sets, perm_sets_sizes, perm_sets_labels, perm_subsets_fun,
                    norm_perms=norm_perms,
                    norm_perm_sets=norm_perm_sets,
                    return_distance=False,
                    return_positions=False,
                    return_labels=False,
                    return_permutations=False,
                    return_rotations=False,
                    return_centroid=True,
                    force_non_heuristic_alignment=force_non_heuristic_alignment,
                    stride=stride, offset=offset, n_cores=n_cores)

            if centroid.size == 0 or centroid.shape == ():
                logger.critical("Centroid is empty, alignment probably failed")
            elif np.any(np.isnan(centroid)):
                print(centroid)
                logger.critical("New centroid contains NaN values.")
                logger.warning("Continuing with old centroid as reference.")
            else:
                assert np.all(centroid.shape == reference.shape), f"{centroid.shape}, {reference.shape}"
                assert centroid.shape[1] == 3, centroid.shape

                perturbation = np.mean(np.linalg.norm(centroid - reference, axis=1))
                logger.info(f"move centroid points by {perturbation:.2g} on average")

                #print(centroid_invariant(centroid))

                reference = np.copy(reference + centroid) / 2

                #print(centroid_invariant(reference))

    calculate_invariant_statistics = True

    centroid, centroid_std, permutations, rotations, group_distance, aligned_partial_positions, num_perms = \
        align(pos, lab, N, reference,
            perm_sets, perm_sets_sizes, perm_sets_labels, perm_subsets_fun,
            norm_perms=norm_perms,
            norm_perm_sets=norm_perm_sets,
            return_distance=return_distance,
            return_positions=(return_positions or calculate_invariant_statistics),
            return_labels=return_labels,
            return_permutations=return_permutations,
            return_rotations=(return_rotations or return_positions),
            return_centroid=return_centroid,
            force_non_heuristic_alignment=force_non_heuristic_alignment,
            stride=1, n_cores=n_cores)

    # --------------------------------------------------------------------------

    M = np.product([np.math.factorial(n) for n in perm_sets_sizes])
    ii = num_perms > 0
    mean = num_perms[ii].mean()
    std = num_perms[ii].std()
    num_zero = np.count_nonzero(num_perms == 0)
    num_incompatible = np.count_nonzero(num_perms == -1)
    logger.warning(f"{mean:.1f} +- {std:.1f} out of {M} ({mean/M*100:.1f} %) num perms were tried on average")
    logger.warning(f"{num_incompatible} out of {N} ({num_incompatible/N*100:.1f} %) structures were incompatible")
    logger.warning(f"{num_zero} out of {N} ({num_zero/N*100:.1f} %) structures were incompatible due to invariant")
    
    # --------------------------------------------------------------------------

    if calculate_invariant_statistics is True:
        At = reference.T
        n = At.shape[1]
        I = np.zeros((n+1, n,))
        J = np.zeros((n+1, n,))
        I = calculate_invariant(At, out=I)
        max_invariant_distance = np.zeros(N, dtype=float)
        for i, B in enumerate(aligned_partial_positions):
            if B is not None:
                J = calculate_invariant(B.T, out=J)
                max_invariant_distance[i] = np.amax(np.abs(I-J), axis=(0,1))
        logger.critical(f"Max invariant different in the current cluster is {np.amax(max_invariant_distance)}")
    else:
        max_invariant_distance = None

    # --------------------------------------------------------------------------

    if return_positions is True:
        for j in tqdm.tqdm(np.arange(N), disable=True):
            R = rotations[j]
            if R is not None:
                pos_new[j] = np.matmul(R, pos[j].T).T
                # pos_new[j] = np.matmul(pos[j], R)
            else:
                pos_new[j] = None

    if return_labels is True:
        for j in tqdm.tqdm(np.arange(N), disable=True):
            lab_new[j] = lab[j]

    return group_distance, pos_new, lab_new, permutations, rotations, centroid, reference_lab, max_invariant_distance


@timer
def main_align_distance_intervals(pos, lab, shl, distance_intervals, ref_pos, ref_lab):

    assert pos.shape[0] == lab.shape[0]

    N = pos.shape[0]

    # Generate permutation subsets
    perm_subsets_fun = lambda g, l: permutation_subsets_distance_intervals(g, l, distance_intervals, separate_atom_types=True)

    perm_sets, perm_sets_sizes = perm_subsets_fun(ref_pos, ref_lab)
    check_permutation_set_sizes(perm_sets_sizes)

    perm_A = np.asarray(np.concatenate(perm_sets))

    lab_new = np.copy(lab)
    pos_new = np.copy(pos)
    shl_new = np.copy(shl)

    aligned_positions = []
    
    reference = ref_pos[perm_A, :]

    for i in np.arange(10):
        centroid, centroid_std, permutations, rotations, group_distance = \
            main_align_core(
                reference, pos, lab, N, perm_subsets_fun,
                return_centroid=True, return_rotations=False, return_permutations=False,
                stride=10, offset=np.random.randint(100))
        print(np.mean(np.abs(centroid - reference)/reference*100))
        reference = np.copy(reference + centroid) / 2

    centroid, centroid_std, permutations, rotations, group_distance = \
        main_align_core(
            reference, pos, lab, N, perm_subsets_fun,
            return_centroid=True, return_rotations=True, return_permutations=True,
            stride=1)

    for j in tqdm.tqdm(np.arange(N)):
        R = rotations[j]
        pos_new[j] = np.matmul(R, pos[j].T).T
        lab_new[j] = lab[j]
        shl_new[j] = shl[j]

    # for j in tqdm.tqdm(np.arange(N)):
    #     B_group = pos[j]
    #     B_lab = lab[j]
    #
    #     # R = np.random.rand(3, 3)
    #     # # make R a proper rotation matrix, force orthonormal
    #     # U, S, Vt = np.linalg.svd(R)
    #     # R = np.matmul(U, Vt)
    #     # R
    #     #
    #     # BB = np.matmul(R, A.T)
    #     # BB = BB.T + 0.1 * np.random.randn(*AA.shape)
    #
    #     perm_sets, perm_sets_sizes = permutation_subsets(
    #         B_group, B_lab, distance_intervals, separate_atom_types=True)
    #
    #     perm, R, B, group_distance[j] = align_through_permutation(
    #         AA, B_group, perm_sets, perm_sets_sizes)
    #
    #     aligned_positions.append(B)
    #     # permutations.append(perm)
    #     # rotations.append(R)
    #     # pos_new[j] = np.matmul(R, pos[j].T).T
    #     # lab_new[j] = lab[j]
    #     # shl_new[j] = shl[j]
    #
    #     # dB = np.linalg.norm(BB, axis=1)
    #     # b = [np.where(dB < limit), np.where(dB >= limit)]
    #     # ii = b[0][0]
    #     #
    #     # R, B = align(A[:5,:], BB[:5,:])
    #     #
    #     # D = np.sum(np.linalg.norm(A[ii,:] - B[ii,:], axis=1))
    #
    #     # if D < np.inf:
    #     #
    #     #     for a in ['LI', 'OW', 'CL', 'HW']:
    #     #         atoms_new[a].append(np.matmul(R, atoms[a][j].T).T)
    #     # else:
    #     #     pass

    return group_distance, pos_new, lab_new, shl_new, permutations, rotations



def get_data_size(folder_neighborhoods):
    with h5py.File(folder_neighborhoods / "neighborhoods.h5", 'r') as f:
        n = f['positions'].shape[0]
    return n

def print_data_info(CONFIG, **kwargs):
    data_size = get_data_size(CONFIG.folder_neighborhoods)
    print(chunk_loader(data_size,**kwargs))
    

def subset_selector(ind, force_data_subset):
    # select only a subset of data if requested
    nn = ind.size
    kk = force_data_subset['max_size']
    if nn <= kk:
        pass
    elif force_data_subset['method'] == 'random':
        logger.warning(f"Randomly selecting subset of data of size {kk} (out of {nn})")
        logger.warning(f"This should be used with care (e.g. only when calculating initial estimate of centroids)")
        ind = ind[np.random.choice(nn, kk, replace=False)]
    elif force_data_subset['method'] == 'start':
        logger.warning(f"Selecting beginning of dataset of size {kk} (out of {nn})")
        logger.warning(f"This should be used with care (e.g. only when calculating initial estimate of centroids)")
        ind = ind[:kk]

    return ind


class AsyncDataLoader(threading.Thread):
    def __init__(self, *args, **kwargs): 
  
        # calling superclass init 
        threading.Thread.__init__(self)  
        self.args = args
        self.kwargs = kwargs
        self.values = None
  
    def run(self): 
        logger.debug("Asynchronous data loading started")
        self.values = load_data(*self.args, **self.kwargs)
        logger.debug("Asynchronous data loading finished")
  

@timer
def load_data(folder_neighborhoods, ind, chunk_size=int(1e4)):

    with h5py.File(folder_neighborhoods / "neighborhoods.h5", 'r') as f:

        n = f['positions'].shape[0]

        if isinstance(ind, str) and ind == 'all':
            ind = np.arange(n)

        N = ind.size

        assert(ind.dtype == int)

        positions = np.empty((N,), dtype=np.object)
        labels = np.empty((N,), dtype=np.object)

        mm = np.mean(np.diff(ind))
        logger.info(f"Average index distance between two data points: {mm}")

        myrange = np.unique(np.hstack([np.arange(0, n, chunk_size, dtype=int), n]))

        logger.info(f'Loading {N} data (out of {n}) between indices {ind[0]} and {ind[-1]}')

        # this is super slow (bug in hdf5 ?)
        # positions = f['positions'][ind]
        # labels = f['labels'][ind]

        jj = 0
        kk = 0

        for j, k in tqdm.tqdm(zip(myrange[:-1], myrange[1:]), total=myrange.size-1):
            # print(j, k)

            ii = ind[np.logical_and(ind >= j, ind < k)] - j

            # print(ii.size)
            if ii.size == 0:
                continue

            pos = f['positions'][j:k]
            lab = f['labels'][j:k]

            kk += ii.size

            positions[jj:kk] = pos[ii]
            labels[jj:kk] = lab[ii]

            jj += ii.size

        # positions = f['positions'][:]
        # positions = np.copy(positions[ind]) # allow to free memory
        #
        # labels = f['labels'][:]
        # labels = np.copy(labels[ind]) # allow to free memory

    logger.info('    Reshaping data ...')

    for i, (p, l) in enumerate(zip(positions, labels)):
        assert l.dtype == 'uint8'
        assert p.dtype == 'float32'
        positions[i] = np.reshape(p, (-1, 3))

    logger.info(f'    Loaded {positions.shape[0]} positions')

    return positions, labels

def save_aligned_positions(CFG, pos_new, lab_new, num_groups, mode='w'):

    logger.info(f"Saving aligned positions and labels to {CFG.file_aligned}")

    with h5py.File(CFG.file_aligned, mode) as f:
        f_positions = f.create_dataset(
            'positions', (num_groups,), maxshape=(None,),
            dtype=h5py.special_dtype(vlen=np.dtype('float32')))
        f_labels = f.create_dataset(
            'labels', (num_groups,), maxshape=(None,),
            dtype=h5py.special_dtype(vlen=np.dtype('uint8')))
        # f_shells = f.create_dataset(
        #     'shells', (num_groups,), dtype=h5py.special_dtype(vlen=np.dtype('uint8')))

        for i, p in enumerate(pos_new):
            pos_new[i] = np.ravel(p)

        f_positions[0:num_groups] = pos_new
        f_labels[0:num_groups] = lab_new
        # f_shells[0:num_groups] = shl_new


def chunk_aligner(CONFIG, ind, FUN, max_chunk_size=int(1e5)):

    num_groups = ind.size

    # make sure the indices are unique and in ascending order
    ind = np.unique(ind)

    out_data = []
    weights = []

    out = None

    myrange = chunk_loader(ind.size, max_chunk_size=max_chunk_size)
    logger.info(f"Processing in {len(myrange)-1} chunks of ~ {myrange[1]} items each")
    for it, (j, k) in tqdm.tqdm(enumerate(zip(myrange[:-1], myrange[1:])), total=myrange.size-1):

        # Load data
        pos, lab = load_data(CONFIG.folder_neighborhoods, ind[j:k])

        weights.append(pos.shape[0]/num_groups)

        # Sort positions by distance from center
        for i, (p, l) in enumerate(zip(pos, lab)):

            rr = np.linalg.norm(p, axis=1)
            if not is_sorted(rr):
                ii = np.argsort(rr)
                pos[i] = p[ii,:]
                lab[i] = l[ii]
                assert pos[i].shape == p.shape

        if CONFIG.aligning_method == 'distance_intervals':
            shl = np.load(CONFIG.folder_hyd_shells / 'shells.npy')
            shl = np.copy(shl[ind]) # allow to free memory

        # --------------------------------------------------------------------------

        out = FUN(it, pos, lab, out)

        # print(out)

        out_data.append(out)

    return out_data, weights

def async_chunk_aligner(CONFIG, ind, FUN):

    num_groups = ind.size

    # make sure the indices are unique and in ascending order
    ind = np.unique(ind)

    out_data = []
    weights = []

    out = None

    myrange = chunk_loader(ind.size, max_chunk_size=int(1e5))
    logger.info(f"Processing in {len(myrange)-1} chunks of ~ {myrange[1]} items each")
    for it in tqdm.tqdm(np.arange(myrange.size-1)):

        if it == 0:
            j = myrange[it]
            k = myrange[it+1]

            bg_loader = AsyncDataLoader(CONFIG.folder_neighborhoods, ind[j:k])
            bg_loader.start()

        bg_loader.join()
        pos, lab = bg_loader.values

        if it < myrange.size-2:
            j = myrange[it+1]
            k = myrange[it+2]
        
            bg_loader = AsyncDataLoader(CONFIG.folder_neighborhoods, ind[j:k])
            bg_loader.start()

        weights.append(pos.shape[0]/num_groups)

        # Sort positions by distance from center
        for i, (p, l) in enumerate(zip(pos, lab)):

            rr = np.linalg.norm(p, axis=1)
            if not is_sorted(rr):
                ii = np.argsort(rr)
                pos[i] = p[ii,:]
                lab[i] = l[ii]
                assert pos[i].shape == p.shape

        if CONFIG.aligning_method == 'distance_intervals':
            shl = np.load(CONFIG.folder_hyd_shells / 'shells.npy')
            shl = np.copy(shl[ind]) # allow to free memory

        # --------------------------------------------------------------------------

        out = FUN(it, pos, lab, out)

        # print(out)

        out_data.append(out)

    return out_data, weights

def main_aligner(CONFIG, it, pos, lab, out, align_kwargs):
    # Align data to random group
    logger.info("Aligning data ...")

    # Set reference group
    if it == 0:
        ref_ind = np.random.randint(pos.shape[0])
        ref_pos = pos[ref_ind]
        ref_lab = lab[ref_ind]
    else:
        # out contains previously returned centroid, centroid_lab
        ref_pos, ref_lab = out

    if CONFIG.aligning_method == 'num_atoms':
        num_atoms = CONFIG.configuration_in_shells['coord_num']
        group_distance, pos_new, lab_new, permutations, \
            rotations, centroid, centroid_lab, _ = main_align_num_atoms(
                pos, lab, num_atoms, ref_pos, ref_lab,
                exclude_atom_types=[encode_labels('HW'), encode_labels(CONFIG.CATION)],
                full_shell_check=full_shell_check,
                n_cores=CONFIG.n_cores, **align_kwargs)

    elif CONFIG.aligning_method == 'distance_intervals':
        group_distance, pos_new, lab_new, shl_new, permutations, \
            rotations = main_align_distance_intervals(pos, lab, shl, distance_intervals, ref_pos, ref_lab)

        del shl

    return centroid, centroid_lab


def main(CONFIG, shell_configuration, only_save_centroids=False, force_data_subset=None):

    print(CONFIG.CATION)
    print(CONFIG.aligning_method)
    print(CONFIG.configuration_in_shells)

    if CONFIG.aligning_method == 'num_atoms':

        assert CONFIG.configuration_in_shells['num_ow'] + CONFIG.configuration_in_shells['num_cl'] == CONFIG.configuration_in_shells['coord_num']

        # print(shell_configuration.shape)
        # print(shell_configuration)

        ind = np.where(np.logical_and(
                shell_configuration[:,0] == CONFIG.configuration_in_shells['num_ow'],
                shell_configuration[:,1] == CONFIG.configuration_in_shells['coord_num']))[0]

    elif CONFIG.aligning_method == 'distance_intervals':

        CONFIG.shell_intervals = 'auto'
        CONFIG.distance_intervals = {
            CONFIG.CATION: None,
            CONFIG.ANION: ['shell 1', ],
            # ANION: None,
            CONFIG.OW: ['shell 1', ],
            CONFIG.HW: None,
        }

        # --------------------------------------------------------------------------
        # Load data

        shell_intervals = np.load(CONFIG.folder_hyd_shells / 'shell_intervals.npy').item()
        configuration_ext = np.load(CONFIG.folder_hyd_shells / 'configuration_ext.npy').item()

        # Find indices matching the requested configuration
        con_indices = configuration_selector(
            configuration_ext['confs'],
            configuration_ext['labels'],
            CONFIG.configuration_in_shells)

        # find groups with the requested configuration
        ind = np.concatenate(
            [np.where(configuration_ext['indices'] == con_index)[0]
                for con_index in con_indices])

        # check that the numbers (counts) match
        assert ind.size == np.sum(np.asarray([
            configuration_ext['counts'][con_index] for con_index in con_indices]))

        # --------------------------------------------------------------------------
        # Create distance intervals for separation of data in permutation subsets
        # based on the shell intervals calculated in hydration_shells.py

        distance_intervals = configure_distance_intervals(shell_intervals, CONFIG)

        np.savez(CONFIG.file_distance_interval, distance_intervals=distance_intervals)

    # --------------------------------------------------------------------------

    if only_save_centroids is True:
        align_kwargs = {
            'return_distance': False,
            'return_positions': False,
            'return_labels': False,
            'return_permutations': False,
            'return_rotations': False,
            'return_centroid': True,
            'num_centroid_iterations': 20,
        }


    CONFIG.folder_aligning.mkdir(parents=False, exist_ok=True)
    logger.info(f"Saving results to\n{CONFIG.folder_aligning}")

    # select only a subset of data if requested
    if force_data_subset is not None:
        ind = subset_selector(ind, force_data_subset)

    aligner_fun = lambda *args: main_aligner(CONFIG, *args, align_kwargs)
    all_data, weights = chunk_aligner(CONFIG, ind, aligner_fun)

    all_centroids = [d[0] for d in all_data]
    all_centroid_labels = [d[1] for d in all_data]
    centroid_lab = all_centroid_labels[0]

    centroid = np.average(np.array(all_centroids), axis=0, weights=weights)

    np.savez(CONFIG.file_centroid,
        centroid=np.array(centroid, dtype='float32'),
        centroid_lab=centroid_lab)


    # --------------------------------------------------------------------------
    # save results - now not functional (need to aggregate results)

    # save_aligned_positions(CONFIG, pos_new, lab_new, num_groups)

    # --------------------------------------------------------------------------

    if PLOT['align_metric_distribution']:
        plt.figure()
        plt.hist(group_distance, bins=100)
        plt.show()


    if PLOT['3d_scatter_data']:
        if len(pos_new) < 50:
            plot_atoms_3d(
                np.concatenate(pos_new, axis=0),
                np.concatenate(lab_new, axis=0))
        else:
            logger.warning("Will not plot 3d - too much data")


    # --------------------------------------------------------------------------
    # Calculate pairwise distance matrix (on subset of the data to save time)

    if False:

        stride = np.int(np.ceil(pos.shape[0] / 512))

        logger.info(f"Calculating pairwise distance on the data (with stride {stride})")
        pwdistance = pairwise_distance(pos[::stride], lab[::stride], distance_intervals)

        del pos
        del lab

        if PLOT['pairwise_distance']:
            plt.figure()
            plt.imshow(pwdistance)
            plt.show()

            plt.figure()
            plt.hist(np.concatenate(pwdistance, axis=0), bins=100)
            plt.show()

    return True

def separate_by_aligning_statistics(configurations, original_assignment, new_assignment, num_data):

    N = len(configurations)

    assert original_assignment.shape[0] == new_assignment.shape[0]

    A = np.zeros((N,N), dtype=int)
    for i in tqdm.tqdm(np.arange(N)):
        for j in np.arange(N):

            ind = np.logical_and(
                original_assignment == i,
                new_assignment == j)

            A[i,j] = np.count_nonzero(ind)

    print("Column: new assignment count")
    print("Row: Original assignment count")
    print(A)

    B = np.zeros((1,N), dtype=int)
    for j in np.arange(N):

        ind = np.logical_and(
            original_assignment == -1,
            new_assignment == j)

        B[0,j] = np.count_nonzero(ind)

    print("Column: new assignment count")
    print("Row: other assignment")
    print(B)

    assert np.sum(A) + np.sum(B) == num_data

    for conf in configurations:
        print(conf['description'])

def separate_by_aligning_save_data(pos_new, lab_new, f):
    
    if len(pos_new) == 0:
        logger.error("No data provided - skipping save")
        return

    for i, p in enumerate(pos_new):
        pos_new[i] = np.ravel(p)

    num_data = len(pos_new)

    i0 = f['positions'].shape[0]
    i1 = i0 + num_data

    f['positions'].resize(i1, axis=0)
    f['labels'].resize(i1, axis=0)

    logger.info(f"Writing to file, positions {i0}:{i1}")

    if num_data == 1: # workaround
        logger.warning("Single dataset encountered, this would create an error, applying workaround.")
        f['positions'][i0] = np.array([pos_new])
        f['labels'][i0] = np.array([lab_new])
    else:
        shapes = np.array([p.size for p in pos_new])
        if np.all(shapes == shapes[0]): # another workaround
            logger.warning("Datasets with same size encountered, this would create an error, applying workaround.")
            for i in np.arange(num_data):
                f['positions'][i0+i] = np.array(pos_new[i])
                f['labels'][i0+i] = np.array(lab_new[i])
        else:
            f['positions'][i0:i1] = pos_new
            f['labels'][i0:i1] = lab_new

@timer
def separate_by_aligning_core_centroids(configurations, positions, labels, n_cores, force_non_heuristic_alignment=False, full_shell_check=True):

    N = positions.shape[0]

    all_distances = np.zeros((positions.shape[0], len(configurations)), dtype=float)

    for k, C in enumerate(configurations):

        logger.debug("Processing configuration {num}/{total}: {description}".format(**C, num=k, total=len(configurations)))

        dist, _, _, _, _, _, _, _ = main_align_num_atoms(
                positions, labels, C['num_atoms'], C['ref_pos'], C['ref_lab'],
                exclude_atom_types=C['exclude_atom_types'],
                estimate_centroid=False,
                return_distance=True,
                return_positions=False,
                return_labels=False,
                return_permutations=False,
                return_rotations=False,
                return_centroid=False,
                perm_subsets_fun='reference',
                force_non_heuristic_alignment=force_non_heuristic_alignment,
                full_shell_check=full_shell_check,
                n_cores=n_cores)

        all_distances[:,k] = dist / (C['num_atoms'] - 1) # or / (C['num_atoms'])

        del dist

    new_assignment = np.argmin(all_distances, axis=1)

    for i in np.arange(all_distances.shape[0]):
        if np.all(np.isposinf(all_distances[i,:])):
            # logger.error("All distances are infinite -- is the group really incompatible with everything?")

            new_assignment[i] = -1

        if all_distances[i,new_assignment[i]] > 2.5:

            new_assignment[i] = -1

    num_not_assigned = np.count_nonzero(new_assignment == -1)

    logger.critical(f"{num_not_assigned} groups ({num_not_assigned/N*100:.1f} %) not assigned at all")

    # TODO: do the above check in other versions as well

    N = len(configurations)

    dist_stats = {}
    dist_stats['mean'] = np.zeros(N, dtype=float)
    dist_stats['std'] = np.zeros(N, dtype=float)
    dist_stats['min'] = np.zeros(N, dtype=float)
    dist_stats['max'] = np.zeros(N, dtype=float)

    centroids = []
    centroids_labs = []

    for k, C in enumerate(configurations):

        logger.debug("Processing configuration {num}/{total}: {description}".format(**C, num=k, total=len(configurations)))

        ind = np.where(new_assignment == k)[0]

        if ind.size <= 2:
            if ind.size == 0:
                logger.warning(f"No structures assigned to centroid #{k} with properties")
            else:
                logger.warning(f"Only {ind.size} structure(s) assigned to centroid #{k} with properties")
            print(C)
            logger.warning("Skipping centroid perturbation")
            centroid = C['ref_pos']
            centroid_lab =  C['ref_lab']
        else:
            _, _, _, _, _, centroid, centroid_lab, _ = main_align_num_atoms(
                    positions[ind], labels[ind], C['num_atoms'], C['ref_pos'], C['ref_lab'],
                    exclude_atom_types=C['exclude_atom_types'],
                    estimate_centroid=True,
                    return_distance=False,
                    return_positions=False,
                    return_labels=False,
                    return_permutations=False,
                    return_rotations=False,
                    return_centroid=True,
                    perm_subsets_fun='reference',  #'num_atoms'
                    force_non_heuristic_alignment=force_non_heuristic_alignment,
                    full_shell_check=full_shell_check,
                    n_cores=n_cores)

        if np.any(centroid == np.NaN):
            logger.critical("Centroid with NaN values encountered")
            centroids.append(None)
            centroids_labs.append(None)
        else:
            centroids.append(centroid)
            centroids_labs.append(centroid_lab)

    return centroids, centroids_labs

@timer
def separate_by_aligning_core(configurations, positions, labels, n_cores):

    all_distances = np.zeros((positions.shape[0], len(configurations)), dtype=float)

    for k, C in enumerate(configurations):

        logger.debug("Processing configuration {num}/{total}: {description}".format(**C, num=k, total=len(configurations)))

        dist, _, _, _, _, _, _, _ = main_align_num_atoms(
                positions, labels, C['num_atoms'], C['ref_pos'], C['ref_lab'],
                exclude_atom_types=C['exclude_atom_types'],
                estimate_centroid=False,
                return_distance=True,
                return_positions=False,
                return_labels=False,
                return_permutations=False,
                return_rotations=False,
                return_centroid=False,
                perm_subsets_fun='reference',
                force_non_heuristic_alignment=force_non_heuristic_alignment,
                full_shell_check=full_shell_check,
                n_cores=n_cores)

        all_distances[:,k] = dist / (C['num_atoms'] - 1) # or / (C['num_atoms'])

        del dist

    for i in np.arange(all_distances.shape[0]):
        if np.all(np.isposinf(all_distances[i,:])):
            logger.error("All distances are infinite -- is the group really incompatible with everything?")

    new_assignment = np.argmin(all_distances, axis=1)

    N = len(configurations)

    dist_stats = {}
    dist_stats['mean'] = np.zeros(N, dtype=float)
    dist_stats['std'] = np.zeros(N, dtype=float)
    dist_stats['min'] = np.zeros(N, dtype=float)
    dist_stats['max'] = np.zeros(N, dtype=float)
    dist_stats['invariant_max'] = np.zeros(N, dtype=float)

    for k, C in enumerate(configurations):

        logger.debug("Processing configuration {num}/{total}: {description}".format(**C, num=k, total=len(configurations)))

        ind = np.where(new_assignment == k)[0]

        if ind.size == 0:
            logger.warning(f"No structures assigned to centroid #{k} with properties")
            print(C)
            logger.warning("Skipping alignment")
            
            dist_stats['mean'][k] = np.NaN
            dist_stats['std'][k] = np.NaN
            dist_stats['min'][k] = np.NaN
            dist_stats['max'][k] = np.NaN
            dist_stats['invariant_max'][k] = np.NaN

        else:
            dist, pos_new, lab_new, _, _, _, _, max_invariant_distance = main_align_num_atoms(
                    positions[ind], labels[ind], C['num_atoms'], C['ref_pos'], C['ref_lab'],
                    exclude_atom_types=C['exclude_atom_types'],
                    estimate_centroid=False,
                    return_distance=True,
                    return_positions=True,
                    return_labels=True,
                    return_permutations=False,
                    return_rotations=False,
                    return_centroid=False,
                    perm_subsets_fun='reference',  #'num_atoms'
                    force_non_heuristic_alignment=force_non_heuristic_alignment,
                    full_shell_check=full_shell_check,
                    n_cores=n_cores)

            dist_stats['mean'][k] = np.mean(dist)
            dist_stats['std'][k] = np.std(dist)
            dist_stats['min'][k] = np.min(dist)
            dist_stats['max'][k] = np.max(dist)
            dist_stats['invariant_max'][k] = np.max(max_invariant_distance)

            # gp.plot((dist, dict(histogram = 'freq', binwidth=1)),terminal = 'dumb 80,40')

            del dist

            # ------------------------------------------------------------------
            # save results

            separate_by_aligning_save_data(
                    pos_new, lab_new, C['outfile'])

            del pos_new
            del lab_new

    logger.info("Statistics of alignment distance")
    pp.pprint(dist_stats)

    return new_assignment

@timer
def separate_by_aligning_core_new(configurations, positions, labels, n_cores, force_non_heuristic_alignment=False, full_shell_check=True):

    N = positions.shape[0]

    new_assignment = np.full(N, -1, dtype=int)
    min_distance = np.full(N, np.inf, dtype=float)
    pos_new = np.full_like(positions, np.NaN)
    lab_new = np.copy(labels)
    max_invariant_distance = np.full(N, np.inf, dtype=float)

    for k, C in enumerate(configurations):

        logger.debug("Processing configuration {num}/{total}: {description}".format(**C, num=k, total=len(configurations)))

        dist, pos, lab, _, _, _, _, max_inv_dist = main_align_num_atoms(
                positions, labels, C['num_atoms'], C['ref_pos'], C['ref_lab'],
                exclude_atom_types=C['exclude_atom_types'],
                estimate_centroid=False,
                return_distance=True,
                return_positions=True,
                return_labels=True,
                return_permutations=False,
                return_rotations=False,
                return_centroid=False,
                perm_subsets_fun='reference',  #'num_atoms'
                force_non_heuristic_alignment=force_non_heuristic_alignment,
                full_shell_check=full_shell_check,
                n_cores=n_cores)

        dist /= (C['num_atoms'] - 1) # or / (C['num_atoms'] - 1)

        ind = np.where(dist < min_distance)

        new_assignment[ind] = k

        min_distance[ind] = dist[ind]
        pos_new[ind] = pos[ind]
        lab_new[ind] = lab[ind]
        max_invariant_distance[ind] = max_inv_dist[ind]

    num_not_assigned = np.count_nonzero(new_assignment == -1)

    logger.critical(f"{num_not_assigned} groups ({num_not_assigned/N*100:.1f} %) not assigned at all")

    N = len(configurations)

    dist_stats = {}
    dist_stats['mean'] = np.zeros(N, dtype=float)
    dist_stats['std'] = np.zeros(N, dtype=float)
    dist_stats['min'] = np.zeros(N, dtype=float)
    dist_stats['max'] = np.zeros(N, dtype=float)

    dist_stats['invariant_max'] = np.zeros(N, dtype=float)
    

    for k, C in enumerate(configurations):

        ind = np.where(new_assignment == k)[0]

        if ind.size == 0:
            logger.warning(f"No structures assigned to centroid #{k} with properties")
            print(C)
            logger.warning("Skipping ...")

            dist_stats['mean'][k] = np.NaN
            dist_stats['std'][k] = np.NaN
            dist_stats['min'][k] = np.NaN
            dist_stats['max'][k] = np.NaN

            dist_stats['invariant_max'][k] = np.NaN

        else:
            dist_stats['mean'][k] = np.mean(min_distance[ind])
            dist_stats['std'][k] = np.std(min_distance[ind])
            dist_stats['min'][k] = np.min(min_distance[ind])
            dist_stats['max'][k] = np.max(min_distance[ind])

            dist_stats['invariant_max'][k] = np.max(max_invariant_distance[ind])

            # ------------------------------------------------------------------
            # save results

            separate_by_aligning_save_data(
                    pos_new[ind], lab_new[ind], C['outfile'])

    logger.info("Statistics of alignment distance")
    pp.pprint(dist_stats)

    return new_assignment

def find_new_configurations_alt(CONFIG, ind, num_atoms, force_data_subset=None, ntries=None, exclude_atom_types=None):

    # select only a subset of data if requested
    if force_data_subset is not None:
        ind = subset_selector(ind, force_data_subset)

    if ind.size == 0:
        logger.critical(f"No configurations provided in find_new_configurations.")
        return None, None, None

    all_centroids, all_centroids_labels, centroid_statistics = \
        find_new_configurations_core(CONFIG, ind, num_atoms, ntries=ntries, exclude_atom_types=exclude_atom_types)

    return all_centroids, all_centroids_labels, centroid_statistics


def find_new_configurations(CONFIG, shell_configuration, force_data_subset=None, ntries=None):

    ind = np.where(np.logical_and(
        shell_configuration[:,0] == CONFIG.configuration_in_shells['num_ow'],
        shell_configuration[:,1] == CONFIG.configuration_in_shells['coord_num']))[0]

    # select only a subset of data if requested
    if force_data_subset is not None:
        ind = subset_selector(ind, force_data_subset)

    if ind.size == 0:
        logger.warning(f"No configurations provided in find_new_configurations.")
        return None, None, None

    num_atoms = CONFIG.configuration_in_shells['coord_num']

    all_centroids, all_centroids_labels, centroid_statistics = \
        find_new_configurations_core(CONFIG, ind, num_atoms, ntries=ntries)

    return all_centroids, all_centroids_labels, centroid_statistics


def find_new_configurations_core(CONFIG, ind, num_atoms, ntries=None, exclude_atom_types=None):

    if exclude_atom_types is None:
        if hasattr(CONFIG, 'CATION'):
            exclude_atom_types = [encode_labels('HW'), encode_labels(CONFIG.CATION)]
        else:
            exclude_atom_types = [encode_labels('H')]

    align_kwargs = {
        'exclude_atom_types': exclude_atom_types,
        'estimate_centroid': False,
        'return_distance': True,
        'return_positions': False,
        'return_labels': False,
        'return_permutations': False,
        'return_rotations': False,
        'return_centroid': True,
        'perm_subsets_fun': 'num_atoms',
        'n_cores': CONFIG.n_cores}

    aligner_fun = lambda it, pos, lab, out: search_for_new_centroids(pos, lab, num_atoms, out, align_kwargs, ntries=ntries)
    all_data, _ = chunk_aligner(CONFIG, ind, aligner_fun, max_chunk_size=ind.size) # force one chunk
    # all_data, _ = async_chunk_aligner(CONFIG, ind, aligner_fun, max_chunk_size=ind.size)

    all_centroids = sum([d[0] for d in all_data], [])
    all_centroids_labels = sum([d[1] for d in all_data], [])
    centroid_statistics = [d[2] for d in all_data]

    # print(all_centroids)
    # print(all_centroids_labels)

    return all_centroids, all_centroids_labels, centroid_statistics

def expand_aligning_configurations(CONFIG, centroid_folder=None, configuration_generator=None, data_subset_size=int(1e5)):

    assert centroid_folder is not None
    assert configuration_generator is not None

    title_info = [
        "expand aligning configurations",
        CONFIG.CATION,
        CONFIG.aligning_method,
        ]

    disp_title(title_info)

    shell_configuration = np.load(CONFIG.folder_hyd_shells / 'shells_distances.npy')

    # --------------------------------------------------------------------------

    num_data = get_data_size(CONFIG.folder_neighborhoods)

    original_assignment = -1 * np.ones(num_data, dtype=int)

    configurations = []

    coords = np.arange(configuration_generator[0], configuration_generator[1]+1)
    num_cl = np.arange(0, configuration_generator[2]+1)

    # --------------------------------------------------------------------------

    data_subset = {
        'method': 'random',
        'max_size': data_subset_size,
    }

    data_subset = {
        'method': 'start',
        'max_size': data_subset_size,
    }

    # --------------------------------------------------------------------------
    # Calculate centroids form initial separation and alignment

    disp_title("Calculating centroids form initial separation and alignment")

    CONFIG.folder_aligning.mkdir(parents=False, exist_ok=True)
    logger.info(f"We will save results to\n{CONFIG.folder_aligning}")

    k = 0
    for i, coord_num in enumerate(coords):
        for n_cl in num_cl:

            n_ow = coord_num - n_cl

            if n_ow < 0 or n_cl > coord_num:
                logger.warning(f"Configuration {n_ow}:{n_cl}:{coord_num} is not possible")
                continue

            CFG = CONFIG
            CFG.configuration_in_shells = {
                    'num_ow': n_ow,
                    'num_cl': n_cl,
                    'coord_num': coord_num}

            CFG.setup_output_files()

            logger.info("Calculating centroids from initial separation")
            logger.info(f"Aligning to configuration {n_ow}:{n_cl}")

            all_centroids, all_centroids_labels, centroid_statistics = \
                find_new_configurations(
                    CFG, shell_configuration, force_data_subset=data_subset, ntries=10)

            if all_centroids is None:
                continue # don't do anything if we have not found anything

            centroid_statistics = centroid_statistics[0]

            with np.printoptions(precision=1, suppress=True, linewidth=np.inf):
                    logger.info('Inter centroid distance:')
                    print(centroid_statistics['inter_centroid_distances'])

                    logger.info('Mean & std centroid distances:')
                    print(centroid_statistics['mean_distances'])
                    print(centroid_statistics['std_distances'])

                    logger.info('Occurence frequency:')
                    print(centroid_statistics['occurence_frequency'])

            (CFG.folder_aligning / centroid_folder).mkdir(parents=False, exist_ok=True)

            m = 0
            for l, (centroid, centroid_lab) in enumerate(zip(
                    all_centroids, all_centroids_labels)):

                
                if centroid_statistics['occurence_frequency'][l] <= 1:
                    logger.debug(f"removing centroid #{l} for having low occurence frequency")
                    continue
                
                M = centroid_statistics['mean_distances'][l] + 3 * centroid_statistics['std_distances'][l]

                if l > 0: # always keep first one
                    if np.any(centroid_statistics['inter_centroid_distances'][0:(l-1),l] < M):
                        mm = np.where(centroid_statistics['inter_centroid_distances'][0:(l-1),l] < M)
                        logger.debug(f"removing centroid #{l} for being too similar to a previous one(s) ({mm})")
                        continue

                fout = CFG.folder_aligning / centroid_folder / f"centroid_guess-{n_ow}-{n_cl}-{m}"

                np.savez( fout,
                    centroid=np.array(centroid, dtype='float32'),
                    centroid_lab=centroid_lab,
                    centroid_id=m,
                    configuration_in_shells=CFG.configuration_in_shells)

                logger.info(fout)

                CFG.centroid_id = m
                CFG.setup_output_files()

                # setup configuration and prepare output files
                desc = "{num_ow} OW : {num_cl} CL ".format(**CFG.configuration_in_shells) + f"(#{m})"

                conf = {
                    'description': desc, 
                    'outfile': None,
                    'outfile_name': CFG.file_aligned,
                    'meta_outfile_name': CFG.file_aligned_metadata,
                    'file_centroid_perturbed': CFG.file_centroid_perturbed,
                    'dataset_positions': None,
                    'dataset_labels': None,
                    'num_atoms': coord_num,
                    'num_ow': CFG.configuration_in_shells['num_ow'],
                    'num_cl': CFG.configuration_in_shells['num_cl'],
                    'coord_num': CFG.configuration_in_shells['coord_num'],
                    'configuration_in_shells': CFG.configuration_in_shells,
                    'centroid_id': CFG.centroid_id,
                    'ref_pos': centroid,
                    'ref_lab': centroid_lab,
                    'exclude_atom_types': [encode_labels(l) for l in ['HW', CFG.CATION]],
                }

                configurations.append(conf)

                m +=1

            logger.info(f"Found {m} new centroids")


            k += 1

    num_configs = len(configurations)    

    logger.info("results saved to " + str(CONFIG.folder_aligning))

    logger.info(" finished")

    
    return configurations, original_assignment


def load_centroids_from_folder(CONFIG, centroid_folder=None, glob_string='centroid*.npz'):

    title_info = [
        "load centroids from folder",
        ]

    disp_title(title_info)

    if centroid_folder is not None:
        parrent_folder = Path(CONFIG.SRC_FOLDER)
        centroid_folder = Path(parrent_folder / centroid_folder)
    else:
        centroid_folder = CONFIG.folder_centroids

    logger.info(f"Loading centroids from\n{centroid_folder}\nlooking for {glob_string}")

    configurations = []

    for f in centroid_folder.glob(glob_string):

        logger.info("Loading centroid file " + str(f))

        tmp = np.load(f, allow_pickle=True)

        CFG = CONFIG
        CFG.configuration_in_shells = tmp['configuration_in_shells'].item()  # .item() is here to recover dict form 0d numpy array

        CFG.centroid_id = tmp['centroid_id']
        CFG.setup_output_files()

        # setup configuration and prepare output files
        #desc = "{num_ow} OW : {num_cl} CL ".format(**CFG.configuration_in_shells) + f"(#{tmp['centroid_id']})"

        desc = f"{CFG.description} #{tmp['centroid_id']})"
        print(CFG.configuration_in_shells)
        conf = {
            'description': desc, 
            'outfile': None,
            'outfile_name': CFG.file_aligned,
            'meta_outfile_name': CFG.file_aligned_metadata,
            'file_centroid_perturbed': CFG.file_centroid_perturbed,
            'dataset_positions': None,
            'dataset_labels': None,
            'num_atoms': CFG.configuration_in_shells['num_atoms'],
            # 'num_ow': CFG.configuration_in_shells['num_ow'],
            # 'num_cl': CFG.configuration_in_shells['num_cl'],
            # 'coord_num': CFG.configuration_in_shells['coord_num'],
            'configuration_in_shells': CFG.configuration_in_shells,
            'centroid_id': CFG.centroid_id,
            'ref_pos': tmp['centroid'],
            'ref_lab': tmp['centroid_lab'],
            'exclude_atom_types': CFG.exclude_atoms_centroid,
        }

        configurations.append(conf)

    num_configs = len(configurations)

    assert num_configs > 0, 'Expected at least one centroid file'

    logger.info(f"Finished loading {num_configs} centroids")

    return configurations


def perturb_centroid_separate_by_aligning(CONFIG, centroid_method, num_data_to_load=int(1e5), **kwargs):

    title_info = [
        "centroid perturbation using separate by aligning",
        CONFIG.CATION,
        CONFIG.aligning_method,
        ]

    disp_title(title_info)

    # --------------------------------------------------------------------------

    logger.info(f"We will save results to {CONFIG.folder_centroids}")

    if CONFIG.folder_centroids.exists() is True:
        logger.info(f"Folder exists, removing contents ...")
        shutil.rmtree(CONFIG.folder_centroids)

    CONFIG.folder_centroids.mkdir(parents=False, exist_ok=True)

    # --------------------------------------------------------------------------

    num_data = get_data_size(CONFIG.folder_neighborhoods)

    if num_data_to_load > num_data:
        raise ValueError(f"Requested to load {num_data_to_load} but only {num_data} found in source.")

    # --------------------------------------------------------------------------
    # Calculate centroids form initial separation and alignment

    if centroid_method == 'shell_configuration':
        configurations, original_assignment = centroids_from_shell_configuration(
                CONFIG, **kwargs)

    elif centroid_method == 'expand_configuration':
        configurations, original_assignment = expand_aligning_configurations(
                CONFIG, **kwargs)

    elif centroid_method == 'folder':
        original_assignment = None
        configurations = load_centroids_from_folder(CONFIG, **kwargs)

    # --------------------------------------------------------------------------

    disp_title("Centroid perturbation using Separating by alignment quality")
        
    ind = np.arange(0, num_data_to_load)
    pos, lab = load_data(CONFIG.folder_neighborhoods, ind=ind)

    num_groups = pos.shape[0]

    # Sort positions by distance from center
    for i, (p, l) in enumerate(zip(pos, lab)):

        rr = np.linalg.norm(p, axis=1)
        if not is_sorted(rr):
            ii = np.argsort(rr)
            pos[i] = p[ii,:]
            lab[i] = l[ii]
            assert pos[i].shape == p.shape

    # Adjust centroids
    start_time = time.perf_counter()

    centroids, centroids_labs = separate_by_aligning_core_centroids(
            configurations, pos, lab, CONFIG.n_cores)
    for i, conf in enumerate(configurations):

        if centroids[i] is None or centroids_labs[i] is None:
            logger.critical("centroid with None value encountered, skipping ...")
            continue

        conf['ref_pos'] = centroids[i]
        conf['ref_lab'] = centroids_labs[i]

        np.savez(conf['file_centroid_perturbed'],
            centroid=np.array(conf['ref_pos'], dtype='float32'),
            centroid_lab=conf['ref_lab'],
            centroid_id=conf['centroid_id'],
            configuration_in_shells=conf['configuration_in_shells'])

    end_time = time.perf_counter()
    run_time = end_time - start_time
    logger.verbose(f"Finished centroid perturbation in {run_time:.4f} sec")

    return True

def align_two_structures(reference, structure, full_shell_check=True):

    ref_pos =reference['positions']
    ref_lab = reference['labels']
    pos = np.array([structure['positions']])
    lab = np.array([structure['labels']])

    num_atoms = ref_pos.shape[0]

    perm_subsets_fun = lambda g, l: permutation_subsets_by_reference(g, l, ref_lab, full_shell_check=full_shell_check)
    perm_sets, perm_sets_sizes = perm_subsets_fun(ref_pos, ref_lab)
    perm_sets_labels = [ref_lab[ps] for ps in perm_sets]

    # logger.info('Aligning to: ' + str([decode_labels(ref_lab[i]) for i in np.concatenate(perm_sets)]))

    perm_A = np.asarray(np.concatenate(perm_sets))

    align = main_align_core

    N = 1

    _, _, _, _, dist, aligned_positions, _ = \
        align(pos, lab, N, ref_pos[perm_A, :],
            perm_sets, perm_sets_sizes, perm_sets_labels, perm_subsets_fun,
            return_distance=True,
            return_positions=True,
            return_labels=False,
            return_permutations=False,
            return_rotations=False,
            return_centroid=False,
            force_non_heuristic_alignment=True,
            stride=1, n_cores=1)

    dist = np.sqrt(dist)/(num_atoms-1)

    return dist, structure

def filter_centroids(CONFIG, centroid_method, **kwargs):

    title_info = [
        "filtering centroids",
        CONFIG.CATION,
        CONFIG.aligning_method,
        ]

    disp_title(title_info)

    configurations = load_centroids_from_folder(CONFIG, **kwargs)

    hashes = set()

    for conf in configurations:
        conf['positions'] = np.copy(conf['ref_pos'])
        conf['labels'] = np.copy(conf['ref_lab'])
        
        c = conf['configuration_in_shells']

        h = blake2s(digest_size=4)
        h.update(("-".join(c.keys())).encode('utf-8'))
        h.update(("-".join(str(c.values()))).encode('utf-8'))
        ha = h.hexdigest()
        hashes.add(ha)
        conf['hash'] = ha
        

    # for conf in configurations:
    #     print(conf)

    # print(hashes)

    for ha in hashes:
        structures = [conf for conf in configurations if conf['hash'] == ha]

        NN = len(structures)

        # print(NN)
        
        print(structures[0]['configuration_in_shells'])

        dist = np.zeros((NN, NN,))
        for i in np.arange(NN):
            for j in np.arange(i+1,NN):
                # print(structures[j], structures[i])
                d, _, = align_two_structures(structures[j], structures[i], full_shell_check=full_shell_check)
                dist[i,j] = d[0]
                dist[j,i] = d[0]

        ii = np.argsort(np.sum(dist, axis=0))#[::-1]

        dist = (dist[ii,:])[:,ii]
        structures = [structures[i] for i in ii]

        with np.printoptions(precision=1, suppress=True, linewidth=np.inf):
            print(10*dist)

        # for i in np.arange(NN):
        #     print(structures[i]['file_centroid_perturbed'])

        centroids = set()
        centroids.add(0)
        for j in np.arange(NN):
            ii = np.asarray(list(centroids))
            if np.all(dist[ii, j] > 0.66):
                centroids.add(j)

        all_structures = set(range(NN))
        print(centroids)

        to_remove = all_structures - centroids
        assert len(to_remove) < NN
        print(f"We will remove {len(to_remove)}/{NN} similar structures")
        for i in to_remove:
            f = structures[i]['file_centroid_perturbed']
            new_name = f + ".unused"
            f.rename(new_name)
            #print(new_name)


def separate_by_aligning(CONFIG, centroid_method, max_chunk_size=int(1e5),
        save_extended_shell_configuration=True,
        data_range=None, process_id=None, align_kwargs={}, **kwargs):

    if (data_range is None and process_id is not None) \
    or (data_range is not None and process_id is None):
        raise ValueError(
            "Expected parameters data_range and process_id be both set or unset."
            + f" data_range = {data_range}, process_id={process_id}")

    if process_id is not None:
        CONFIG.process_id = process_id
        CONFIG.setup_output_files()

    if data_range is not None:
        CONFIG.data_range = data_range
        CONFIG.setup_output_files()

    title_info = [
        "separate by aligning",
        CONFIG.name,
        CONFIG.aligning_method,
        ]

    if process_id is not None:
        title_info.append(f"process id {process_id}")

    if data_range is not None:
        title_info.append(f"data range {data_range}")

    disp_title(title_info)

    # --------------------------------------------------------------------------

    logger.info(f"We will save results to {CONFIG.folder_aligning}")

    if process_id is None:
        if CONFIG.folder_aligning.exists() is True:
            logger.info(f"Folder exists, removing contents ...")
            shutil.rmtree(CONFIG.folder_aligning)

    CONFIG.folder_aligning.mkdir(parents=False, exist_ok=True)

    # --------------------------------------------------------------------------
    # Calculate centroids form initial separation and alignment

    if centroid_method == 'shell_configuration':
        configurations, original_assignment = centroids_from_shell_configuration(
                CONFIG, **kwargs)

    elif centroid_method == 'expand_configuration':
        configurations, original_assignment = expand_aligning_configurations(
                CONFIG, **kwargs)

    elif centroid_method == 'folder':
        original_assignment = None
        configurations = load_centroids_from_folder(CONFIG, **kwargs)

    # --------------------------------------------------------------------------

    disp_title("Separating by alignment quality")

    # --------------------------------------------------------------------------

    invalid_configurations = []

    for i, conf in enumerate(configurations):
        if np.any(np.isnan(conf['ref_pos'])):
            invalid_configurations.append(i)

    if len(invalid_configurations) > 0:
        logger.critical("Ivalid configurations found!")
    
    for index in sorted(invalid_configurations, reverse=True):
        del configurations[index]


    for i, conf in enumerate(configurations):
        logger.debug("Opening " + str(conf['outfile_name']))

    with contextlib.ExitStack() as stack:
        files = [stack.enter_context(h5py.File(conf['outfile_name'], 'w'))
                    for conf in configurations]

        for conf, file in zip(configurations, files):

            conf['outfile'] = file

            conf['dataset_positions'] = conf['outfile'].create_dataset(
                'positions', (0,), maxshape=(None,),
                dtype=h5py.special_dtype(vlen=np.dtype('float32')))

            conf['dataset_labels'] = conf['outfile'].create_dataset(
                'labels', (0,), maxshape=(None,),
                dtype=h5py.special_dtype(vlen=np.dtype('uint8')))

        # ----------------------------------------------------------------------

        new_assigns = []

        load_asynchronously = True

        if data_range is None:
            num_data = get_data_size(CONFIG.folder_neighborhoods)
            myrange = chunk_loader(num_data, max_chunk_size=max_chunk_size)
        else:
            num_data = data_range[1] - data_range[0]
            myrange = chunk_loader(*data_range, max_chunk_size=max_chunk_size)

        for it in tqdm.tqdm(np.arange(myrange.size-1)):

            start_time = time.perf_counter()

            if load_asynchronously is True:
                if it == 0:
                    j = myrange[it]
                    k = myrange[it+1]
                    
                    ind = np.arange(j,k)

                    bg_loader = AsyncDataLoader(CONFIG.folder_neighborhoods, ind=ind)
                    bg_loader.start()

                bg_loader.join()
                pos, lab = bg_loader.values

                if it < myrange.size-2:
                    j = myrange[it+1]
                    k = myrange[it+2]

                    ind = np.arange(j,k)
                
                    bg_loader = AsyncDataLoader(CONFIG.folder_neighborhoods, ind=ind)
                    bg_loader.start()
            else:
                j = myrange[it]
                k = myrange[it+1]
                ind = np.arange(j,k)
                pos, lab = load_data(CONFIG.folder_neighborhoods, ind=ind)

            num_groups = pos.shape[0]

            if num_groups == 0:
                print(j, k)
                raise(ValueError("Expected positions of nonzero length"))

            # Sort positions by distance from center
            for i, (p, l) in enumerate(zip(pos, lab)):

                rr = np.linalg.norm(p, axis=1)
                if not is_sorted(rr):
                    ii = np.argsort(rr)
                    pos[i] = p[ii,:]
                    lab[i] = l[ii]
                    assert pos[i].shape == p.shape

            # Align data to centroids
            logger.info(f"Separating by alignment, chunk {it + 1} out of {myrange.size-1}")

            # NOTE: the "new" version is faster for large # of atoms in a reference structure,
            # but slower for small structure sizes
            # new_assign = separate_by_aligning_core(configurations, pos, lab, CONFIG.n_cores)
            new_assign = separate_by_aligning_core_new(configurations, pos, lab, CONFIG.n_cores, **align_kwargs)
            print(new_assign)
            new_assigns.append(new_assign)

            end_time = time.perf_counter()
            run_time = end_time - start_time
            logger.verbose(f"Finished iteration #{it} of 'separate_by_aligning' in {run_time:.4f} sec")

    new_assignment = np.concatenate(new_assigns, axis=0)

    N = np.size(new_assignment, axis=0)

    num_not_assigned = np.count_nonzero(new_assignment == -1)

    logger.critical(f"IN TOTAL {num_not_assigned} out of {N} groups ({num_not_assigned/N*100:.1f} %) not assigned at all")

    # --------------------------------------------------------------------------
    if original_assignment is not None:
        separate_by_aligning_statistics(configurations, original_assignment, new_assignment, num_data)

    # --------------------------------------------------------------------------
    if save_extended_shell_configuration is True:
        shell_configuration = np.load(CONFIG.folder_hyd_shells / 'shells_distances.npy')
        print(shell_configuration.shape)
        new_shell_configuration = np.zeros((num_data, 3,), dtype=int)

        for k, conf in enumerate(configurations):
            print(conf)
            ind = np.where(new_assignment == k)[0]
            new_shell_configuration[ind,0] = conf['num_ow']
            new_shell_configuration[ind,1] = conf['coord_num']
            new_shell_configuration[ind,2] = conf['centroid_id']

            k += 1

        np.save(CONFIG.file_configuration, new_shell_configuration)
    else:
        new_shell_configuration = np.zeros((num_data, 1,), dtype=int)

        for k, conf in enumerate(configurations):
            print(conf)
            ind = np.where(new_assignment == k)[0]
            new_shell_configuration[ind,0] = conf['centroid_id']

            k += 1

        np.save(CONFIG.file_configuration, new_shell_configuration)
    logger.info(f"Configuration saved to {str(CONFIG.file_configuration)}")

    return True


def centroids_from_shell_configuration(CONFIG, configuration_generator=None):
    disp_title("Calculating centroids form initial separation and alignment")

    num_data = get_data_size(CONFIG.folder_neighborhoods)

    coords = np.arange(configuration_generator[0], configuration_generator[1]+1)
    num_cl = np.arange(0, configuration_generator[2]+1)

    shell_configuration = np.load(CONFIG.folder_hyd_shells / 'shells_distances.npy')

    original_assignment = -1 * np.ones(num_data, dtype=int)

    k = 0
    for i, coord_num in enumerate(coords):
        for j in num_cl:

            CFG = CONFIG
            CFG.configuration_in_shells = {
                    'num_ow': coord_num - j,
                    'num_cl': j,
                    'coord_num': coord_num}

            CFG.setup_output_files()

            logger.info("Calculating centroids from initial separation")
            logger.info(f"Aligning to configuration {coord_num-j}:{j}")

            data_subset = {
                'method': 'random',
                'max_size': int(1e3),
            }

            data_subset = {
                'method': 'start',
                'max_size': int(1e5),
            }

            main(CFG, shell_configuration, only_save_centroids=True, force_data_subset=data_subset)

            ind = np.where(np.logical_and(
                    shell_configuration[:,0] == CFG.configuration_in_shells['num_ow'],
                    shell_configuration[:,1] == CFG.configuration_in_shells['coord_num']))[0]
            original_assignment[ind] = k

            k += 1

    num_configs = k

    logger.info("Initial alignment finished")

    # --------------------------------------------------------------------------
    # setup configuration and prepare output files

    configurations = []
    for i, coord_num in enumerate(coords):
        for j in num_cl:

            CFG = CONFIG
            CFG.configuration_in_shells = {
                    'num_ow': coord_num - j,
                    'num_cl': j,
                    'coord_num': coord_num}

            CFG.setup_output_files()

            tmp = np.load(CFG.file_centroid)
            ref_pos = tmp['centroid']
            ref_lab = tmp['centroid_lab']

            desc = "{num_ow} OW : {num_cl} CL ".format(**CFG.configuration_in_shells)

            conf = {
                'description': desc,
                'outfile': None,
                'outfile_name': CFG.file_aligned,
                'meta_outfile_name': CFG.file_aligned_metadata,
                'file_centroid_perturbed': CFG.file_centroid_perturbed,
                'dataset_positions': None,
                'dataset_labels': None,
                'num_atoms': coord_num,
                'num_ow': CFG.configuration_in_shells['num_ow'],
                'num_cl': CFG.configuration_in_shells['num_cl'],
                'coord_num': CFG.configuration_in_shells['coord_num'],
                'configuration_in_shells': CFG.configuration_in_shells,
                'centroid_id': 0,
                'ref_pos': ref_pos,
                'ref_lab': ref_lab,
                'exclude_atom_types': [encode_labels(l) for l in ['HW', CFG.CATION]],
            }

            configurations.append(conf)

    return configurations, original_assignment


if __name__ == '__main__':
    CONFIG = config.Config(stride=100, CATION='K')
    main(CONFIG)
