# coding: utf-8
import numpy as np

from IPython import get_ipython
if get_ipython() is not None:
    get_ipython().run_line_magic('load_ext', 'autoreload')
    get_ipython().run_line_magic('autoreload', '2')

import argparse

import iwc.config as config
import iwc.neighborhoods as neighborhoods
import iwc.hydration_shells as hydration_shells
import iwc.shell_distances as shell_distances
import iwc.clustering as clustering
import iwc.histograms as histograms
import iwc.aligned_data_analyser as aligned_data_analyser

from iwc.common.common import logger, configuration_generator

# ------------------------------------------------------------------------------

parser = argparse.ArgumentParser()

parser.add_argument("--n-cores", type=int, default=1,
                    help="")

parser.add_argument("--cation", type=str, required=True,
                    help="")

parser.add_argument("--stride", type=int, default=1,
                    help="")

parser.add_argument("--do-all", action="store_true",
                    help="")

parser.add_argument("--do-neighborhoods", action="store_true",
                    help="")
parser.add_argument("--nhood-distance", type=int, default=10,
                    help="")

parser.add_argument("--do-shell-distances", action="store_true",
                    help="")
parser.add_argument("--num-neighbors", type=int, default=10,
                    help="")

parser.add_argument("--do-centroid-perturbation", action="store_true",
                    help="")
parser.add_argument("--num-data-to-load", type=int, default=0,
                    help="")

parser.add_argument("--do-centroid-filtering", action="store_true",
                    help="")

parser.add_argument("--do-print-data-info", action="store_true",
                    help="")
parser.add_argument("--do-aligning", action="store_true",
                    help="")
parser.add_argument("--do-histograms", action="store_true",
                    help="")
parser.add_argument("--do-analyse-hydrogen-bonds", action="store_true",
                    help="")

parser.add_argument("--configuration", nargs='+', type=int,
                    help="")

parser.add_argument("--configuration-generator", nargs='+', type=int,
                    help="")

parser.add_argument("--centroid-source", type=str, default='shell_configuration',
                    help="")

parser.add_argument("--max-chunk-size", type=int,
                    help="")

parser.add_argument("--centroid-folder", type=str,
                    help="")

parser.add_argument("--centroid-glob", type=str, default="centroid*.npz",
                    help="")

parser.add_argument("--hist-num-bins", nargs='+', type=int,
                    help="")

parser.add_argument("--num-chunks", type=int,
                    help="")

parser.add_argument("--data-range", nargs='+', type=int,
                    help="")

parser.add_argument("--process-id", type=int,
                    help="")

args = parser.parse_args()

"""
conda install -c conda-forge numpy scipy matplotlib tqdm joblib pandas seaborn numba ipython h5py MDAnalysis

screen -S iwc
cd ~/repo/ion-water-clusters/; goconda; conda activate iwc;

python iwc/analysis.py --cation LI --do-all --configuration 4 0 4
python iwc/analysis.py --cation LI --do-neighborhoods --do-shell-distances --do-aligning --do-histograms --configuration 4 0 4
python iwc/analysis.py --cation NA --do-aligning --do-histograms --stride 1 --configuration 6 0 6

python iwc/analysis.py --cation NA --do-histograms --stride 1 --configuration 6 0 6 --hist-num-bins 195 197 199

python iwc/analysis.py --do-analyse-hydrogen-bonds --cation K --stride 100 --configuration 6 0 6
"""

print(args)

if args.do_all \
    or args.do_centroid_perturbation \
    or args.do_aligning \
    or args.do_histograms\
    or args.do_analyse_hydrogen_bonds:

    if args.configuration is not None:
        ARR = [{ 'num_ow': args.configuration[0],
                'num_cl': args.configuration[1],
                'coord_num': args.configuration[2]}]
    elif args.configuration_generator is not None:
        c_min = args.configuration_generator[0]
        c_max = args.configuration_generator[1]
        a_max = args.configuration_generator[2]

        ARR = configuration_generator(c_min, c_max, a_max)

    elif args.centroid_source == 'folder':
        ARR = [{}]
        pass
    elif args.centroid_folder is not None:
        ARR = [{}]
        pass
    else:
        raise ValueError("configuration required")

# ==============================================================================

# ------------------------------------------------------------------------------

if args.do_all or args.do_neighborhoods:

    CONFIG = config.Config(
            CATION=args.cation,
            stride=args.stride,
            nhood_distance=args.nhood_distance,
            )

    neighborhoods.main(**vars(CONFIG))

# ------------------------------------------------------------------------------

if args.do_all or args.do_shell_distances:

    CONFIG = config.Config(
            CATION=args.cation,
            stride=args.stride,
            nhood_distance=args.nhood_distance,
            )

    # hydration_shells.main(CONFIG)
    shell_distances.main(CONFIG, args.num_neighbors)

# ------------------------------------------------------------------------------

if args.do_all or args.do_centroid_perturbation:

    CONFIG = config.Config(
            n_cores=args.n_cores,
            CATION=args.cation,
            stride=args.stride,
            nhood_distance=args.nhood_distance,
            aligning_method='num_atoms',
            configuration_in_shells=ARR[0])

    extra_kwargs = {}

    if args.num_data_to_load is not 0:
        extra_kwargs['num_data_to_load'] = args.num_data_to_load

    if args.centroid_source == 'shell_configuration':
        clustering.perturb_centroid_separate_by_aligning(CONFIG, args.centroid_source, **extra_kwargs,
                configuration_generator=args.configuration_generator)
    elif args.centroid_source == 'expand_configuration':
        clustering.perturb_centroid_separate_by_aligning(CONFIG, args.centroid_source, **extra_kwargs,
                centroid_folder=args.centroid_folder,
                configuration_generator=args.configuration_generator)
    elif args.centroid_source == 'folder':
        clustering.perturb_centroid_separate_by_aligning(CONFIG, args.centroid_source, **extra_kwargs,
                centroid_folder=args.centroid_folder,
                glob_string=args.centroid_glob)
    else:
        raise(ValueError(f"Unrecognised option {args.centroid_source}"))

if args.do_all or args.do_centroid_filtering:

    CONFIG = config.Config(
            n_cores=args.n_cores,
            CATION=args.cation,
            stride=args.stride,
            nhood_distance=args.nhood_distance,
            aligning_method='num_atoms',
            configuration_in_shells={})

    extra_kwargs = {}


    clustering.filter_centroids(CONFIG, args.centroid_source, **extra_kwargs,
            centroid_folder=args.centroid_folder,
            glob_string=args.centroid_glob)


# ------------------------------------------------------------------------------

if args.do_print_data_info:

    CONFIG = config.Config(
            n_cores=args.n_cores,
            CATION=args.cation,
            stride=args.stride,
            nhood_distance=args.nhood_distance,
            aligning_method='num_atoms',
            configuration_in_shells={})

    clustering.print_data_info(CONFIG, num_chunks=args.num_chunks)

if args.do_all or args.do_aligning:

    CONFIG = config.Config(
            n_cores=args.n_cores,
            CATION=args.cation,
            stride=args.stride,
            nhood_distance=args.nhood_distance,
            aligning_method='num_atoms',
            configuration_in_shells=ARR[0])

    # clustering.expand_aligning_configurations(CONFIG, args.configuration_generator)

    extra_kwargs = {}

    for arg in ['max_chunk_size', 'data_range', 'process_id']:
        val = getattr(args, arg)
        if arg is not None:
            extra_kwargs[arg] = val

    if args.centroid_source == 'shell_configuration':
        clustering.separate_by_aligning(CONFIG, args.centroid_source, **extra_kwargs,
                configuration_generator=args.configuration_generator)
    elif args.centroid_source == 'expand_configuration':
        clustering.separate_by_aligning(CONFIG, args.centroid_source, **extra_kwargs,
                centroid_folder=args.centroid_folder,
                configuration_generator=args.configuration_generator)
    elif args.centroid_source == 'folder':
        clustering.separate_by_aligning(CONFIG, args.centroid_source, **extra_kwargs,
                centroid_folder=args.centroid_folder,
                glob_string=args.centroid_glob)
    else:
        raise(ValueError(f"Unrecognised option {args.centroid_source}"))

# ------------------------------------------------------------------------------

if args.do_all or args.do_histograms:
    total_num_groups = 0

    for C in ARR:
        CONFIG = config.Config(
                n_cores=args.n_cores,
                CATION=args.cation,
                stride=args.stride,
                nhood_distance=args.nhood_distance,
                num_bins=args.hist_num_bins,
                hist_range=[(-args.nhood_distance, args.nhood_distance) for i in [1, 2, 3]],
                aligning_method='num_atoms',
                configuration_in_shells=C)

        # histograms.main(CONFIG)
        total_num_groups += histograms.main_wrapper(CONFIG)

    logger.critical(f"Total number of groups processed: {total_num_groups}")

# ------------------------------------------------------------------------------

if args.do_all or args.do_analyse_hydrogen_bonds:
    total_num_groups = 0

    for C in ARR:
        CONFIG = config.Config(
                n_cores=args.n_cores,
                CATION=args.cation,
                stride=args.stride,
                nhood_distance=args.nhood_distance,
                aligning_method='num_atoms',
                configuration_in_shells=C)

        total_num_groups += aligned_data_analyser.main_wrapper(CONFIG)

    logger.critical(f"Total number of groups processed: {total_num_groups}")