# coding: utf-8

from IPython import get_ipython
if get_ipython() is not None:
    get_ipython().run_line_magic('load_ext', 'autoreload')
    get_ipython().run_line_magic('autoreload', '2')

import numpy as np
from iwc.cluster_plot_3d import ShellPlot3d

import argparse

parser = argparse.ArgumentParser()

parser.add_argument("--centroid-id", type=int, default=None,
                    help="")


"""
"""

args = parser.parse_args()

# ------------------------------------------------------------------------------
# CONFIGURATION

import config
# See config.py

if __name__ == '__main__':

    CONFIG = config.Config(
        # CATION='LI',
        CATION='NA',
        # CATION='K',
        # stride=5,
        stride=1,
        num_bins=(128, 128, 128,),
        # num_bins=(97, 99, 101,),
        # num_bins=(197, 199, 201,),
        # num_bins=(195, 197, 199,),
        # num_bins=(295, 297, 299,),
        hist_range=[(-10,10), (-10,10), (-10,10),],
        # hist_range=[(-9,9), (-9,9), (-9,9),],
        # nhood_distance=5, # Angstrom
        # nhood_distance=8, # Angstrom
        nhood_distance=10, # Angstrom
        # nhood_distance=12, # Angstrom
        aligning_method='num_atoms',
        # configuration_in_shells=np.asarray(['*', 2, 2, '*', '*', '*', '*', '*']),
        # configuration_in_shells=np.asarray(['*', 3, 1, '*', '*', '*', '*', '*']),
        # configuration_in_shells=np.asarray(['*', 4, 0, '*', '*', '*', '*', '*']),
        # configuration_in_shells=np.asarray(['*', 6, 0, '*', '*', '*', '*', '*']),

        # configuration_in_shells={'num_ow': 3, 'num_cl': 0, 'coord_num': 3},

        # configuration_in_shells={'num_ow': 4, 'num_cl': 0, 'coord_num': 4},
        # configuration_in_shells={'num_ow': 3, 'num_cl': 1, 'coord_num': 4},
        # configuration_in_shells={'num_ow': 2, 'num_cl': 2, 'coord_num': 4},

        # configuration_in_shells={'num_ow': 5, 'num_cl': 0, 'coord_num': 5},
        # configuration_in_shells={'num_ow': 4, 'num_cl': 1, 'coord_num': 5},
        # configuration_in_shells={'num_ow': 3, 'num_cl': 2, 'coord_num': 5},
        # configuration_in_shells={'num_ow': 2, 'num_cl': 3, 'coord_num': 5},

        # configuration_in_shells={'num_ow': 6, 'num_cl': 0, 'coord_num': 6},
        configuration_in_shells={'num_ow': 5, 'num_cl': 1, 'coord_num': 6},
        # configuration_in_shells={'num_ow': 4, 'num_cl': 2, 'coord_num': 6},
        # configuration_in_shells={'num_ow': 3, 'num_cl': 3, 'coord_num': 6},
        # configuration_in_shells={'num_ow': 2, 'num_cl': 4, 'coord_num': 6},

        # configuration_in_shells={'num_ow': 7, 'num_cl': 0, 'coord_num': 7},
        # configuration_in_shells={'num_ow': 6, 'num_cl': 1, 'coord_num': 7},
        # configuration_in_shells={'num_ow': 5, 'num_cl': 2, 'coord_num': 7},

        centroid_id=args.centroid_id,
        # centroid_id=1,
        # centroid_id=2,
        # centroid_id=None,
        )

    exclude_atom_types = [
        # 'HW',
        # 'CL',
        # 'OW',
        # 'NA',
        # 'K',
        # 'LI',
        ]

    L = 0.0 # low level of opacity
    H = 0.0 # high level of opacity
    S = 0.2 # slider level of opacity

    volume_range = { # in multiples of mean
        # 'LI': (2, 10),
        'NA': (2, 10),
        # 'K':  (2, 10),
        'OW': (2, 10),
        'CL': (2, 10),
        'HW': (2, 10),
    }

    contour_levels = { # in multiples of mean
        # 'LI': [(4, L), (10, H), (20, S)],
        'NA': [(2, L), (5, H), (10, S)],
        # 'K':  [(4, L), (10, H), (20, S)],
        'OW': [(2, L), (10, H), (20, S)],
        'CL': [(2, L), (20, H), (40, S)],
        'HW': [(2, L), (5, H), (10, S)],
    }

    # plot_type = 'volume'
    plot_type = 'contour'

    # plot_object = ['density']
    # plot_object = ['void']
    plot_object = ['void', 'density']

    void_atom_types = ['OW']
    # void_atom_types = ['all']

    separate_atom_types = True
    # separate_atom_types = False

    # print(CONFIG)

    shellplot = ShellPlot3d(CONFIG,
        normalization=1e-3,
        plot_type=plot_type,
        exclude_atom_types=exclude_atom_types,
        separate_atom_types=separate_atom_types,
        void_atom_types=void_atom_types,
        volume_range=volume_range,
        contour_levels=contour_levels)
    shellplot.main(plot_object=plot_object)
    if 'void' in plot_object and 'density' in plot_object:
        shellplot.configure_traits(view='view_all')
    elif 'void' in plot_object:
        shellplot.configure_traits(view='view_voids')
    elif 'density' in plot_object:
        shellplot.configure_traits(view='view_all')
