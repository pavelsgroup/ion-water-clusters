# coding: utf-8
import os
import numpy as np
import argparse
import h5py
import scipy
import matplotlib
import time
from tqdm import tqdm
# from joblib import Parallel, delayed
from pathlib import Path

# from IPython import get_ipython
# if get_ipython() is not None:
#     get_ipython().run_line_magic('load_ext', 'autoreload')
#     get_ipython().run_line_magic('autoreload', '2')

#     get_ipython().run_line_magic('matplotlib', 'inline')
#     # matplotlib.use('Qt5Agg')

import matplotlib.pyplot as plt

from iwc.common.common import timer, encode_labels, decode_labels, configuration_generator
from iwc.densityanalysis import DensityAnalysis, compare_density_analysis, combine_density_analysis, EmptyAfterCroppingError
# import iwc.config as config

from iwc.tma_phil.config_tma import Config

matplotlib.rcParams.update({'font.size': 22})

"""

conda install -c conda-forge numpy scipy matplotlib tqdm joblib pandas seaborn numba hashlib


conda install -c conda-forge ipykernel
python -m ipykernel install --user --name iwc

"""

parser = argparse.ArgumentParser()

parser.add_argument("--overwrite-parent-dir", type=str, default=None,
                    help="")

args = parser.parse_args()

# ------------------------------------------------------------------------------

if __name__ == '__main__':

    STRIDE_1 = 1
    # ini_file_1 = 'config_tma_central_full.ini'
    # ini_file_1 = 'config_tma_central_scaled75.ini'
    # ini_file_1 = 'config_tma_charm36.ini'
    # ini_file_1 = 'config_tma_prosecco75.ini'
    # ini_file_1 = 'config_tma_super_ion.ini'
    ini_file_1 = 'config_tma_surface_full.ini'
    


    # --------------------------------------------------------------------------

    STRIDE_2 = 1
    # ini_file_2 = 'config_tma_central_full.ini'
    # ini_file_2 = 'config_tma_central_scaled75.ini'
    # ini_file_2 = 'config_tma_charm36.ini'
    # ini_file_2 = 'config_tma_prosecco75.ini'
    ini_file_2 = 'config_tma_super_ion.ini'
    # ini_file_2 = 'config_tma_surface_full.ini'
    

    # --------------------------------------------------------------------------

    # c_min = 3
    # c_max = 11
    # a_max = 11

    configuration_in_shells = {
        'name': 'TMA',
        'num_atoms': 4,
    }

    ALL_CONFS = [configuration_in_shells]

    PAR_1 = {
        'use_atom_weight': False,
        'normalization': 1e-3,
        'highlight_high_densities': False,
        'log_transform': True, # !!!
        # 'h_range': [np.linspace(1,9.7,128), np.linspace(0, 0.5, 256)],
        'h_range': [np.linspace(1,9.7,128), np.linspace(0, 2.5, 256)],
        'h_range': [np.linspace(1,9.7,128), np.linspace(1, 2.5, 256)],
        # 'h_range': [np.linspace(1,9.7,128), np.linspace(0, 2.5, 128)],
        # 'h_range': [np.linspace(1,9.7,128), np.linspace(1, 2.5, 64)],
        }
    PAR_1_plot = {
        'log_color': True, # !!!
    }
    
    PAR_2 = {
        'use_atom_weight': False,
        'normalization': 1e-3,
        'highlight_high_densities': False,
        'log_transform': False,
        # 'h_range': [np.linspace(1,9.7,128), np.linspace(0, 250, 256)],
        'h_range': [np.linspace(1,9.7,128), np.linspace(4, 200, 256)],
        }
    PAR_2_plot = {
        'log_color': False,
    }

    PAR_3 = {
        'use_atom_weight': False,
        'normalization': 1e-3,
        'highlight_high_densities': False,
        'log_transform': False,
        'h_range': [np.linspace(1,9.7,128), np.linspace(0,0.1,16)],
        }
    PAR_3_plot = {
        'log_color': False,
    }
    
    PAR = PAR_1; PAR_plot = PAR_1_plot
    # PAR = PAR_2; PAR_plot = PAR_2_plot
    # PAR = PAR_3; PAR_plot = PAR_3_plot
       

    DA1 = {}
    DA2 = {}
    for atom_type in ['HW']:

        DA1[atom_type] = []
        DA2[atom_type] = []

        for ARR_1 in ALL_CONFS: # configuration_generator(c_min, c_max, a_max):
            for i in range(10):

                CONFIG_1 = Config(
                    config_ini=ini_file_1,
                    stride=STRIDE_1,
                    # num_bins=(128, 128, 128,),
                    num_bins=(100, 100, 100,),
                    # num_bins=(64, 64, 64,),
                    hist_range=[(-10,10), (-10,10), (-10,10),],
                    centroid_id=i,
                    aligning_method='num_atoms',
                    overwrite_parent_dir=args.overwrite_parent_dir,
                    configuration_in_shells=ARR_1)

                if CONFIG_1.file_histogram.exists():
                    da = DensityAnalysis(CONFIG_1, atom_type=atom_type)
                    try:
                        da.analyze(**PAR)
                        da.analyze_rdf(x_range=(0,9.7))
                        DA1[atom_type].append(da)
                    except EmptyAfterCroppingError as e:
                        print("Empty configuration encountered - skipping")
                else:
                    pass
                    #print("skipping")

        DA1[atom_type] = combine_density_analysis(DA1[atom_type], CONFIG_1, atom_type)

        for ARR_2 in ALL_CONFS: # configuration_generator(c_min, c_max, a_max):
            for i in range(10):

                CONFIG_2 = Config(
                    config_ini=ini_file_2,
                    stride=STRIDE_2,
                    # num_bins=(128, 128, 128,),
                    num_bins=(100, 100, 100,),
                    # num_bins=(64, 64, 64,),
                    hist_range=[(-10,10), (-10,10), (-10,10),],
                    centroid_id=i,
                    aligning_method='num_atoms',
                    overwrite_parent_dir=args.overwrite_parent_dir,
                    configuration_in_shells=ARR_2)
                
                if CONFIG_2.file_histogram.exists():
                    da = DensityAnalysis(CONFIG_2, atom_type=atom_type)
                    try:
                        da.analyze(**PAR)
                        da.analyze_rdf(x_range=(0,9.7))
                        DA2[atom_type].append(da)
                    except EmptyAfterCroppingError as e:
                        print("Empty configuration encountered - skipping")

                else:
                    pass
                    #print("skipping")

        DA2[atom_type] = combine_density_analysis(DA2[atom_type], CONFIG_2, atom_type)


        figname = 'all'

        # compare_density_analysis(DA1[atom_type][0], DA2[atom_type][0],
        #         figname=figname + f'_all', do_plot_rdf=True, **PAR_plot)

        compare_density_analysis(DA1[atom_type], DA2[atom_type],
                figname=figname + f'_all', do_plot_rdf=False, **PAR_plot)
