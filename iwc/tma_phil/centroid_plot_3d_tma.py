# coding: utf-8
import numpy as np
import re
from iwc.cluster_plot_3d import ShellPlot3dGenerator

import argparse

parser = argparse.ArgumentParser()

parser.add_argument("--ini-file", type=str, default='config_tma_dihedrals_and_scaled.ini',
                    help="")

parser.add_argument("--centroid-id", type=int, default=0,
                    help="")

parser.add_argument("--stride", type=int, default=2,
                    help="")

parser.add_argument("--num-bins", type=int, default=100,
                    help="")

parser.add_argument("--overwrite-parent-dir", type=str, default=None,
                    help="")

"""
conda activate spsalign-mayavi

conda install -c conda-forge numpy scipy matplotlib tqdm joblib pandas seaborn numba h5py MDAnalysis coloredlogs verboselogs mayavi
"""

args = parser.parse_args()


# ------------------------------------------------------------------------------
# CONFIGURATION

from iwc.tma_phil.config_tma import Config

configuration_in_shells = {
    'name': 'TMA',
    'num_atoms': 4,
}

if __name__ == '__main__':

    description = args.ini_file
    description = re.sub(r'_', R' ', description)
    description = re.sub(r'\.ini', R'', description)
    description = re.sub(r'config', R' ', description)
    description = re.sub(r'tma', R' ', description)
    description = re.sub(r'\s+', R' ', description)
    description = description.strip()

    CONFIG = Config(
        description=description,
        config_ini=args.ini_file,
        stride=args.stride,
        hist_range=[(-10,10), (-10,10), (-10,10),],
        num_bins=(args.num_bins, args.num_bins, args.num_bins,),
        nhood_distance=10, # Angstrom
        aligning_method='num_atoms',
        centroid_id=args.centroid_id,
        overwrite_parent_dir=args.overwrite_parent_dir,
        configuration_in_shells=configuration_in_shells)


    exclude_centroids = []
    # exclude_centroids = [1,2,4,5,6]
    # exclude_centroids = [1,3,4,5,6]
    # exclude_centroids = [1,2,3,4,5,6] # Li-3-2-5

    # shellplot = ShellPlot3d(CONFIG,
    #         plot_type='centroid',)
    # shellplot.draw_centroids(exclude_centroids=exclude_centroids)
    # shellplot.configure_traits(view='view_centroids')

    ShellPlot3d = ShellPlot3dGenerator(
        atom_list=['H', 'HW', 'OW', 'C', 'N', 'CL'],
        atom_slider_range={
            'H': [0.01, 5., 2.,],
            'HW': [0.01, 5., 2.,],
            'OW': [0.01, 5., 2.,],
            'C': [0.01, 5., 2.,],
            'N': [0.01, 5., 2.,],
            'CL': [0.01, 5., 2.,],
        },
        )

    shellplot = ShellPlot3d(CONFIG)

    shellplot.draw_centroids()

    shellplot.configure_traits(view='view_all')