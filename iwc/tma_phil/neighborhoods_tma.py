# coding: utf-8
import configparser
import os
# from MDAnalysis.analysis import align
import time
# from joblib import Parallel, delayed
from pathlib import Path

import h5py
import iwc.config as config
import MDAnalysis as mda
import numpy as np
from iwc.common.common import decode_labels, encode_labels, notqdm
from numba import jit
# from MDAnalysis.coordinates.memory import MemoryReader
# from MDAnalysis.analysis.distances import distance_array
from scipy.spatial import cKDTree
from tqdm import tqdm

disable_tqdm = bool(os.environ.get("DISABLE_TQDM"))
if disable_tqdm is True:
    print("encountered environment variable DISABLE_TQDM, disabling tqdm")
    tqdm = notqdm


"""
conda install -c conda-forge numpy scipy tqdm joblib MDAnalysis h5py coloredlogs verboselogs

"""

# ------------------------------------------------------------------------------


def rename_labels(labels, target_label, possible_names):
    """
    Renames all instances of `possible_names` in `labels` to `target_label`.

    Example:
        labels = ['OW', 'HW1', 'HW2', 'HW1', 'HW2']
        possible_names = ['HW1', 'HW2']
        target_label = 'H'

        returns labels = ['OW', 'HW', 'HW', 'HW', 'HW']
    """
    
    for lab in labels:
        for l in possible_names:
            lab[lab == l] = target_label
    return labels


def reorder_dataset(positions, labels, atom_type_order, remove_closest=False):

    for i, (pos, lab) in enumerate(zip(positions, labels)):
        assert pos.shape[1] == 3
        assert pos.shape[0] == lab.shape[0]

        # first order the array by distances from center
        ind = np.argsort(np.linalg.norm(pos, axis=1), kind='heapsort')

        if remove_closest:
            ind = ind[1:]

        pos = pos[ind, :]
        lab = lab[ind]

        # then shuffle
        ind = np.concatenate([np.where(lab == atom_type) for atom_type in atom_type_order], axis=1)

        ind = ind[0]

        pos = pos[ind, :]
        lab = lab[ind]

        positions[i] = pos
        labels[i] = lab

    return positions, labels


@jit(nopython=True)
def tranformation(a, b, q, p):
    """
    Transforms coordinate q when we center p (p = 0)
    with periodic boundary conditions
    a ... left border
    b ... rigth border
    """
    return a + np.mod(q - p - a, b - a)


def neighborhood_of_atom(
        i, center_atom_pos, atoms_pos, a, b, ele, HWs,
        nhood_distance=None, num_neighbours=None,
        include_hydrogens=True, **kwargs):
    """
    """

    # if __debug__:
    #     start = time.time()

    new_pos_center = np.asarray((0, 0, 0))

    allnew_ato = tranformation(a, b, atoms_pos, center_atom_pos)

    # if __debug__:
    #     print("transform all: ", (time.time() - start) * 1e3, " ms")
    #     start = time.time()

    tree = cKDTree(allnew_ato, compact_nodes=False)  # allnew_ato ... array_like, shape (n,m) The n data points of dimension m to be indexed
    if num_neighbours is not None:
        _, id_in_array = tree.query(new_pos_center, 1 + num_neighbours)  # find N + 1 atoms closest to (0,0,0)
    elif nhood_distance is not None:
        id_in_array = tree.query_ball_point(new_pos_center, nhood_distance)
    else:
        raise(ValueError('One of num_neighbours, nhood_distance arguments must be not None.'))

    pos_ato_new = allnew_ato[id_in_array, :]

    cluster_labels = ele[id_in_array].names

    # print(cluster_labels)

    if include_hydrogens:
        # if __debug__:
        #     print("search tree: ", (time.time() - start) * 1e3, " ms")
        #     start = time.time()

        hydrogen_selection = 'name {}'.format(' or name '.join(HWs))
        H = ele.residues[id_in_array].atoms.select_atoms(hydrogen_selection)
        H_labels = H.names

        pos_H_new = tranformation(a, b, H.positions, center_atom_pos)

        # if __debug__:
        #     print("transform H: ", (time.time() - start) * 1e3, " ms")
        #     start = time.time()

        pos = np.append(pos_ato_new, pos_H_new, axis=0)
        lab = np.append(cluster_labels, H_labels)

    else:
        pos = pos_ato_new
        lab = cluster_labels

    # each array of 11 atoms, tranform the H positions that bind to closest Oxygens into the new positions, then add them to the list of 11 atoms created before.

    # if __debug__:
    #     print("finalize: ", (time.time() - start) * 1e3, " ms")

    return pos, lab


def main(wrk_dir, topology_file, trajectory_file, stride=1, **kwargs):

    # ALL_ATOMS = ['C1', 'H', 'N', 'HW', 'OW']    
    atom_type_order = ['N', 'C', 'H', 'HW', 'OW', 'CL']

    HWs = ['HW1', 'HW2'] # water hydrogens

    equivalent_groups = [
        {
            'label': 'HW',
            'names': ['HW1', 'HW2']
        }, {
            'label': 'H',
            'names': ['H1A', 'H1B', 'H1C', 'H2A', 'H2B', 'H2C', 'H3A', 'H3B', 'H3C', 'H4A', 'H4B', 'H4C']
        }, {
            'label': 'OW',
            'names': ['OW', 'OW1']
        },{
            'label': 'C',
            'names': ['C1', 'C2', 'C3', 'C4']
        },
    ]

    CENTER_ATOM = 'N'

    center_selection = 'name {}'.format(CENTER_ATOM)
    # atoms_selection = 'name {}'.format(' or name '.join(ALL_ATOMS))
    atoms_selection = 'name *'

    print(f"Analysing files\n{topology_file}\n{trajectory_file}\nwith stride {stride}")

    u = mda.Universe(str(topology_file), str(trajectory_file))

    ele = u.select_atoms(atoms_selection)

    center_atoms = u.select_atoms(center_selection)

    n_center_atoms = len(center_atoms)

    if n_center_atoms == 0:
        raise ValueError(f"No center atoms {CENTER_ATOM} found in trajectory!")


    n_frames = int(np.floor(u.trajectory.n_frames / stride))
    chunk_size = int(np.floor(n_frames / 10))

    wrk_dir.mkdir(parents=False, exist_ok=True)

    found_labels = set()

    with h5py.File(wrk_dir / "neighborhoods.h5", 'w') as f:

        f_positions = f.create_dataset(
            'positions', (n_frames*n_center_atoms,), 
            dtype=h5py.special_dtype(vlen=np.dtype('float32')))

        f_labels = f.create_dataset(
            'labels', (n_frames*n_center_atoms,), 
            dtype=h5py.special_dtype(vlen=np.dtype('uint8')))

        for m, ts in tqdm(enumerate(u.trajectory[::stride]), total=n_frames):

            box = u.dimensions
            ele.pack_into_box(box=box)

            ele_pos = ele.positions
            a = -u.dimensions[:3] / 2.0
            b = u.dimensions[:3] / 2.0

            positions = []
            labels = []

            # ------------------------------------------------------------------
            for j, ca in enumerate(center_atoms):

                ca_pos = ca.position

                pos, lab = neighborhood_of_atom(
                    j, ca_pos, np.copy(ele_pos), a, b, ele, HWs, **kwargs,
                    include_hydrogens=False)

                positions.append(pos)
                labels.append(lab)
                found_labels = found_labels.union(set(lab))

            # ------------------------------------------------------------------

            k1 = m * n_center_atoms
            k2 = (m + 1) * n_center_atoms

            for eq_group in equivalent_groups:                
                labels = rename_labels(labels, eq_group['label'], eq_group['names'])

            positions, labels = reorder_dataset(
                positions, labels, atom_type_order,
                remove_closest=False) # remove closest = remove centered atom

            for i, (pos, lab) in enumerate(zip(positions, labels)):
                positions[i] = np.ravel(pos)
                labels[i] = np.asarray(encode_labels(lab), dtype='uint8')

            f_positions[k1:k2] = positions
            f_labels[k1:k2] = labels

        print('Found the following lables (atom names): ')
        print(found_labels)

        # for eq_group in equivalent_groups:                
        #     found_labels = rename_labels(list(found_labels), eq_group['label'], eq_group['names'])
        # print(found_labels)

        # print(labels[0])
        # print(decode_labels(labels[0]))

        # print(f"Saving results to\n{folder_neighborhoods}")

    # np.save(folder_neighborhoods / "positions.npy", all_pos)
    # np.save(folder_neighborhoods / "labels.npy", all_lab)


if __name__ == '__main__':

    top_dir = Path('/qrk/phil_TMA')

    # dihedrals
    # dihedrals_and_scaled
    # original
    # original-tip4p2005
    # scaled

    data_dir = top_dir / 'original'

    topology_file = data_dir / 'conf.gro'
    trajectory_file = data_dir / 'traj_comp.xtc'
    wrk_dir = data_dir / 'analysis'

    main(wrk_dir, topology_file, trajectory_file, stride=1, nhood_distance=10)
