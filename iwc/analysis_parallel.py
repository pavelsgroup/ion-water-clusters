# coding: utf-8
%load_ext autoreload
%autoreload 2

import numpy as np
import iwc.config as config
import iwc.neighborhoods as neighborhoods
import iwc.hydration_shells as hydration_shells
import iwc.shell_distances as shell_distances
import iwc.clustering as clustering
import iwc.histograms as histograms

from tqdm import tqdm
from joblib import Parallel, delayed

STRIDE = 100

CONFIGS = []

# CATION = 'NA'
# for ARR in [
#         np.asarray(['*', 2, 2, '*', '*', '*', '*', '*']),
#         np.asarray(['*', 3, 1, '*', '*', '*', '*', '*']),
#         np.asarray(['*', 4, 0, '*', '*', '*', '*', '*']),
#
#         np.asarray(['*', 3, 2, '*', '*', '*', '*', '*']),
#         np.asarray(['*', 4, 1, '*', '*', '*', '*', '*']),
#         np.asarray(['*', 5, 0, '*', '*', '*', '*', '*']),
#
#         np.asarray(['*', 4, 2, '*', '*', '*', '*', '*']),
#         np.asarray(['*', 5, 1, '*', '*', '*', '*', '*']),
#         np.asarray(['*', 6, 0, '*', '*', '*', '*', '*']),
#         ]:
#     CONFIG.append(config.Config(
#             CATION=CATION,
#             stride=STRIDE,
#             configuration_in_shells=ARR))

CATION = 'LI'
for ARR in [
        {'num_ow': 4, 'num_cl': 0, 'coord_num': 4},
        {'num_ow': 3, 'num_cl': 1, 'coord_num': 4},
        {'num_ow': 2, 'num_cl': 2, 'coord_num': 4},
        {'num_ow': 4, 'num_cl': 0, 'coord_num': 4},
        {'num_ow': 3, 'num_cl': 1, 'coord_num': 4},
        {'num_ow': 2, 'num_cl': 2, 'coord_num': 4},
        {'num_ow': 4, 'num_cl': 0, 'coord_num': 4},
        {'num_ow': 3, 'num_cl': 1, 'coord_num': 4},
        {'num_ow': 2, 'num_cl': 2, 'coord_num': 4},
        # np.asarray(['*', 2, 2, '*', '*', '*', '*', '*']),
        # np.asarray(['*', 3, 1, '*', '*', '*', '*', '*']),
        # np.asarray(['*', 4, 0, '*', '*', '*', '*', '*']),
        ]:
    for NB in [ (97, 99, 101,),
                    # (195, 197, 199,),
                    # (295, 297, 299,),
                  ]:
            CONFIGS.append(config.Config(
                    CATION=CATION,
                    stride=STRIDE,
                    num_bins=NB,
                    hist_range=[(-9,9), (-9,9), (-9,9),],
                    aligning_method='num_atoms',
                    configuration_in_shells=ARR))

# CATION = 'K'
# for ARR in [
#         #np.asarray(['*', 2, 2, '*', '*', '*', '*', '*']),
#         np.asarray(['*', 3, 1, '*', '*', '*', '*', '*']),
#         np.asarray(['*', 4, 0, '*', '*', '*', '*', '*']),
#
#         np.asarray(['*', 3, 2, '*', '*', '*', '*', '*']),
#         np.asarray(['*', 4, 1, '*', '*', '*', '*', '*']),
#         np.asarray(['*', 5, 0, '*', '*', '*', '*', '*']),
#
#         np.asarray(['*', 3, 3, '*', '*', '*', '*', '*']),
#         np.asarray(['*', 4, 2, '*', '*', '*', '*', '*']),
#         np.asarray(['*', 5, 1, '*', '*', '*', '*', '*']),
#         np.asarray(['*', 6, 0, '*', '*', '*', '*', '*']),
#
#         np.asarray(['*', 5, 2, '*', '*', '*', '*', '*']),
#         np.asarray(['*', 6, 1, '*', '*', '*', '*', '*']),
#         np.asarray(['*', 7, 0, '*', '*', '*', '*', '*']),
#
#         np.asarray(['*', 6, 2, '*', '*', '*', '*', '*']),
#         np.asarray(['*', 7, 1, '*', '*', '*', '*', '*']),
#         np.asarray(['*', 8, 0, '*', '*', '*', '*', '*']),
#
#         #np.asarray(['*', 8, 1, '*', '*', '*', '*', '*']),
#         #np.asarray(['*', 9, 0, '*', '*', '*', '*', '*']),
#         ]:
#     CONFIG.append(config.Config(
#             CATION=CATION,
#             stride=STRIDE,
#             configuration_in_shells=ARR))

from clustering import main

# def task(CON):
#     clustering.main(CON)
#     histograms.main(CON)
#     return 1

# for CON in tqdm(CONFIG):
#     main(CON)

result = Parallel(n_jobs=8)(delayed(main)(CON) for CON in tqdm(CONFIGS))

print(result)

# TODO - replace int8 by uint8
