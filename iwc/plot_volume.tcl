# Example use:
# vmd -e plot_volume.tcl -args `ls *.cube | sort -V`


# extract command line arguments
set i 0; foreach n $argv {set [incr i] $n}

set filenames [lrange $argv 1 $argc-1]

# some global settings, tweak to taste
display rendermode GLSL
color Display Background white
axes location Off
animate style Once

# CENTROID

mol new $1 type {gro} first 0 last -1 step 1 waitfor 1
mol modstyle 0 0 VDW 1.000000 24.000000
mol modmaterial 0 0 Transparent

# VOLUMES

set i -1
foreach filename $filenames {
    # mol addfile $filename type cube waitfor all
    set molecule [mol new $filename type cube waitfor all]

    incr i

    # transparent contour
    mol representation Isosurface 3 0 0 0 1 1
    mol color ColorID $i
    mol material Glass1
    mol addrep top

    mol representation Isosurface 4 0 0 0 1 1
    mol color ColorID $i
    mol material Glass1
    mol addrep top

    # mol representation Isosurface 3 0 0 0 1 1
    # mol color ColorID 1
    # mol material BlownGlass
    # mol addrep top

    # mol representation Isosurface 4 0 0 0 1 1
    # mol color ColorID 1
    # mol material BlownGlass
    # mol addrep top

}

# proc do_render {} {

set TACHYON "/usr/lib/vmd/tachyon_LINUXAMD64"

render Tachyon \
    "vmdscene.dat" \
    "$TACHYON" \
    -res 2048 2048 \
    -aasamples 12 %s \
    -format TARGA \
    -o %s.tga

file delete "vmdscene.dat"
# }

# do_render