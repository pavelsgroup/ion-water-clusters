# coding: utf-8
import os
import numpy as np
import numba
from numba import jit

from iwc.common.common_numba import find_instr

from iwc.common.testing_common import measuretime_precompile
import iwc.aligning as aligning

# np.__config__.show()
os.environ['OPENBLAS_NUM_THREADS'] = '1'
os.environ['MKL_NUM_THREADS'] = '1'

@jit(nopython=True)
def sqdiff(x, y):
    out = np.empty_like(x)
    for i in range(x.shape[0]):
        out[i] = (x[i] - y[i])**2
    return out

def test_sqdiff_example():
    x32 = np.linspace(1, 2, 10000, dtype=np.float32)
    y32 = np.linspace(2, 3, 10000, dtype=np.float32)
    sqdiff(x32, y32)

    x64 = x32.astype(np.float64)
    y64 = y32.astype(np.float64)
    sqdiff(x64, y64)

    sqdiff.signatures

    print('float32:')
    find_instr(sqdiff, keyword='subp', sig=0)
    print('---\nfloat64:')
    find_instr(sqdiff, keyword='subp', sig=1)

def dist_square_reference(x, y, out=None, diff=None):
    """
    Calculates square distance
    out[i] = sum_{j,k} (x[i,j,k] - y[i,j,k])^2

    x.shape = (n, m, d,) or (m, d,)
    y.shape = (n, m, d,) or (m, d,)
    out.shape = (n,)
    where
        n ... number of datasets
        m ... size of each dataset
        d ... dimension
    """
    if out is None:
        out = np.empty(x.shape[0])
    if diff is None:
        diff = np.empty_like(x)

    out = np.sum(np.square(x - y, out=diff), axis=(1,2), out=out)

    return out

def test_dist_square():

    n = int(1e5)
    m = 10
    d = 3

    # -----------------------------------------

    x = np.random.rand(n,m,d,)
    y = np.random.rand(n,m,d,)
    
    r = dist_square_reference(x, y)
    
    out = np.empty((n,))
    diff = np.empty((n,m,d,))
    
    diff_hex = hex(id(diff))
    out_hex = hex(id(out))

    out = aligning.dist_square(x, y, out=out, diff=diff)
    # aligning.dist_square(x, y, out=out, diff=diff)

    assert hex(id(diff)) == diff_hex
    assert hex(id(out)) == out_hex

    err = np.abs(r - out)
    assert np.all(np.all(err < 1e-10)), np.max(err)

    out = aligning.dist_square_inplace(x, y, out=out)

    assert hex(id(out)) == out_hex

    err = np.abs(r - out)
    assert np.all(np.all(err < 1e-10)), np.max(err)

    print(f"No errors found")

    x = x.astype('float32')
    y = y.astype('float32')
    out = out.astype('float32')

    out = aligning.dist_square_inplace(x, y, out=out)

    find_instr(aligning.dist_square_inplace, keyword='subp', sig=0)
    find_instr(aligning.dist_square_inplace, keyword='subp', sig=1)

    # -----------------------------------------

    x = np.random.rand(n,m,d,)
    y = np.random.rand(m,d,)
    
    r = dist_square_reference(x, y)
    
    out = np.empty((n,))
    diff = np.empty((n,m,d,))
    
    diff_hex = hex(id(diff))
    out_hex = hex(id(out))

    out = aligning.dist_square_inplace_broadcast(x, y, out=out)

    assert hex(id(diff)) == diff_hex
    assert hex(id(out)) == out_hex

    err = np.abs(r - out)
    assert np.all(np.all(err < 1e-10)), np.max(err)

    print(f"No errors found")


    # -----------------------------------------

    print(f"Testing speed with float64")
    
    x = np.array(np.random.rand(n,m,d,), dtype='float64')
    y = np.array(np.random.rand(n,m,d,), dtype='float64')

    out = np.empty((n,), dtype='float64')
    diff = np.empty((n,m,d,), dtype='float64')

    measuretime_precompile(20, dist_square_reference, x, y, out=out, diff=diff)
    measuretime_precompile(20, aligning.dist_square, x, y, out=out, diff=diff)
    measuretime_precompile(20, aligning.dist_square_inplace, x, y, out=out)

    y = np.array(np.random.rand(m,d,), dtype='float64')

    measuretime_precompile(20, dist_square_reference, x, y, out=out, diff=diff)
    measuretime_precompile(20, aligning.dist_square_inplace_broadcast, x, y, out=out)

    print(f"Testing speed with float32")

    x = np.array(np.random.rand(n,m,d,), dtype='float32')
    y = np.array(np.random.rand(n,m,d,), dtype='float32')

    out = np.empty((n,), dtype='float32')
    diff = np.empty((n,m,d,), dtype='float32')

    measuretime_precompile(20, dist_square_reference, x, y, out=out, diff=diff)
    measuretime_precompile(20, aligning.dist_square, x, y, out=out, diff=diff)
    measuretime_precompile(20, aligning.dist_square_inplace, x, y, out=out)

    y = np.array(np.random.rand(m,d,), dtype='float32')

    measuretime_precompile(20, dist_square_reference, x, y, out=out, diff=diff)
    measuretime_precompile(20, aligning.dist_square_inplace_broadcast, x, y, out=out)

if __name__ == '__main__':

    test_sqdiff_example()

    test_dist_square()

