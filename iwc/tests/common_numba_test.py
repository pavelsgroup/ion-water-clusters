import numpy as np
from iwc import common_numba

from iwc.tests.testing_common import measuretime

def numpy_is_sorted(x):
    return np.all(x[:-1] < x[1:])

def test_is_sorted():
    n = int(1e8)
    
    check_results = lambda x: numpy_is_sorted(x) == common_numba.is_sorted(x)

    sorted_array = np.arange(n)
    assert check_results(sorted_array)

    unsorted_array = np.arange(n)
    unsorted_array[0] = n
    assert check_results(unsorted_array)

    random_array = np.random.rand(n)
    assert check_results(random_array)

    measuretime(20, numpy_is_sorted, np.arange(n))
    measuretime(20, common_numba.is_sorted, np.arange(n))

    measuretime(20, numpy_is_sorted, np.random.rand(n))
    measuretime(20, common_numba.is_sorted, np.random.rand(n))


def numpy_is_all_zero(x):
    return np.all(x == 0)

def test_is_all_zero():
    n = int(1e8)

    check_results = lambda x: numpy_is_all_zero(x) == common_numba.is_all_zero(x)

    zero_array = np.zeros(n)
    assert check_results(zero_array)

    nonzero_array = np.arange(n)
    nonzero_array[0] = n
    assert check_results(nonzero_array)

    random_array = np.random.rand(n)
    assert check_results(random_array)

    measuretime(20, numpy_is_all_zero, np.arange(n))
    measuretime(20, common_numba.is_all_zero, np.arange(n))

    measuretime(20, numpy_is_all_zero, np.random.rand(n))
    measuretime(20, common_numba.is_all_zero, np.random.rand(n))


if __name__ == '__main__':

    test_is_sorted()
    test_is_all_zero()
