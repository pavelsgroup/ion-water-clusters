# coding: utf-8

import os
import numpy as np
import h5py
import MDAnalysis as mda
# from MDAnalysis.coordinates.memory import MemoryReader
# from MDAnalysis.analysis.distances import distance_array
from scipy.spatial import cKDTree
# from MDAnalysis.analysis import align
import time
from tqdm import tqdm
# from joblib import Parallel, delayed
from pathlib import Path
import configparser
from numba import jit

from IPython import get_ipython
if get_ipython() is not None:
    get_ipython().run_line_magic('load_ext', 'autoreload')
    get_ipython().run_line_magic('autoreload', '2')

    # get_ipython().run_line_magic('matplotlib', 'inline')
    # matplotlib.use('Qt5Agg')
  
from iwc.common.common import encode_labels, decode_labels, notqdm
import iwc.config as config

disable_tqdm = bool(os.environ.get("DISABLE_TQDM"))
if disable_tqdm is True:
    print("encountered environment variable DISABLE_TQDM, disabling tqdm")
    tqdm = notqdm


"""
conda install -c conda-forge numpy scipy tqdm joblib MDAnalysis h5py



Input:
    .tpr and .xtc file of the simulation of LiCl solution.
    Number of Li ... 187
    Number of Cl ... 187
    Number of frame ... 15302

Output:
    List: count_ato_allframe ... list of 3d np arrays
        dim 0: number of frames in the simulation
        dim 1: number of Li in the box
        dim 2: number of occurrences of each type of atoms in the following order (Li, O, Cl, H)
    list: list11H_label_allframe ... list of 3d np array
        dim 0: number of frames of simulation.
        dim 1: number of Li in the box.
        dim 2: name of all the atoms in every cluster
    list: list11H_pos_allframe ... list of 4d np array
        dim 0: number of frames of simulation.
        dim 1: number of Li in the box.
        dim 2: number of atoms in each cluster.
        dim 3: coordinates (x,y,z) of the atoms

"""

# ------------------------------------------------------------------------------


def rename_labels(labels, HW, HWs):
    for lab in labels:
        for h in HWs:
            lab[lab == h] = HW
    return labels


def reorder_dataset(positions, labels, atom_type_order, remove_closest=False):

    for i, (pos, lab) in enumerate(zip(positions, labels)):
        assert pos.shape[1] == 3
        assert pos.shape[0] == lab.shape[0]

        # first order the array by distances from center
        ind = np.argsort(np.linalg.norm(pos, axis=1), kind='heapsort')

        if remove_closest:
            ind = ind[1:]

        pos = pos[ind, :]
        lab = lab[ind]

        # then shuffle
        ind = np.concatenate([np.where(lab == atom_type) for atom_type in atom_type_order], axis=1)

        ind = ind[0]

        pos = pos[ind, :]
        lab = lab[ind]

        positions[i] = pos
        labels[i] = lab

    return positions, labels


@jit(nopython=True)
def tranformation(a, b, q, p):
    """
    transform coordinate q when we center p (p = 0)
    with periodic boundary conditions
    a ... left border
    b ... rigth border
    """
    return a + np.mod(q - p - a, b - a)


def neighborhood_of_atom(
        i, Li_pos, ele_pos, a, b, ele, HWs,
        nhood_distance=None, num_neighbours=None,
        include_hydrogens=True, **kwargs):
    """
    """

    # if __debug__:
    #     start = time.time()

    new_pos_Li = np.asarray((0, 0, 0))

    allnew_ato = tranformation(a, b, ele_pos, Li_pos)

    # if __debug__:
    #     print("transform all: ", (time.time() - start) * 1e3, " ms")
    #     start = time.time()

    tree = cKDTree(allnew_ato, compact_nodes=False)  # allnew_ato ... array_like, shape (n,m) The n data points of dimension m to be indexed
    if num_neighbours is not None:
        _, id_in_array = tree.query(new_pos_Li, 1 + num_neighbours)  # find N + 1 atoms closest to (0,0,0)
    elif nhood_distance is not None:
        id_in_array = tree.query_ball_point(new_pos_Li, nhood_distance)

    pos_ato_new = allnew_ato[id_in_array, :]  # list of 11 atom positions

    cluster_labels = ele[id_in_array].names

    if include_hydrogens:
        # if __debug__:
        #     print("search tree: ", (time.time() - start) * 1e3, " ms")
        #     start = time.time()

        hydrogen_selection = 'name {}'.format(' or name '.join(HWs))
        H = ele.residues[id_in_array].atoms.select_atoms(hydrogen_selection)
        H_labels = H.names

        pos_H_new = tranformation(a, b, H.positions, Li_pos)

        # if __debug__:
        #     print("transform H: ", (time.time() - start) * 1e3, " ms")
        #     start = time.time()

        pos = np.append(pos_ato_new, pos_H_new, axis=0)
        lab = np.append(cluster_labels, H_labels)

    else:
        pos = pos_ato_new
        lab = cluster_labels

    # each array of 11 atoms, tranform the H positions that bind to closest Oxygens into the new positions, then add them to the list of 11 atoms created before.

    # if __debug__:
    #     print("finalize: ", (time.time() - start) * 1e3, " ms")

    return pos, lab


def main(ANION, CATION, OW, HW, HWs, tpr_file, xtc_file, folder_neighborhoods, stride=1, **kwargs):

    print(f"Analysing files\n{tpr_file}\n{xtc_file}\nwith stride {stride}")

    u = mda.Universe(str(tpr_file), str(xtc_file))

    cation_selection = 'name {}'.format(CATION)
    atoms_selection = 'name {}'.format(' or name '.join([CATION, ANION, OW]))

    cations = u.select_atoms(cation_selection)

    n_cations = len(cations)

    if n_cations == 0:
        raise ValueError("No {} atoms found in trajectory!".format(CATION))

    ele = u.select_atoms(atoms_selection)

    # all_pos = []
    # all_lab = []

    n_frames = int(np.floor(u.trajectory.n_frames / stride))
    chunk_size = int(np.floor(n_frames / 10))

    folder_neighborhoods.mkdir(parents=False, exist_ok=True)

    with h5py.File(folder_neighborhoods / "neighborhoods.h5", 'w') as f:

        f_positions = f.create_dataset(
            'positions', (n_frames*n_cations,), dtype=h5py.special_dtype(vlen=np.dtype('float32')))

        f_labels = f.create_dataset(
            'labels', (n_frames*n_cations,), dtype=h5py.special_dtype(vlen=np.dtype('uint8')))

        for m, ts in tqdm(enumerate(u.trajectory[::stride]), total=n_frames):

            box = u.dimensions
            ele.pack_into_box(box=box)

            ele_pos = ele.positions
            a = -u.dimensions[:3] / 2.0
            b = u.dimensions[:3] / 2.0

            positions = []
            labels = []

            # ------------------------------------------------------------------
            for j, cation in enumerate(cations):

                cation_pos = cation.position

                pos, lab = neighborhood_of_atom(
                    j, cation_pos, np.copy(ele_pos), a, b, ele, HWs, **kwargs,
                    include_hydrogens=True)

                positions.append(pos)
                labels.append(lab)

            # ------------------------------------------------------------------

            k1 = m * n_cations
            k2 = (m + 1) * n_cations

            atom_type_order = [CATION, ANION, OW, HW]

            labels = rename_labels(labels, HW, HWs)
            positions, labels = reorder_dataset(positions, labels, atom_type_order, remove_closest=True) # remove closest = remove centered atom

            for i, (pos, lab) in enumerate(zip(positions, labels)):
                positions[i] = np.ravel(pos)
                labels[i] = np.asarray(encode_labels(lab), dtype='uint8')

            f_positions[k1:k2] = positions
            f_labels[k1:k2] = labels

        # print(labels)
        # print(decode_labels(labels[0]))

        # print(f"Saving results to\n{folder_neighborhoods}")

    # np.save(folder_neighborhoods / "positions.npy", all_pos)
    # np.save(folder_neighborhoods / "labels.npy", all_lab)


if __name__ == '__main__':

    CONFIG = config.Config()

    main(**vars(CONFIG))
