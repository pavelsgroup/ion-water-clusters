# coding: utf-8
import os
import numpy as np
import h5py
import scipy
import matplotlib
import time
from tqdm import tqdm
# from joblib import Parallel, delayed
from pathlib import Path

# from IPython import get_ipython
# if get_ipython() is not None:
#     get_ipython().run_line_magic('load_ext', 'autoreload')
#     get_ipython().run_line_magic('autoreload', '2')

#     get_ipython().run_line_magic('matplotlib', 'inline')
#     # matplotlib.use('Qt5Agg')

import matplotlib.pyplot as plt

from iwc.common.common import timer, encode_labels, decode_labels
import iwc.config as config

"""

conda install -c conda-forge numpy scipy matplotlib tqdm joblib pandas seaborn numba hashlib


conda install -c conda-forge ipykernel
python -m ipykernel install --user --name iwc

"""

cmap_density = 'gist_heat_r'

# cmap = 'RdBu'
cmap = 'seismic_r'

# ------------------------------------------------------------------------------

def atom_weight(atom_type):
    WEIGHTS = {'OW': 16, 'HW': 1}
    return WEIGHTS[atom_type]

class EmptyAfterCroppingError(ValueError): pass

class DensityAnalysis():

    def __init__(self, CONFIG, atom_type='OW'):

        self.CONFIG = CONFIG
        self.atom_type = atom_type


    def crop_data(self, H, D):

        print(f"H array range: [{H.min()}, {H.max()}]")
        print(f"D array range: [{D.min()}, {D.max()}]")

        print("Cropping data...")

        # remove boxes outside range of analysis

        if self.x_range is not None:
            H = H[np.where(D <= self.x_range[-1])]
            D = D[np.where(D <= self.x_range[-1])]
            H = H[np.where(D >= self.x_range[0])]
            D = D[np.where(D >= self.x_range[0])]

        if self.y_range is not None:
            D = D[np.where(H >= self.y_range[0])]
            H = H[np.where(H >= self.y_range[0])]
            D = D[np.where(H <= self.y_range[-1])]
            H = H[np.where(H <= self.y_range[-1])]

        if H.size == 0 or D.size == 0:
            raise(EmptyAfterCroppingError("H or D arrays are empty. Maybe loosen the cropping constraints."))
        
        print(f"H array range: [{H.min()}, {H.max()}]")
        print(f"D array range: [{D.min()}, {D.max()}]")

        return H, D


    def load_data(self):

        with np.load(self.CONFIG.file_histogram, allow_pickle=True) as npzfile:
            print(npzfile.files)
            histograms = npzfile['histograms'].item()  # .item() is here to recover dict form 0d numpy array
            bin_volumes = npzfile['bin_volumes'].item()
            edges = npzfile['edges'].item()
            num_frames = npzfile['num_groups'].item()
            # avg_num_atoms = npzfile['avg_num_atoms'].item()
            # distance_intervals = npzfile['distance_intervals'].item()

        histograms = histograms[self.atom_type]
        bin_volumes = bin_volumes[self.atom_type]
        edges = edges[self.atom_type]

        if bin_volumes is None:
            bin_sizes = [np.diff(e) for e in edges]
            X, Y, Z = np.meshgrid(*bin_sizes, indexing='ij')
            bin_volumes = X * Y * Z

        print(f"Loaded data of {num_frames} structures")

        return histograms, edges, bin_volumes, num_frames


    def plot_rdf(self, fig=None, ax=None, xlim=[None, None]):
        if fig is None or ax is None:
            fig = plt.figure(figsize=(12, 9),)
            plt.rc('text', usetex=True)
            ax = plt.gca()

        ax.plot(self.RDF_r, self.RDF_no_norm)

        plt.ylabel(r"RDF " + f"{self.atom_type}" + " [atoms / Å]")
        plt.xlabel("distance from center [Å]")

        if xlim[0] is not None and xlim[1] is not None:
            Y = self.RDF_no_norm[np.logical_and(
                xlim[0] < self.RDF_r,
                xlim[1] > self.RDF_r)]

            return [np.min(Y), np.max(Y)]


    def plot_cdf(self, fig=None, ax=None, xlim=[None, None]):
        if fig is None or ax is None:
            fig = plt.figure(figsize=(12, 9),)
            plt.rc('text', usetex=True)
            ax = plt.gca()

        ax.plot(self.RDF_r, self.CDF)

        plt.ylabel(r"CDF " + f"{self.atom_type}" + " [atoms]")
        plt.xlabel("distance from center [Å]")

        if xlim[0] is not None and xlim[1] is not None:
            Y = self.CDF[np.logical_and(
                xlim[0] < self.RDF_r,
                xlim[1] > self.RDF_r)]

            return [np.min(Y), np.max(Y)]

    def calculate_variance(self, ):
        variance = np.zeros(self.HH.shape[1])
        means = np.zeros(self.HH.shape[1])

        xmids = 0.5*(self.xedges[1:] + self.xedges[:-1])
        ymids = 0.5*(self.yedges[1:] + self.yedges[:-1])

        for i in range(self.HH.shape[1]):
            # n, bins = np.histogram(x)
            n = self.HH[:,i]
            if np.sum(n) == 0:
                var = 0
                mean = 0
            else:     
                mean = np.average(ymids, weights=n)
                var = np.average((ymids - mean)**2, weights=n)
            variance[i] = var
            means[i] = mean
        
        return variance, means

    def plot_density_stddev(self, fig=None, ax=None, xlim=[None, None]):
        if fig is None or ax is None:
            fig = plt.figure(figsize=(12, 9),)
            plt.rc('text', usetex=True)
            ax = plt.gca()

        xmids = 0.5*(self.xedges[1:] + self.xedges[:-1])

        var, mean =  self.calculate_variance()

        ax.plot(xmids, np.sqrt(var))
        #ax.plot(xmids, np.sqrt(mean))
        try:
            print(self.CONFIG.CATION)
        except(AttributeError):
            pass

        #plt.ylabel(r"CDF " + f"{self.atom_type}" + " [atoms]")
        plt.xlabel("distance from center [Å]")

        # if xlim[0] is not None and xlim[1] is not None:
        #     Y = self.CDF[np.logical_and(
        #         xlim[0] < self.RDF_r,
        #         xlim[1] > self.RDF_r)]

        #     return [np.min(Y), np.max(Y)]


    def plot(self, fig=None, ax=None, vmin=None, vmax=None):

        if fig is None or ax is None:
            fig = plt.figure(figsize=(12, 9),)
            plt.rc('text', usetex=True)
            ax = plt.gca()

        X, Y = np.meshgrid(self.xedges, self.yedges)

        # self.statistics(self.HH, do_print=True)

        im = ax.pcolormesh(X, Y, self.HH, cmap=cmap_density, vmin=vmin, vmax=vmax)

        fig.colorbar(im, ax=ax, label=f"")

        plt.ylabel(r"$\varrho_{" + f"{self.atom_type}" + "}$ (density) []")
        plt.xlabel("distance from center [Å]")
        # plt.show()

        # plt.figure(figsize=(12, 9),)
        # plt.hist(H, bins=100)
        # plt.show()


    def load_and_preprocess(self):
        # Load and prepocess data

        hist, edge, bin_volumes, self.num_frames = self.load_data()

        # we assume that the center is at (0, 0, 0)
        cation_pos = (0, 0, 0,)

        bin_centers = [(e[:-1] + e[1:])/2 for e in edge]

        # distance of bin center from origin
        X, Y, Z = np.meshgrid(*bin_centers, indexing='ij')
        dist = np.sqrt(X**2 + Y**2 + Z**2)

        assert cation_pos == (0, 0, 0,)
        assert dist.shape == hist.shape == bin_volumes.shape

        H = hist.flatten()
        D = dist.flatten()
        V = bin_volumes.flatten()

        if abs(V.max() - V.min())/V.mean() > 0.01:
            print("Bin volumes differ - do you really have non-uniform bins?")

        return H, D, V


    def analyze(self,
            use_atom_weight=False,
            normalization=1,
            log_transform=False,
            highlight_high_densities=False,
            h_range=None):

        self.use_atom_weight = use_atom_weight
        self.normalization = normalization
        self.log_transform = log_transform
        self.highlight_high_densities = highlight_high_densities
        self.x_range = h_range[0]
        drho = h_range[1]

        # --------------------------------------------------------------------------
        # Load and prepocess data

        H, D, V = self.load_and_preprocess()

        # normalize to the number of frames (now we get density)
        # additional normalization

        # self.statistics(H, do_print=True)

        H = H / (self.num_frames * V * self.normalization)

        # self.statistics(H, do_print=True)

        v = np.min(V)
        fac = v * self.num_frames * normalization

        if self.use_atom_weight is True:
            H *= atom_weight(self.atom_type)
            fac *= atom_weight(self.atom_type)

        if self.log_transform is True:
            #H = np.log10(H - H.min() + 1)
            H = np.log10(H + 1)

        if self.log_transform is True:
            self.y_range = drho
        else:
            dH = [d * fac for d in drho]
            # mdH = np.max([1, np.round(np.mean(np.diff(dH)))])
            # dH = np.arange(np.floor(dH[0]) - 0.5, np.ceil(dH[-1]) + mdH, mdH)
            # print(dH)
            # print(np.diff(dH))
            # print(dH / fac)
            drho = dH[:drho.size] / fac
            self.y_range = drho

        # Crop data to analysis range
        H, D = self.crop_data(H, D)

        self.statistics(H, do_print=True)

        # --------------------------------------------------------------------------
        # Compute histogram

        W = 1 / (4 * np.pi * D**2)

        if self.highlight_high_densities is True:
            W *= np.sqrt(H)

        HH, xedges, yedges = np.histogram2d(D, H, bins=(self.x_range, self.y_range), weights=W)

        HH = HH.T  # Let each row list bins with common y range.

        # HH = np.log10(HH)

        self.HH = HH
        self.xedges = xedges
        self.yedges = yedges

        self.xcentres = (xedges[1:] + xedges[:-1]) / 2

        HHH, xedges = np.histogram(D, bins=self.x_range, weights=W)

        # HHH = np.log10(HHH)

        self.hdensity = H
        self.distances = D
        self.weights = W

        self.HHH = HHH

    def analyze_rdf(self, x_range=None):

        #self.x_range = x_range
        #self.y_range = None

        # --------------------------------------------------------------------------
        # Load and prepocess data

        H, D, V = self.load_and_preprocess()

        # Crop data to analysis range
        H, D = self.crop_data(H, D)

        # --------------------------------------------------------------------------
        # Compute radial distribution function and cumulative distribution function

        gr, rdf_edges = np.histogram(D, bins=self.x_range, weights=H, normed=False)

        r = (rdf_edges[1:] + rdf_edges[:-1]) / 2
        self.RDF_r = r

        R = rdf_edges[-1] - rdf_edges[0]
        N = r.size

        gr = gr * (N / self.num_frames) / R

        # non-normalized
        self.RDF_no_norm = gr

        # normalized
        self.RDF_norm = gr / (4 * np.pi * r**2)

        # cumulative
        self.CDF = np.cumsum(gr * np.diff(rdf_edges))

    @staticmethod
    def statistics(s, nan=False, do_print=False):

        if nan is True:
            max = np.nanmax(s)
            min = np.nanmin(s)
            mean = np.nanmean(s)
            med = np.nanmedian(s)
            std = np.nanstd(s)
        else:
            max = s.max()
            min = s.min()
            mean = s.mean()
            med = np.median(s)
            std = s.std()

        if do_print:
            print(f'\tmin = {min}\n\tmax = {max}\n\tmean = {mean}\n\tstd = {std}\n\tmed = {med}')

        return max, min, mean, med, std


def combine_density_analysis(arr, CONFIG, atom_type, ):

    CONFIG.centroid_id = None

    DA = DensityAnalysis(CONFIG, atom_type=atom_type)

    hdensity = []
    distances = []
    x_range = []
    y_range = []
    weights = []
    total_num_frames = 0
    DA.max_hdensity = []

    for da in arr:
        hdensity.append(da.hdensity)
        distances.append(da.distances)
        x_range.append(da.x_range)
        y_range.append(da.y_range)
        weights.append(da.weights * da.num_frames)
        total_num_frames += da.num_frames
        DA.max_hdensity.append(da.hdensity.max())
    
    print(f"Combining the following configurations:")
    for da in arr:
        try:
            print("OW: {num_ow}  CL: {num_cl}  = ({coord_num})".format(**da.CONFIG.configuration_in_shells)
                + f"   {100*da.num_frames/total_num_frames:.2f} %")
        except(KeyError):
            pass

    DA.HH, DA.xedges, DA.yedges = np.histogram2d(
            np.concatenate(distances, axis=0),
            np.concatenate(hdensity, axis=0),
            bins=(x_range[0], y_range[0]),
            weights=np.concatenate(weights, axis=0) / total_num_frames)

    DA.HH = DA.HH.T

    DA.HHH, xedges = np.histogram(
            np.concatenate(distances, axis=0),
            bins=x_range[0],
            weights=np.concatenate(weights, axis=0) / total_num_frames)

    

    DA.distances = distances
    DA.weights = weights

    return DA

def compare_density_analysis(DA1, DA2, figname=None, do_plot_rdf=True, log_color=False):

    DensityAnalysis.statistics(DA1.HH, do_print=True)
    DensityAnalysis.statistics(DA2.HH, do_print=True)

    if log_color is True:
        DA1.HH = np.log10(DA1.HH - DA1.HH.min() + 1)
        DA2.HH = np.log10(DA2.HH - DA2.HH.min() + 1)

    H = DA1.HH - DA2.HH

    # H = np.log10(DA1.HH - DA1.HH.min() + 1) - np.log10(DA2.HH - DA2.HH.min() + 1)

    # assert np.all(DA1.xedges == DA2.xedges)
    # assert np.all(DA1.yedges == DA2.yedges)

    xdiff = np.abs(DA1.xedges - DA2.xedges)
    ydiff = np.abs(DA1.yedges - DA2.yedges)

    print(f"difference between x-edges: {xdiff.mean()} (mean), {xdiff.max()} (max)")
    print(f"difference between y-edges: {ydiff.mean()} (mean), {ydiff.max()} (max)")

    tol = 1e-6
    if xdiff.max() > tol:
        raise(ValueError(f"xedges difference > tol ({xdiff.max()} > {tol})"))
    if ydiff.max() > tol:
        raise(ValueError(f"xedges difference > tol ({ydiff.max()} > {tol})"))

    xedges = DA1.xedges
    yedges = DA1.yedges

    xmids = 0.5*(xedges[1:] + xedges[:-1])
    ymids = 0.5*(yedges[1:] + yedges[:-1])

    #print(xedges)
    #print(yedges)

    # print(DA1.max_hdensity)
    # print(DA2.max_hdensity)

    plt.rc('text', usetex=True)

    fig, axs = plt.subplots(
            figsize=(12, 16),
            ncols=2, nrows=2,
            sharey=True,
            gridspec_kw={'width_ratios': [1, 1],
                         'height_ratios': [1, 2]})

    gs = axs[1, 0].get_gridspec()

    # remove the underlying axes
    for ax in axs[1, 0:]:
        ax.remove()

    axbig = fig.add_subplot(gs[1, :])

    vmin = min([DA1.HH.min(), DA2.HH.min()])
    vmax = max([DA1.HH.max(), DA2.HH.max()])

    DA1.plot(fig=fig, ax=axs[0, 0], vmin=vmin, vmax=vmax)
    ax = axs[0, 0].twinx()
    DA1.plot_density_stddev(fig=fig, ax=ax)

    DA2.plot(fig=fig, ax=axs[0, 1], vmin=vmin, vmax=vmax)
    ax = axs[0, 1].twinx()
    DA2.plot_density_stddev(fig=fig, ax=ax)

    m = np.max(np.abs(H))
    X, Y = np.meshgrid(xedges, yedges)

    DensityAnalysis.statistics(H, do_print=True)

    im = axbig.pcolormesh(X, Y, H, cmap=cmap, vmin=-m, vmax=m)

    try:
        cbar_label = f"{DA1.CONFIG.CATION}" + r" $-$ " + f"{DA2.CONFIG.CATION}"
    except(AttributeError):
        cbar_label = "TODO"# f"{DA1.CONFIG.config_ini}" + r" $-$ " + f"{DA2.CONFIG.config_ini}"
        pass

    fig.colorbar(im, ax=axbig, label=cbar_label)

    if do_plot_rdf is True:
        ax = axbig.twinx()

        xlim = [xedges[0], xedges[-1]]

        ylim1 = DA1.plot_cdf(fig=fig, ax=ax, xlim=xlim)
        ylim1 = DA1.plot_rdf(fig=fig, ax=ax, xlim=xlim)
        plt.xlim(xlim)

        ylim2 = DA2.plot_cdf(fig=fig, ax=ax, xlim=xlim)
        ylim2 = DA2.plot_rdf(fig=fig, ax=ax, xlim=xlim)
        plt.xlim(xlim)

        newylim = [np.min([ylim1[0], ylim2[0]]), np.max([ylim1[1], ylim2[1]])]
        dy = (newylim[1] - newylim[0])*0.05
        newylim = [newylim[0] - dy, newylim[1] + dy]
        plt.ylim(newylim)

        plt.ylabel(r"$\varrho_{" + f"{DA1.atom_type}" + f"{DA2.atom_type}" + "}$ (density) []")


    plt.xlabel("distance from center [Å]")

    fig.tight_layout()

    plt.show()

    if figname is not None:
        fig.savefig(figname + '_A.pdf', bbox_inches='tight')

    cc = plt.cm.get_cmap(cmap)([0.2, 0.8])

    if False:

        fig = plt.figure(figsize=(10.5, 3),)
        # plt.plot(DA1.xcentres, DA1.HHH)
        # plt.plot(DA2.xcentres, DA2.HHH)
        diff = DA1.HHH - DA2.HHH
        # plt.plot(DA1.xcentres, diff, '*')
        # plt.fill_between(DA1.xcentres, diff, where=diff>0, cmap=cmap)#color=cc[1])
        # plt.fill_between(DA1.xcentres, diff, where=diff<0, cmap=cmap)#color=cc[0])

        m = 2*np.max(abs(diff))
        cc = plt.cm.get_cmap(cmap)(0.5 + diff/m)

        for i, (d, c) in enumerate(zip(diff, cc)):
            if d > 0:
                plt.fill_between(DA1.xedges[i:i+2], [d,d], color=c)
            elif d < 0:
                plt.fill_between(DA1.xedges[i:i+2], [d,d], color=c)

        plt.xlim((DA1.xedges[0], DA1.xedges[-1]))

        try:
            y_label = f"{DA1.CONFIG.CATION}" + r" $-$ " + f"{DA2.CONFIG.CATION}"
        except(AttributeError):
            y_label = "TODO"# f"{DA1.CONFIG.config_ini}" + r" $-$ " + f"{DA2.CONFIG.config_ini}"
            pass
        plt.ylabel(y_label)
        plt.xlabel("distance from center [Å]")

        fig.tight_layout()

        plt.show()

        if figname is not None:
            fig.savefig(figname + '_B.pdf', bbox_inches='tight')


if __name__ == '__main__':

    normalization=1e-3

    h_range = {}

    # range['OW']=[np.linspace(1,8,200), np.linspace(0.0,16.0,200)]
    # range['HW']=[np.linspace(1,8,200), np.linspace(0.0,2.0,200)]
    # range['OW']=[np.linspace(1,8,200), np.linspace(0.0,20.0,200)]
    # range['HW']=[np.linspace(1,8,200), np.linspace(0.0,2.0,200)]
    h_range['OW']=[np.linspace(1,6,30), np.linspace(0,13,30)]
    # range['HW']=[np.linspace(1,6,200), np.linspace(0.1,200,200)]
    # range['OW']=[np.linspace(1,8,50), np.linspace(0.0,100.0,20)]
    # range['HW']=[np.linspace(1,8,200), np.linspace(0.0,20000.0,200)]

    # --------------------------------------------------------------------------

    STRIDE = 1
    CATION_1 = 'LI'

    # ARR = np.asarray(['*', 2, 2, '*', '*', '*', '*', '*'])
    # ARR = np.asarray(['*', 3, 1, '*', '*', '*', '*', '*'])
    ARR = np.asarray(['*', 4, 0, '*', '*', '*', '*', '*'])

    CONFIG = config.Config(
            CATION=CATION_1,
            stride=STRIDE,
            num_bins=(97, 99, 101,),
            # num_bins=(195, 197, 199,),
            # num_bins=(295, 297, 299,),
            hist_range=[(-9,9), (-9,9), (-9,9),],
            configuration_in_shells=ARR)

    # --------------------------------------------------------------------------

    DA1 = {}

    for atom_type in ['OW']:#, 'HW']:
        DA1[atom_type] = DensityAnalysis(
                CONFIG,
                use_atom_weight=False,
                normalization=normalization,
                h_range=h_range[atom_type],
                atom_type=atom_type)
        DA1[atom_type].analyze()
        DA1[atom_type].plot()

    # --------------------------------------------------------------------------

    STRIDE = 1
    # CATION_2 = 'LI'
    CATION_2 = 'NA'

    # ARR = np.asarray(['*', 2, 2, '*', '*', '*', '*', '*'])
    # ARR = np.asarray(['*', 5, 0, '*', '*', '*', '*', '*'])
    ARR = np.asarray(['*', 4, 0, '*', '*', '*', '*', '*'])

    CONFIG = config.Config(
            CATION=CATION_2,
            stride=STRIDE,
            num_bins=(97, 99, 101,),
            # num_bins=(195, 197, 199,),
            # num_bins=(295, 297, 299,),
            hist_range=[(-9,9), (-9,9), (-9,9),],
            configuration_in_shells=ARR)

    # --------------------------------------------------------------------------

    DA2 = {}

    for atom_type in ['OW']:#, 'HW']:
        DA2[atom_type] = DensityAnalysis(
                CONFIG,
                use_atom_weight=False,
                normalization=normalization,
                h_range=h_range[atom_type],
                atom_type=atom_type)
        DA2[atom_type].analyze()
        DA2[atom_type].plot()

    # --------------------------------------------------------------------------

    for atom_type in ['OW']:#, 'HW']:
        compare_density_analysis(DA1[atom_type], DA2[atom_type])

    # --------------------------------------------------------------------------

    # common_range = [np.linspace(1,8,200), np.linspace(0.0,10.0,200)]
    # common_range = [np.linspace(1,8,200), np.linspace(0,20,200)]
    common_range = [np.linspace(1,8,30), np.linspace(0.1,500,30)]

    DA1['ALL'] = DensityAnalysis(
            CONFIG, normalization=normalization, h_range=h_range['OW'], atom_type='ALL')
    DA2['ALL'] = DensityAnalysis(
            CONFIG, normalization=normalization, h_range=h_range['OW'], atom_type='ALL')

    for atom_type in ['OW', 'HW']:
        DA1[atom_type].x_range = common_range[0]
        DA1[atom_type].y_range = common_range[1]
        DA1[atom_type].analyze()

        DA2[atom_type].x_range = common_range[0]
        DA2[atom_type].y_range = common_range[1]
        DA2[atom_type].analyze()

    DA1['ALL'].HH = DA1['OW'].HH + DA1['HW'].HH
    DA2['ALL'].HH = DA2['OW'].HH + DA2['HW'].HH

    assert np.all(DA1['OW'].xedges == DA1['HW'].xedges)
    assert np.all(DA1['OW'].yedges == DA1['HW'].yedges)

    DA1['ALL'].xedges = DA1['OW'].xedges
    DA1['ALL'].yedges = DA1['OW'].yedges

    assert np.all(DA2['OW'].xedges == DA2['HW'].xedges)
    assert np.all(DA2['OW'].yedges == DA2['HW'].yedges)

    DA2['ALL'].xedges = DA2['OW'].xedges
    DA2['ALL'].yedges = DA2['OW'].yedges

    compare_density_analysis(DA1['ALL'], DA2['ALL'])
