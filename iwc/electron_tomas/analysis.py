# coding: utf-8
import numpy as np
import argparse
from pathlib import Path
import iwc.electron_tomas.neighborhoods_electron as neighborhoods
import iwc.hydration_shells as hydration_shells
import iwc.shell_distances as shell_distances
import iwc.clustering as clustering
import iwc.histograms as histograms
import iwc.aligned_data_analyser as aligned_data_analyser
from iwc.common.common import logger, configuration_generator, encode_labels, decode_labels
from iwc.tma_phil.config_tma import Config

"""
conda activate spsalign-tma

conda install -c conda-forge numpy scipy numba tqdm joblib MDAnalysis h5py coloredlogs verboselogs


python iwc/electron_tomas/analysis.py --do-preprocess --ini-file=config_electron.ini
python iwc/electron_tomas/analysis.py --do-align--ini-file=config_electron.ini
python iwc/electron_tomas/analysis.py --do-separate-by-aligning --ini-file=config_electron.ini
python iwc/electron_tomas/analysis.py --do-histograms --ini-file=config_electron.ini

"""

parser = argparse.ArgumentParser()

parser.add_argument("--ini-file", type=str, default='config_electron.ini',
                    help="")

parser.add_argument("--n-cores", type=int, default=1,
                    help="")

parser.add_argument("--stride", type=int, default=1,
                    help="")

parser.add_argument("--nhood-distance", type=int, default=10,
                    help="")

parser.add_argument("--do-preprocess", action="store_true",
                    help="")

parser.add_argument("--do-align", action="store_true",
                    help="")

parser.add_argument("--do-search-centroids", action="store_true",
                    help="")

parser.add_argument("--do-separate-by-aligning", action="store_true",
                    help="")

parser.add_argument("--do-histograms", action="store_true",
                    help="")

parser.add_argument("--num-bins", type=int, default=100,
                    help="")

parser.add_argument("--overwrite-parent-dir", type=str, default=None,
                    help="")

args = parser.parse_args()

configuration_in_shells = {
    'name': 'electron',
    'num_atoms': 10,
}


# -----------------------------------------------------------------------------

CONFIG = Config(
    name='electron',
    n_cores=args.n_cores,
    config_ini=args.ini_file,
    stride=args.stride,
    aligning_method='num_atoms',
    nhood_distance=args.nhood_distance,
    hist_range=[(-args.nhood_distance, args.nhood_distance) for i in [1, 2, 3]],
    num_bins=(args.num_bins, args.num_bins, args.num_bins,),
    overwrite_parent_dir=args.overwrite_parent_dir,
    configuration_in_shells=configuration_in_shells)
CONFIG.setup_output_files()

# -----------------------------------------------------------------------------


if args.do_preprocess is True:

    CONFIG.folder_neighborhoods.mkdir(parents=False, exist_ok=True)
    neighborhoods.main(
        CONFIG.folder_neighborhoods, 
        CONFIG.topology_file, 
        CONFIG.trajectory_file, 
        stride=CONFIG.stride, 
        nhood_distance=CONFIG.nhood_distance)


# -----------------------------------------------------------------------------

nums = [
    {'N': 5, 'H': 0},
    {'N': 6, 'H': 0},
    {'N': 7, 'H': 0},
    {'N': 8, 'H': 0},
    {'N': 9, 'H': 0},
]


if args.do_align:

    for i, num in enumerate(nums):

        num_data = clustering.get_data_size(CONFIG.folder_neighborhoods)
        ind = np.arange(0, num_data)

        positions, labels = clustering.load_data(CONFIG.folder_neighborhoods, ind, chunk_size=int(1e4))

        pos = {}
        lab = {}

        for atom_type in num:
            pos[atom_type] = positions[0][labels[0] == encode_labels(atom_type)]
            lab[atom_type] = labels[0][labels[0] == encode_labels(atom_type)]

        ref_pos = np.concatenate([pos[atom_type][:num[atom_type]] for atom_type in num])
        ref_lab = np.concatenate([lab[atom_type][:num[atom_type]] for atom_type in num])

        conf = {
            'description': f'c{i}',
            'exclude_atom_types': ['N', 'H'], # ??
            'num_atoms': ref_lab.size,
            'ref_pos': ref_pos,
            'ref_lab': ref_lab,
            'outfile': None,
            'outfile_name': str(CONFIG.analysis_dir / f'c{i}.npz'),
        }

        configurations = []

        configurations.append(conf)

        print(conf)

    centroids, centroids_labs = clustering.separate_by_aligning_core_centroids(
            configurations, positions, labels, n_cores=args.n_cores, 
            force_non_heuristic_alignment=False, full_shell_check=False)

    # print(centroids)
    # print(centroids_labs)

    CONFIG.folder_centroids.mkdir(parents=False, exist_ok=True)


    for centroid, centroid_lab in zip(centroids, centroids_labs):
        
        CONFIG.centroid_id = 0
        CONFIG.setup_output_files()

        np.savez(CONFIG.file_centroid,
            centroid_id=CONFIG.centroid_id,
            configuration_in_shells=configuration_in_shells,
            centroid=np.array(centroid, dtype='float32'),
            centroid_lab=centroid_lab)

# -----------------------------------------------------------------------------


if args.do_search_centroids is True:
    ind = np.arange(400)
    for num_atoms in range(6,11):
        centroids, centroids_labs, centroid_statistics = \
            clustering.find_new_configurations_alt(CONFIG, ind, num_atoms, ntries=10)

        print(centroids)
        print(centroids_labs)
        print(centroid_statistics)

        for i, (centroid, centroid_lab) in enumerate(zip(centroids, centroids_labs)):
                
            CONFIG.centroid_id = i
            CONFIG.setup_output_files()

            np.savez(CONFIG.file_centroid,
                centroid_id=CONFIG.centroid_id,
                configuration_in_shells=configuration_in_shells,
                centroid=np.array(centroid, dtype='float32'),
                centroid_lab=centroid_lab)


# -----------------------------------------------------------------------------


if args.do_separate_by_aligning is True:

    clustering.separate_by_aligning(CONFIG, centroid_method='folder',
            save_extended_shell_configuration=False,
            align_kwargs={'force_non_heuristic_alignment':False, 'full_shell_check':False})

# -----------------------------------------------------------------------------


if args.do_histograms is True:
    for i in range(11):
        CONFIG.centroid_id = i
        histograms.main_wrapper(CONFIG)