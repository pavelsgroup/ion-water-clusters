# coding: utf-8

# from IPython import get_ipython
# if get_ipython() is not None:
#     get_ipython().run_line_magic('load_ext', 'autoreload')
#     get_ipython().run_line_magic('autoreload', '2')


import numpy as np
import re
from iwc.cluster_plot_3d import ShellPlot3dGenerator

import argparse

parser = argparse.ArgumentParser()

parser.add_argument("--ini-file", type=str, default='config_electron.ini',
                    help="")

parser.add_argument("--centroid-id", type=int, default=0,
                    help="")

parser.add_argument("--stride", type=int, default=1,
                    help="")

parser.add_argument("--num-bins", type=int, default=100,
                    help="")

"""
conda activate spsalign-mayavi

conda install -c conda-forge numpy scipy matplotlib tqdm joblib pandas seaborn numba h5py MDAnalysis coloredlogs verboselogs mayavi
"""

args = parser.parse_args()

# ------------------------------------------------------------------------------
# CONFIGURATION

configuration_in_shells = {
    'name': 'electron',
    'num_atoms': 10,
}

from iwc.tma_phil.config_tma import Config

if __name__ == '__main__':

    description = args.ini_file
    description = re.sub(r'_', R' ', description)
    description = re.sub(r'\.ini', R'', description)
    description = re.sub(r'config', R' ', description)
    description = re.sub(r'tma', R' ', description)
    description = re.sub(r'\s+', R' ', description)
    description = description.strip()

    CONFIG = Config(
        name='electron',
        description=description,
        config_ini=args.ini_file,
        stride=args.stride,
        hist_range=[(-10,10), (-10,10), (-10,10),],
        num_bins=(args.num_bins, args.num_bins, args.num_bins,),
        nhood_distance=10, # Angstrom
        aligning_method='num_atoms',
        centroid_id=args.centroid_id,
        configuration_in_shells=configuration_in_shells,
        )

    #print(CONFIG.description)
    #raise(ValueError('A'))

    exclude_atom_types = [
        # 'HW',
        # 'CL',
        # 'OW',
        # 'NA',
        # 'K',
        # 'LI',
        ]

    volume_range = { # in multiples of mean
        'H': (3, 3.1),
        'HW': (3, 3.1),
        'OW': (1.2, 1.5),
        'C': (3, 3.1),
        'N': (3, 3.1),
        'CL': (3, 3.1),
    }

    L = 0.0 # low level of opacity
    H = 0.0 # high level of opacity
    S = 0.2 # slider level of opacity

    contour_levels = { # in multiples of mean
        # 'label': [[level, opacity], ...]
        'H':  [[1.5, L], [5.0, H], [4.0, S]],
        'HW': [[1.5, L], [5.0, H], [2.2, S]],
        'OW': [[2.5, L], [2.9, H], [2.7, S]],
        'C':  [[1.5, L], [5.0, H], [3.0, S]],
        'N':  [[1.5, L], [5.0, H], [4.0, S]],
        'CL': [[1.5, L], [5.0, H], [4.0, S]],
    }

    contour_range = { # percent
        'H': 10,
        'HW': 10,
        'OW': 10,
        'C': 10,
        'N': 10,
        'CL': 10,
    }

    num_extra_contours = {
        'H':  0,
        'HW': 0,
        'OW': 0,
        'C':  0,
        'N':  0,
        'CL': 0,
    }

    for atom_type in num_extra_contours:
        s = contour_levels[atom_type][2][1]
        contour_levels[atom_type][2][1] = s / (1 + num_extra_contours[atom_type])

    # plot_type = 'volume'
    plot_type = 'contour'

    plot_object = ['density']
    # plot_object = ['void']
    # plot_object = ['void', 'density']

    void_atom_types = ['OW']
    # void_atom_types = ['all']

    separate_atom_types = True
    # separate_atom_types = False

    ShellPlot3d = ShellPlot3dGenerator(
        atom_list=['H', 'N'],
        atom_slider_range={
            'H': [0.01, 5., 2.,],
            'N': [0.01, 50., 2.,],
        },
        )

    shellplot = ShellPlot3d(CONFIG,
        normalization=1e-3,
        plot_type=plot_type,
        exclude_atom_types=exclude_atom_types,
        separate_atom_types=separate_atom_types,
        void_atom_types=void_atom_types,
        volume_range=volume_range,
        contour_range=contour_range,
        num_contours=num_extra_contours,
        contour_levels=contour_levels)
    shellplot.main(plot_object=plot_object)
    if 'void' in plot_object and 'density' in plot_object:
        shellplot.configure_traits(view='view_all')
    elif 'void' in plot_object:
        shellplot.configure_traits(view='view_voids')
    elif 'density' in plot_object:
        shellplot.configure_traits(view='view_general')
