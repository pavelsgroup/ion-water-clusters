# coding: utf-8

import os
import numpy as np
import h5py
import MDAnalysis as mda
# from MDAnalysis.coordinates.memory import MemoryReader
# from MDAnalysis.analysis.distances import distance_array
from scipy.spatial import cKDTree
# from MDAnalysis.analysis import align
import time
from tqdm import tqdm
# from joblib import Parallel, delayed
from pathlib import Path
import configparser
from numba import jit

from IPython import get_ipython
if get_ipython() is not None:
    get_ipython().run_line_magic('load_ext', 'autoreload')
    get_ipython().run_line_magic('autoreload', '2')

    # get_ipython().run_line_magic('matplotlib', 'inline')
    # matplotlib.use('Qt5Agg')
  
from iwc.common.common import encode_labels, decode_labels, notqdm
import iwc.config as config

disable_tqdm = bool(os.environ.get("DISABLE_TQDM"))
if disable_tqdm is True:
    print("encountered environment variable DISABLE_TQDM, disabling tqdm")
    tqdm = notqdm


"""
conda install -c conda-forge numpy scipy tqdm joblib MDAnalysis h5py



Input:
    .tpr and .xtc file of the simulation of LiCl solution.
    Number of Li ... 187
    Number of Cl ... 187
    Number of frame ... 15302

Output:
    List: count_ato_allframe ... list of 3d np arrays
        dim 0: number of frames in the simulation
        dim 1: number of Li in the box
        dim 2: number of occurrences of each type of atoms in the following order (Li, O, Cl, H)
    list: list11H_label_allframe ... list of 3d np array
        dim 0: number of frames of simulation.
        dim 1: number of Li in the box.
        dim 2: name of all the atoms in every cluster
    list: list11H_pos_allframe ... list of 4d np array
        dim 0: number of frames of simulation.
        dim 1: number of Li in the box.
        dim 2: number of atoms in each cluster.
        dim 3: coordinates (x,y,z) of the atoms

"""

# ------------------------------------------------------------------------------


def rename_labels(labels, HW, HWs):
    for lab in labels:
        for h in HWs:
            lab[lab == h] = HW
    return labels


def reorder_dataset(positions, labels, atom_type_order, remove_closest=False):

    for i, (pos, lab) in enumerate(zip(positions, labels)):
        assert pos.shape[1] == 3
        assert pos.shape[0] == lab.shape[0]

        # first order the array by distances from center
        ind = np.argsort(np.linalg.norm(pos, axis=1), kind='heapsort')

        if remove_closest:
            ind = ind[1:]

        pos = pos[ind, :]
        lab = lab[ind]

        # then shuffle
        ind = np.concatenate([np.where(lab == atom_type) for atom_type in atom_type_order], axis=1)

        ind = ind[0]

        pos = pos[ind, :]
        lab = lab[ind]

        positions[i] = pos
        labels[i] = lab

    return positions, labels


def transform(positions, labels):

    m = np.mean(positions[labels == 'C'], axis=0)

    assert(m.size == 3)
    
    return positions - m, labels


def main(wrk_dir, pos_file, stride=1):

    print(f"Analysing file\n{pos_file}\nwith stride {stride}")

    u = mda.Universe(str(pos_file))

    atoms_selection = 'name {}'.format(' or name '.join(['C', 'H', 'O', 'N']))

    ele = u.select_atoms(atoms_selection)

    n_frames = int(np.floor(u.trajectory.n_frames / stride))
    chunk_size = int(np.floor(n_frames / 10))

    wrk_dir.mkdir(parents=False, exist_ok=True)

    with h5py.File(wrk_dir / "neighborhoods.h5", 'w') as f:

        f_positions = f.create_dataset(
            'positions', (n_frames,), dtype=h5py.special_dtype(vlen=np.dtype('float32')))

        f_labels = f.create_dataset(
            'labels', (n_frames,), dtype=h5py.special_dtype(vlen=np.dtype('uint8')))


        for m, ts in tqdm(enumerate(u.trajectory[::stride]), total=n_frames):

            # box = u.dimensions
            # ele.pack_into_box(box=box)

            pos, lab = transform(ele.positions, ele.names)

            positions = []
            labels = []

            positions.append(pos)
            labels.append(lab)

            # ------------------------------------------------------------------

            k1 = m
            k2 = m + 1

            atom_type_order = ['C', 'H', 'O', 'N']

            positions, labels = reorder_dataset(positions, labels, atom_type_order, remove_closest=False) # remove closest = remove centered atom

            for i, (pos, lab) in enumerate(zip(positions, labels)):
                positions[i] = np.ravel(pos)
                labels[i] = np.asarray(encode_labels(lab), dtype='uint8')

            f_positions[k1:k2] = positions
            f_labels[k1:k2] = labels

        # print(positions)
        # print(labels)
        # print(decode_labels(labels[0], possible_labels=['C', 'H', 'O', 'N']))


if __name__ == '__main__':

    data_dir = Path('/wrk/krystof/birch_reduction/H2O_C6H6_AIMD/S001/S001-prod/S001-01/production_run')
    pos_file = data_dir / 'S001-01-production-pos-1.xyz'
    wrk_dir = Path('/wrk/krystof/analysis')

    main(wrk_dir, pos_file)
