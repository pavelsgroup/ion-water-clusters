import re
import numpy as np
from pathlib import Path
from hashlib import blake2s
import pprint
import configparser

class Config():

    # --------------------------------------------------------------------------
    # CUSTOM SETUP OF SOURCE FILES

    def setup_simulation_sources(self):

        config = configparser.ConfigParser()
        config.read(self.config_ini)

        # allow to overwrite parrent dir
        if self.overwrite_parent_dir:
            parent_dir = Path(self.overwrite_parent_dir)
        else:
            parent_dir = Path(config['global']['parent_dir'])
        

        self.data_dir = parent_dir / config['global']['data_dir']
        self.analysis_dir = parent_dir / config['global']['analysis_dir']
        
        self.pos_file = self.data_dir / config['global']['pos_file']
                
        self.analysis_dir.mkdir(parents=False, exist_ok=True)        

        self.SRC_FOLDER = self.analysis_dir

    # --------------------------------------------------------------------------

    def __init__(self,
            config_ini='config_benzene.ini',
            name='benzene',
            n_cores=1,
            centroid_id=None,
            stride=1,
            num_neighbours=None,
            nhood_distance=10,
            num_bins=(100, 100, 100,),
            hist_range='auto',
            # hist_range=[(-9,9), (-9,9), (-9,9), ],
            aligning_method=None, 
            overwrite_parent_dir=None,
            configuration_in_shells=None):
        """
        """

        self.overwrite_parent_dir = overwrite_parent_dir

        self.config_ini = config_ini

        self.name = name

        self.centroid_id = centroid_id

        self.process_id = None
        self.data_range = None

        # assert n_cores is not None
        self.n_cores = n_cores

        self.exclude_atoms_centroid = []

        assert num_neighbours is None or isinstance(num_neighbours, int)
        self.num_neighbours = num_neighbours

        assert nhood_distance is None or isinstance(nhood_distance, int)
        self.nhood_distance = nhood_distance

        assert isinstance(stride, int)
        self.stride = stride

        # alligning method
        assert aligning_method is None or isinstance(aligning_method, str)
        assert aligning_method in [None, 'num_atoms', 'distance_intervals']
        self.aligning_method = aligning_method

        # configuration of solvation shells
        # if aligning_method == 'num_atoms':
        #    assert isinstance(configuration_in_shells, dict)
        #
        # elif aligning_method == 'distance_intervals':
        #    assert isinstance(configuration_in_shells, np.ndarray)

        self.configuration_in_shells = configuration_in_shells
        self.cfg_short = '{name}'.format(**self.configuration_in_shells)
        self.description = '{name}'.format(**self.configuration_in_shells)

        # number of histogram bins in each dimension
        assert isinstance(num_bins, tuple) or isinstance(num_bins, list)
        for n in num_bins:
            assert isinstance(n, int)
        self.num_bins = tuple(num_bins)


        # range of the 3d histogram
        assert isinstance(hist_range, str) or isinstance(hist_range, list)
        if isinstance(hist_range, list):
            for r in hist_range:
                assert isinstance(r, tuple)
                assert len(r) == 2
                assert isinstance(r[0], int)
                assert isinstance(r[1], int)
                assert r[0] < r[1]
        self.hist_range = hist_range

        # check some more parameter consistence
        self.check_parameter_consistence()

        # setup soruce files and output files
        # self.setup_simulation_sources(parent_dir='/dev/shm/iwc/')
        self.setup_simulation_sources()
        self.setup_output_files()


    def check_parameter_consistence(self):

        if (((self.nhood_distance is None) and (self.num_neighbours is None))
            or (self.nhood_distance is not None) and (self.num_neighbours is not None)):
            raise("Exactly one of the parameters self.nhood_distance and self.num_neighbours must be specified")

        if self.num_neighbours is not None:
            self.neighborhood_method = 'num_neighbours'
        elif self.nhood_distance is not None:
            self.neighborhood_method = 'distance'


    def setup_output_files(self):

        cwd = self.analysis_dir

        if self.neighborhood_method == 'num_neighbours':
            fspec = f"{self.name}-N{self.num_neighbours}-{self.stride}"

        elif self.neighborhood_method == 'distance':
            fspec = f"{self.name}-D{self.nhood_distance}-{self.stride}"

        self.folder_neighborhoods = cwd / f"neighborhoods-{fspec}"
        self.folder_hyd_shells = cwd / f"hyd_shells-{fspec}"
        self.folder_histograms = cwd / f"histograms-{fspec}"
        self.folder_aligning = cwd / f"aligned-{fspec}"
        self.folder_centroids = cwd / f"centroids-{fspec}"

        if self.aligning_method is not None and self.configuration_in_shells is not None:

            align_hash = self.get_align_hash()
            hist_hash = self.hash_hist_configuration(self.num_bins, self.hist_range)

            self.file_histogram = self.folder_histograms / f"{align_hash}-{hist_hash}.npz"
            self.file_distance_interval = self.folder_aligning / f"{align_hash}.npz"
            self.file_aligned = self.folder_aligning / f"{align_hash}.h5"
            self.file_aligned_metadata = self.folder_aligning / f"{align_hash}.meta.npz"
            self.file_centroid = self.folder_centroids / f"centroid-{align_hash}.npz"
            self.file_centroid_perturbed = self.folder_centroids / f"centroid_perturbed-{align_hash}.npz"

            self.file_configuration = self.folder_aligning / f'configurations-{self.get_data_per_partes_id()}.npy'

    def search_for_aligned_files(self, centroid_id):

        parent = self.file_aligned.parent
        stem = self.file_aligned.stem
        suffix =  self.file_aligned.suffix

        glob_string = stem + "_*-*_pid*" + suffix

        pid = []
        start_index = []
        end_index = []

        print(f"Looking for {parent}/{glob_string}")

        for f in parent.glob(glob_string):

            name = str(f.stem)

            m = re.match(stem + r"_([0-9]+)-([0-9]+)_pid([0-9]+)", name)
            if not m:
                continue

            start_index.append(int(m.group(1)))
            end_index.append(int(m.group(2)))
            pid.append(int(m.group(3)))
            
        pid = np.asarray(pid)
        start_index = np.asarray(start_index)
        end_index = np.asarray(end_index)

        I = np.argsort(start_index)
        J = np.argsort(end_index)

        if not np.all(I == J):
            raise ValueError(f"{start_index}, {end_index}")

        if not np.all(start_index[I][1:] == end_index[I][:-1]):
            raise ValueError(f"{start_index}, {end_index}")

        file_info = []
        for i in I:
            file_info.append({
                'data_range': [start_index[i], end_index[i]],
                'process_id': pid[i],})

        return file_info


    def __str__(self):
        return pprint.pformat(self.__dict__, indent=4)

    def get_data_per_partes_id(self):

        string = ""

        if self.data_range is not None:
            string += f"_{self.data_range[0]}-{self.data_range[-1]}"

        if self.process_id is not None:
            string += f"_pid{self.process_id}"

        return string

    def get_align_hash(self):

        align_hash = self.hash_configration(self.configuration_in_shells)

        if self.centroid_id is not None:
            align_hash += f"_{self.centroid_id}"
        
        align_hash += self.get_data_per_partes_id()

        return align_hash

    def hash_configration(self, conf):
        h = blake2s(digest_size=4)
        if type(conf) == dict:
            # print(("-".join(conf.keys())).encode('utf-8'))
            # print(("-".join(str(conf.values()))).encode('utf-8'))
            h.update(("-".join(conf.keys())).encode('utf-8'))
            h.update(("-".join(str(conf.values()))).encode('utf-8'))
        else:
            h.update(("-".join(conf)).encode('utf-8'))
        return h.hexdigest()


    def hash_hist_configuration(self, num_bins, range):
        h = blake2s(digest_size=4)
        h.update(f"{num_bins}".encode('utf-8'))
        h.update(f"{range}".encode('utf-8'))
        return h.hexdigest()
