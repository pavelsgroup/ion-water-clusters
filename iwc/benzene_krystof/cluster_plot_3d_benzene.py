# coding: utf-8

from IPython import get_ipython
if get_ipython() is not None:
    get_ipython().run_line_magic('load_ext', 'autoreload')
    get_ipython().run_line_magic('autoreload', '2')


import numpy as np
from cluster_plot_3d import ShellPlot3d

import argparse

parser = argparse.ArgumentParser()

parser.add_argument("--centroid-id", type=int, default=None,
                    help="")

parser.add_argument("--stride", type=int, default=1,
                    help="")

parser.add_argument("--num-bins", type=int, default=100,
                    help="")

"""
"""

args = parser.parse_args()

# ------------------------------------------------------------------------------
# CONFIGURATION

configuration_in_shells = {
    'name': 'benzene',
    'num_atoms': 6,
}

from iwc.config_krystof import Config

if __name__ == '__main__':

    CONFIG = Config(
        # config_ini='config_benzene_water.ini',
        # config_ini='config_benzene_amonia.ini',
        config_ini='config_benzene_anion_amonia.ini',
        stride=args.stride,
        hist_range=[(-10,10), (-10,10), (-10,10),],
        num_bins=(args.num_bins, args.num_bins, args.num_bins,),
        nhood_distance=10, # Angstrom
        aligning_method='num_atoms',
        centroid_id=args.centroid_id,
        configuration_in_shells=configuration_in_shells,
        )

    exclude_atom_types = [
        # 'HW',
        # 'CL',
        # 'OW',
        # 'NA',
        # 'K',
        # 'LI',
        ]

    L = 0.0 # low level of opacity
    H = 0.0 # high level of opacity
    S = 0.1 # slider level of opacity

    volume_range = { # in multiples of mean
        'N': (2, 10),
        'C': (2, 10),
        'O': (2, 10),
        'H': (2, 10),
    }

    contour_levels = { # in multiples of mean
        'N': [(2.5, L), (5, H), (10, S)],
        'C': [(1.5, L), (10, H), (20, S)],
        'O': [(2.5, L), (20, H), (40, 2*S)],
        'H': [(1.5, L), (5, H), (10, S)],
    }

    # plot_type = 'volume'
    plot_type = 'contour'

    plot_object = ['density']
    # plot_object = ['void']
    # plot_object = ['void', 'density']

    void_atom_types = ['OW']
    # void_atom_types = ['all']

    separate_atom_types = True
    # separate_atom_types = False

    shellplot = ShellPlot3d(CONFIG,
        normalization=1e-3,
        plot_type=plot_type,
        exclude_atom_types=exclude_atom_types,
        separate_atom_types=separate_atom_types,
        void_atom_types=void_atom_types,
        volume_range=volume_range,
        contour_levels=contour_levels)
    shellplot.main(plot_object=plot_object)
    if 'void' in plot_object and 'density' in plot_object:
        shellplot.configure_traits(view='view_all')
    elif 'void' in plot_object:
        shellplot.configure_traits(view='view_voids')
    elif 'density' in plot_object:
        shellplot.configure_traits(view='view_general')
