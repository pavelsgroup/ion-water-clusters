# coding: utf-8
import numpy as np
import argparse
from pathlib import Path
import iwc.config as config
import iwc.neighborhoods as neighborhoods
import iwc.hydration_shells as hydration_shells
import iwc.shell_distances as shell_distances
import iwc.clustering as clustering
import iwc.histograms as histograms
import iwc.aligned_data_analyser as aligned_data_analyser
import iwc.preprocess_trajectory as preprocess_trajectory
from iwc.common.common import logger, configuration_generator, encode_labels, decode_labels
from iwc.config_krystof import Config

parser = argparse.ArgumentParser()

parser.add_argument("--ini-file", type=str, default='config_benzene_water.ini',
                    help="")

parser.add_argument("--n-cores", type=int, default=1,
                    help="")

parser.add_argument("--stride", type=int, default=1,
                    help="")

parser.add_argument("--nhood-distance", type=int, default=10,
                    help="")

parser.add_argument("--do-preprocess", action="store_true",
                    help="")

parser.add_argument("--do-search-centroids", action="store_true",
                    help="")

parser.add_argument("--do-separate-by-aligning", action="store_true",
                    help="")

parser.add_argument("--do-histograms", action="store_true",
                    help="")

parser.add_argument("--num-bins", type=int, default=100,
                    help="")

parser.add_argument("--overwrite-parent-dir", type=str, default=None,
                    help="")

args = parser.parse_args()

configuration_in_shells = {
    'name': 'benzene',
    'num_atoms': 6,
}


# -----------------------------------------------------------------------------

CONFIG = Config(
    n_cores=args.n_cores,
    # config_ini='config_benzene_water.ini',
    # config_ini='config_benzene_amonia.ini',
    # config_ini='config_benzene_anion_amonia.ini',
    config_ini=args.ini_file,
    stride=args.stride,
    aligning_method='num_atoms',
    nhood_distance=args.nhood_distance,
    hist_range=[(-args.nhood_distance, args.nhood_distance) for i in [1, 2, 3]],
    num_bins=(args.num_bins, args.num_bins, args.num_bins,),
    overwrite_parent_dir=args.overwrite_parent_dir,
    configuration_in_shells=configuration_in_shells)
CONFIG.setup_output_files()

# -----------------------------------------------------------------------------


if args.do_preprocess is True:

    CONFIG.folder_neighborhoods.mkdir(parents=False, exist_ok=True)
    preprocess_trajectory.main(CONFIG.folder_neighborhoods, CONFIG.pos_file, CONFIG.stride)


# -----------------------------------------------------------------------------


if True:
    num_data = clustering.get_data_size(CONFIG.folder_neighborhoods)
    ind = np.arange(0, num_data)

    positions, labels = clustering.load_data(CONFIG.folder_neighborhoods, ind, chunk_size=int(1e4))

    p1 = positions[0][labels[0] == encode_labels('C')]
    l1 = labels[0][labels[0] == encode_labels('C')]

    p2 = positions[0][labels[0] == encode_labels('H')]
    l2 = labels[0][labels[0] == encode_labels('H')]

    ref_pos = np.concatenate([p1[:6], p2[:6]])
    ref_lab = np.concatenate([l1[:6], l2[:6]])

    conf = {
        'description': 'c0',
        'exclude_atom_types': ['C', 'H'],
        'num_atoms': ref_lab.size,
        'ref_pos': ref_pos,
        'ref_lab': ref_lab,
        'outfile': None,
        'outfile_name': str(CONFIG.analysis_dir / 'c0.npz'),
    }

    configurations = []

    configurations.append(conf)

    print(conf)

    centroids, centroids_labs = clustering.separate_by_aligning_core_centroids(
            configurations, positions, labels, n_cores=args.n_cores, 
            force_non_heuristic_alignment=True, full_shell_check=False)

    # print(centroids)
    # print(centroids_labs)

    CONFIG.folder_centroids.mkdir(parents=False, exist_ok=True)


    for centroid, centroid_lab in zip(centroids, centroids_labs):
        
        CONFIG.centroid_id = 0
        CONFIG.setup_output_files()

        np.savez(CONFIG.file_centroid,
            centroid_id=CONFIG.centroid_id,
            configuration_in_shells=configuration_in_shells,
            centroid=np.array(centroid, dtype='float32'),
            centroid_lab=centroid_lab)

# -----------------------------------------------------------------------------


if args.do_search_centroids is True:
    ind = np.arange(1000)
    num_atoms = 6
    centroids, centroids_labs, centroid_statistics = \
        clustering.find_new_configurations_alt(CONFIG, ind, num_atoms, ntries=10)

    print(centroids)
    print(centroids_labs)
    print(centroid_statistics)

    for i, (centroid, centroid_lab) in enumerate(zip(centroids, centroids_labs)):
            
        CONFIG.centroid_id = i
        CONFIG.setup_output_files()

        np.savez(CONFIG.file_centroid,
            centroid_id=CONFIG.centroid_id,
            configuration_in_shells=configuration_in_shells,
            centroid=np.array(centroid, dtype='float32'),
            centroid_lab=centroid_lab)


# -----------------------------------------------------------------------------


if args.do_separate_by_aligning is True:

    clustering.separate_by_aligning(CONFIG, centroid_method='folder',
            save_extended_shell_configuration=False,
            align_kwargs={'force_non_heuristic_alignment':True, 'full_shell_check':False})

# -----------------------------------------------------------------------------


if args.do_histograms is True:

    histograms.main_wrapper(CONFIG)