# coding: utf-8
import os
import numpy as np
import h5py
import scipy
import matplotlib
import time
import tqdm
# from joblib import Parallel, delayed
from pathlib import Path
import pprint
from copy import copy, deepcopy

from iwc.common.common_numba import is_sorted, is_all_zero

# from IPython import get_ipython
# if get_ipython() is not None:
#     get_ipython().run_line_magic('load_ext', 'autoreload')
#     get_ipython().run_line_magic('autoreload', '2')

#     get_ipython().run_line_magic('matplotlib', 'inline')
#     # matplotlib.use('Qt5Agg')

import matplotlib.pyplot as plt

from iwc.common.common import logger, notqdm, disp_title, timer, split, chunk_loader, encode_labels, decode_labels

try:
    from joblib import Parallel, delayed
except ModuleNotFoundError:
    # ImportWarning
    logger.warning("Module joblib not found, parallel processing will be disabled")

import iwc.config as config

disable_tqdm = bool(os.environ.get("DISABLE_TQDM"))
if disable_tqdm is True:
    print("encountered environment variable DISABLE_TQDM, disabling tqdm")
    tqdm = notqdm

pp = pprint.PrettyPrinter(indent=4)

"""

conda install -c conda-forge numpy scipy matplotlib tqdm joblib pandas seaborn numba hashlib


conda install -c conda-forge ipykernel
python -m ipykernel install --user --name iwc

"""

# ------------------------------------------------------------------------------

class NoDataFoundError(ValueError): pass


def sort_by_distance(pos, lab):
    # Sort positions by distance from center
    for i, (p, l) in enumerate(zip(pos, lab)):

        rr = np.linalg.norm(p, axis=1)
        if not is_sorted(rr):
            ii = np.argsort(rr)
            pos[i] = p[ii,:]
            lab[i] = l[ii]
            assert pos[i].shape == p.shapes

    return pos, lab

class AlignedDataAnalyser():
    def __init__(self, CONFIG, disable_tqdm=False):

        CONFIG.setup_output_files()

        self.file_aligned = CONFIG.file_aligned
        self.aligning_method = CONFIG.aligning_method

        self.configuration_in_shells = CONFIG.configuration_in_shells
        self.hist_range = CONFIG.hist_range
        self.num_bins = CONFIG.num_bins

        self.histogramdd_kwargs = {
            'bins': self.num_bins,
            'range': self.hist_range,
        }

        self.n_cores = CONFIG.n_cores

        self.disable_tqdm=disable_tqdm

        self.print_config()

    def print_config(self, indent=4):
        print("Selected config details:")
        for attr in ["configuration_in_shells", "aligning_method", "hist_range", "num_bins"]:
            print(" "*indent + "{}: {}".format(attr, getattr(self, attr)))

    @timer
    def load_data(self, j=0, k=None):

        logger.debug(f"Reading from {self.file_aligned}")

        if k is None:
            k = self.get_data_size()

        with h5py.File(self.file_aligned, 'r') as f:

            labels = f['labels'][j:k]

            positions = np.copy(f['positions'][j:k])

            num_groups = positions.shape[0]

        ind = np.ones_like(labels, dtype=bool)

        for i, (pos, lab) in enumerate(zip(positions, labels)):
            if np.any(np.isnan(pos)):
                logger.critical(f"NaN encountered at position {i} -- applying patch")
                ind[i] = 0
            else:
                positions[i] = np.reshape(pos, (-1, 3))

                assert lab.dtype == 'uint8'
                assert pos.dtype == 'float32'
        
        positions = positions[ind]
        labels = labels[ind]

        logger.debug(f"Loaded {num_groups} structures")

        return positions, labels, num_groups

    def get_data_size(self):
        with h5py.File(self.file_aligned, 'r') as f:
            n = f['positions'].shape[0]
        return n

    @timer
    def core(self):

        num_groups = self.get_data_size()

        if num_groups == 0:
            logger.critical(f"No data found in {self.file_aligned}")
            raise NoDataFoundError()

        # Load data
        myrange = chunk_loader(num_groups, max_chunk_size=32768)
        logger.spam(f"Processing in {len(myrange)-1} chunks of ~ {myrange[1]} items each")

        if self.n_cores == 1:
            results = self.core_main_loop(myrange)

        else:
            # parallel processing here
            raise ValueError("Not implemented")

        return results

    def core_main_loop(self, myrange):

        results = []

        for it, (j, k) in tqdm.tqdm(
                enumerate(zip(myrange[:-1], myrange[1:])),
                total=myrange.size-1, disable=self.disable_tqdm):

            results.append(self.data_processor(j, k))

        return results

    def data_processor(self, j, k):

        positions, labels, num_groups = self.load_data(j, k)
        
        self.analyse_data(positions, labels)

    def analyse_data(self, positions, labels):

        
        print(40*"-")

        print(labels.shape)
        print(positions.shape)

        for pos, lab in zip(positions, labels):

            pos, lab = sort_by_distance(pos, lab)

            print(40*"-")

            print(pos.shape)
            print(lab.shape)

            print(40*"-")

            print(pos)
            print(lab)

            print(40*"-")

            print(decode_labels(lab))

            raise ValueError("I want to quit")

        # str_labels_set = decode_labels(set(labels))
        # int_labels_set = list(set(labels))

        # for atom_type, int_atom_type in zip(str_labels_set, int_labels_set):

        #     # maybe select data like this
        #     data = positions[labels == int_atom_type]
        
    
    def get_metadata(self):
        return {
            'configuration_in_shells': self.configuration_in_shells,
            'hist_range': self.hist_range,
            'num_bins': self.num_bins,}


# @timer
def data_combiner(data, partial_data):
    
    if data is None:
        data = {
            'num_atoms': {},
            'histograms': {},
            'edges': {},
            'bin_volumes': {},
            'num_groups': 0,
        }
    
    data['num_groups'] += partial_data['num_groups']

    for at in partial_data['num_atoms']: # at = atom_type

        if at not in data['num_atoms']:
            for key in ['num_atoms', 'histograms', 'edges', 'bin_volumes']:
                data[key][at] = partial_data[key][at]
        else:
            for key in ['num_atoms', 'histograms']:
                data[key][at] += partial_data[key][at]
            
            for i in np.arange(len(partial_data['edges'][at])):
                assert np.all(np.asarray(data['edges'][at][i])
                    == np.asarray(partial_data['edges'][at][i]))

            if partial_data['bin_volumes'][at] is not None:
                assert np.all(data['bin_volumes'][at]
                    == partial_data['bin_volumes'][at])

    return data

@timer
def save_data(filepath, data, metadata):

    filepath.parent.mkdir(parents=False, exist_ok=True)

    np.savez(filepath, **data, **metadata,
        distance_intervals=[])

    logger.debug(f"Results saved to {filepath}")


def main(CONFIG):
   
    analyser = AlignedDataAnalyser(CONFIG)

    data = analyser.core()
    metadata = analyser.get_metadata()

    # save_data(CONFIG.file_histogram, data, metadata)


def analyser_core_pfun(analyser):
    try:
        return analyser.core()
    except NoDataFoundError:
        logger.critical(f"Error encountered when processing {analyser.file_aligned}. Attempting to continue with other files.")
        return None

@timer
def main_multiple(CONFIG, attributes):

    data = None

    n_cores = CONFIG.n_cores

    outfile = CONFIG.file_histogram

    metadata = None

    analysers = []
    for attr in attributes:

        for key in attr:
            setattr(CONFIG, key, attr[key])

        CONFIG.n_cores = 1
        CONFIG.setup_output_files()

        analyser = deepcopy(AlignedDataAnalyser(CONFIG, disable_tqdm=True))
        analysers.append(analyser)
        print(analyser.file_aligned)

        if metadata is None:
            metadata = analyser.get_metadata()
        else:
            assert metadata == analyser.get_metadata()

    if n_cores == 1:
        logger.warning("Processing serially")
        for analyser in analysers:
            try:
                data = data_combiner(data, analyser.core())
            except NoDataFoundError:
                logger.critical(f"Error encountered when processing {CONFIG.file_aligned}. Attempting to continue with other files.")
                pass
    else:
        logger.warning(f"Processing in parallel using {n_cores} processes")

        results = Parallel(n_jobs=n_cores)(delayed(analyser_core_pfun)(analyser)
                    for analyser in analysers)

        for r in results:
            if r is not None:
                data = data_combiner(data, r)

        del results
    
    if data is not None:
        save_data(outfile, data, metadata)
        return data['num_groups']
    else:
        return 0


def main_wrapper(CONFIG):

    total_num_groups = 0

    k = 0
    while True:
        CONFIG.centroid_id = k
        CONFIG.setup_output_files()

        # print(CONFIG)

        if os.path.exists(CONFIG.file_aligned):
            try:
                main(CONFIG)
            except NoDataFoundError:
                logger.critical(f"Error encountered when processing {CONFIG.file_aligned}. Attempting to continue with other files.")
                pass
        else:

            file_info = CONFIG.search_for_aligned_files(k)

            if not file_info:
                logger.warning(f"file {CONFIG.file_aligned} does not exist (this may be expected)" )
            else:
                logger.info("Found the following files:")
                pp.pprint(file_info)

                num_groups = main_multiple(deepcopy(CONFIG), file_info)
                total_num_groups += num_groups
        k += 1

        if k >= 10:
            break

    return total_num_groups

