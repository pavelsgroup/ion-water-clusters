import sys
import numpy as np
import functools
import time
from hashlib import blake2s
import random
import string

import logging

assert sys.version_info >= (3, 6)

# import tqdm
# class TqdmLoggingHandler(logging.Handler):
#     def __init__(self, level=logging.NOTSET):
#         super().__init__(level)

#     def emit(self, record):
#         try:
#             msg = self.format(record)
#             tqdm.tqdm.write(msg)
#             self.flush()
#         except (KeyboardInterrupt, SystemExit):
#             raise
#         except:
#             self.handleError(record)  

try:
    import coloredlogs, verboselogs

    logger = verboselogs.VerboseLogger('iwc')
    #level = 'SPAM'
    level = 'DEBUG'
    # logger.setLevel(level)
    coloredlogs.install(fmt='%(asctime)s %(message)s', level=level, logger=logger)

except ModuleNotFoundError:
    
    logging.basicConfig(format='%(asctime)s %(message)s')
    logger = logging.getLogger('iwc')
    logger.warning("Modules coloredlogs and/or verboselogs not found, using original logging module.")
    logger.setLevel(logging.DEBUG)

#logger.addHandler(TqdmLoggingHandler())

class notqdm():
    @staticmethod
    def tqdm(iterable, *args, **kwargs):
        """
        replacement for tqdm that just passes back the iterable
        useful to silence `tqdm` in tests
        """
        return iterable


def disp_title(string_list, border='*', width=80, frame=1, centered=False, offset=4):
    """Prints formated title on screen"""

    if not isinstance(string_list, list):
        string_list = [string_list]

    if frame > 0:
        print(border * width)

    for string in string_list:
        if not isinstance(string, str):
            string = str(string)

        if centered is True:
            n = width - len(string) - 2 # 2 for spaces
            m = int((n - np.mod(n,2)) / 2)
            p = int(m + np.mod(n,2))
        else:
            n = width - len(string) - 2; # 2 for spaces
            m = offset
            p = width - m - len(string) - 2


        if frame == 0:
            print(border * m + " " + string + " " + border * p)
        else:
            print(border + " " * m + string + " " * p + border)

    if frame > 0:
        print(border * width)


def timer(func):
    """Print the runtime of the decorated function"""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()    # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()      # 2
        run_time = end_time - start_time    # 3
        logger.verbose(f"Finished {func.__name__!r} in {run_time:.4f} sec")
        return value
    return wrapper_timer

def split(a: int, n: int) -> list:
    k, m = divmod(len(a), n)
    return [a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in np.arange(n)]
    # return (a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in np.arange(n))

def chunk_loader(ii: int, jj: int=None, chunk_size: int=int(1e5), 
        num_chunks: int=None, max_chunk_size: int=None) -> np.array:

    if jj is None:
        jj = ii
        ii = 0
    
    N = jj - ii

    if max_chunk_size is not None or num_chunks is not None:
        if num_chunks is None:
            n = np.ceil(N / max_chunk_size)
        else:
            n = num_chunks
        k, m = divmod(N, n)
        return np.array([ii + int(i * k + min(i, m)) for i in np.arange(n+1)])
    else:
        myrange = np.unique(np.hstack([np.arange(ii , jj, chunk_size, dtype=int), jj]))

    return myrange

def myencode(string: str):
    h = blake2s(digest_size=1)
    h.update(string.encode('utf-8'))
    return int(h.digest()[0])


def encode_labels(labels):

    if type(labels) == str:
        labels = [labels]

    labels = np.asarray(labels)

    codes = np.zeros(labels.shape[0], dtype='uint8')

    label = set(labels)
    code = [myencode(l) for l in label]

    for l, c in zip(label, code):
        codes[labels == l] = c

    # codes = np.asarray([myencode(l) for l in labels])

    return codes


def decode_labels(codes, possible_labels=['OW', 'HW', 'CL', 'LI', 'NA', 'K', 'H', 'O', 'C', 'N']):

    scalar_input = False
    if np.issubdtype(type(codes), np.floating):
        print('WARNING: you should provide integer type to this function')
        codes = [int(codes)]
        scalar_input = True
    if np.issubdtype(type(codes), np.integer):
        codes = [codes]
        scalar_input = True
    elif type(codes) == set:
        codes = list(codes)

    codes = np.asarray(codes)
    labels = np.empty(codes.shape[0], dtype=object)

    code = [myencode(l) for l in possible_labels]

    code = np.asarray(code, dtype='uint8')

    for l, c in zip(possible_labels, code):
        labels[codes == c] = l

    if scalar_input is True:
        return labels[0]
    else:
        return labels


def configuration_generator(c_min, c_max, a_max):
    ARR = []
    for coord_num in np.arange(c_min, c_max+1):
        for num_cl in np.arange(a_max+1):
            ARR.append({
                'num_ow': coord_num - num_cl,
                'num_cl': num_cl,
                'coord_num': coord_num
            })
    return ARR



def random_string(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

if __name__ == "__main__":
    

    n = chunk_loader(10, 1000, num_chunks=9)
    print(n)