import time
import numpy as np

def print_time(t):
    if t >= 1:
        return f"{t:.2f} sec"
    elif t >= 1e-3:
        return f"{1e3*t:.2f} ms"
    elif t >= 1e-6:
        return f"{1e6*t:.2f} us"
    else:
        return f"{1e9*t:.2f} ns"

def measuretime(nloops, func, *args, **kwargs):
    run_time = np.zeros(nloops)
    for i in np.arange(nloops):
        start_time = time.perf_counter()    # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()      # 2
        run_time[i] = end_time - start_time    # 3

    run_time_mean = np.mean(run_time)
    run_time_std = np.std(run_time)

    print(f"{func.__name__!r: <32} {print_time(run_time_mean)} ± {print_time(run_time_std)}")


def measuretime_precompile(nloops, func, *args, **kwargs):

    value = func(*args, **kwargs)
    measuretime(nloops, func, *args, **kwargs)