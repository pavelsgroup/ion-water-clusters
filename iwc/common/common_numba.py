import numba

@numba.jit(nopython=True)
def is_in(a, A):
    for i in range(A.size):
        if a == A[i]:
            return True
    return False

@numba.jit(nopython=True)
def is_in_sorted(a, A):
    for i in range(A.size):
        if a == A[i]:
            return True
        elif a > A[i]:
            return False
    return False

@numba.jit(nopython=True)
def is_sorted(a):
    for i in range(a.size - 1):
        if a[i+1] < a[i] :
            return False
    return True

@numba.jit(nopython=True)
def is_all_zero(a):
    for i in range(a.size):
        if a[i] != 0:
            return False
    return True

@numba.jit(nopython=True)
def is_equal(a, b):
    for i in range(a.size):
        if a[i] != b[i]:
            return False
    return True

@numba.jit("int64(int64)", nopython=True)
def factorial(n):
    N = 1  
    for i in range(2, n+1):
        N *= i
    return N
    
def find_instr(func, keyword, sig=0, limit=5):
    count = 0
    for l in func.inspect_asm(func.signatures[sig]).split('\n'):
        if keyword in l:
            count += 1
            print(l)
            if count >= limit:
                break
    if count == 0:
        print('No instructions found')