# coding: utf-8
import os
import copy
import numpy as np
import h5py
import scipy
import itertools
import matplotlib

from IPython import get_ipython
if get_ipython() is not None:
    get_ipython().run_line_magic('load_ext', 'autoreload')
    get_ipython().run_line_magic('autoreload', '2')

    get_ipython().run_line_magic('matplotlib', 'inline')
    # matplotlib.use('Qt5Agg')

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time
import re
from tqdm import tqdm
# from joblib import Parallel, delayed
import seaborn as sns
import pandas as pd
from pathlib import Path
import numba
from numba import jit

from iwc.common.common import timer, encode_labels, decode_labels

# import sys
# for var, obj in locals().items():
#     print(var, sys.getsizeof(obj))

"""

conda install -c conda-forge numpy scipy matplotlib tqdm joblib pandas seaborn numba hashlib


conda install -c conda-forge ipykernel
python -m ipykernel install --user --name iwc

"""

# ------------------------------------------------------------------------------
# CONFIGURATION

import iwc.config as config

# See config.py


PLOT = {
}

# ------------------------------------------------------------------------------


def configuration_selector(confs, labels, requested_config):
    DF = pd.DataFrame(data=confs, columns=labels)

    query = []
    for l, r in zip(labels, requested_config):
        if r == '*':
            pass
        else:
            query.append(f"{l} == {r}")

    query = " and ".join(query)
    # print(query)

    df = DF.query(query)

    return df.index.tolist()


@timer
def load_data(folder_hyd_shells, folder_neighborhoods, ind):

    with h5py.File(folder_neighborhoods / "neighborhoods.h5", 'r') as f:

        # this is super slow (bug in hdf5 ?)
        # positions = f['positions'][ind]
        # labels = f['labels'][ind]

        positions = f['positions'][:]
        positions = np.copy(positions[ind]) # allow to free memory

        labels = f['labels'][:]
        labels = np.copy(labels[ind]) # allow to free memory

    for i, pos in enumerate(positions):
        positions[i] = np.reshape(pos, (-1, 3))

    shells = np.load(folder_hyd_shells / 'shells.npy')
    shells = np.copy(shells[ind]) # allow to free memory

    return positions, labels, shells


def configuration_weight_distance_intervals(CONFIG, configuration_in_shells):

    shell_intervals = np.load(CONFIG.folder_hyd_shells / 'shell_intervals.npy').item()
    configuration_ext = np.load(CONFIG.folder_hyd_shells / 'configuration_ext.npy').item()

    # Find indices matching the requested configuration
    con_indices = configuration_selector(
        configuration_ext['confs'],
        configuration_ext['labels'],
        configuration_in_shells)

    # find groups with the requested configuration
    tmp = [np.where(configuration_ext['indices'] == con_index)[0]
        for con_index in con_indices]
    if tmp:
        ind = np.concatenate(tmp)
    else:
        ind = np.asarray([])

    # check that the numbers (counts) match
    assert ind.size == np.sum(np.asarray([
        configuration_ext['counts'][con_index] for con_index in con_indices]))

    num_groups = ind.size

    # make sure the indices are unique and in ascending order
    ind = np.unique(ind)

    return ind.size

def get_configuration_source(CONFIG, separation_method):

    if separation_method == 'deltas':
        shell_configuration = np.load(CONFIG.folder_hyd_shells / 'shells_distances.npy')
    elif separation_method == 'aligning':
        # shell_configuration = np.load(CONFIG.folder_hyd_shells / 'new_shells_distances.npy')
        pathlist = Path(CONFIG.folder_aligning).glob("configurations-*")
        pathlist = list(pathlist)
        confs = []
        for i, path in enumerate(pathlist):
            tmp = np.load(str(path))
            confs.append(tmp)
        shell_configuration = np.concatenate(confs)
    # print(shell_configuration.shape)

    return shell_configuration

def configuration_weight_num_atoms(CONFIG, configuration_in_shells, configuration_source):

    assert configuration_in_shells['num_ow'] + configuration_in_shells['num_cl'] == configuration_in_shells['coord_num']
    
    ind = np.where(np.logical_and(
            configuration_source[:,0] == configuration_in_shells['num_ow'],
            configuration_source[:,1] == configuration_in_shells['coord_num']))[0]

    # make sure the indices are unique and in ascending order
    ind = np.unique(ind)

    return ind.size

def configuration_weight_num_atoms_not_decided(CONFIG, configuration_source):

    ind = np.where(configuration_source[:,0] < 0)[0]

    # make sure the indices are unique and in ascending order
    ind = np.unique(ind)

    return ind.size

def main(CONFIG, separation_method, figure_id=0, subplot_id=None):

    # CONFIG.shell_intervals = 'auto'
    # CONFIG.distance_intervals = {
    #     CONFIG.CATION: None,
    #     CONFIG.ANION: ['shell 1', ],
    #     # ANION: None,
    #     CONFIG.OW: ['shell 1', ],
    #     CONFIG.HW: None,
    # }

    # --------------------------------------------------------------------------
    # Load data

    # conf_nums = [configuration_weight(CONFIG, configuration_in_shells)
    #         for configuration_in_shells in [
    #             np.asarray(['*', 2, 2, '*', '*', '*', '*', '*']),
    #             np.asarray(['*', 3, 1, '*', '*', '*', '*', '*']),
    #             np.asarray(['*', 4, 0, '*', '*', '*', '*', '*']),
    #             np.asarray(['*', 4, 1, '*', '*', '*', '*', '*']),
    #             np.asarray(['*', 5, 0, '*', '*', '*', '*', '*']),
    #             np.asarray(['*', 5, 1, '*', '*', '*', '*', '*']),
    #             np.asarray(['*', 6, 0, '*', '*', '*', '*', '*']),
    #             ]]

    # scale = 1.1
    # plt.figure(figure_id, figsize=(8*scale, 3*scale),)
    fig = plt.gcf()
    ax = fig.add_subplot(subplot_id)

    configuration_source = get_configuration_source(CONFIG, separation_method)
    if CONFIG.aligning_method == 'num_atoms':
        w_NA = configuration_weight_num_atoms_not_decided(CONFIG, configuration_source)

        plt.bar(f"NA", w_NA, color='k', label='')

    start = 4
    end = 11

    # cmap = plt.cm.get_cmap('jet')(np.linspace(0.1, 0.9, end-start+1))
    # cmap = plt.cm.get_cmap('gnuplot')(np.linspace(0.1, 0.9, end-start+1))
    # cmap = plt.cm.get_cmap('hsv')(np.linspace(0.1, 0.9, end-start+1))
    # cmap = plt.cm.get_cmap('gnuplot2')(np.linspace(0.1, 0.9, end-start+1))
    cmap = plt.cm.get_cmap('gnuplot2')(np.linspace(0.3, 0.8, end-start+1))
    #colors = matplotlib.cm.get_cmap('Accent')

    N = []
    W = []
    group_index = []
    group_weight = np.zeros(end - start + 1)

    for i, num_atoms_in_first_shell in enumerate(range(start, end + 1)):
        for j, n1 in enumerate(np.arange(num_atoms_in_first_shell + 1)):
            n2 = num_atoms_in_first_shell - n1;

            if CONFIG.aligning_method == 'num_atoms':
                configuration_in_shells = {'num_ow': n1, 'num_cl': n2, 'coord_num': n1 + n2}
                w = configuration_weight_num_atoms(CONFIG, configuration_in_shells, configuration_source)

            elif CONFIG.aligning_method == 'distance_intervals':
                configuration_in_shells = np.asarray(['*', n1, n2, '*', '*', '*', '*', '*'])
                w = configuration_weight_distance_intervals(CONFIG, configuration_in_shells, configuration_source)

            N.append((n1,n2,))
            W.append(w)
            group_index.append(i)
            group_weight[i] += w

    #print(W)
    S = np.sum(np.asarray(W)) + w_NA
    print(f'Total number of groups: {S}')

    group_weight /= S

    HH = np.full((12,12), np.nan)

    for j, (w, n, i) in enumerate(zip(W, N, group_index)):
        cn = np.sum(n)

        HH[cn, n[0]] = w

    HH[(HH / np.nansum(HH)) < 0.005] = np.nan

    xticks = ['NA']

    WWW = {}

    ax = plt.gca()

    for j, (w, n, i) in enumerate(zip(W, N, group_index)):
        cn = np.sum(n)
        if j == 0 or group_index[j-1] != i:
            legend = f'coord num: {cn} ({100*group_weight[i]:.2f} %)'
            WWW[f'{cn}'] = [w]
        else:
            legend = ''        
            WWW[f'{cn}'].append(w)

        if np.isnan(HH[cn, n[0]]):
            xticks.append("")
        else:
            xticks.append(f"{n[0]}")

        h = w / S * 100
        plt.bar(f"{n[0]}-{n[1]}", h, color=cmap[i,:], label=legend)


    yspan = ax.get_ylim()[1]
    for j, (w, n, i) in enumerate(zip(W, N, group_index)):
        h = w / S * 100
        if h > 0.2:
            ax.annotate(f'{n[1]}', (j + 0.5, h + yspan*0.015),
                    fontsize='large',
                    color='#0c9141')
    
    ax.annotate(f'number of Cl-', (46, yspan*0.35), 
                    fontsize='large',
                    color='#0c9141')

    # plt.xticks(np.arange(len(xticks)), xticks)
    # plt.xticks(np.arange(len(xticks)), ['' for i in xticks])
    plt.xticks([])

    # ax.tick_params(axis='x', rotation=90)

    # Add this loop to add the annotations
    if False:
        for j, p in enumerate(ax.patches):
            width, height = p.get_width(), p.get_height()

            # percentage = height / S * 100
            percentage = height
            if percentage > 1:

                x, y = p.get_xy()
                # ax.annotate(f'{percentage:.1f}%', (p.get_x()+.15*width, p.get_y() + height + 0.01))
                # print(N[j][1])
                # ax.annotate(f'{N[j][1]}%', (p.get_x()+.15*width, p.get_y() + height + 0.01))

    ax.margins(0.0, 0.075)

    cs = np.cumsum([0] + [len(WWW[w]) for w in WWW])
    for k, cn in enumerate(WWW):
        if np.mod(k+1,2) == 0:
            plt.axvspan(cs[k]+0.5, cs[k+1]+0.5, facecolor='0.9', zorder=0)
        
        xx = (cs[k]+cs[k+1])/2
        xx -= 0.6*(len(cn))

        ax.annotate(f'{cn}', (xx, ax.get_ylim()[1]*0.7),
                    color='0.4',
                    fontweight='bold',
                    fontsize='x-large')
        # ax.annotate(f'{100*group_weight[k]:.1f} %', ((cs[k]+cs[k+1])/2-1, ax.get_ylim()[1]*0.92))

    # plt.axhspan(ax.get_ylim()[1]*0.87, ax.get_ylim()[1]*1.0, facecolor='0.0', alpha=0.1, zorder=1)

    # ax.margins(0.0, 0.0)

    # plt.title(f'{CONFIG.CATION} (separation method = {separation_method})')
    # plt.legend()

    # plt.xlabel("number of oxygens")

    plt.ylabel("structure occurrence [%]")

    ax2 = ax.twinx()  # instantiate a second axes that shares the same x-axis
    # ax2.set_ylim((0,None))
    ax2.plot((cs[1:]+cs[:-1])/2, 100*group_weight,
        color='0.0',
        marker='o',
        linestyle=':')

    ax2.margins(0, 0.15)

    for k, cn in enumerate(WWW):
        if 100*group_weight[k] > 1.0:
            ax2.annotate(f'{100*group_weight[k]:.0f} %', ((cs[k]+cs[k+1])/2-1, 100*group_weight[k] + ax2.get_ylim()[1]*0.05))

    plt.ylim((0,None))

    plt.ylabel("group occurrence [%]")

    plt.tight_layout()

    fname = CONFIG.folder_hyd_shells / f'shell-1-hist-{separation_method}.pdf'
    print(f'figure saved to {fname}')
    plt.savefig(fname, bbox_inches='tight')
    # plt.show()

    # ----------------------------------------------------------------------------
    if False:

        plt.figure(figure_id + 1)
        #ax = sns.heatmap(HH, linewidth=0.5, cmap="gist_heat_r")

        k = 0    
        for i in range(4, 12):
            if np.mod(k+1,2) == 0:
                plt.axvspan(12*k, 12*(k+1), facecolor='0.9')

            plt.plot(k*12 + np.arange(12), HH[i,:], color=cmap[i-4,:])
            k += 1

        
        # plt.xlabel("configuration number")
        # plt.ylabel("number of oxygens")
        
        fname = CONFIG.folder_hyd_shells / f'shell-1-heatmap-{separation_method}.pdf'
        print(f'figure saved to {fname}')
        plt.savefig(fname, bbox_inches='tight')
        # plt.show()

    # # --------------------------------------------------------------------------
    # CONFIG.folder_aligning.mkdir(parents=False, exist_ok=True)
    # print(f"Saving results to\n{CONFIG.folder_aligning}")
    #
    #
    # # --------------------------------------------------------------------------
    # # save results
    #
    # with h5py.File(CONFIG.file_aligned, 'w') as f:
    #     f_positions = f.create_dataset(
    #         'positions', (num_groups,), dtype=h5py.special_dtype(vlen=np.dtype('float')))
    #     f_labels = f.create_dataset(
    #         'labels', (num_groups,), dtype=h5py.special_dtype(vlen=np.dtype('int8')))
    #     f_shells = f.create_dataset(
    #         'shells', (num_groups,), dtype=h5py.special_dtype(vlen=np.dtype('int8')))
    #
    #     for i, p in enumerate(pos_new):
    #         pos_new[i] = np.ravel(p)
    #
    #     f_positions[0:num_groups] = pos_new
    #     f_labels[0:num_groups] = lab_new
    #     f_shells[0:num_groups] = shl_new


if __name__ == '__main__':
    figure_id = 0
    scale = 1.1
    plt.figure(figure_id, figsize=(8*scale, 3*3*scale),)
    subplot_id = 310
    for CATION in ['LI', 'NA', 'K']:
        #for separation_method in ['deltas', 'aligning']:
        for separation_method in ['aligning']:
            CONFIG = config.Config(
                CATION=CATION,
                stride=100,
                # CATION='NA',
                aligning_method='num_atoms',
                nhood_distance=10,
                configuration_in_shells=dict(),
                )
            subplot_id += 1 

            main(CONFIG, separation_method, figure_id=figure_id, subplot_id=subplot_id)

            fname = f'configuration-occurrence.pdf'
            print(f'figure saved to {fname}')
            plt.savefig(fname, bbox_inches='tight')
            
            # figure_id += 1

    plt.show()