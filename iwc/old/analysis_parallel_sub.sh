#!/bin/bash
#SBATCH -J iwc
#SBATCH --time=48:00:00
#SBATCH --nodes=1
#SBATCH --exclusive
#SBATCH -p bigmem

set -e

source /opt/uochb/soft/anaconda3/2019.03/etc/profile.d/conda.sh
conda activate /home/tichacek/conda-envs/iwc

source /opt/uochb/soft/spack/20190608/share/spack/setup-env.sh
spack load parallel

#cd $(mktemp -d)

WRKDIR=/home/tichacek/iwc-run-01
mkdir -p $WRKDIR
cd $WRKDIR

set +e
cp /home/tichacek/ion-water-clusters/* .
set -e

WRKDIR=$(pwd)

printf "\n\
======================================================================\n\
#\n\
#  This is script:     analysis_parallel.sh\n\
#\n\
#  WRKDIR = $WRKDIR\n\
#\n\
======================================================================\n"

STRIDE=100

parallel -j 6 --bar --keep-order \
  python analysis.py --cation {1}  --stride $STRIDE --nhood-distance 10 \
      --num-neighbors 10 \
      --do-neighborhoods \
      --do-shell-distances \
      ::: LI NA K \
      >> parallel.log

# ------------------------------------------------------------------------------
# LI

parallel -j 36 --bar --colsep ' ' --no-run-if-empty --keep-order \
  --delay 600 \
  python analysis.py --cation LI --stride $STRIDE --nhood-distance 10 \
      --configuration {1} {2} {3} \
      --do-aligning \
      :::: analysis_parallel.input.4 \
      >> parallel.log

parallel -j 1 --bar --colsep ' ' --no-run-if-empty --keep-order \
  --delay 30 \
  python analysis.py  --cation LI --stride $STRIDE --nhood-distance 10 \
      --configuration {1} {2} {3} \
      --hist-num-bins 128 128 128 \
      --do-histograms \
      :::: analysis_parallel.input.4 \
      >> parallel.log

# ------------------------------------------------------------------------------
# NA

parallel -j 36 --bar --colsep ' ' --no-run-if-empty --keep-order \
  --delay 600 \
  python analysis.py --cation NA --stride $STRIDE --nhood-distance 10 \
      --configuration {1} {2} {3} \
      --do-aligning \
      :::: analysis_parallel.input.6 \
      >> parallel.log

parallel -j 1 --bar --colsep ' ' --no-run-if-empty --keep-order \
  --delay 30 \
  python analysis.py  --cation NA --stride $STRIDE --nhood-distance 10 \
      --configuration {1} {2} {3} \
      --hist-num-bins 128 128 128 \
      --do-histograms \
      :::: analysis_parallel.input.6 \
      >> parallel.log

# ------------------------------------------------------------------------------
# K

parallel -j 36 --bar --colsep ' ' --no-run-if-empty --keep-order \
  --delay 600 \
  python analysis.py --cation K --stride $STRIDE --nhood-distance 10 \
      --configuration {1} {2} {3} \
      --do-aligning \
      :::: analysis_parallel.input.8 \
      >> parallel.log

parallel -j 1 --bar --colsep ' ' --no-run-if-empty --keep-order \
  --delay 30 \
  python analysis.py  --cation K --stride $STRIDE --nhood-distance 10 \
      --configuration {1} {2} {3} \
      --hist-num-bins 128 128 128 \
      --do-histograms \
      :::: analysis_parallel.input.8 \
      >> parallel.log


printf "\n\
======================================================================\n\
#\n\
#  Finished:     analysis_parallel.sh\n\
#\n\
#  You might want to delete the tmp directory\n\
#
#  $WRKDIR\n\
#\n\
======================================================================\n"


echo $WRKDIR
