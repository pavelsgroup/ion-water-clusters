import numpy as np
import re

from iwc.cubetools import write_cube
from iwc.common.common import decode_labels

def load_data(CONFIG):

    try:
        with np.load(CONFIG.file_histogram, allow_pickle=True) as npzfile:
            #print(npzfile.files)
            histograms = npzfile['histograms'].item()  # .item() is here to recover dict form 0d numpy array
            bin_volumes = npzfile['bin_volumes'].item()
            edges = npzfile['edges'].item()

            if 'num_frames' in npzfile:
                num_frames = npzfile['num_frames'].item()
            elif 'num_groups' in npzfile:
                num_frames = npzfile['num_groups'].item()
                
            # avg_num_atoms = npzfile['avg_num_atoms'].item()
            # distance_intervals = npzfile['distance_intervals'].item()
    except FileNotFoundError as e:
        f = CONFIG.file_histogram.name
        g = re.sub('_\d+', R'_*', str(f))
        path = CONFIG.folder_histograms
        paths = list(Path(path).rglob(g))
        print('')
        print('File not found. Compatible centroids in the directory:')
        for p in paths:
            print(p.name)
        print('')
        raise(e)

    try:
        with np.load(CONFIG.file_centroid, allow_pickle=True) as npzfile:
            print(npzfile.files)
            centroid = npzfile['centroid']
            centroid_lab = npzfile['centroid_lab']
            centroid_lab = decode_labels(centroid_lab)

            # print(centroid)
            # print(centroid_lab)
                
            # avg_num_atoms = npzfile['avg_num_atoms'].item()
            # distance_intervals = npzfile['distance_intervals'].item()
    except FileNotFoundError as e:
        # f = CONFIG.file_histogram.name
        # g = re.sub('_\d+', R'_*', str(f))
        # path = CONFIG.folder_histograms
        # paths = list(Path(path).rglob(g))
        # print('')
        # print('File not found. Compatible centroids in the directory:')
        # for p in paths:
        #     print(p.name)
        # print('')
        raise(e)

    
    # 'file_centroid_perturbed'

    print(f"Loaded data of {num_frames} structures")

    # currently, we store overall data in [0], the next elements contain data of different shells
    return histograms, edges, bin_volumes, num_frames, centroid, centroid_lab

def save_centroid_structures(fname, centroid, centroid_lab):


    with open(fname, 'w') as text_file:

        text_file.write(f"XXX centroid {1}\n")
        text_file.write(f"{len(centroid)}\n")

        for i, (pos, lab) in enumerate(zip(centroid, centroid_lab)):
            text_file.write("%5d%-5s%5s%5d%8.3f%8.3f%8.3f%8.4f%8.4f%8.4f\n" % (1, "XXX", lab, i+1, pos[0]/10, pos[1]/10, pos[2]/10, 0, 0, 0))
    
        text_file.write(f"  10.00000  10.00000  10.00000\n")

def main(CONFIG):

    histograms, edges, bin_volumes, num_frames, centroid, centroid_lab = load_data(CONFIG)

    save_centroid_structures(CONFIG.folder_histograms / f'centroid.gro', centroid, centroid_lab)

    CONFIG.folder_histograms_sub.mkdir(parents=False, exist_ok=True)

    # Cubefile is in bohrs
    # 1 angstrom = 1.88972 bohr

    K = 1.88972

    atoms = [[1, pos * K] for pos in centroid]

    for atom in histograms:

        print(f"Processing atom {atom}")
          
        bin_size = [np.mean(np.diff(e)) for e in edges[atom]]

        origin = [e[0] for e in edges[atom]]

        bin_centers = [(e[:-1] + e[1:])/2 for e in edges[atom]]

        # distance of bin center from origin
        X, Y, Z = np.meshgrid(*bin_centers, indexing='ij')
        dist = np.sqrt(X**2 + Y**2 + Z**2)

        print(bin_size)
        print(bin_centers)

        fname = CONFIG.folder_histograms_sub / f"{atom}.cube"

        meta_data = {
            'org': np.array(origin) * K, # np.array([-10.0, -10.0, -10.0])  * K,
            'xvec': np.array([bin_size[0], 0.0, 0.0]) * K,
            'yvec': np.array([0.0, bin_size[1], 0.0]) * K,
            'zvec': np.array([0.0, 0.0, bin_size[2]]) * K,
            'atoms': atoms,
            # 'atoms': [[1, [0,0,0]],],
        }

        volumetric_data = histograms[atom]

        volumetric_data[dist > CONFIG.nhood_distance] = 0

        volumetric_data = volumetric_data / np.mean(volumetric_data[dist <= CONFIG.nhood_distance])

        # print(np.max(volumetric_data))
        # print(np.mean(volumetric_data))

        write_cube(volumetric_data, meta_data, fname)

        print(f"Results saved to {fname}")