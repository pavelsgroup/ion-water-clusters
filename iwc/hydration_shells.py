# coding: utf-8
import numpy as np
import h5py
import scipy
import matplotlib
from tqdm import tqdm
import seaborn as sns
import pandas as pd
from pathlib import Path

# from IPython import get_ipython
# if get_ipython() is not None:
#     get_ipython().run_line_magic('load_ext', 'autoreload')
#     get_ipython().run_line_magic('autoreload', '2')

#     get_ipython().run_line_magic('matplotlib', 'inline')
#     # matplotlib.use('Qt5Agg')

import matplotlib.pyplot as plt

from iwc.common.common import timer, encode_labels, decode_labels
import iwc.config as config


"""

conda install -c conda-forge numpy scipy matplotlib tqdm joblib pandas seaborn


conda install -c conda-forge ipykernel
python -m ipykernel install --user --name iwc

"""

# ------------------------------------------------------------------------------


@timer
def load_data(folder_neighborhoods):
    # pos = np.load(folder_neighborhoods / "positions.npy")
    # lab = np.load(folder_neighborhoods / "labels.npy")

    with h5py.File(folder_neighborhoods / "neighborhoods.h5", 'r') as f:
        pos = f['positions'][:]
        lab = f['labels'][:]

    for i, (p, l) in enumerate(zip(pos, lab)):
        assert l.dtype == 'uint8'
        assert p.dtype == 'float32'
        pos[i] = np.reshape(p, (-1, 3))

    for p in pos:
        assert p.shape[1] == 3

    assert pos.shape == lab.shape
    return pos, lab


def count(CATION, OXYGEN, ANION, HYDROGEN, array=None, get_order=False):
    """
    This function is used to count the number of atoms in the list containing
    11 atoms
    """

    if get_order:
        return (CATION, OXYGEN, ANION, HYDROGEN)

    if array is None:
        raise ValueError("array must be specified if get_order = False")

    num_cation = np.count_nonzero(array == encode_labels(CATION))
    num_oxygen = np.count_nonzero(array == encode_labels(OXYGEN))
    num_anion = np.count_nonzero(array == encode_labels(ANION))
    num_hydrogen = np.count_nonzero(array == encode_labels(HYDROGEN))

    return list((num_cation, num_oxygen, num_anion, num_hydrogen))


def gauss(x, mu, sigma, A):
    return A * np.exp(-(x - mu)**2 / 2 / sigma**2)


def gauss_bimodal(x, mu1, mu2, sigma1, sigma2, A1, A2):
    return gauss(x, mu1, sigma1, A1) + gauss(x, mu2, sigma2, A2)


def gauss_bimodal_fit(data, mu=None, sigma=None, kernel=None, x=None, y=None):

    if x is None:
        x = np.linspace(np.min(data), np.max(data), 100)

    if kernel is None:
        kernel = scipy.stats.gaussian_kde(data)

    if y is None:
        y = kernel(x)

    if mu is None:
        mu = np.mean(data)

    if sigma is None:
        sigma = np.std(data)

    A = np.max(data) / 2

    estimates = [np.max([mu - 2 * sigma, 0]), mu + 2 * sigma, sigma / 2, sigma, 1, 0.5]
    bounds = (
        [np.max([mu - 6 * sigma, 0]),   # mu1 lower bound
         np.max([mu, mu - 6 * sigma]),  # mu2 lower bound
         0, 0,
         0.5, 0.5],
        [np.min([mu + 6 * sigma, mu]),  # mu1 upper bound
         mu + 6 * sigma,                # mu2 upper bound
         sigma, 2 * sigma,
         2, 1],)
    # print(estimates)
    # print(bounds)

    params, cov = scipy.optimize.curve_fit(gauss_bimodal, x, y, p0=estimates, bounds=bounds)
    # print(params)

    return params, x

@timer
def estimate_density(
        CONFIG,
        positions, labels, colors,
        plot_density=False,
        shells=None, shell=None,
        exclude=[], exclude_fitting=None,
        fit_bimodal=False, plot_bimodal=True,
        shells_by_minimum=True, numx=200):

    fig = plt.figure()
    cross = {}
    for atom_group in decode_labels(set(labels)):
        if atom_group not in exclude:
            if shell is None:
                pos = positions[np.where((labels == encode_labels(atom_group)))]
            else:
                pos = positions[np.where((labels == encode_labels(atom_group)) & (shells == shell))]

            # print(labels)
            # print(atom_group)
            # print(positions.size)

            if pos.size == 0:
                raise(ValueError("position is empty - maybe selection of shell didn't work"))

            dist = np.linalg.norm(pos, axis=1)

            # print(atom_group)
            # print(dist)

            # sns.kdeplot(dist, bw=0.05, color=colors[atom_group], label=atom_group)
            kernel = scipy.stats.gaussian_kde(dist)
            sigma = dist.std()
            x = np.linspace(dist.min() - sigma / 4, dist.max() + sigma / 4, numx)
            y = kernel(x)
            plt.plot(x, y, color=colors[atom_group], label=atom_group)

            if fit_bimodal and atom_group not in exclude_fitting:
                params, x = gauss_bimodal_fit(dist, sigma=sigma, kernel=kernel, x=x)
                # plt.plot(x, gauss_bimodal(x, *params), color=colors[atom_group], linestyle=':')
                y1 = gauss(x, *params[::2])
                y2 = gauss(x, *params[1::2])
                if plot_bimodal:
                    plt.plot(x, y1, color=colors[atom_group], linestyle=':')
                    plt.plot(x, y2, color=colors[atom_group], linestyle=':')
                if shells_by_minimum:
                    z = y.copy()
                    mu1 = params[0]
                    mu2 = params[1]
                    z[np.where((x < mu1) | (x > mu2))] = np.inf
                    cross[atom_group] = x[np.argmin(z)]
                else:
                    cross[atom_group] = x[np.argmin(np.abs((y1 - y2) / y1 / y2))]
            if not fit_bimodal and atom_group not in exclude_fitting:
                cross[atom_group] = None

    ax = plt.gca()
    xlim = ax.get_xlim()
    for atom_group in decode_labels(set(labels)):
        if atom_group not in exclude:
            if fit_bimodal and atom_group not in exclude_fitting:
                ax.axvspan(xlim[0], cross[atom_group], facecolor=colors[atom_group], alpha=0.1)

    plt.xlim(xlim)
    plt.legend()
    CONFIG.folder_hyd_shells.mkdir(parents=False, exist_ok=True)
    fname = CONFIG.folder_hyd_shells / f'density.pdf'
    print(f'figure saved to {fname}')
    plt.savefig(fname, bbox_inches='tight')
    plt.show()
    plt.show()

    if fit_bimodal:
        return cross


def configure_shell_intervals(suggested_edge, CONFIG):

    print("Suggested edges between 1st and 2nd shell are:")
    print(pd.DataFrame(data=suggested_edge, index=[0]))

    print("Current shells are:")
    print(CONFIG.shell_intervals)

    shell_intervals = {}
    for atom_type in suggested_edge:
        if CONFIG.shell_intervals[atom_type] is not 'auto':
            shell_intervals[atom_type] = CONFIG.shell_intervals[atom_type]
        else:
            e = suggested_edge[atom_type]
            shell_intervals[atom_type] = [(0, e), (e, np.inf)]

    print("New shells are:")
    print(shell_intervals)

    return shell_intervals


@timer
def get_configurations(num, labels):

    [confs, indices, counts] = np.unique(num,  return_inverse=True, return_counts=True, axis=0)

    return confs, indices, counts

def print_configurations(confs, counts, labels):
    data = np.concatenate([counts[:, np.newaxis], confs], axis=1)
    ind = np.argsort(counts)[::-1]
    print(pd.DataFrame(data=data[ind], columns=labels))


def group_by_configuration(num, conf):

    ind = [i for i, n in enumerate(num) if np.all(n == conf)]

    if len(ind) == 0:
        raise(ValueError(f"Configuration {conf} not found."))

    return ind

@timer
def group_by_shell(positions, labels, shell_intervals):

    shell = np.asarray([np.full_like(lab, -1, dtype='int8') for lab in labels])

    for i, (pos, lab) in tqdm(enumerate(zip(positions, labels))):
        dist = np.linalg.norm(pos, axis=1)

        for atom_type in shell_intervals:
            for j, interval in enumerate(shell_intervals[atom_type]):
                ind = np.where(
                    (dist > interval[0])
                    & (dist <= interval[1])
                    & (lab == encode_labels(atom_type))
                )[0]
                if ind.size > 0:
                    shell[i][ind] = j
    return shell


def main(CONFIG):

    CONFIG.estimate_shell_intervals = True
    CONFIG.fit_bimodal = False

    PLOT = {
        'distance_distribution':            False,
        'distance_distribution_shells':     False,
    }

    # ------------------------------------------------------------------------------

    CATION = CONFIG.CATION
    ANION = CONFIG.ANION
    OXYGEN = CONFIG.OW
    HYDROGEN = CONFIG.HW
    HYDROGENS = CONFIG.HWs

    colors = {
        CATION: 'b',
        OXYGEN: 'r',
        ANION: 'green',
        HYDROGEN: 'k',
    }

    colormaps = {
        CATION: 'Blues',
        OXYGEN: 'Reds',
        ANION: 'Greens',
        HYDROGEN: 'Greys',
    }

    if CATION == 'LI':
        CONFIG.shell_intervals = {
            # 'LI': 'auto',
            'LI': [(0, np.inf)],
            # 'OW': 'auto',
            'OW': [(0, 2.7,), (2.7, np.inf)],
            # 'CL': 'auto',
            'CL': [(0, 3.0,), (3.0, np.inf)],
        }

    elif CATION == 'NA':
        CONFIG.shell_intervals = {
            'NA': [(0, np.inf)],
            'OW': [(0, 3.0,), (3.0, np.inf)],
            'CL': [(0, 3.3,), (3.3, np.inf)],
        }

    elif CATION == 'K':
        CONFIG.shell_intervals = {
            'K': [(0, np.inf)],
            'OW': [(0, 3.5,), (3.5, np.inf)],
            'CL': [(0, 3.75,), (3.75, np.inf)],
        }

    # --------------------------------------------------------------------------
    # Load data

    pos, lab = load_data(CONFIG.folder_neighborhoods)
    # --------------------------------------------------------------------------
    # Get counts of different atoms in each group
    # Based on the counts, split data into basic configurations

    if False:
        counts = np.asarray([count(CATION, OXYGEN, ANION, HYDROGEN, l) for l in lab])

        order = count(CATION, OXYGEN, ANION, HYDROGEN, get_order=True)
        confs_num = get_configurations(counts, ['count', *order])

        print("Following configurations based on the number of atoms have been found:")
        print(confs_num)

        configuration = np.full_like(lab, None)
        shell = np.full_like(lab, None)

        for i, row in confs_num.iterrows():

            conf = np.asarray([row[key] for key in order])

            print(f"Exploring configuration: {conf}")

            ind = group_by_configuration(counts, conf)

            if len(ind) == 1:
                # we may not be able to determine the distance distribution correctly
                # as there might be only one atom in the dataset
                shell[ind] = [np.full_like(lab[ind][0], None)]
                continue

            configuration[ind] = i

            # estimate splitting of shells -- now, this is done for each configuration
            # separately, which might be good/bad -- needs testing
            suggested_edge = estimate_density(
                np.concatenate(pos[ind], axis=0),
                np.concatenate(lab[ind], axis=0),
                colors,
                plot_density=PLOT['distance_distribution'],
                fit_bimodal=True,
                plot_bimodal=False,
                exclude_fitting=[HYDROGEN])

            shell_intervals = configure_shell_intervals(suggested_edge)

            for j in ind:
                shell[j] = group_by_shell([pos[j]], [lab[j]], shell_intervals)[0]

            # for s in [0, 1]:
            #     estimate_density(
            #         np.concatenate(pos, axis=0),
            #         np.concatenate(lab, axis=0),
            #         colors,
            #         shells=np.concatenate(shell[ind], axis=0),
            #         shell=s, exclude=[HYDROGEN],
            #         plot_density=PLOT['distance_distribution_shells'])


    # now estimate shells together
    if CONFIG.estimate_shell_intervals:
        suggested_edge = estimate_density(
            CONFIG,
            np.concatenate(pos, axis=0),
            np.concatenate(lab, axis=0),
            colors,
            plot_density=PLOT['distance_distribution'],
            fit_bimodal=CONFIG.fit_bimodal,
            plot_bimodal=False,
            exclude_fitting=[HYDROGEN])

    if CONFIG.estimate_shell_intervals and  CONFIG.fit_bimodal:
        shell_intervals = configure_shell_intervals(suggested_edge, CONFIG)
    else:
        shell_intervals = CONFIG.shell_intervals

    shell_full = group_by_shell(pos, lab, shell_intervals)

    # for s in [0, 1]:
    #     estimate_density(
    #         np.concatenate(pos, axis=0),
    #         np.concatenate(lab, axis=0),
    #         colors,
    #         shells=np.concatenate(shell_full, axis=0),
    #         shell=s, exclude=[HYDROGEN],
    #         plot_density=PLOT['distance_distribution_shells'])

    del pos

    # --------------------------------------------------------------------------
    # Get counts of different atoms in each group and each shell
    # Based on the counts, split data into other configurations

    counts = []
    for l, s in tqdm(zip(lab, shell_full)):  # we are using global definition of shells
        counts.append(np.asarray(
            np.concatenate([
                count(CATION, OXYGEN, ANION, HYDROGEN, l[np.where(s == 0)]),
                count(CATION, OXYGEN, ANION, HYDROGEN, l[np.where(s == 1)])]), dtype='int16'))

    del lab

    order = count(CATION, OXYGEN, ANION, HYDROGEN, get_order=True)
    order = [f"{L}_{i}" for i in [0, 1] for L in order]

    configuration_ext = {}
    configuration_ext['labels'] = order
    configuration_ext['confs'], \
    configuration_ext['indices'], \
    configuration_ext['counts'] = get_configurations(counts, ['count', *order])

    print("Following configurations based on the number of atoms in hydration shells have been found:")
    print_configurations(configuration_ext['confs'], configuration_ext['counts'], ['count', *order])

    # configuration_ext = np.full_like(lab, None)
    #
    # for i, row in tqdm(confs_num_shell.iterrows()):
    #
    #     conf = np.asarray([row[key] for key in order])
    #     ind = group_by_configuration(counts, conf)
    #
    #     configuration_ext[ind] = i

    # --------------------------------------------------------------------------
    # Save results to a file

    print(f"Saving results to\n{CONFIG.folder_hyd_shells}")

    CONFIG.folder_hyd_shells.mkdir(parents=False, exist_ok=True)

    np.save(CONFIG.folder_hyd_shells / 'shells.npy', shell_full)
    np.save(CONFIG.folder_hyd_shells / 'shell_intervals.npy', shell_intervals)
    np.save(CONFIG.folder_hyd_shells / 'configuration_ext.npy', configuration_ext)


if __name__ == '__main__':
    CONFIG = config.Config(stride=1000)
    main(CONFIG)
