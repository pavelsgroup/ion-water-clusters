import numpy as np
import numba
from iwc.common.common_numba import factorial, is_in_sorted

import itertools

from iwc.common.testing_common import measuretime, measuretime_precompile

def test_perms():
    n = int(1e8)

    #check_results = lambda x: numpy_is_all_zero(x) == common_numba.is_all_zero(x)

    #zero_array = np.zeros(n)
    #assert check_results(zero_array)

    #nonzero_array = np.arange(n)
    #nonzero_array[0] = n
    #assert check_results(nonzero_array)

    #random_array = np.random.rand(n)
    #assert check_results(random_array)

    A = np.arange(0,10)
    n = A.size
    c = np.empty(n, dtype=int)

    measuretime(20, generate, n, A, c)
    measuretime(20, generate_recursive, n, A)



def all_perms(s):
    if len(s) <= 1: 
        yield s
    else:
        for i in range(len(s)):
            for p in all_perms(s[:i] + s[i+1:]):
                yield s[i] + p

@numba.njit()
def permutations(A, k):
    r = [[i for i in range(0)]]
    for i in range(k):
        r = [[a] + b for a in A for b in r if (a in b)==False]
    return r

@numba.njit()
def is_odd(num):
    return num & 0x1

@numba.njit()
def swap(A, i, j):
    tmp = A[i]
    A[i] = A[j]
    A[j] = tmp

@numba.njit()
def roll_right(A, i, j):
    tmp = A[j]
    for k in range(j,i,-1):
        A[k] = A[k-1]
    A[i] = tmp

@numba.njit()
def roll_left(A, i, j):
    tmp = A[i]
    for k in range(i,j):
        A[k] = A[k+1]
    A[j] = tmp

@numba.njit()
def generate(n, A, c):
    # n : integer, A : array of any
    # c is an encoding of the stack state.
    # c[k] encodes the for-loop counter for when generate(k+1, A) is called
    # c : array of int

    for i in range(n):
        c[i] = 0

    if A[2] == 3:
        print('A')
    else:
        print(A)
    
    # i acts similarly to the stack pointer
    i = 0
    while i < n:

        if c[i] < i:
            if is_odd(i):
                swap(A, c[i], i)
            else:
                swap(A, 0, i)

            if A[2] == 3: # does not work
                print('A')
                c[i] = 0
                i += 1
            else:
                print(A)
                # Swap has occurred ending the for-loop.
                # Simulate the increment of the for-loop counter
                c[i] += 1
                # Simulate recursive call reaching the base case by
                # bringing the pointer to the base case analog in the array
                i = 0
        else:
            # Calling generate(i+1, A) has ended as the for-loop terminated.
            # Reset the state and simulate popping the stack by incrementing
            # the pointer.
            c[i] = 0
            i += 1

@numba.njit()
def generate_recursive(k, A):#, n):
    # k : integer, A : array of any

    #n[0] += 1

    # if k == 2 and A[2] == 3:
    #     return

    if k == 1:
        #n[1] += 1
        print(A)
    else:
        # Generate permutations with kth unaltered
        # Initially k == length(A)
        generate_recursive(k - 1, A)#, n)

        # Generate permutations for kth swapped with each k-1 initial
        for i in range(0, k - 1):
            # Swap choice dependent on parity of k (even or odd)
            if is_odd(k):
                swap(A, 0, k-1)
                # swap(A[0], A[k-1])
            else:
                swap(A, i, k-1)
                # swap(A[i], A[k-1]) # zero-indexed, the kth is at k-1
                
            generate_recursive(k - 1, A)#, n)

# global kk
# kk = 0

@numba.njit()
def generate_recursive_gen(j, k, A):#, n):
    # k : integer, A : array of any

    #n[0] += 1

    # if k == 2 and A[2] == 3:
    #     return

    if k == 1 + j:
        #n[1] += 1
        # global kk
        # kk += 1
        print(A)
    else:
        # Generate permutations with kth unaltered
        # Initially k == length(A)
        generate_recursive_gen(j, k - 1, A)#, n)

        # Generate permutations for kth swapped with each k-1 initial
        for i in range(j, k - 1):
            # Swap choice dependent on parity of k (even or odd)
            if is_odd(k+j):
                swap(A, j, k-1)
            else:
                swap(A, i, k-1)
                
            generate_recursive_gen(j, k - 1, A)#, n)

@numba.njit()
def generate_recursive_2_groups(j1, j2, k1, k2, A):#, n):
    # k : integer, A : array of any

    #n[0] += 1

    # if k == 2 and A[2] == 3:
    #     return

    if k1 == 1 + j1:
        #n[1] += 1
        #print(A)
        generate_recursive_gen(j2, k2, A)
    else:
        # Generate permutations with kth unaltered
        # Initially k == length(A)
        generate_recursive_2_groups(j1, j2, k1 - 1, k2, A)#, n)

        # Generate permutations for kth swapped with each k-1 initial
        for i in range(j1, k1 - 1):
            # Swap choice dependent on parity of k (even or odd)
            if is_odd(k1 + j1):
                swap(A, j1, k1 - 1)
            else:
                swap(A, i, k1 - 1)
                
            generate_recursive_2_groups(j1, j2, k1 - 1, k2, A)#, n)

global kk; kk = 0

def permutation_check_all_template(A):
    for k in np.arange(A.size):
        if permutation_check_template(0, A, k) == False:
            return False
    return True

@numba.njit()
def permutation_check_template(it, A, k):
    check = True

    if k+1 == 4 and A[k] == 2:
        check = False
    if k+1 == 3 and A[k] == 1:
        check = False
    if k+1 == 2 and A[k] == 4:
        check = False
    if k+1 == 1 and A[k] == 3:
        check = False

    return check

@numba.njit()
def no_permutation_check(it, A, k):
    return True

@numba.njit()
def return_permutation(A, it, out):
    out[it,:] = A
    return out

@numba.njit()
def simulate_skipped_permutations(m, j, k, J, K, A):
    # N ... number of permutations to be performed
    N = factorial(k - j - 1) # number of permutations in this group            
    for mm in range(m): # number of permutations in "lower" groups
        N = N * factorial(K[mm] - J[mm])

    # print(k,j)
    # print(A)
    if k - j > 2:
        if is_odd(k + j):
            if (k - j - 1) > 5:
                roll_right(A, j+1, k-3)
                swap(A, k-3, k-2)
                swap(A, j, k-2)
            else: 
                roll_left(A, j, k-2)
        else:
            swap(A, j, k-2)
    # print(A)
    return N

@numba.njit()
def generate_recursive_multigroup_reference(m, j, k, J, K, A, it, out):
    """
        Generates permutations among groups of A defined by arrays J, K
            * J[1:] = K[:-1] are the borders between the groups
            * size of J (or K) is the number of groups
        m ... index of the group currently being processed
            has to be initialized to the number of groups (size of J, K)
        j ... starting and
        k ... ending index of A where to perform permutation
            has to be initialized to 
                j = starting index of last group
                k = ending index of last group
    """

    if k == 1+j:

        if m == 0:
            # print(A)
            out[it,:] = A
            # out[it] = 1
            return it+1, out
        else:
            it, out = generate_recursive_multigroup_reference(m-1, J[m-1], K[m-1], J, K, A, it, out)

            # This swap/roll is added such that the order of permutations
            # in lower groups is always the same. Strictly speaking, this 
            # is suboptimal in terms of performance, but allows us to make
            # a check here, skipping the generation of all the permutations
            # if necessary

            # NOTE: this needs further checking

            if is_odd(K[m-1] - J[m-1]):
                swap(A, J[m-1], K[m-1]-1)
            else:
                roll_right(A, J[m-1], K[m-1]-1)
    else:

        it, out = generate_recursive_multigroup_reference(m, j, k-1, J, K, A, it, out)

        # Generate permutations for kth swapped with each k-1 initial
        for i in range(j, k-1):
            # Swap choice dependent on parity of k (even or odd)
            if is_odd(k + j):
                swap(A, j, k-1)
            else:
                swap(A, i, k-1)
            
            it, out = generate_recursive_multigroup_reference(m, j, k-1, J, K, A, it, out)

    return it, out

def generate_recursive_multigroup_factory(outfun_switch=0, outfun=None, checking=False, checkingfun=None):

    @numba.njit()
    def generate_recursive_multigroup(m, j, k, J, K, A, it, out, *args):
        """
            Generates permutations among groups of A defined by arrays J, K
                * J[1:] = K[:-1] are the borders between the groups
                * size of J (or K) is the number of groups
            m ... index of the group currently being processed
                has to be initialized to the number of groups (size of J, K)
            j ... starting and
            k ... ending index of A where to perform permutation
                has to be initialized to 
                    j = starting index of last group
                    k = ending index of last group
        """

        if k == 1+j:

            if checking is True:
                check = checkingfun(it, A, k-1, *args)

            if checking is True and check is False:
                N = simulate_skipped_permutations(m, j, k, J, K, A)
                if outfun_switch == 0:
                    out[it:it+N,:] = 0
                elif outfun_switch == 1:
                    out[it:it+N] = 0
                it = it + N
            else:
                if m == 0:
                    # print(A)
                    if outfun_switch == 0:
                        out[it,:] = A
                    elif outfun_switch == 1:
                        out[it] = 1
                    else:
                        out = outfun(A, it, out) # using outfun like this here is about 25 % slower
                    return it+1, out
                else:
                    it, out = generate_recursive_multigroup(m-1, J[m-1], K[m-1], J, K, A, it, out, *args)

                    # This swap/roll is added such that the order of permutations
                    # in lower groups is always the same. Strictly speaking, this 
                    # is suboptimal in terms of performance, but allows us to make
                    # a check here, skipping the generation of all the permutations
                    # if necessary

                    # NOTE: this needs further checking

                    if is_odd(K[m-1] - J[m-1]):
                        swap(A, J[m-1], K[m-1]-1)
                    else:
                        roll_right(A, J[m-1], K[m-1]-1)

        else:

            if checking is True:
                check = checkingfun(it, A, k-1, *args)

            if checking is True and check is False:
                N = simulate_skipped_permutations(m, j, k, J, K, A)
                if outfun_switch == 0:
                    out[it:it+N,:] = 0
                elif outfun_switch == 1:
                    out[it:it+N] = 0
                it = it + N
            else:
                # Generate permutations with kth unaltered
                # Initially k == length(A)
                it, out = generate_recursive_multigroup(m, j, k-1, J, K, A, it, out, *args)
                
            # Generate permutations for kth swapped with each k-1 initial
            for i in range(j, k-1):
                # Swap choice dependent on parity of k (even or odd)
                if is_odd(k + j):
                    swap(A, j, k-1)
                else:
                    swap(A, i, k-1)
                
                if checking is True:
                    check = checkingfun(it, A, k-1, *args)

                if checking is True and check is False:
                    N = simulate_skipped_permutations(m, j, k, J, K, A)
                    if outfun_switch == 0:
                        out[it:it+N,:] = 0
                    elif outfun_switch == 1:
                        out[it:it+N] = 0
                    it = it + N
                else:
                    it, out = generate_recursive_multigroup(m, j, k-1, J, K, A, it, out, *args)                    

        return it, out

    return generate_recursive_multigroup

standard_permutation_gfun = generate_recursive_multigroup_factory()

def permutation_multigroup(sets, *args, dtype=None, out=None, gfun=None, **kwargs):

    A = [np.array(a) for a in sets]
    S = [a.size for a in A]
    M = np.cumsum([0] + S)
    J = np.array(M[:-1])
    K = np.array(M[1:])

    m = len(A) - 1
    j = J[m]
    k = K[m]

    A = np.concatenate(A)

    num_perms = np.product([np.math.factorial(s) for s in S])

    if dtype is not None:
        A = A.astype(dtype)

    if out is None:
        out = np.zeros((num_perms, A.size), dtype=A.dtype)

    if gfun is None and kwargs == {}:
        gfun = standard_permutation_gfun
    elif gfun is None:
        gfun = generate_recursive_multigroup_factory(**kwargs)

    it = 0
    it, out = gfun(m, j, k, J, K, A, it, out, *args)

    return it, out


def test_random(num_iters=100):    
    for NN in np.arange(4, 10):
        perm_sets = [[i for i in range(1,NN+1)]]

        it_ref, out_ref = permutation_multigroup(perm_sets)

        for i in np.arange(num_iters):
            
            failed_checks = np.random.randint(0, np.math.factorial(NN), int(np.math.factorial(NN)/10))
            # failed_checks = np.array([  0,  50,  62,  84,  92,  97, 110, 115, 118], dtype=int)
            # failed_checks = np.array([  0], dtype=int)

            failed_checks = np.sort(failed_checks)

            @numba.njit()
            def permutation_check_random(it, A, k):
                # if np.any(failed_checks == it):
                if is_in_sorted(it, failed_checks):
                    return False
                else:
                    return True

            it, out = permutation_multigroup(perm_sets, checking=True, checkingfun=permutation_check_random)

            # for i in np.arange(out.shape[0]):
            #     print(out[i,:])
            # print(it)

            # for i in np.arange(out.shape[0]):
            #     print(out_ref[i,:], out[i,:])
            # print(it)
            
            print(failed_checks)

            for i in np.arange(out.shape[0]):        
                if not np.all(out[i,:] == 0):
                    if not np.all(out[i,:] == out_ref[i,:]):
                        for j in np.arange(i+1):
                            print(out_ref[j,:], out[j,:])
                        print(it)
                    assert np.all(out[i,:] == out_ref[i,:]), f"{i} {out_ref[i,:]} {out[i,:]}"

        print(f"Test for size {NN} passed")
    print("All tests passed")

def test_performance(num_iters=100):
    
    perm_sets = [np.arange(8), np.arange(4)]
    N = 4 + 8

    num_perms = np.product([np.math.factorial(s.size) for s in perm_sets])

    gfun = generate_recursive_multigroup_reference

    print("reference with int8, int6, int32, int64")
    measuretime_precompile(num_iters, permutation_multigroup, perm_sets, dtype='int8', out=np.zeros((num_perms,N,), dtype='int8'), gfun=gfun)
    measuretime_precompile(num_iters, permutation_multigroup, perm_sets, dtype='int16', out=np.zeros((num_perms,N,), dtype='int16'), gfun=gfun)
    measuretime_precompile(num_iters, permutation_multigroup, perm_sets, dtype='int32', out=np.zeros((num_perms,N,), dtype='int32'), gfun=gfun)
    measuretime_precompile(num_iters, permutation_multigroup, perm_sets, dtype='int64', out=np.zeros((num_perms,N,), dtype='int64'), gfun=gfun)

    gfun = generate_recursive_multigroup_factory()
    print("generated with int8, int6, int32, int64")
    measuretime_precompile(num_iters, permutation_multigroup, perm_sets, dtype='int8', out=np.zeros((num_perms,N,), dtype='int8'), gfun=gfun)
    measuretime_precompile(num_iters, permutation_multigroup, perm_sets, dtype='int16', out=np.zeros((num_perms,N,), dtype='int16'), gfun=gfun)
    measuretime_precompile(num_iters, permutation_multigroup, perm_sets, dtype='int32', out=np.zeros((num_perms,N,), dtype='int32'), gfun=gfun)
    measuretime_precompile(num_iters, permutation_multigroup, perm_sets, dtype='int64', out=np.zeros((num_perms,N,), dtype='int64'), gfun=gfun)

    gfun = generate_recursive_multigroup_factory(outfun_switch=1)

    print("generated, simple output with int8, int6, int32, int64")
    measuretime_precompile(n, permutation_multigroup, perm_sets, dtype='int8', out=np.zeros((num_perms,), dtype='int8'), gfun=gfun)
    measuretime_precompile(n, permutation_multigroup, perm_sets, dtype='int16', out=np.zeros((num_perms,), dtype='int16'), gfun=gfun)
    measuretime_precompile(n, permutation_multigroup, perm_sets, dtype='int32', out=np.zeros((num_perms,), dtype='int32'), gfun=gfun)
    measuretime_precompile(n, permutation_multigroup, perm_sets, dtype='int64', out=np.zeros((num_perms,), dtype='int64'), gfun=gfun)            


if __name__ == "__main__":

    # print(list(all_perms('345')))

    # print(permutations([3,4,5],3))

    A = np.arange(0,9)
    n = A.size
    l = np.zeros(2, dtype=int)
    #generate_recursive(n-1, A)#, l)
    #generate_recursive_gen(3, 6, A)#, l)
    #generate_recursive_2_groups(0, 3, 3, 6, A)#, l)    
    # generate_recursive_multigroup(0, 3, 0, 3, np.array([0, 3, 6]), np.array([3, 6, 9]), A)
    #print(kk)

    # A = np.arange(0,4)
    # n = A.size
    # c = np.empty(n, dtype=int)
    # generate(n, A, c)

    #test_perms()

    perm_sets = [[0,1,2], [3,4,5], [6,7,8]]

    it_perm_sets = [itertools.permutations(s) for s in perm_sets]
    it_pool = tuple(itertools.product(*it_perm_sets))

    print(len(it_pool))

    # permutation_multigroup(perm_sets)
    # print(kk)

    # perm_sets = [[0,1,2,3],[4,5,6],[7,8]]
    # perm_sets = [[0,1,2,3,4],[5,6]]
    # perm_sets = [[0,1,2,3],[4,5]]
    # perm_sets = [[0,1,2],[3,4,5]]
    # perm_sets = [[0,1],[2,3,4,5]]
    # perm_sets = [[1,2,3,4,5]]
    perm_sets = [[1,2,3,4,5]]

    it_ref, out_ref = permutation_multigroup(perm_sets)
    
    for i in np.arange(out_ref.shape[0]):
        print(out_ref[i,:])
    print(it_ref)

    it, out = permutation_multigroup(perm_sets, checking=True, checkingfun=permutation_check_template)
    
    # for i in np.arange(out.shape[0]):
    #     print(out[i,:])
    # print(it)

    # print(out_ref[852,:]-1)

    # for i in np.arange(out.shape[0]):
    #     if permutation_check_all_template(out_ref[i,:]) is True:
    #         assert np.all(out[i,:] == out_ref[i,:]), f"{i} {out_ref[i,:]} {out[i,:]}"
    #     else:
    #         assert np.all(out[i,:] == 0)

    # for i in np.arange(out.shape[0]):
    #     print(out_ref[i,:], out[i,:])
    # print(it)
    
    test_random()
    test_performance()
    
    
    