# coding: utf-8
import numpy as np
from pathlib import Path

import matplotlib


from IPython import get_ipython
if get_ipython() is not None:
    get_ipython().run_line_magic('load_ext', 'autoreload')
    get_ipython().run_line_magic('autoreload', '2')

    get_ipython().run_line_magic('matplotlib', 'inline')
    # matplotlib.use('Qt5Agg')

import matplotlib.pyplot as plt

import iwc.config as config
# import neighborhoods
# import hydration_shells
# import shell_distances
import iwc.shell_classes_analysis as shell_classes_analysis
# import clustering
# import histograms

# matplotlib.rcParams.update({'font.size': 22})

# ==============================================================================
# LI

STRIDE = 100

save_folder = Path("/wrk/repo/ion-water-clusters/analysis/shell_rdf_LI_NA/")

# ------------------------------------------------------------------------------

DATA_LI = []

for i, ARR in enumerate([
    ['coord_num == 4'],
    ['coord_num != 4'],
    # ['num_ow == 4', 'coord_num == 4'],
    # ['num_ow == 3', 'coord_num == 4'],
    # ['num_ow == 2', 'coord_num == 4'],
    ]):

    CONFIG = config.Config(
            CATION='LI',
            nhood_distance=10,
            stride=STRIDE,
            configuration_in_shells=ARR)

    DATA_LI.append(shell_classes_analysis.main(CONFIG,
        target_labels=['OW', 'HW', 'CL', 'LI', 'all'],
        rdf_kwargs={'num_bins': 300}))
    DATA_LI[i]['configuration_in_shells'] = ARR

# ------------------------------------------------------------------------------

DATA_NA = []

for i, ARR in enumerate([
    ['coord_num == 4'],
    ['coord_num == 5'],
    ['coord_num == 6'],
    ['coord_num != 4', 'coord_num != 5', 'coord_num != 6'],
    # {'num_ow': 6, 'num_cl': 0, 'coord_num': 6},
    # {'num_ow': 5, 'num_cl': 1, 'coord_num': 6},
    # {'num_ow': 4, 'num_cl': 2, 'coord_num': 6},
    # {'num_ow': 3, 'num_cl': 3, 'coord_num': 6},
    # {'num_ow': 2, 'num_cl': 4, 'coord_num': 6},

    # {'num_ow': 5, 'num_cl': 0, 'coord_num': 5},
    # {'num_ow': 4, 'num_cl': 1, 'coord_num': 5},
    # {'num_ow': 3, 'num_cl': 2, 'coord_num': 5},
    # {'num_ow': 2, 'num_cl': 3, 'coord_num': 5},

    # {'num_ow': 4, 'num_cl': 0, 'coord_num': 4},
    # {'num_ow': 3, 'num_cl': 1, 'coord_num': 4},
    # {'num_ow': 2, 'num_cl': 2, 'coord_num': 4},
    # ['num_ow == 4', 'coord_num == 4'],
    # ['num_ow == 3', 'coord_num == 4'],
    # ['coord_num > 4'],
    ]):

    CONFIG = config.Config(
            CATION='NA',
            nhood_distance=10,
            stride=STRIDE,
            configuration_in_shells=ARR)

    DATA_NA.append(shell_classes_analysis.main(CONFIG,
        target_labels=['OW', 'HW', 'CL', 'NA', 'all'],
        rdf_kwargs={'num_bins': 300}))
    DATA_NA[i]['configuration_in_shells'] = ARR

# ------------------------------------------------------------------------------
cc_LI = plt.cm.get_cmap('winter')(np.linspace(0, 1, len(DATA_LI)))
cc_NA = plt.cm.get_cmap('autumn')(np.linspace(0, 0.8, len(DATA_NA)))

for (target_label_LI, target_label_NA) in zip(['OW', 'HW', 'CL', 'LI', 'all'], ['OW', 'HW', 'CL', 'NA', 'all']):

    for plot_object in ['rdf', 'rdf_norm']:

        plt.figure(figsize=(15,12))

        if plot_object == 'rdf':
            plt_fun = shell_classes_analysis.plot_rdf
            plt.title("non-normalized RDF")
            plot_kwargs={
                # 'x_shift': 0.32,
                'linestyle': '--'}
        elif plot_object == 'rdf_norm':
            plt_fun = shell_classes_analysis.plot_rdf_norm
            plt.title("normalized RDF with shift for Li = +0.32 A")
            plot_kwargs={
                'x_shift': 0.32,
                'linestyle': '--'}
        else:
            raise(ValueError(f"Unrecognised plot_object {plot_object}"))

        for i, DATA in enumerate(DATA_LI):
            D = DATA[target_label_LI]
            plt_fun(D['bin_counts'], D['bin_edges'], D['bin_centers'], D['num_groups'],
                     # label="Li: #{coord_num}: {num_ow}/{num_cl} (O/Cl)".format(**DATA['configuration_in_shells']),
                     color=cc_LI[i,:], **plot_kwargs)

        plot_kwargs={
            'linestyle': ':'}

        for i, DATA in enumerate(DATA_NA):
            D = DATA[target_label_NA]
            plt_fun(D['bin_counts'], D['bin_edges'], D['bin_centers'], D['num_groups'],
                     # label="Na: #{coord_num}: {num_ow}/{num_cl} (O/Cl)".format(**DATA['configuration_in_shells']),
                     color=cc_NA[i,:], **plot_kwargs)

        plt.xlim(1.8, None)
        plt.xlabel("r [A]")

        plt.legend()
        plt.ylabel(f"{target_label_LI}, {target_label_NA}")

        fname = f"{plot_object}-{target_label_LI}-{target_label_NA}.pdf"

        save_folder.mkdir(parents=True, exist_ok=True)
        plt.savefig(save_folder / fname, bbox_inches='tight')

        plt.show()
