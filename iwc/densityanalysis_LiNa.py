# coding: utf-8
import os
import numpy as np
import h5py
import scipy
import matplotlib
import time
from tqdm import tqdm
# from joblib import Parallel, delayed
from pathlib import Path

from IPython import get_ipython
if get_ipython() is not None:
    get_ipython().run_line_magic('load_ext', 'autoreload')
    get_ipython().run_line_magic('autoreload', '2')

    get_ipython().run_line_magic('matplotlib', 'inline')
    # matplotlib.use('Qt5Agg')

import matplotlib.pyplot as plt

from iwc.common.common import timer, encode_labels, decode_labels
from iwc.densityanalysis import DensityAnalysis, compare_density_analysis
import iwc.config as config

matplotlib.rcParams.update({'font.size': 22})

"""

conda install -c conda-forge numpy scipy matplotlib tqdm joblib pandas seaborn numba hashlib


conda install -c conda-forge ipykernel
python -m ipykernel install --user --name iwc

"""

# ------------------------------------------------------------------------------

if __name__ == '__main__':

    STRIDE_1 = 1
    CATION_1 = 'LI'

    # ARR_1 = np.asarray(['*', 2, 2, '*', '*', '*', '*', '*'])
    # ARR_1 = np.asarray(['*', 3, 1, '*', '*', '*', '*', '*'])
    # ARR_1 = np.asarray(['*', 4, 0, '*', '*', '*', '*', '*'])
    ARR_1 = {'num_ow': 4, 'num_cl': 0, 'coord_num': 4}
    # ARR_1 = {'num_ow': 3, 'num_cl': 1, 'coord_num': 4}
    # ARR_1 = {'num_ow': 2, 'num_cl': 2, 'coord_num': 4}
    # ARR_1 = {'num_ow': 1, 'num_cl': 3, 'coord_num': 4}

    CONFIG_1 = config.Config(
            CATION=CATION_1,
            stride=STRIDE_1,
            num_bins=(128, 128, 128,),
            hist_range=[(-10,10), (-10,10), (-10,10),],
            centroid_id=1,
            aligning_method='num_atoms',
            configuration_in_shells=ARR_1)

    # --------------------------------------------------------------------------

    STRIDE_2 = 1
    # CATION_2 = 'LI'
    CATION_2 = 'NA'

    # ARR_2 = np.asarray(['*', 2, 2, '*', '*', '*', '*', '*'])
    # ARR_2 = np.asarray(['*', 6, 0, '*', '*', '*', '*', '*'])
    # ARR_2 = np.asarray(['*', 4, 0, '*', '*', '*', '*', '*'])
    # ARR_2 = {'num_ow': 4, 'num_cl': 0, 'coord_num': 4}
    # ARR_2 = {'num_ow': 3, 'num_cl': 1, 'coord_num': 4}
    # ARR_2 = {'num_ow': 2, 'num_cl': 2, 'coord_num': 4}
    #
    ARR_2 = {'num_ow': 5, 'num_cl': 0, 'coord_num': 5}
    # ARR_2 = {'num_ow': 4, 'num_cl': 1, 'coord_num': 5}
    # ARR_2 = {'num_ow': 5, 'num_cl': 2, 'coord_num': 5}
    # ARR_2 = {'num_ow': 2, 'num_cl': 3, 'coord_num': 5}
    #
    # ARR_2 = {'num_ow': 6, 'num_cl': 0, 'coord_num': 6}
    # ARR_2 = {'num_ow': 5, 'num_cl': 1, 'coord_num': 6}
    # ARR_2 = {'num_ow': 4, 'num_cl': 2, 'coord_num': 6}
    # ARR_2 = {'num_ow': 3, 'num_cl': 3, 'coord_num': 6}
    # ARR_2 = {'num_ow': 2, 'num_cl': 4, 'coord_num': 6}

    CONFIG_2 = config.Config(
            CATION=CATION_2,
            stride=STRIDE_2,
            num_bins=(128, 128, 128,),
            hist_range=[(-10,10), (-10,10), (-10,10),],
            centroid_id=1,
            aligning_method='num_atoms',
            configuration_in_shells=ARR_2)

    # --------------------------------------------------------------------------

    DA1 = {}
    DA2 = {}

    for atom_type in ['OW', 'HW']:
        DA1[atom_type] = DensityAnalysis(
                CONFIG_1,
                atom_type=atom_type)
        DA2[atom_type] = DensityAnalysis(
                CONFIG_2,
                atom_type=atom_type)

    figname = ('{num_ow}_{num_cl}'.format(**ARR_1)
            + '_vs_'
            + '{num_ow}_{num_cl}'.format(**ARR_2))

    # PAR = {
    #     'use_atom_weight': False,
    #     'normalization': 1e-3,
    #     'log_transform': False,
    #     }
    #
    # DA1['OW'].analyze(**PAR, range=[np.linspace(1,6,50), np.linspace(0,6,50)])
    # H, D, V, num_frames = DA1['OW'].load_and_preprocess()
    # np.std(H)
    # plt.hist(H[np.logical_and(H>0, H<100)], bins=100)
    # HH = DA1['OW'].HH
    # plt.hist(HH[np.logical_and(HH>0, HH<1e10)], bins=100)

    # ==========================================================================

    normalization=1e-3

    h_range = {}
    x_range = {}

    SKIP_AT = set(['HW'])

    # --------------------------------------------------------------------------
    # --------------------------------------------------------------------------

    PAR = {
        'use_atom_weight': False,
        'normalization': 1e-3,
        'log_transform': True,
        }

    # range['OW'] = [np.linspace(1,6,10), np.linspace(0,1,30)]
    #
    # for atom_type in set(['OW', 'HW']) - SKIP_AT:
    #     # DA1[atom_type].analyze(**PAR, range=range[atom_type])
    #     # DA1[atom_type].plot()
    #     DA2[atom_type].analyze(**PAR, range=range[atom_type])
    #     DA2[atom_type].plot()
    #
    #     # compare_density_analysis(DA1[atom_type], DA2[atom_type])
    #     np.set_printoptions(threshold=np.nan)
    #     print(DA1[atom_type].HH * np.asarray(DA1[atom_type].HH > 8))


# if False:

    fignum = 0

    x_range['OW'] = np.linspace(1,8,90)
    x_range['HW'] = np.linspace(1,8,90)

    for atom_type in set(['OW', 'HW']) - SKIP_AT:
        DA1[atom_type].analyze_rdf(x_range=x_range[atom_type])
        DA2[atom_type].analyze_rdf(x_range=x_range[atom_type])

        fig = plt.figure(figsize=(12, 9),)
        ax = plt.gca()
        plt.rc('text', usetex=True)
        DA1[atom_type].plot_rdf(fig=fig, ax=ax)
        DA2[atom_type].plot_rdf(fig=fig, ax=ax)
        plt.show()

        fig = plt.figure(figsize=(12, 9),)
        ax = plt.gca()
        plt.rc('text', usetex=True)
        DA1[atom_type].plot_cdf(fig=fig, ax=ax)
        DA2[atom_type].plot_cdf(fig=fig, ax=ax)
        plt.show()

# if False:

    PAR = {
        'use_atom_weight': False,
        'normalization': 1e-3,
        'log_transform': True,
        }

    h_range['OW'] = [np.linspace(1,6,100), np.linspace(0,4,100)]
    h_range['HW'] = [np.linspace(1,6,100), np.linspace(0,4,100)]

    for atom_type in set(['OW', 'HW']) - SKIP_AT:
        DA1[atom_type].analyze(**PAR, h_range=h_range[atom_type])
        DA2[atom_type].analyze(**PAR, h_range=h_range[atom_type])

        fignum += 1
        compare_density_analysis(DA1[atom_type], DA2[atom_type], figname=figname + f'_{fignum}')


    h_range['OW'] = [np.linspace(1,3.25,100), np.linspace(0.1,4,100)]
    h_range['HW'] = [np.linspace(1,3.25,100), np.linspace(0.1,4,100)]

    for atom_type in set(['OW', 'HW']) - SKIP_AT:
        DA1[atom_type].analyze(**PAR, h_range=h_range[atom_type])
        DA2[atom_type].analyze(**PAR, h_range=h_range[atom_type])

        fignum += 1
        compare_density_analysis(DA1[atom_type], DA2[atom_type], figname=figname + f'_{fignum}')

    # --------------------------------------------------------------------------

    PAR = {
        'use_atom_weight': False,
        'normalization': 1e-3,
        'highlight_high_densities': False,
        'log_transform': False,
        }

    h_range['OW'] = [np.linspace(1,8,100), np.linspace(0,100,100)]
    h_range['HW'] = [np.linspace(1,8,100), np.linspace(0,200,100)]

    for atom_type in set(['OW', 'HW']) - SKIP_AT:
        DA1[atom_type].analyze(**PAR, h_range=h_range[atom_type])
        DA2[atom_type].analyze(**PAR, h_range=h_range[atom_type])

        fignum += 1
        compare_density_analysis(DA1[atom_type], DA2[atom_type], figname=figname + f'_{fignum}')


    h_range['OW'] = [np.linspace(1,8,100), np.linspace(5,100,100)]
    h_range['HW'] = [np.linspace(1,8,100), np.linspace(10,200,100)]

    for atom_type in set(['OW', 'HW']) - SKIP_AT:
        DA1[atom_type].analyze(**PAR, h_range=h_range[atom_type])
        DA2[atom_type].analyze(**PAR, h_range=h_range[atom_type])

        fignum += 1
        compare_density_analysis(DA1[atom_type], DA2[atom_type], figname=figname + f'_{fignum}')

    # %%

    h_range['OW'] = [np.linspace(3.25,8,100), np.linspace(20,60,100)]
    h_range['HW'] = [np.linspace(3.25,8,100), np.linspace(10,200,100)]

    for atom_type in set(['OW', 'HW']) - SKIP_AT:
        DA1[atom_type].analyze(**PAR, h_range=h_range[atom_type])
        DA2[atom_type].analyze(**PAR, h_range=h_range[atom_type])

        fignum += 1
        compare_density_analysis(DA1[atom_type], DA2[atom_type], figname=figname + f'_{fignum}')

    # %%

    h_range['OW'] = [np.linspace(1,4.5,100), np.linspace(0,5,40)]
    h_range['HW'] = [np.linspace(1,4.5,100), np.linspace(0,10,40)]

    for atom_type in set(['OW', 'HW']) - SKIP_AT:
        DA1[atom_type].analyze(**PAR, h_range=h_range[atom_type])
        DA2[atom_type].analyze(**PAR, h_range=h_range[atom_type])

        fignum += 1
        compare_density_analysis(DA1[atom_type], DA2[atom_type], figname=figname + f'_{fignum}')
