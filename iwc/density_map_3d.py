# coding: utf-8

from IPython import get_ipython
if get_ipython() is not None:
    get_ipython().run_line_magic('load_ext', 'autoreload')
    get_ipython().run_line_magic('autoreload', '2')

import numpy as np
from iwc.cluster_plot_3d import ShellPlot3d

import argparse

"""
"""
# ------------------------------------------------------------------------------
# CONFIGURATION

import iwc.config as config
# See config.py

def main(CONFIG):
    exclude_atom_types = [
    # 'HW',
    # 'CL',
    # 'OW',
    # 'NA',
    # 'K',
    # 'LI',
    ]

    L = 0.0 # low level of opacity
    H = 0.0 # high level of opacity
    S = 0.4 # slider level of opacity

    volume_range = { # in multiples of mean
        'LI': (2, 10),
        'NA': (2, 10),
        'K':  (2, 10),
        'OW': (2, 10),
        'CL': (2, 10),
        'HW': (2, 10),
    }

    contour_levels = { # in multiples of mean
        'LI': [(4, L), (10, H), (20, S)],
        'NA': [(2, L), (5, H), (10, S)],
        'K':  [(4, L), (6, H), (20, S)],
        'OW': [(3, L), (6, H), (10, S)],
        'CL': [(4, L), (6, H), (15, S)],
        'HW': [(3, L), (5, H), (10, S)],
    }

    # plot_type = 'volume'
    plot_type = 'contour'

    plot_object = ['density']
    # plot_object = ['void']
    # plot_object = ['void', 'density']

    void_atom_types = ['OW']

    separate_atom_types = True
    # separate_atom_types = False

    shellplot = ShellPlot3d(CONFIG,
        normalization=1e-3,
        plot_type=plot_type,
        exclude_atom_types=exclude_atom_types,
        separate_atom_types=separate_atom_types,
        void_atom_types=void_atom_types,
        volume_range=volume_range,
        contour_levels=contour_levels)
        
    shellplot.main(plot_object=plot_object)
    if 'void' in plot_object and 'density' in plot_object:
        shellplot.configure_traits(view='view_all')
    elif 'void' in plot_object:
        shellplot.configure_traits(view='view_voids')
    elif 'density' in plot_object:
        shellplot.configure_traits(view=f'view_{CONFIG.CATION}')


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("--cation", type=str, required=True,
                        help="")
    parser.add_argument("--stride", type=int, default=1,
                        help="")
    parser.add_argument("--centroid-id", type=int, default=None,
                        help="")
    parser.add_argument("--configuration", nargs='+', type=int, default=None,
                        help="")
    parser.add_argument("--hist-num-bins", nargs='+', type=int, default=(128, 128, 128,),
                        help="")
    parser.add_argument("--nhood-distance", type=int, default=10,
                        help="")
    args = parser.parse_args()

    if args.configuration is not None:
        configuration = { 'num_ow': args.configuration[0],
            'num_cl': args.configuration[1],
            'coord_num': args.configuration[2]}

    hist_range = [(-args.nhood_distance, args.nhood_distance), 
                  (-args.nhood_distance, args.nhood_distance),
                  (-args.nhood_distance, args.nhood_distance)]

    CONFIG = config.Config(
        CATION=args.cation,
        stride=args.stride,
        num_bins=args.hist_num_bins,
        hist_range=hist_range,
        nhood_distance=args.nhood_distance, # Angstrom
        aligning_method='num_atoms',
        configuration_in_shells=configuration,
        centroid_id=args.centroid_id,
        )

    main(CONFIG)
