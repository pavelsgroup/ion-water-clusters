# coding: utf-8
import os
import numpy as np
import h5py
import scipy
import matplotlib
import time
from tqdm import tqdm
# from joblib import Parallel, delayed
from pathlib import Path

from IPython import get_ipython
if get_ipython() is not None:
    get_ipython().run_line_magic('load_ext', 'autoreload')
    get_ipython().run_line_magic('autoreload', '2')

    get_ipython().run_line_magic('matplotlib', 'inline')
    # matplotlib.use('Qt5Agg')

import matplotlib.pyplot as plt

from iwc.common.common import timer, encode_labels, decode_labels, configuration_generator
from iwc.densityanalysis import DensityAnalysis, compare_density_analysis, combine_density_analysis, EmptyAfterCroppingError
import iwc.config as config

matplotlib.rcParams.update({'font.size': 22})

"""

conda install -c conda-forge numpy scipy matplotlib tqdm joblib pandas seaborn numba hashlib


conda install -c conda-forge ipykernel
python -m ipykernel install --user --name iwc

"""

# ------------------------------------------------------------------------------

if __name__ == '__main__':

    STRIDE_1 = 1
    CATION_1 = 'LI'
    # CATION_1 = 'NA'
    # CATION_1 = 'K'

    # --------------------------------------------------------------------------

    STRIDE_2 = 1
    # CATION_2 = 'LI'
    CATION_2 = 'NA'
    # CATION_2 = 'K'

    # --------------------------------------------------------------------------

    c_min = 3
    c_max = 11
    a_max = 11

    PAR_1 = {
        'use_atom_weight': False,
        'normalization': 1e-3,
        'highlight_high_densities': False,
        'log_transform': True, # !!!
        'h_range': [np.linspace(1,9.7,128), np.linspace(0, 4.5, 256)],
        }
    PAR_1_plot = {
        'log_color': True, # !!!
    }
    
    PAR_2 = {
        'use_atom_weight': False,
        'normalization': 1e-3,
        'highlight_high_densities': False,
        'log_transform': False,
        'h_range': [np.linspace(1,9.7,128), np.linspace(20, 50, 192)],
        }
    PAR_2_plot = {
        'log_color': False,
    }

    PAR_3 = {
        'use_atom_weight': False,
        'normalization': 1e-3,
        'highlight_high_densities': False,
        'log_transform': False,
        'h_range': [np.linspace(1,9.7,128), np.linspace(0,0.1,16)],
        }
    PAR_3_plot = {
        'log_color': False,
    }
    
    PAR = PAR_1; PAR_plot = PAR_1_plot
    # PAR = PAR_2; PAR_plot = PAR_2_plot
    # PAR = PAR_3; PAR_plot = PAR_3_plot
       

    DA1 = {}
    DA2 = {}
    for atom_type in ['OW']:

        DA1[atom_type] = []
        DA2[atom_type] = []

        for ARR_1 in configuration_generator(c_min, c_max, a_max):
            for i in range(10):

                CONFIG_1 = config.Config(
                    CATION=CATION_1,
                    stride=STRIDE_1,
                    num_bins=(128, 128, 128,),
                    hist_range=[(-10,10), (-10,10), (-10,10),],
                    centroid_id=i,
                    aligning_method='num_atoms',
                    configuration_in_shells=ARR_1)

                if CONFIG_1.file_histogram.exists():
                    da = DensityAnalysis(CONFIG_1, atom_type=atom_type)
                    try:
                        da.analyze(**PAR)
                        DA1[atom_type].append(da)
                    except EmptyAfterCroppingError as e:
                        print("Empty configuration encountered - skipping")
                else:
                    pass
                    #print("skipping")

        DA1[atom_type] = combine_density_analysis(DA1[atom_type], CONFIG_1, atom_type)

        for ARR_2 in configuration_generator(c_min, c_max, a_max):
            for i in range(10):

                CONFIG_2 = config.Config(
                    CATION=CATION_2,
                    stride=STRIDE_2,
                    num_bins=(128, 128, 128,),
                    hist_range=[(-10,10), (-10,10), (-10,10),],
                    centroid_id=i,
                    aligning_method='num_atoms',
                    configuration_in_shells=ARR_2)
                
                if CONFIG_2.file_histogram.exists():
                    da = DensityAnalysis(CONFIG_2, atom_type=atom_type)
                    try:
                        da.analyze(**PAR)
                        DA2[atom_type].append(da)
                    except EmptyAfterCroppingError as e:
                        print("Empty configuration encountered - skipping")

                else:
                    pass
                    #print("skipping")

        DA2[atom_type] = combine_density_analysis(DA2[atom_type], CONFIG_2, atom_type)


        figname = 'all'

        compare_density_analysis(DA1[atom_type], DA2[atom_type],
                figname=figname + f'_all', do_plot_rdf=False, **PAR_plot)
