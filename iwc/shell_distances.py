# coding: utf-8
import os
import numpy as np
import h5py
import scipy
import matplotlib
from tqdm import tqdm
import seaborn as sns
import pandas as pd
from pathlib import Path
import numba as nb
import re
import operator

# from IPython import get_ipython
# if get_ipython() is not None:
#     get_ipython().run_line_magic('load_ext', 'autoreload')
#     get_ipython().run_line_magic('autoreload', '2')

#     get_ipython().run_line_magic('matplotlib', 'inline')
#     # matplotlib.use('Qt5Agg')

import matplotlib.pyplot as plt

from iwc.common.common import notqdm, timer, encode_labels, decode_labels
import iwc.config as config

disable_tqdm = bool(os.environ.get("DISABLE_TQDM"))
if disable_tqdm is True:
    print("encountered environment variable DISABLE_TQDM, disabling tqdm")
    tqdm = notqdm

"""

conda install -c conda-forge numpy scipy matplotlib tqdm joblib pandas seaborn


conda install -c conda-forge ipykernel
python -m ipykernel install --user --name iwc

"""

# ------------------------------------------------------------------------------

def op_to_opfun(op):
    if op == '==':
        return operator.eq
    elif op == '>':
        return operator.gt
    elif op == '>=':
        return operator.ge
    elif op == '<':
        return operator.lt
    elif op == '<=':
        return operator.le
    elif op == '!=':
        return operator.ne
    else:
        raise(ValueError(f"operator {op} is not yet supprted"))


class ShellClasses():
    def __init__(self, CONFIG):
        self.CONFIG = CONFIG
        self.shell_configuration = np.load(self.CONFIG.folder_hyd_shells / 'shells_distances.npy')
        # print(shell_configuration.shape)

    def select(self, selector):

        for i, condition in enumerate(selector):
            tmp = re.split('\s+', condition)
            var = tmp[0]
            opfun = op_to_opfun(tmp[1])
            val = float(tmp[2])

            if var == 'num_ow':
                j = 0
            elif var == 'coord_num':
                j = 1
            else:
                raise(ValueError(f"variable {var} is not yet supprted"))

            tmp_I = opfun(self.shell_configuration[:,j], val)
            if i == 0:
                I = tmp_I
            else:
                I = np.logical_and(I, tmp_I)

        ind = np.where(I)[0]

        # make sure the indices are unique and in ascending order
        ind = np.unique(ind)

        num_groups = ind.size

        return ind, num_groups

    @timer
    def load_data(self, ind, chunk_size=int(1e5)):

        with h5py.File(self.CONFIG.folder_neighborhoods / "neighborhoods.h5", 'r') as f:

            n = f['positions'].shape[0]
            N = ind.size

            assert(ind.dtype == int)

            positions = np.empty((N,), dtype=np.object)
            labels = np.empty((N,), dtype=np.object)

            myrange = np.unique(np.hstack([np.arange(0, n, chunk_size, dtype=int), n]))

            print(f'Loading {N} data (out of {n})')

            # this is super slow (bug in hdf5 ?)
            # positions = f['positions'][ind]
            # labels = f['labels'][ind]

            jj = 0
            kk = 0

            for j, k in tqdm(zip(myrange[:-1], myrange[1:]), total=myrange.size-1):
                # print(j, k)

                pos = f['positions'][j:k]
                lab = f['labels'][j:k]

                ii = ind[np.logical_and(ind >= j, ind < k)] - j

                kk += ii.size

                # print(jj, kk)

                positions[jj:kk] = pos[ii]
                labels[jj:kk] = lab[ii]

                jj += ii.size

            # positions = f['positions'][:]
            # positions = np.copy(positions[ind]) # allow to free memory
            #
            # labels = f['labels'][:]
            # labels = np.copy(labels[ind]) # allow to free memory

        print('Reshaping data ...')

        for i, (p, l) in enumerate(zip(positions, labels)):
            assert l.dtype == 'uint8'
            assert p.dtype == 'float32'
            positions[i] = np.reshape(p, (-1, 3))

        return positions, labels


@timer
def load_data(folder_neighborhoods):
    # pos = np.load(folder_neighborhoods / "positions.npy")
    # lab = np.load(folder_neighborhoods / "labels.npy")

    with h5py.File(folder_neighborhoods / "neighborhoods.h5", 'r') as f:
        pos = f['positions'][:]
        lab = f['labels'][:]

    for i, (p, l) in enumerate(zip(pos, lab)):
        assert l.dtype == 'uint8'
        assert p.dtype == 'float32'
        pos[i] = np.reshape(p, (-1, 3))

    for p in pos:
        assert p.shape[1] == 3

    assert pos.shape == lab.shape
    return pos, lab

@nb.njit
def first_index_delta_numba(val, arr):
    for idx in range(len(arr)):
        if arr[idx] > val:
            return idx
    return np.nan

@nb.njit
def first_index_DELTA_numba(val1, val2, arr):
    for idx in range(len(arr)-1):
        if arr[idx] > val1 and arr[idx+1] < val2:
            return idx
    return np.nan

@nb.njit
def first_index_delta_DELTA_numba(val0, val1, val2, delta, DELTA):
    for idx in range(len(DELTA)-1):
        if delta[idx+1] > val0 and DELTA[idx] > val1 and DELTA[idx+1] < val2:
            return idx
    return np.nan

def group_by_count(pos, lab, L, N, selection):

    return np.asarray([np.count_nonzero(l[selection] == L) == N for l in lab])

# @timer
def compute_differences(pos, lab):

    dist = []
    delta = []
    DELTA = []
    df = []

    for i, (p, l) in enumerate(zip(pos, lab)):
        assert p.shape[1] == 3

        rr = np.linalg.norm(p, axis=1)
        ii = np.argsort(rr)
        r = rr[ii]
        l = l[ii]
        # r = np.sort(np.linalg.norm(p, axis=1))

        # r = r[r < 5]
        d = np.diff(r)
        D = np.diff(d)

        dist.append(r)
        delta.append(d)
        DELTA.append(D)
        # q = first_index_delta_numba(0.7, d)
        # qq = first_index_DELTA_numba(0.25, -0.25, D) + 1
        qq = first_index_delta_DELTA_numba(0.5, 0.25, -0.25, d, D) + 1
        # qq = first_index_delta_DELTA_numba(0.3, 0.15, -0.15, d, D) + 1

        df.append(qq)

    dist = np.stack(dist, axis=0)
    delta = np.stack(delta, axis=0)
    DELTA = np.stack(DELTA, axis=0)
    df = np.asarray(df)

    return dist, delta, DELTA, df


def plot_differences(dist, delta, DELTA, df):

    for j in np.arange(10):
        continue
        tmp = np.asarray([d[j] for d in delta])
        rr.append(np.asarray(tmp))

        plt.figure(figsize=(12, 6),)
        plt.hist(rr[j], bins=200, color='b')
        plt.title(f"dist between {j+1} and {j+2} OW")
        plt.show()

    range = list(np.arange(10)) + [np.nan]

    for j in range:
        continue
        if np.isnan(j):
            ii = np.asarray(np.isnan(df))
        else:
            ii = np.asarray(df == j)

        # plt.hist(np.concatenate([d for d, f in zip(delta, df[ii]]), bins=200)
        # Q = np.concatenate([d for d, f in zip(dist, df) if f == j])
        Q = dist[ii,:]
        if Q.shape[0] > 0:
            plt.figure(figsize=(12, 6),)
            plt.hist(Q, bins=200, color='r')
            plt.title(f"num OW below gap {j+1}: ({np.count_nonzero(ii)/df.size*100} %)")
            plt.show()
        else:
            print(f"Nothing found for num OW below gap {j+1}")

    for j in range:

        if np.isnan(j):
            ii = np.asarray(np.isnan(df))
        else:
            ii = np.asarray(df == j)

        # plt.hist(np.concatenate([d for d, f in zip(delta, df[ii]]), bins=200)
        # Q = np.asarray([d[:10] for d, f in zip(delta, df) if f == j])
        Q = delta[ii,:]
        # plt.errorbar(np.arange(10)+1, np.mean(Q, axis=0), yerr=np.std(Q, axis=0), color='g')
        # plt.violinplot(Q, np.arange(10)+1, points=20, widths=0.3,
        #               showmeans=True, showextrema=True, showmedians=True)
        if Q.shape[0] > 0:
            plt.figure(figsize=(12, 6),)
            plt.boxplot(Q)
            plt.violinplot(Q, np.arange(Q.shape[1])+1, points=100, widths=0.3, showextrema=False)#,
                          #showmeans=True, showextrema=True, showmedians=True)
            plt.title(f"num OW below gap {j+1}: ({np.count_nonzero(ii)/df.size*100} %)")
            plt.ylabel("delta")
            plt.show()
        else:
            print(f"Nothing found for num OW below gap {j+1}")

    for j in range:

        if np.isnan(j):
            ii = np.asarray(np.isnan(df))
        else:
            ii = np.asarray(df == j)

        # plt.hist(np.concatenate([d for d, f in zip(delta, df[ii]]), bins=200)
        # Q = np.asarray([D[:10] for D, f in zip(DELTA, df) if f == j])
        Q = DELTA[ii,:]
        # plt.errorbar(np.arange(10)+1, np.mean(Q, axis=0), yerr=np.std(Q, axis=0), color='g')
        if Q.shape[0] > 0:
            plt.figure(figsize=(12, 6),)
            plt.boxplot(Q)
            plt.violinplot(Q, np.arange(Q.shape[1])+1, points=100, widths=0.3, showextrema=False)#,
                          #showmeans=True, showextrema=True, showmedians=True)
            plt.title(f"num OW below gap {j+1}: ({np.count_nonzero(ii)/df.size*100} %)")
            plt.ylabel("DELTA")
            plt.show()
        else:
            print(f"Nothing found for num OW below gap {j+1}")

# @timer
def split_shells(pos, lab, L, N, plot=False):

    dist, delta, DELTA, df = compute_differences(pos, lab)

    cf = np.zeros_like(df)
    for i, (l, DF) in enumerate(zip(lab, df)):
        if np.isnan(DF):
            cf[i] = np.iinfo(np.int8).min / 2
            df[i] = np.iinfo(np.int8).min / 2
        else:
            DF = int(DF)
            cf[i] = np.count_nonzero(l[:(DF + 1)] == L)

    if plot is True:
        plot_differences(dist, delta, DELTA, df)

    return df + 1, cf


@timer
@nb.jit
def sort_and_trim_data(pos, lab, L0, L1, num_neighbors):
    for i, (p, l) in enumerate(zip(pos, lab)):
        ii = np.logical_or(l == L0, l == L1)
        p = p[ii]
        l = l[ii]

        rr = np.linalg.norm(p, axis=1)
        jj = np.argsort(rr)

        pos[i] = p[jj[:num_neighbors+2]]
        lab[i] = l[jj[:num_neighbors+2]]
    return pos, lab


# @nb.jit(nopython=True)
def sort_and_trim(p, l, L0, L1, num_neighbors):

    ii = np.logical_or(l == L0, l == L1)

    p = p[ii]
    l = l[ii]

    rr = np.linalg.norm(p, axis=1)
    # rr = np.zeros(p.shape[0])
    # for i in np.arange(p.shape[0]):
    #     for j in np.arange(p.shape[1]):
    #         rr[i] += p[i,j]
    #
    # assert(np.all(rrr == rr))

    jj = np.argsort(rr)

    return p[jj[:num_neighbors+2]], l[jj[:num_neighbors+2]]

@timer
def load_and_preprocess_data(folder_neighborhoods, num_neighbors, L, chunk_size=int(1e5)):

    with h5py.File(folder_neighborhoods / "neighborhoods.h5", 'r') as f:

        N = f['positions'].shape[0]
        assert N == f['labels'].shape[0]

        pos = np.empty((N, num_neighbors + 2, 3), dtype=np.float32)
        lab = np.empty((N, num_neighbors + 2), dtype=np.uint8)

        myrange = np.unique(np.hstack([np.arange(0, N, chunk_size, dtype=int), N]))

        # import pdb; pdb.set_trace()

        for j, k in tqdm(zip(myrange[:-1], myrange[1:]), total=myrange.size-1):
            # print(j, k)
            for i, (p, l) in enumerate(zip(f['positions'][j:k], f['labels'][j:k])):

                assert l.dtype == 'uint8'
                assert p.dtype == 'float32'

                # print(j+i, p)

                p = np.reshape(p, (-1, 3))

                p, l = sort_and_trim(p, l, L[0], L[1], num_neighbors)

                pos[j+i,:,:] = p
                lab[j+i,:] = l

    # assert pos.shape[2] == 3
    # assert pos.shape[:2] == lab.shape

    return pos, lab


def core(pos, lab, num_neighbors, plot):

    assert num_neighbors > 0

    print(pos.shape)

    configuration = -1 * np.ones((pos.shape[0], 2), dtype='int8')

    # split by number of OW in data
    configs = [
        np.where(group_by_count(pos, lab, encode_labels('OW'), i, np.arange(num_neighbors)))[0]
        for i in tqdm(np.arange(num_neighbors + 1))]


    for i, conf in tqdm(enumerate(configs)):

        # print(conf.shape)

        if conf.size > 0:
            df, cf = split_shells(pos[conf], lab[conf], encode_labels('OW'), num_neighbors, plot=plot)

            if ((np.any(df < np.iinfo(np.int8).min) or
                np.any(df > np.iinfo(np.int8).max)) or
                (np.any(cf < np.iinfo(np.int8).min) or
                np.any(cf > np.iinfo(np.int8).max))):
                raise(ValueError("Potential overflow"))

            configuration[conf, 0] = cf # how many OW in the first min{C, 6} atoms where C is the coordination number
            configuration[conf, 1] = df # coordination number

        N = num_neighbors

        A = np.zeros((N,N), dtype=int)
        for i in np.arange(N):
            for j in np.arange(N):
                A[i,j] = np.count_nonzero(np.logical_and(
                    configuration[conf,0] == i,
                    configuration[conf,1] == j))
        # print(A)

    return configuration

def main(CONFIG, num_neighbors=7, plot=False):

    # --------------------------------------------------------------------------

    L = [encode_labels('OW'), encode_labels('CL')]

    # Load data, keep only OW and CL, sort by distance form center, trim to num_neighbors

    # pos, lab = load_data(CONFIG.folder_neighborhoods)
    # pos, lab = sort_and_trim_data(pos, lab, L[0], L[1], num_neighbors)

    pos, lab = load_and_preprocess_data(CONFIG.folder_neighborhoods, num_neighbors, L)

    configuration = core(pos, lab, num_neighbors, plot)

    # for i, _ in enumerate(configs):
    #     print(np.count_nonzero(configuration[:,0] == i))

    N = num_neighbors

    A = np.zeros((N,N), dtype=int)
    for i in np.arange(N):
        for j in np.arange(N):

            ind = np.logical_and(
                configuration[:,0] == i,
                configuration[:,1] == j)

            A[i,j] = np.count_nonzero(ind)

    print("Column: coordination number (starting at 0)")
    print("Row: how many OW in the first min{C, 6} atoms where C is the coordination number")
    print(A)

    print(f"Not decided: {np.count_nonzero(configuration[:,0] < 0)} (out of {configuration.shape[0]})")
    # print(configuration.shape)

    CONFIG.folder_hyd_shells.mkdir(parents=False, exist_ok=True)
    np.save(CONFIG.folder_hyd_shells / 'shells_distances.npy', configuration)


if __name__ == '__main__':

    CONFIG = config.Config(stride=100, CATION='K')
    main(CONFIG, num_neighbors=10, plot=True)
