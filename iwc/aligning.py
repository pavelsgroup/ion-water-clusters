# coding: utf-8
import os
import warnings
import numpy as np
# import scipy
import itertools
import time
import pprint
import numba
from numba import jit
from iwc.common.common_numba import is_sorted, is_all_zero, factorial
from iwc.common.common import logger, notqdm, disp_title, timer, split, chunk_loader, encode_labels, decode_labels
from iwc.perms import generate_recursive_multigroup_factory, permutation_multigroup, swap, roll_right, is_odd
import iwc.config as config

from multiprocessing import Manager

# from IPython import get_ipython
# if get_ipython() is not None:
#     get_ipython().run_line_magic('load_ext', 'autoreload')
#     get_ipython().run_line_magic('autoreload', '2')
# 
#     get_ipython().run_line_magic('matplotlib', 'inline')
#     # matplotlib.use('Qt5Agg')

try:
    from joblib import Parallel, delayed
except ModuleNotFoundError:
    # ImportWarning
    logger.warning("Module joblib not found, parallel processing will be disabled")

try:
    import tqdm
except ModuleNotFoundError:
    logger.warning("Module tqdm not found, progress bar will be disabled")
    tqdm = notqdm

disable_tqdm = bool(os.environ.get("DISABLE_TQDM"))
if disable_tqdm is True:
    logger.info("Encountered environment variable DISABLE_TQDM, disabling tqdm")
    tqdm = notqdm

pp = pprint.PrettyPrinter(indent=4)

"""

conda install -c conda-forge numpy scipy matplotlib tqdm joblib pandas seaborn numba hashlib


conda install -c conda-forge ipykernel
python -m ipykernel install --user --name iwc

Profiling:

python -m cProfile -o clustering.profile analysis.py -

snakeviz clustering.profile

"""

# ------------------------------------------------------------------------------

# np.set_printoptions(precision=2, suppress=True, linewidth=np.inf)

class IncompatiblePermutationError(ValueError): pass


@jit(nopython=True)
def euclidean_dist_square(x, y):

    diff = x - y
    r = np.empty((diff.shape[0], diff.shape[1],))

    for i in range(diff.shape[0]):
        for j in range(diff.shape[1]):
            r[i,j] = 0
            for k in range(diff.shape[2]):
                r[i,j] += np.square(diff[i,j,k])
    return r

@jit(nopython=True)
def dist_add(x, y, out=None, diff=None):
    """
    Calculates square distance
    out[i] = sum_{j} sqrt( sum_{k} (x[i,j,k] - y[i,j,k])^2 )

    x.shape = (n, m, d,) or (m, d,)
    y.shape = (n, m, d,) or (m, d,)
    out.shape = (n,)
    where
        n ... number of datasets
        m ... size of each dataset
        d ... dimension
    """

    assert x.shape[-1] == 3
    assert y.shape[-1] == 3

    # diff = x - y
    # np.subtract(x, y, out=diff) # this does not work with numba.jit
    np.subtract(x, y, diff)

    for i in range(diff.shape[0]):
        out[i] = 0
        for j in range(diff.shape[1]):
            d = 0
            for k in range(diff.shape[2]):
                d += np.square(diff[i,j,k])
            out[i] += np.sqrt(d)

@jit(nopython=True, fastmath=True)
def dist_square(x, y, out=None, diff=None):
    """
    Calculates square distance
    out[i] = sum_{j,k} (x[i,j,k] - y[i,j,k])^2

    x.shape = (n, m, d,) or (m, d,)
    y.shape = (n, m, d,) or (m, d,)
    out.shape = (n,)
    where
        n ... number of datasets
        m ... size of each dataset
        d ... dimension
    """
    # print('x', x.shape)
    # print('y', y.shape)

    diff = y - x
    # np.subtract(x, y, out=diff) # this does not work with numba.jit
    # np.subtract(x, y, diff)

    # print('diff', diff.shape)
    # print('out', out.shape)

    dt = x.dtype

    for i in range(diff.shape[0]):
        out[i] = dt.type(0.0)
        for j in range(diff.shape[1]):
            for k in range(diff.shape[2]):
                out[i] += np.square(diff[i,j,k])
                # out[i] += (diff[i,j,k])**2
    return out


@jit(nopython=True, fastmath=True)
def dist_square_inplace(x, y, out=None):
    """
    Calculates square distance
    out[i] = sum_{j,k} (x[i,j,k] - y[j,k])^2

    x.shape = (n, m, d,)
    y.shape = (m, d, d,)
    out.shape = (n,)
    where
        n ... number of datasets
        m ... size of each dataset
        d ... dimension
    """

    dt = x.dtype

    for i in range(x.shape[0]):
        out[i] = dt.type(0.0)
        for j in range(x.shape[1]):
            for k in range(x.shape[2]):
                out[i] += np.square(x[i,j,k] - y[i,j,k])
                # out[i] += (x[i,j,k] - y[i,j,k])**2
    return out

@jit(nopython=True, fastmath=True)
def dist_square_inplace_broadcast(x, y, out=None):
    """
    Calculates square distance
    out[i] = sum_{j,k} (x[i,j,k] - y[j,k])^2

    x.shape = (n, m, d,)
    y.shape = (m, d,)
    out.shape = (n,)
    where
        n ... number of datasets
        m ... size of each dataset
        d ... dimension
    """

    dt = x.dtype

    for i in range(x.shape[0]):
        out[i] = dt.type(0.0)
        for j in range(x.shape[1]):
            for k in range(x.shape[2]):
                out[i] += np.square(x[i,j,k] - y[j,k])

    return out

# II = 3
# @jit(nopython=True)
def rigid_transform_3D_vectorized(At, B, H, out=None):
    """
    B is 2D array (matrix). It is broadcaseted along the 0-axis of At.
    """

    # R = out
    Rt = out

    # assert At.shape[0] == B.shape[2] # no need to check, B gets broadcaseted
    assert At.shape[1] == B.shape[1]#, f"{At.shape} {B.shape}"
    assert At.shape[2] == B.shape[0]#, f"{At.shape} {B.shape}"

    np.matmul(At, B, out=H)

    # print(":::H"), print(H[II,:,:])

    U, S, Vt = np.linalg.svd(H)#, full_matrices=False)

    # print(":::U"), print(U[II,:,:])

    # print(":::Vt"), print(Vt[II,:,:])

    # V = np.transpose(Vt, (0, 2, 1))
    # Ut = np.transpose(U, (0, 2, 1))
    #
    # np.matmul(V, Ut, out=R)
    np.matmul(U, Vt, out=Rt)

    # print(":::Rt"), print(Rt[II,:,:])

    # assert(np.all(np.transpose(R, (0, 2, 1)) == np.matmul(U, Vt)))

    # There’s a special case when finding the rotation matrix that we have to
    # take care of. Sometimes the SVD will return a ‘reflection’ matrix, which
    # is numerically correct but is actually nonsense in real life.
    # https://nghiaho.com/?page_id=671

    # Other reference:
    # D.W. Eggert, A. Lorusso, R.B. Fisher
    # Estimating 3-D rigid body transformations: a comparisonof four major algorithms
    # Machine Vision and Applications (1997) 9: 272–290
    # https://graphics.stanford.edu/~smr/ICP/comparison/eggert_comparison_mva97.pdf

    # ind = np.linalg.det(R) < 0
    ind = np.linalg.det(Rt) < 0

    # if determinant(R) < 0
    #     multiply 3rd column of V by -1
    #     recompute R
    # end if
    
    if METHOD == 0: 
        for i in np.where(ind)[0]:
            Z = np.eye(3)
            Z[2,2] = np.linalg.det(np.dot(U[i,:,:], Vt[i,:,:]))
            R = np.dot(np.dot(U[i,:,:], Z), Vt[i,:,:])
            Rt[i,:,:] = R
    elif METHOD == 1:
        # V[ind, :, 2] *= -1
        Vt[ind, 2, :] *= -1
        # R[ind,:,:] = np.matmul(V[ind,:,:], Ut[ind,:,:])
        Rt[ind,:,:] = np.matmul(U[ind,:,:], Vt[ind,:,:])

    # Alternative check (not sure which one is correct)
    #
    # if determinant(R) < 0
    #     multiply 3rd column of R by -1
    # end if

    elif METHOD == 2:
        # R[ind,:,2] *= -1
        Rt[ind,2,:] *= -1

    elif METHOD == 3:
        for i in np.where(ind)[0]:
            #print(i)
            TrR = np.trace(Rt[i,:,:])
            #print(TrR)
            costheta = (TrR + 1)/2
            sintheta = np.sqrt((3+TrR)*(1-TrR))/2
            k = np.asarray([Rt[i,1,2] - Rt[i,2,1], Rt[i,2,0] - Rt[i,0,2], Rt[i,0,1] - Rt[i,1,0]]) / np.sqrt((3+TrR)*(1-TrR))
            #print(k)
            K = np.array([[0, -k[2], k[1]], [k[2],  0, -k[0]], [-k[1], k[0], 0]])

            R1 = np.eye(3) + sintheta * K + (1-costheta) * np.matmul(K, K)
            R2 = np.array([
                [costheta + k[0]*k[0]*(1-costheta),   k[0]*k[1]*(1-costheta) - k[2]*sintheta,   k[0]*k[2]*(1-costheta) + k[1]*sintheta],
                [k[0]*k[1]*(1-costheta) + k[2]*sintheta,   costheta + k[1]*k[1]*(1-costheta),   k[1]*k[2]*(1-costheta) - k[0]*sintheta],
                [k[0]*k[2]*(1-costheta) - k[1]*sintheta,   k[1]*k[2]*(1-costheta) + k[0]*sintheta,   costheta + k[2]*k[2]*(1-costheta)]
                ])
            # print(R1-R2)

            Rbar = np.eye(3)
            for ii in np.arange(3):
                for jj in np.arange(3):
                    Rbar[ii,jj] -= 2*k[ii]*k[jj]

            Rt[i,:,:] = R2.T

    elif METHOD == 4:
        for i in np.where(ind)[0]:
            
            AAt = At[i,:,:]
            Rt[ind,2,:] *= -1

            for j in np.arange(10):
                theta = np.random.rand(1)
                k = np.random.rand(3)
                k /= np.linalg.norm(k)
                K = np.array([[0, -k[2], k[1]], [k[2],  0, -k[0]], [-k[1], k[0], 0]])
                R = np.eye(3) + np.sin(theta) * K + (1-np.cos(theta)) * np.matmul(K, K)

                Rnew = rigid_transform_3D(np.transpose(np.matmul(R, AAt)), B)
                Rnew = np.transpose(np.matmul(Rnew, R))

                if np.linalg.det(Rnew) > 0:
                    Rt[i,:,:] = Rnew
                    print("success")
                    break


    # print(":::Rt"), print(Rt[II,:,:])

    # print(ind)

    # out = np.transpose(Rt, (0, 2, 1)) # this will not work !! WHY?
    out = Rt

    # return R

#@jit(nopython=True)
def align_vectorized(A, At, Bt, D, B, H, Rt, diff):
    """
    roughly equivalent to:

    D = align_vectorized(A, At, Bt)

    arguments B, H, R, diff are just allocated memory
    """
    # Find optimal rotation matrix R (array of matrices)
    # writes to R, uses H as a memory
    try:
        rigid_transform_3D_vectorized(Bt, A, H, out=Rt)
    except np.linalg.LinAlgError as err:
        if 'SVD did not converge' in str(err):
            logger.critical("Caught numpy.linalg.LinAlgError('SVD did not converge') in align_vectorized")
            D = np.full_like(D, np.inf)
        else:
            raise

    # print(":::Rt"), print(Rt[II,:,:])

    # Perfom the tranformation
    # writes to B
    np.matmul(np.transpose(Rt, (0, 2, 1)), Bt, out=B)

    # print(":::B"), print(B[II,:,:])

    # Rn = np.empty_like(R)
    # rigid_transform_3D_vectorized(B, A, H, out=Rn)

    # print(Rn)

    # B = np.matmul(Rn, B)

    # R = np.matmul(np.transpose(Rn, (0, 2, 1)), np.transpose(R, (0, 2, 1)))

    # Calculate the distance between transformed arrays B and the refference At
    # Since later we are interested only in comparison of the distances, we can
    # compute the distance^2 which is faster

    # writes to D, uses diff as a memory
    dist_square(At, B, out=D, diff=diff)
    # dist_square_inplace_broadcast(B, At, out=D)
    
    # print(D)
    # print(np.argmin(D), np.min(D))
    # print(R[np.argmin(D),:,:])
    # print(B[np.argmin(D),:,:])
    
    # these are alternatives:
    # np.sum(np.linalg.norm(At - B, axis=1), axis=1, out=D)
    # np.sum(np.square(np.linalg.norm(At - B, axis=1)), axis=1, out=D)
    # D = np.sum(euclidean_dist_square(At, B), axis=1)
    # return D


METHOD = 1

@jit(nopython=True)
def rigid_transform_3D_t(At, B):
    
    assert At.shape[0] == 3#, f"A.shape = {A.shape}"
    assert At.T.shape == B.shape#, f"A.shape = {A.shape} , B.shape = {B.shape}"

    H = np.dot(At, B)
    # H = np.matmul(A.T, B)

    # print("H"), print(H)

    U, S, Vt = np.linalg.svd(H)

    # print("U"), print(U)
    # print("Vt"), print(Vt)

    R = np.dot(Vt.T, U.T)
    # R = np.matmul(Vt.T, U.T)
    
    # print("R"), print(R)

    # special reflection case
    if np.linalg.det(R) < 0:
        if METHOD == 0:
            #Z = np.eye(3, dtype=np.float32)
            Z = np.array([[1.0,0.0,0.0],[0.0,1.0,0.0],[0.0,0.0,1.0]], dtype=np.float32)
            Z[2,2] = np.linalg.det(np.dot(U, Vt))
            R = np.dot(np.dot(U, Z), Vt)

        if METHOD == 1:
            Vt[2, :] *= -1
            # Vt[:, 2] *= -1
            R = np.dot(Vt.T, U.T)
            # R = np.matmul(Vt.T, U.T)
            
        elif METHOD == 2 or METHOD == 4:
            # R[2,:] *= -1
            R[:,2] *= -1
        elif METHOD == 3:
            
            TrR = np.trace(R)
            
            costheta = (TrR + 1)/2
            sintheta = np.sqrt((3+TrR)*(1-TrR))/2
            k = np.array([R[2,1] - R[1,2], R[0,2] - R[2,0], R[1,0] - R[0,1]], dtype=np.float32) / np.sqrt((3+TrR)*(1-TrR))
            
            K = np.array([[0, -k[2], k[1]], [k[2],  0, -k[0]], [-k[1], k[0], 0]], dtype=np.float32)

            # R1 = np.eye(3) + sintheta * K + (1-costheta) * np.dot(K, K)
            R2 = np.array([
                [costheta + k[0]*k[0]*(1-costheta),   k[0]*k[1]*(1-costheta) - k[2]*sintheta,   k[0]*k[2]*(1-costheta) + k[1]*sintheta],
                [k[0]*k[1]*(1-costheta) + k[2]*sintheta,   costheta + k[1]*k[1]*(1-costheta),   k[1]*k[2]*(1-costheta) - k[0]*sintheta],
                [k[0]*k[2]*(1-costheta) - k[1]*sintheta,   k[1]*k[2]*(1-costheta) + k[0]*sintheta,   costheta + k[2]*k[2]*(1-costheta)]
                ], dtype=np.float32)
            # print(R1-R2)
            R = R2

    assert (np.linalg.det(R) > 0)
    
    # print("R"), print(R)

    return R

@jit(nopython=True)
def rigid_transform_3D(A, B):
    
    assert A.shape[1] == 3#, f"A.shape = {A.shape}"
    assert A.shape == B.shape#, f"A.shape = {A.shape} , B.shape = {B.shape}"

    H = np.dot(A.T, B)
    # H = np.matmul(A.T, B)

    # print("H"), print(H)

    U, S, Vt = np.linalg.svd(H)

    # print("U"), print(U)
    # print("Vt"), print(Vt)

    R = np.dot(Vt.T, U.T)
    # R = np.matmul(Vt.T, U.T)
    
    # print("R"), print(R)

    # special reflection case
    if np.linalg.det(R) < 0:
        if METHOD == 0:
            #Z = np.eye(3, dtype=np.float32)
            Z = np.array([[1.0,0.0,0.0],[0.0,1.0,0.0],[0.0,0.0,1.0]], dtype=np.float32)
            Z[2,2] = np.linalg.det(np.dot(U, Vt))
            R = np.dot(np.dot(U, Z), Vt)

        if METHOD == 1:
            Vt[2, :] *= -1
            # Vt[:, 2] *= -1
            R = np.dot(Vt.T, U.T)
            # R = np.matmul(Vt.T, U.T)
            
        elif METHOD == 2 or METHOD == 4:
            # R[2,:] *= -1
            R[:,2] *= -1
        elif METHOD == 3:
            
            TrR = np.trace(R)
            
            costheta = (TrR + 1)/2
            sintheta = np.sqrt((3+TrR)*(1-TrR))/2
            k = np.array([R[2,1] - R[1,2], R[0,2] - R[2,0], R[1,0] - R[0,1]], dtype=np.float32) / np.sqrt((3+TrR)*(1-TrR))
            
            K = np.array([[0, -k[2], k[1]], [k[2],  0, -k[0]], [-k[1], k[0], 0]], dtype=np.float32)

            # R1 = np.eye(3) + sintheta * K + (1-costheta) * np.dot(K, K)
            R2 = np.array([
                [costheta + k[0]*k[0]*(1-costheta),   k[0]*k[1]*(1-costheta) - k[2]*sintheta,   k[0]*k[2]*(1-costheta) + k[1]*sintheta],
                [k[0]*k[1]*(1-costheta) + k[2]*sintheta,   costheta + k[1]*k[1]*(1-costheta),   k[1]*k[2]*(1-costheta) - k[0]*sintheta],
                [k[0]*k[2]*(1-costheta) - k[1]*sintheta,   k[1]*k[2]*(1-costheta) + k[0]*sintheta,   costheta + k[2]*k[2]*(1-costheta)]
                ], dtype=np.float32)
            # print(R1-R2)
            R = R2

    assert (np.linalg.det(R) > 0)
    
    # print("R"), print(R)

    return R


@jit(nopython=True)
def align_numba(A, B):
    # assert A.dtype == np.float32
    # assert B.dtype == np.float32
    
    R = rigid_transform_3D(B, A)

    # print("R"), print(R)
    B = np.dot(R, B.T)
    # B = np.matmul(R, B.T)
    # print("B"), print(B)
    B = B.T
    return R, B

#@jit(nopython=True)
def align(A, B):
    assert A.dtype == np.float32
    assert B.dtype == np.float32

    try:
        R = rigid_transform_3D(B, A)
    except np.linalg.LinAlgError as err:
        if 'SVD did not converge' in str(err):
            logger.critical("Caught numpy.linalg.LinAlgError('SVD did not converge') in align")
            R = np.eye(3, dtype='float32')
        elif 'Array must not contain infs or NaNs' in str(err):
            logger.critical("Caught numpy.linalg.LinAlgError('Array must not contain infs or NaNs') in align")
            R = np.eye(3, dtype='float32')
        else:
            raise # not yet supported by numba
    
    # print("R"), print(R)
    B = np.dot(R, B.T)
    # B = np.matmul(R, B.T)
    # print("B"), print(B)
    B = B.T
    return R, B

@jit(nopython=True)
def rigid_transform_3D_f(A, B, R, H):
    assert A.shape == B.shape

    # H = np.dot(A.T, B)
    H = np.dot(A.T, B, out=H)

    U, S, Vt = np.linalg.svd(H)

    # R = np.dot(Vt.T, U.T)
    R = np.dot(Vt.T, U.T, out=R)

    # R = np.matmul(Vt.T, U.T)

    # special reflection case
    if np.linalg.det(R) < 0:
        Vt[2, :] *= -1
        # R = np.dot(Vt.T, U.T)
        np.dot(Vt.T, U.T, out=R)

    return R

@jit(nopython=True)
def align_f(A, B, R, H):

    R = rigid_transform_3D_f(B, A, R, H)

    B = np.dot(R, B.T)
    B = B.T

    return R, B

def check_permutation_set_sizes(perm_sets_sizes, perm_size_limit: int=12):
    if np.any(np.asarray(perm_sets_sizes) > perm_size_limit):
        raise(ValueError(f"Permutation subset(s) too large: {perm_sets_sizes}"))


def align_through_permutation(AA, B, perm_sets, perm_sets_sizes, M, m, t,
        DD, BBBt, B_mem, H_mem, R_mem, diff_mem):

    assert AA.shape[1] == 3
    assert B.shape[1] == 3

    it_perm_sets = [itertools.permutations(s) for s in perm_sets]
    it_pool = tuple(itertools.product(*it_perm_sets))

    # currently dA is unsupported
    # dA = np.linalg.norm(AA, axis=1) ** 2
    # dA = 1
    # D[i] = np.sum(np.linalg.norm(AA - BB, axis=1) / dA)

    # --------------------------------------------------------------------------
    # Version 0
    #
    # # M = np.product([np.math.factorial(n) for n in perm_sets_sizes])
    # D = np.zeros(M)
    #
    # # R = np.empty((3,3), dtype=np.float32)
    # # H_mem = np.empty((3,3), dtype=np.float32)
    #
    # for i, perm in enumerate(it_pool):
    #     perm = np.asarray(np.concatenate(list(perm)), dtype=int)
    #     BB = B[perm, :]
    #
    #     R, BB = align(AA, BB)
    #     # R, BB = align_f(AA, BB, R, H_mem)
    #
    #     # Find the error
    #     # D[i] = np.sum(np.linalg.norm(AA - BB, axis=1))
    #     D[i] = np.sum(np.square(np.linalg.norm(AA - BB, axis=1)))
    #
    # ind_min = D.argmin()

    # --------------------------------------------------------------------------
    # Version 1 (vectorised)
    #
    Bt = B.T
    # start = time.time()
    BBBt = np.stack([Bt[:,np.asarray(np.concatenate(list(perm)), dtype=int)]
                for perm in it_pool], axis=0, out=BBBt)
    # t[0] += time.time() - start
    # start = time.time()

    # writes to DD
    align_vectorized(AA, AA.T, BBBt, DD, B_mem, H_mem, R_mem, diff_mem)
    # t[1] += time.time() - start

    ind_min = DD.argmin()

    # print(D)
    # print(DD)
    # assert(DD.argmin() == D.argmin())
    # assert(np.all(abs(DD - D)/D < 1e-3))
    # --------------------------------------------------------------------------

    perm = it_pool[ind_min]
    perm = np.asarray(np.concatenate(list(perm)), dtype=int)

    BB = B[perm, :]

    # BB = align(AA, BB, R, H_mem)
    R, BB = align(AA, BB)

    return perm, R, BB, DD[ind_min], t


@timer
def normalized_permutations(perm_sets_sizes):

    perm_sets = []
    i = 0
    for n in perm_sets_sizes:
        perm_sets.append(np.arange(n) + i)
        i += n

    # use_itertools = True
    use_itertools = False

    if use_itertools is True:
        it_perm_sets = [itertools.permutations(s) for s in perm_sets]
        it_pool = tuple(itertools.product(*it_perm_sets))

        perms = [np.asarray(np.concatenate(list(perm)), dtype=int)
                    for perm in it_pool]
    else:
        _, all_perms = permutation_multigroup(perm_sets, dtype=np.int8)
        perms = [all_perms[i, :] for i in np.arange(all_perms.shape[0])]

    perms = np.array(perms, dtype=np.int8)
    perm_sets = np.array(perm_sets)

    return perms, perm_sets


@jit(nopython=True)#, fastmath=True)
def calculate_invariant(A, out=None):
    n = A.shape[1]

    # out = np.zeros((n+1,n,))

    # distance from center (this makes sense as there is actually another atom in the center)
    for i in range(n):
        out[0,i] = 0
        for k in range(3):
            out[0,i] += np.square(A[k,i])

    # pariwise distance
    for i in range(n):
        for j in range(i+1,n):
            out[i+1,j] = 0
            for k in range(3):
                out[i+1,j] += np.square(A[k,i] - A[k,j])

    return np.sqrt(out)

# @jit("int8(float32[:,:], float32[:,:], int64[:], float64)", nopython=True, fastmath=True)
@jit(nopython=True)#, fastmath=True) # this decorator seems faster then specifying the types explicitely
def check_permutation_precalc(it, I, B, perm, tol, verbose):
    n = B.shape[1]

    dt = B.dtype

    # distance from center (this makes sense as there is actually another atom in the center)
    for i in range(n):
        p = dt.type(0.0)
        for k in range(3):
            p += np.square(B[k,perm[i]])
        if np.abs(np.sqrt(p) - I[0,i]) > tol:
            # if verbose:
            #     print("checking F1", it, i, perm)
            return 0

    # pariwise distance
    for i in range(n):
        for j in range(i+1,n):
            p = dt.type(0.0)
            for k in range(3):
                p += np.square(B[k,perm[i]] - B[k,perm[j]])
            if np.abs(np.sqrt(p) - I[i+1,j]) > tol:
                # print(p, r)
                # if verbose:
                #     print("checking F2", it, i, j, perm)
                return 0

    # if verbose:
    #     print("checking OK", it, perm)
    return 1

@jit(nopython=True)#, fastmath=True)
def check_permutation(A, B, perm, tol):

    n = A.shape[1]

    dtA = A.dtype
    dtB = B.dtype

    # distance from center (this makes sense as there is actually another atom in the center)
    for i in range(n):
        r = dtA.type(0.0)
        p = dtB.type(0.0)
        for k in range(3):
            r += np.square(A[k,i])
            p += np.square(B[k,perm[i]])
        if np.abs(np.sqrt(p) - np.sqrt(r)) > tol:
            return 0

    # pariwise distance
    for i in range(n):
        for j in range(i+1,n):
            r = dtA.type(0.0)
            p = dtB.type(0.0)
            for k in range(3):
                r += np.square(A[k,i] - A[k,j])
                p += np.square(B[k,perm[i]] - B[k,perm[j]])
            if np.abs(np.sqrt(p) - np.sqrt(r)) > tol:
                # print(p, r)
                return 0

    return 1

@jit(nopython=True)#, fastmath=True)
def check_permutation_partial_precalc(it, perm, i, I, B, tol):#, l):
    n = B.shape[1]

    dt = B.dtype

    # assert n == perm.size

    # distance from center (this makes sense as there is actually another atom in the center)
    p = dt.type(0.0)
    for k in range(3):
        p += np.square(B[k,perm[i]])
    if np.abs(np.sqrt(p) - I[0,i]) > tol:
        # print("checking F1", it, i, perm, perm[i:])
        return False

    # pariwise distance
    for j in range(i+1,n):
        p = dt.type(0.0)
        for k in range(3):
            p += np.square(B[k,perm[i]] - B[k,perm[j]])
        if np.abs(np.sqrt(p) - I[i+1,j]) > tol:
            # print(p, r)
            # print("checking F2", it, i, j, perm, perm[i:])
            return False

    # print("checking OK", it, i, perm, perm[i:])
    return True

cfun = check_permutation_partial_precalc
gfun = generate_recursive_multigroup_factory(
        outfun_switch=1, #
        checking=True,
        checkingfun=cfun)

def eliminate_permutations(A, B, norm_perms, norm_perm_sets, tol, ind, I):

    # assert(np.all(A.shape == B.shape))

    I = calculate_invariant(A, out=I)
    # with np.printoptions(precision=2, suppress=True, linewidth=np.inf):
    #     print(I)
    
    ver1 = False
    ver2 = False
    ver3 = True
    check_versions = False

    if ver1 is True:
        for it, perm in enumerate(norm_perms):
            # not really sure which is faster
            # ind[it] = check_permutation(A, B, perm, tol)
            
            verbose = False
            ind[it] = check_permutation_precalc(it, I, B, perm, tol, verbose)

            # J = calculate_invariant(B[:,perm])
            # with np.printoptions(precision=2, suppress=True, linewidth=np.inf):
            #     print(perm)
            #     print(J)

    if check_versions is True:
        tmp = np.copy(ind)
        ind = np.empty_like(ind)

    if ver2 is True:
        # This version excludes other permutations if we know they are same
        # up to the point where it failed in the previous step
        ind = partial_permutation_elimination(norm_perm_sets, ind, I, B, tol)

    if ver3 is True:
        # This version excludes other permutations if we know they are same
        # up to the point where it failed in the previous step
        _, ind = permutation_multigroup(norm_perm_sets, I, B, tol, out=ind, gfun=gfun)

    if check_versions is True:
        if not np.all(ind == tmp):
            ii = np.where(ind != tmp)
            print(ii)
            print(ind[ii])
            print(tmp[ii])
        assert np.all(ind == tmp)

    return ind

def partial_permutation_elimination(sets, ind, *args):

    # print(sets)

    A = [np.array(a) for a in sets]
    S = [a.size for a in A]
    M = np.cumsum([0] + S)
    J = np.array(M[:-1])
    K = np.array(M[1:])

    m = len(A) - 1
    j = J[m]
    k = K[m]

    A = np.concatenate(A)
    
    num_perms = np.product([np.math.factorial(s) for s in S])

    # ind = -1 * np.ones(num_perms, dtype=int)

    it = 0
    check = True

    it, ind = check_recursive_multigroup(m, j, k, J, K, A, it, ind, check, *args)

    return ind


@jit(nopython=True)
def check_recursive_multigroup(m, j, k, J, K, A, it, out, check_in, I, B, tol):
    """
        Generates permutations among groups of A defined by arrays J, K
            * J[1:] = K[:-1] are the borders between the groups
            * size of J (or K) is the number of groups
        m ... index of the group currently being processed
              has to be initialized to the number of groups (size of J, K)
        j ... starting and
        k ... ending index of A where to perform permutation
              has to be initialized to 
                j = starting index of last group
                k = ending index of last group

    """

    check = check_in

    if k == 1+j:

        if check_in is True:
            check = check_permutation_partial_precalc(it, A, k-1, I, B, tol)#, str(it)+'C')

        if m == 0:
            # print(A)
            out[it] = check
            it = it + 1
        else:
            if check is False:
                # N ... number of permutations to be performed
                # N = np.math.factorial(k - j - 1) # number of permutations in this group
                N = factorial(k - j - 1)
                for mm in range(m): # number of permutations in "lower" groups
                    # N = N * np.math.factorial(K[mm] - J[mm])
                    N = N * factorial(K[mm] - J[mm])
                # print('A', N)
                out[it:it+N] = 0
                it = it + N

            else:
                it, out = check_recursive_multigroup(m-1, J[m-1], K[m-1], J, K, A, it, out, check, I, B, tol)
            
                # This swap/roll is added such that the order of permutations
                # in lower groups is always the same. Strictly speaking, this 
                # is suboptimal in terms of performance, but allows us to make
                # a check here, skipping the generation of all the permutations
                # if necessary

                # NOTE: this needs further checking

                if is_odd(K[m-1] - J[m-1]):
                    swap(A, J[m-1], K[m-1]-1)
                else:
                    roll_right(A, J[m-1], K[m-1]-1)
    else:

        if check_in is True:
            check = check_permutation_partial_precalc(it, A, k-1, I, B, tol)#, str(it)+'A')

        # Generate permutations with kth unaltered
        # Initially k == length(A)
        it, out = check_recursive_multigroup(m, j, k-1, J, K, A, it, out, check, I, B, tol)

        # Generate permutations for kth swapped with each k-1 initial
        for i in range(j, k-1):
            # Swap choice dependent on parity of k (even or odd)
            if is_odd(k + j):
                swap(A, j, k-1)
            else:
                swap(A, i, k-1)
            
            if check_in is True:
                check = check_permutation_partial_precalc(it, A, k-1, I, B, tol)#, str(it)+'B')
            
            it, out = check_recursive_multigroup(m, j, k-1, J, K, A, it, out, check, I, B, tol)

    return it, out

#def gather_acceptable_perms():

def align_through_normalized_permutations_heur(AA, B, original_perm_sets, norm_perms, M, m, t,
        DD, BBBt, B_mem, H_mem, R_mem, diff_mem, perms_ind_mem, inv_mem, norm_perm_sets, tol, tolstep, maxtol, max_perm_num):

    assert tolstep > 0
    assert tol <= maxtol
    assert tol > 0

    if np.any(AA == np.NaN) or np.any(B == np.NaN):
        # raise(ValueError("NaN vlaue encountered"))
        return None, None, None, np.Inf, t, 0

    ind = np.concatenate(original_perm_sets)

    Bt = B[ind,:].T

    start = time.time()
    acceptable_perms_ind = eliminate_permutations(AA.T, Bt, norm_perms, norm_perm_sets, tol, perms_ind_mem, inv_mem)
    t[0] += time.time() - start
    start = time.time()

    # print(acceptable_perms_ind.flags)

    # n = len(norm_perms)
    # print(n)

    all_zero = is_all_zero(acceptable_perms_ind)
    t[1] += time.time() - start
    start = time.time()

    if all_zero:
        NN = 0
        if tol + tolstep <= maxtol:
            tol += tolstep
            # print(f"Increasing tolerance to {tol}")
            return align_through_normalized_permutations_heur(AA, B, original_perm_sets, norm_perms, M, m, t,
                        DD, BBBt, B_mem, H_mem, R_mem, diff_mem, perms_ind_mem, inv_mem, norm_perm_sets, tol, tolstep, maxtol, max_perm_num)
        else:
            # print("It seems like no permutation is acceptable")
            return None, None, None, np.inf, t, NN
    else:
        # for perm, a in zip(norm_perms, acceptable_perms_ind):
        #     if a != 0:
        #         print(a, perm)        

        if False:
            # this version is slow
            acceptable_perms = ([perm for perm, a in zip(norm_perms, acceptable_perms_ind) if a])
            NN = len(acceptable_perms)
        else:
            acceptable_perms = norm_perms[acceptable_perms_ind,:]

        if len(acceptable_perms) == 0:
            return None, None, None, np.inf, t, NN
    
    t[2] += time.time() - start
    start = time.time()

    # print(acceptable_perms)
    # print(acceptable_perms_ind)
    
    N = len(acceptable_perms)

    assert N <= max_perm_num, f"N {N} > max_perm_num {max_perm_num}. Increase the max_perm_num limit."

    # print(f"{N}/{n}")
    # logger.debug(f"Acceptable permutations: {N}/{n} ({N/n*100:.1f} %)")

    BBBt[:N,:,:] = np.stack([Bt[:,perm] for perm in acceptable_perms], axis=0, out=BBBt[:N,:,:])
    t[3] += time.time() - start
    start = time.time()

    # logger.debug("Starting alignment")

    # writes to DD
    align_vectorized(AA, AA.T, BBBt[:N,:,:], DD[:N], B_mem[:N,:,:], H_mem[:N,:,:], R_mem[:N,:,:], diff_mem[:N,:,:])
    t[4] += time.time() - start
    start = time.time()

    ind_min = DD[:N].argmin()

    min_perm = acceptable_perms[ind_min]
    perm = ind[min_perm]

    BB = B[perm, :]

    # print('AA')
    # print(np.abs(np.asarray(centroid_invariant(AA)) - np.asarray(centroid_invariant(BB))))

    if np.any(AA == np.NaN) or np.any(BB == np.NaN):
        # raise(ValueError("NaN vlaue encountered"))
        return None, None, None, np.Inf, t, 0

    # BB = align(AA, BB, R, H_mem)
    R, BB = align(AA, BB)
    t[5] += time.time() - start

    return perm, R, BB, DD[ind_min], t, N


def align_through_normalized_permutations(AA, B, original_perm_sets, norm_perms, M, m, t,
        DD, BBBt, B_mem, H_mem, R_mem, diff_mem):

    if np.any(AA == np.NaN) or np.any(B == np.NaN):
        # raise(ValueError("NaN vlaue encountered"))
        return None, None, None, np.Inf, t, 0

    ind = np.concatenate(original_perm_sets)

    Bt = B[ind,:].T
    # start = time.time()
    BBBt = np.stack([Bt[:,perm] for perm in norm_perms], axis=0, out=BBBt)
    # t[0] += time.time() - start
    # start = time.time()

    # writes to DD
    align_vectorized(AA, AA.T, BBBt, DD, B_mem, H_mem, R_mem, diff_mem)
    # t[1] += time.time() - start

    ind_min = DD.argmin()

    min_perm = norm_perms[ind_min]
    perm = ind[min_perm]

    BB = B[perm, :]

    if np.any(AA == np.NaN) or np.any(BB == np.NaN):
        # raise(ValueError("NaN vlaue encountered"))
        return None, None, None, np.Inf, t, 0

    # BB = align(AA, BB, R, H_mem)
    R, BB = align(AA, BB)

    N = len(DD)

    return perm, R, BB, DD[ind_min], t, N


def main_align_core(pos, lab, N, shared_variables, reference,
        perm_sets_ref, perm_sets_sizes_ref, perm_sets_labels_ref, perm_subsets_fun,
        return_distance=False, return_positions=False, return_labels=False,
        return_permutations=False, return_rotations=False, return_centroid=False,
        stride=1, offset=0, return_packed=False, disable_tqdm=False,
        force_non_heuristic_alignment=False,
        norm_perms=None, norm_perm_sets=None,
        n_cores=None):

    if force_non_heuristic_alignment is True:
        logger.debug("Using standard alignment")
    else:
        logger.debug("Using heuristic alignment")

    num_permutations_tried = np.zeros(N, dtype=int)

    group_distance = np.full(N, np.inf)
    aligned_positions = []
    permutations = []
    rotations = []

    myrange = np.arange(N)[offset::stride]
    if myrange.size == 0:
        logger.warning(f"range is empty (using offset = {offset} and stride = {stride}) - is {N} enough structures?")
        myrange = np.arange(N) # fix issue (indended for debugging only)


    t = np.zeros(6)
    tt = [np.zeros(2), np.zeros(6)]

    # permutation magnitudes
    M = np.product([np.math.factorial(n) for n in perm_sets_sizes_ref])
    m = np.sum(perm_sets_sizes_ref)

    max_perm_num = M
    if force_non_heuristic_alignment is False:
        max_perm_num = min(M, 1000000) # this is to save memory
        if max_perm_num < M:
            logger.warning(f'Max number of permutations for heuristic alignment set to {max_perm_num}')

    logger.debug("Allocating memory")
    # allocate memory
    mem = [
        np.empty((max_perm_num,), dtype=np.float32), # distaces
        np.empty((max_perm_num, 3, m), dtype=np.float32), # array of Bt matrices
        np.empty((max_perm_num, 3, m), dtype=np.float32), # array of B matrices
        np.empty((max_perm_num, 3, 3), dtype=np.float32), # array of H matrices
        np.empty((max_perm_num, 3, 3), dtype=np.float32), # array of R matrices
        np.empty((max_perm_num, 3, m), dtype=np.float32), # array of At - B matrices
    ]
    logger.debug("Memory allocated")

    # print(mem[0].flags)

    # print(shared_variables)

    # norm_perms = shared_variables.norm_perms

    if norm_perms is None:
        logger.warning('Calculating normalized permutations in place.')
        norm_perms, norm_perm_sets = normalized_permutations(perm_sets_sizes_ref)

    m_max = np.max(perm_sets_sizes_ref)

    # m_max_heur_limit = 5
    # m_max_heur_limit = 6

    # if m_max >= m_max_heur_limit:
        # m = n_max <= 4 => slower
        # m = n_max = 5 => 10 % faster
        # m = n_max = 6 =>   3x faster
        # m = n_max = 7 =>  10x faster
        # m = n_max = 8 =>   3x faster
    if M >= np.math.factorial(6) and force_non_heuristic_alignment is False:
        align = lambda *args: align_through_normalized_permutations_heur(*args, norm_perm_sets, tol=2.0, tolstep=0.5, maxtol=3.0, max_perm_num=max_perm_num)
        mem.append(np.empty((M,), dtype=np.bool))
        mem.append(np.zeros((m+1,m), dtype=np.float32))
    else:
        align = align_through_normalized_permutations

    ttt = 0
    total_num_perms = 0

    for i, j in tqdm.tqdm(enumerate(myrange), disable=disable_tqdm):
        
        # tt = [np.zeros(2), np.zeros(6)]

        start = time.time()

        B_group = pos[j]
        B_lab = lab[j]

        # t[0] += time.time() - start
        # start = time.time()

        try:
            perm_sets, perm_sets_sizes = perm_subsets_fun(B_group, B_lab)
        except IncompatiblePermutationError:
            logger.spam("Incompatible permutation encountered, skipping this dataset")
            perm = np.arange(len(perm_sets_labels_ref), dtype=int)
            R = np.identity(3)
            B = []
            group_distance[j] = np.inf
            # tt[1] = []
            num_permutations_tried[j] = -1
        else:
            # t[1] += time.time() - start
            # start = time.time()

            # print(perm_sets_labels_ref)
            # print(perm_sets)
            assert(len(perm_sets_labels_ref) == len(perm_sets))
            for r, p in zip(perm_sets_labels_ref, perm_sets):
                try:
                    assert(np.all(r == B_lab[p]))
                except AssertionError as e:
                    e.args += ('Reference labels: ', perm_sets_labels_ref)
                    e.args += ('Current labels: ', [B_lab[p] for p in perm_sets])
                    raise

            # t[2] += time.time() - start
            # start = time.time()
            #
            # perm, R, B, group_distance[j], tt[0] = align_through_permutation(
            #     reference, B_group, perm_sets, perm_sets_sizes, M, m, tt[0], *mem)
            #
            # t[3] += time.time() - start
            # start = time.time()

            perm, R, B, group_distance[j], tt[1], num_perms = align(
                reference, B_group, perm_sets, norm_perms, M, m, tt[1], *mem)

            # print(tt)

            # t[4] += time.time() - start
            # start = time.time()

            total_num_perms += num_perms

            num_permutations_tried[j] = num_perms

            if return_centroid is True or return_positions is True:
                aligned_positions.append(B)

        if return_rotations is True:
            rotations.append(R)

        if return_permutations is True:
            permutations.append(perm)

        # t[5] += time.time() - start

        ttt += time.time() - start

        if i in [5, 10, 100, 1000] or (i > 1000 and np.mod(i,1000) == 0):
            print(f"avg. {total_num_perms/(i+1):.1f} perms at {ttt/(i+1)*1e3:.1f} ms/it, done {100 * i / len(myrange):.0f} %, approx, {(len(myrange) - i+1)*ttt/(i+1)/60:.0f} minutes left")

    # print(t)
    # print(tt)

    if return_distance is False:
        group_distance = None

    if return_centroid is True:
        P = [p for p in aligned_positions if p is not None]
        centroid = np.mean(np.asarray(P), axis=0)
        centroid_std = np.std(np.asarray(P), axis=0)

    else:
        centroid = []
        centroid_std = []

    if return_packed is True:
        return (centroid, centroid_std, permutations, rotations, group_distance, aligned_positions, num_permutations_tried)
    else:
        return centroid, centroid_std, permutations, rotations, group_distance, aligned_positions, num_permutations_tried

def main_align_core_parallel(pos, lab, N, *args, n_cores=None, norm_perms=None, norm_perm_sets=None, **kwargs):

    kwargs['return_packed'] = True
    kwargs['disable_tqdm'] = True

    if norm_perms is None or norm_perm_sets is None:
        perm_sets_sizes_ref = args[2]
        print(perm_sets_sizes_ref)

        # the  norm_perms, norm_perm_sets  variables should be read only,
        # therefore we can create them outside of the loop
        norm_perms, norm_perm_sets = normalized_permutations(perm_sets_sizes_ref)

        norm_perms.flags.writeable = False
        # norm_perm_sets.flags.writeable = False

    if False:
        manager = Manager()
        shared_variables = manager.Namespace()
        shared_variables.norm_perms = norm_perms

        # print(norm_perms.flags)
        # print(norm_perm_sets.flags)

    kwargs['norm_perms'] = norm_perms
    kwargs['norm_perm_sets'] = norm_perm_sets

    shared_variables = None

    # start = time.time()

    pos_set = split(pos, n_cores)
    lab_set = split(lab, n_cores)

    # t = time.time() - start
    # print(t)

    NN = [len(p) for p in pos_set]

    pfun = lambda p, l, n, sv: main_align_core(p, l, n, sv, *args, **kwargs)

    results = Parallel(n_jobs=n_cores)(
                delayed(pfun)(pos_set[i], lab_set[i], NN[i], shared_variables)
                    for i in np.arange(n_cores))

    # --------------------------------------------------------------------------
    # Reduction

    if kwargs['return_centroid'] is True:
        centroid = np.mean(np.array([results[i][0] for i in np.arange(n_cores)]), axis=0)
        centroid_std = None # TODO: implement this
    else:
        centroid = None
        centroid_std = None

    if kwargs['return_permutations'] is True:
        permutations = sum([results[i][2] for i in np.arange(n_cores)], [])
    else:
        permutations = None

    if kwargs['return_rotations'] is True:
        rotations = sum([results[i][3] for i in np.arange(n_cores)], [])
    else:
        rotations = None

    if kwargs['return_distance'] is True:
        group_distance = np.concatenate([results[i][4] for i in np.arange(n_cores)])
    else:
        group_distance = None

    if kwargs['return_positions'] is True:
        aligned_positions = sum([results[i][5] for i in np.arange(n_cores)], [])
    else:
        aligned_positions = None

    num_permutations_tried = np.concatenate([results[i][6] for i in np.arange(n_cores)])

    # --------------------------------------------------------------------------

    # for i in np.arange(len(permutations)):
    #     assert(np.all(permutations[i] == permutations_[i]))
    #     assert(np.all(rotations[i] == rotations_[i]))
    #
    # assert(np.all(group_distance == group_distance_))

    return centroid, centroid_std, permutations, rotations, group_distance, aligned_positions, num_permutations_tried


def centroid_invariant(centroid):
    d = []
    #print(centroid)
    for i in np.arange(centroid.shape[0]):
        d.append(np.linalg.norm(centroid[i,:]))
        for j in np.arange(i+1, centroid.shape[0]):
            d.append(np.linalg.norm(centroid[i,:] - centroid[j,:]))
    return d