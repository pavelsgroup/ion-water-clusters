import numpy as np
from numba import njit

def make_f(function, offset):

    @njit
    def f(L):
        S = 0.0
        for i in range(3):
            s = 0.0
            for x in L[i]:
                g = function  # compile-time constant
                s += g(x + offset)
            S += s
            i += 1
        return S

    return f


def main():
    y = (np.ones(200, dtype=np.float), 
         np.zeros(100, dtype=np.float), 
         np.ones(50, dtype=np.float))
    f = make_f(np.sin, 1)
    result = f(y)
    print(result)

if __name__ == "__main__":
    main()