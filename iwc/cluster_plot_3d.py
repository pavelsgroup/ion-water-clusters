# coding: utf-8

import os

os.system("export QT_STYLE_OVERRIDE=gtk2")

import re

import numpy as np
import scipy

from mayavi import mlab

from traits.api import HasTraits, Range, Instance, on_trait_change, Str, Int, Bool, Float
from traitsui.api import View, Item, Group, HGroup
from mayavi.core.ui.api import MayaviScene, SceneEditor, MlabSceneModel

from tvtk.util import ctf

from tvtk.tools import visual

from pathlib import Path

import matplotlib
import matplotlib.pyplot as plt

from iwc.common.common import logger, encode_labels, decode_labels, random_string

from iwc.aligning import main_align_core, dist_square
from iwc.clustering import permutation_subsets_by_reference
import pprint
pp = pprint.PrettyPrinter(indent=4)



"""
conda activate spsalign-mayavi

conda install -c conda-forge numpy scipy matplotlib tqdm joblib pandas seaborn numba h5py MDAnalysis coloredlogs verboselogs mayavi
"""

# ------------------------------------------------------------------------------
# CONFIGURATION

# mlab.options.backend = 'envisage'

def convert_color(rgb):
    return tuple([i/255 for i in rgb])

my_red = convert_color((215,25,28))
# my_purple = convert_color((214,25,123))
my_purple = convert_color((211,25,214))

# my_green = convert_color((26,215,65))
my_green = convert_color((26, 215, 174))
black = convert_color((0,0,0))
white = convert_color((255,255,255))
blue = convert_color((0,0,255))

text_color = convert_color((128,128,128))
title_text_color = convert_color((128,128,128))

def ShellPlot3dGenerator(
    atom_list,
    atom_colormaps = {
        'H': 'Greys',
        'HW': 'Greys',
        'LI': 'Blues',
        'NA': 'Blues',
        'K': 'Blues',
        'O': 'Reds',
        'OW': 'Reds',
        'CL': 'Greens',
        'C': 'Purples',
        'N': 'Blues',
    },
    atom_colors={
        #'H': (0.8, 0.8, 0.8),
        'H': convert_color((244, 244, 14)),
        #'HW': (1, 1, 1), # white
        'HW': (0.8, 0.8, 0.8),
        'LI': convert_color((0, 102, 224)),
        'NA': convert_color((72, 0, 227)),
        'K':  convert_color((0, 6, 228)),
        'O':  my_red,
        'OW': my_red,
        'CL': my_green,
        'C': my_purple,
        'N': convert_color((0, 6, 228)),
    },
    atom_colors_alt={
        'OW': convert_color((215, 98, 25)),
        # 'CL': convert_color((26, 215, 174)),
        'CL': convert_color((16, 204, 144)),
    },
    atom_ionic_radius={ # A
        'H': 0.25,
        'HW': 0.25,
        'LI': 0.90,
        'NA': 1.16,
        'K': 1.52,
        'O': 1.26,
        'OW': 1.26,
        'CL': 1.67,
        'C': 0.70,
        'N': 0.65,
    },
    atom_weights={
        'HW': 1.008,
        'LI': 6.94,
        'OW': 16.0,
        'NA': 22.99,
        'K':  39.1,
        'CL': 35.45,
    },
    atom_slider_range={
        'H': [0.01, 50., 2.,],
        'HW': [0.01, 50., 2.,],
        'LI': [0.01, 50., 2.,],
        'NA': [0.01, 50., 2.,],
        'K': [0.01, 50., 2.,],
        'O': [0.01, 50., 2.,],
        'OW': [0.01, 50., 2.,],
        'CL': [0.01, 50., 2.,],
        'C': [0.01, 50., 2.,],
        'N': [0.01, 50., 2.,],
    },
    max_crop_distance=10.0,
    default_crop_distance=np.inf,
    scene_width=1000,
    scene_height=1000
    ):


    class ShellPlot3d(HasTraits):

        balls = {}

        show_balls = Bool(True)
        show_first_shell_cutoff = Bool(True)

        atoms = atom_list
        common_atoms = atom_list

        for a in atoms:
            locals()[f'show_{a}'] = Bool(True)
            locals()[f'show_balls_{a}'] = Bool(True)
            locals()[f'show_text_{a}'] = Bool(False)
            locals()[f'show_dist_text_{a}'] = Bool(False)

        show_names = [f'show_{a}' for a in atoms]

        # The scene model.
        scene = Instance(MlabSceneModel, ())

        mayavi_scene = Item(
            name='scene',
            editor=SceneEditor(scene_class=MayaviScene),
            width=scene_width,
            height=scene_height,
            show_label=False)

        slider_groups_voids = [
            HGroup("slider_voids_cutoff", label='voids', show_labels=False),]

        slider_groups = [
            HGroup(
                HGroup("show_balls", label='Show Atoms', show_labels=False),
                HGroup("show_first_shell_cutoff", label='show cutoff', show_labels=False),
                HGroup("slider_dist_low", label='dist', show_labels=False),
                HGroup("slider_dist_high", label='dist', show_labels=False),
            ),]

        slider_groups_all = slider_groups_voids + slider_groups

        slider_groups_atoms = {}
        slider_groups_atoms_common = []

        for a in atoms:
            slider_groups_atoms[a] = HGroup(
                    Item(name=f"show_balls_{a}",     tooltip=f"Show {a} atom balls"),
                    Item(name=f"show_{a}",           tooltip=f"Show {a} contour"),
                    Item(name=f"show_text_{a}",      tooltip=f"Show {a} contour %"),
                    Item(name=f"show_dist_text_{a}", tooltip=f"Show {a} distance"),
                    Item(name=f"slider_{a}",         tooltip=f"Contour level"),
                    Item(name=f"slider_{a}_dist_low", tooltip=f"low"),
                    Item(name=f"slider_{a}_dist_high", tooltip=f"high"),
                    label=a, show_labels=False),
            slider_groups_all += slider_groups_atoms[a]
            if a in common_atoms:
                slider_groups_atoms_common += slider_groups_atoms[a]

        view_general = View(mayavi_scene, slider_groups, slider_groups_atoms_common, resizable=True)

        # view_LI = View(mayavi_scene, slider_groups, slider_groups_atoms_common, slider_groups_atoms['LI'], resizable=True)
        # view_NA = View(mayavi_scene, slider_groups, slider_groups_atoms_common, slider_groups_atoms['NA'], resizable=True)
        # view_K = View(mayavi_scene, slider_groups, slider_groups_atoms_common, slider_groups_atoms['K'], resizable=True)

        view_voids = View(mayavi_scene, slider_groups_voids, resizable=True)
        view_all = View(mayavi_scene, slider_groups_all, resizable=True)

        slider_voids_cutoff = Range(0.01, 100.0, 5.0,)

        slider_atom_names = []
        slider_atom_dist_names = []

        for a in atoms:
            locals()[f'slider_{a}'] = Range(*atom_slider_range[a])
            locals()[f'slider_{a}_dist_low'] = Range(0.0, max_crop_distance, 0)
            locals()[f'slider_{a}_dist_high'] = Range(0.01, max_crop_distance, default_crop_distance)

        slider_atom_names = [f'slider_{a}' for a in atoms]
        slider_atom_dist_names = [f'slider_{a}_dist_low' for a in atoms] + [f'slider_{a}_dist_high' for a in atoms]

        slider_dist_low  = Range(0.0, max_crop_distance, 0) #, mode='spinner')
        slider_dist_high  = Range(0.01, max_crop_distance, default_crop_distance) #, mode='spinner')

        group_centroids = [
            HGroup(f"show_c{i}", label=f'c{i}', show_labels=False)
                for i in np.arange(10)]

        view_centroids = View(mayavi_scene, group_centroids, resizable=True)
        # --------------------------------------------------------------------------

        ionic_radius = atom_ionic_radius

        atom_weight = atom_weights

        first_shell_cutoff = {
            'LI': 3.2,
            'NA': 3.2,
            'K': 3.8,
        }

        # --------------------------------------------------------------------------

        contour = {}
        stats = {}
        displayed_atoms = set()

        colors = atom_colors
        colors_alt = atom_colors_alt

        colormaps = atom_colormaps

        spheres = []
        spheres_props = {}
        sphere_extra = {
            'atoms': {},
            'contour': {},
        }
        # --------------------------------------------------------------------------


        def __init__(self, CONFIG, normalization=1, plot_type="volume",
                exclude_atom_types=[], void_atom_types=[],
                separate_atom_types=True, volume_range=None, 
                contour_range=None,
                num_contours=None,
                contour_levels=None):

            HasTraits.__init__(self)

            self.CONFIG = CONFIG
            self.normalization = normalization
            self.plot_type = plot_type

            self.centroid = []

            if self.plot_type == 'centroid':
                return

            self.exclude_atom_types = exclude_atom_types
            self.separate_atom_types = separate_atom_types
            self.void_atom_types=void_atom_types
            self.volume_range = volume_range

            self.contour_range=contour_range
            self.num_contours=num_contours
            self.contour_levels = contour_levels

            if self.contour_levels is not None:
                for atom_type in self.contour_levels:
                    setattr(self, f'slider_{atom_type}', self.contour_levels[atom_type][2][0])

            self.void_data_mean = 0
            self.void_data = []

            self.volume = dict()
            self.volume_data = dict()

            self.contour_data = dict()        

        # @on_trait_change('slider_OW,slider_HW,slider_CL')
        @on_trait_change('slider_voids_cutoff')
        def slider_voids_cutoff_changed(self):
            slider_val = self.slider_voids_cutoff
            cutoff = self.void_data_mean * slider_val / 100.0
            # self.plot_voids(*self.void_data, cutoff)
            S = self.plot_voids_prepare_data(*self.void_data, cutoff)
            self.voids_volume.mlab_source.scalars = S
            # self.voids_volume.print_traits()

        @on_trait_change(['slider_dist_low', 'slider_dist_high'])
        def slider_dist_changed(self):
            for atom_type in self.contour:
                s = np.copy(self.contour_data[atom_type][0])
                dist = self.contour_data[atom_type][4]
                s[np.logical_or(
                    dist < self.slider_dist_low, 
                    dist > self.slider_dist_high)] = np.nan

                for C in self.contour[atom_type]:
                    C.mlab_source.scalars = s
    
        @on_trait_change(slider_atom_dist_names)
        def slider_dist_atom_changed(self, trait_name, new_value):
            m = re.search("slider_(.*)_dist_(low|high)", trait_name)
            if m is not None:
                atom_type = m.group(1)
                
                print(atom_type, getattr(self, f'slider_{atom_type}_dist_low'), getattr(self, f'slider_{atom_type}_dist_high'))

                s = np.copy(self.contour_data[atom_type][0])
                dist = self.contour_data[atom_type][4]
                s[np.logical_or(
                    dist < getattr(self, f'slider_{atom_type}_dist_low'), 
                    dist > getattr(self, f'slider_{atom_type}_dist_high'))] = np.nan

                for C in self.contour[atom_type]:
                    C.mlab_source.scalars = s
        
        @on_trait_change(slider_atom_names)
        def slider_atom_changed(self, obj, trait_name, new_value):
            m = re.search("slider_(.*)", trait_name)
            if m is not None:
                atom_type = m.group(1)
                mean = self.stats[atom_type]['mean']

                density_cutoff = mean * new_value

                if self.num_contours[atom_type] == 0:
                    contours = [density_cutoff]
                else:
                    contours = [mean * new_value]
                    d = self.contour_range[atom_type] * new_value / 100
                    for i in range(self.num_contours[atom_type]):
                        # contours.append((new_value - (i+1)*d) * mean)
                        contours.append((new_value + (i+1)*d) * mean)
                    contours = sorted(contours)
                    print(contours)

                if self.plot_type == 'contour':
                    contour = self.contour[atom_type][2].contour
                    if density_cutoff != contour.contours[0]:
                        contour.trait_set(contours=contours)

                        self.whats_within_contour(density_cutoff, atom_type)

                        for sph_id in self.spheres_props:
                            if self.spheres_props[sph_id]['label'] == atom_type:
                                self.whats_within_contour_sphere(density_cutoff, atom_type, self.spheres_props[sph_id], self.ionic_radius[atom_type])

                elif self.plot_type == 'volume':
                    vmin = 0
                    vmax = density_cutoff # / 100.0
                    S = self.plot_volume_prepare_data(*self.volume_data[atom_type], vmin, vmax)
                    self.volume[atom_type].mlab_source.scalars = S
            else:
                ValueError(trait_name)    

        @on_trait_change(show_names)
        def show_atom_changed(self, obj, trait_name, new_value):
            m = re.search("show_(.*)", trait_name)
            if m is not None:
                atom_type = m.group(1)
                for contour in self.contour[atom_type]:
                    contour.trait_set(visible=new_value)
            else:
                ValueError(trait_name)

        @on_trait_change('show_first_shell_cutoff')
        def show_first_shell_cutoff_changed(self, obj, trait_name, new_value):
            self.balls['first_shell_cutoff'].trait_set(visible=new_value)

        show_names = [f'show_balls_{atom_type}' for atom_type in atoms]
        @on_trait_change(show_names)
        def show_balls_atom_changed(self, obj, trait_name, new_value):
            m = re.search("show_balls_(.*)", trait_name)
            if m is not None:
                atom_type = m.group(1)
                for sphere in self.spheres[atom_type]:
                    sphere.trait_set(visible=new_value)
            else:
                ValueError(trait_name)

        @on_trait_change('show_balls')
        def show_balls_changed(self, obj, trait_name, new_value):
            for atom_type in self.spheres:
                for sphere in self.spheres[atom_type]:
                    sphere.trait_set(visible=new_value)

        show_names = [f'show_c{i}' for i in np.arange(10)]
        @on_trait_change(show_names)
        def show_centroid_changed(self, obj, trait_name, new_value):
            m = re.search("show_c(.*)", trait_name)
            if m is not None:
                ind = int(m.group(1))
                for ball in self.centroid[ind]:
                    ball.trait_set(visible=new_value)
            else:
                ValueError(trait_name)

        show_names = [f'show_text_{atom_type}' for atom_type in atoms]
        @on_trait_change(show_names)
        def show_text_atom_changed(self, obj, trait_name, new_value):
            m = re.search("show_text_(.*)", trait_name)
            if m is not None:
                atom_type = m.group(1)
                for sph_id in self.sphere_extra['contour']:
                    if self.spheres_props[sph_id]['label'] == atom_type:
                        self.sphere_extra['contour'][sph_id]['sphere_text3d'].trait_set(visible=new_value)
            else:
                ValueError(trait_name)

        show_names = [f'show_dist_text_{atom_type}' for atom_type in atoms]
        @on_trait_change(show_names)
        def show_dist_text_atom_changed(self, obj, trait_name, new_value):
            m = re.search("show_dist_text_(.*)", trait_name)
            if m is not None:
                atom_type = m.group(1)
                for sph_id in self.sphere_extra['atoms']:
                    if self.spheres_props[sph_id]['label'] == atom_type:
                        self.sphere_extra['atoms'][sph_id]['sphere_text3d'].trait_set(visible=new_value)
            else:
                ValueError(trait_name)

        @on_trait_change('scene.activated')
        def configure_scene(self):

            for atom_type in self.atoms:
                for sph_id in self.spheres_props:
                    if self.spheres_props[sph_id]['label'] == atom_type:
                        self.display_sphere_distance(self.spheres_props[sph_id])
                        for cnt in self.contour_levels[atom_type]:
                            mean = self.stats[atom_type]['mean']
                            density_cutoff = cnt[0] * mean
                            self.whats_within_contour_sphere(density_cutoff, atom_type, self.spheres_props[sph_id], self.ionic_radius[atom_type])

            # show box around
            # mlab.outline(extent=[-5,5,-5,5,-5,5], figure=self.scene.mayavi_scene)

            mlab.view(distance=30.0)

            # cam = self.scene.camera
            # cam.zoom(3.0)

        # --------------------------------------------------------------------------

        def whats_within_contour(self, contour_cutoff, atom_type):
            s = self.contour_data[atom_type][0]
            dist = self.contour_data[atom_type][4]

            if hasattr(self.CONFIG, 'CATION'):
                r1 = self.first_shell_cutoff[self.CONFIG.CATION]
            else:
                r1 = 0
            r2 = 5

            radius_cutoff = [(0, r1), (r1, r2),]

            mean = self.stats[atom_type]['mean']
            i_cont = s >= contour_cutoff
            
            # perc_within_contour = 100 * np.sum(s[i_cont]) / np.sum(s)

            print(f"{atom_type} within contour:  d >= {contour_cutoff/mean:.1f} rel ({contour_cutoff:.0f} abs)")
            for i, (d1, d2) in enumerate(radius_cutoff):
                i_dist = np.logical_and(dist >= d1, dist < d2)
                perc_within_contour_dist = 100 * np.sum(s[np.logical_and(i_cont, i_dist)]) / np.sum(s[i_dist])        
                print(f"    shell #{i} ({d1} <= r < {d2} A):  {perc_within_contour_dist:.1f} %")

        def whats_within_contour_sphere(self, contour_cutoff, atom_type, sph, sph_dist_cutoff):
            s = self.contour_data[atom_type][0]

            x = self.contour_data[atom_type][1]
            y = self.contour_data[atom_type][2]
            z = self.contour_data[atom_type][3]

            center = sph['center']

            sph_dist = np.sqrt((x - center[0])**2 + (y - center[1])**2 + (z - center[2])**2)

            dist = self.contour_data[atom_type][4]

            if hasattr(self.CONFIG, 'CATION'):
                r1 = self.first_shell_cutoff[self.CONFIG.CATION]
            else:
                r1 = 0

            radius_cutoff = [(0, r1),]

            mean = self.stats[atom_type]['mean']
            i_cont = s >= contour_cutoff
            i_sph_dist = sph_dist < sph_dist_cutoff

            # print(f"{atom_type} within contour:  d >= {contour_cutoff/mean:.1f} rel ({contour_cutoff:.0f} abs)")
            for i, (d1, d2) in enumerate(radius_cutoff):
                i_dist = np.logical_and(dist >= d1, dist < d2)
                ii = np.logical_and(i_cont, i_dist)
                ii = np.logical_and(ii, i_sph_dist)
                perc_within_contour_dist = 100 * np.sum(s[ii]) / np.sum(s[i_dist])

                if sph['id'] not in self.sphere_extra['contour']:
                    self.sphere_extra['contour'][sph['id']] = {}

                text = f"{perc_within_contour_dist:.0f} %"

                if 'sphere_text3d' not in self.sphere_extra['contour'][sph['id']]:
                    self.sphere_extra['contour'][sph['id']]['sphere_text3d'] = mlab.text3d(
                        *center, text, 
                        color=text_color,
                        scale=0.25, # color=self.colors[L],
                        figure=self.scene.mayavi_scene)

                    self.sphere_extra['contour'][sph['id']]['sphere_text3d'].trait_set(
                        visible=bool(getattr(self, f'show_text_{atom_type}')))
                else:
                    self.sphere_extra['contour'][sph['id']]['sphere_text3d'].text = text

        def display_sphere_distance(self, spheres_props):

            center = spheres_props['center']
            sph_id = spheres_props['id']
            atom = spheres_props['label']

            self.sphere_extra['atoms'][sph_id] = {}
            self.sphere_extra['atoms'][sph_id]['sphere_text3d'] = mlab.text3d(
                *center, f"{np.linalg.norm(center):.3f}", 
                scale=0.25, color=self.colors[atom], 
                figure=self.scene.mayavi_scene)
            
            self.sphere_extra['atoms'][sph_id]['sphere_text3d'].trait_set(
                        visible=bool(getattr(self, f'show_dist_text_{atom}')))

        def load_data(self):

            try:
                with np.load(self.CONFIG.file_histogram, allow_pickle=True) as npzfile:
                    #print(npzfile.files)
                    histograms = npzfile['histograms'].item()  # .item() is here to recover dict form 0d numpy array
                    bin_volumes = npzfile['bin_volumes'].item()
                    edges = npzfile['edges'].item()

                    if 'num_frames' in npzfile:
                        num_frames = npzfile['num_frames'].item()
                    elif 'num_groups' in npzfile:
                        num_frames = npzfile['num_groups'].item()
                        
                    # avg_num_atoms = npzfile['avg_num_atoms'].item()
                    # distance_intervals = npzfile['distance_intervals'].item()
            except FileNotFoundError as e:
                f = self.CONFIG.file_histogram.name
                g = re.sub('_\d+', R'_*', str(f))
                path = self.CONFIG.folder_histograms
                paths = list(Path(path).rglob(g))
                print('')
                print('File not found. Compatible centroids in the directory:')
                for p in paths:
                    print(p.name)
                print('')
                raise(e)

            print(f"Loaded data of {num_frames} structures")

            # currently, we store overall data in [0], the next elements contain data of different shells
            return histograms, edges, bin_volumes, num_frames


        def preprocess(self, hist, edge, bin_volumes, num_frames: int):
            # Load and prepocess data

            # we assume that the center is at (0, 0, 0)
            cation_pos = (0, 0, 0,)

            bin_centers = [(e[:-1] + e[1:])/2 for e in edge]

            # distance of bin center from origin
            X, Y, Z = np.meshgrid(*bin_centers, indexing='ij')
            dist = np.sqrt(X**2 + Y**2 + Z**2)

            assert cation_pos == (0, 0, 0,)
            assert dist.shape == hist.shape == bin_volumes.shape

            return dist

        def normalize_data(self, atom_type: str, histograms, edges, bin_volumes, num_frames: int):
            s = histograms[atom_type]
            e = edges[atom_type]
            V = bin_volumes[atom_type]

            if V is None: # we need to calculate volumes ourselves
                bin_sizes = [np.diff(ee) for ee in e]
                X, Y, Z = np.meshgrid(*bin_sizes, indexing='ij')
                V = X * Y * Z

            dist = self.preprocess(s, e, V, num_frames)


            s = s / (num_frames * V * self.normalization)

            # if use_atom_weight is True:
                # s *= self.atom_weight[atom_type]

            # if log_transform is True:
                # s = np.log10(s - s.min() + 1)

            xc = (e[0][:-1] + e[0][1:]) / 2
            yc = (e[1][:-1] + e[1][1:]) / 2
            zc = (e[2][:-1] + e[2][1:]) / 2

            # assert xc[0] == yc[0] == zc[0]
            # assert xc[-1] == yc[-1] == zc[-1]

            x, y, z = np.mgrid[xc[0] : xc[-1] : xc.size*1j,
                            yc[0] : yc[-1] : yc.size*1j,
                            zc[0] : zc[-1] : zc.size*1j]

            return s, x, y, z, dist

    # ------------------------------------------------------------------------------
        @staticmethod
        def statistics(s, nan: bool=False, do_print: bool=False):

            if nan is True:
                M = np.nanmax(s)
                m = np.nanmin(s)
                mean = np.nanmean(s)
                med = np.nanmedian(s)
                std = np.nanstd(s)
            else:
                M = s.max()
                m = s.min()
                mean = s.mean()
                med = np.median(s)
                std = s.std()

            if do_print:
                print(f'\tmin = {m:0.3g}\n\tmax = {M:0.3g}\n\tmean = {mean:0.3g}\n\tstd = {std:0.3g}\n\tmed = {med:0.3g}')

            return {'max': M, 'min': m, 'mean': mean, 'med': med, 'std': std}

        @staticmethod
        def draw_sphere(r: float=1.0, pos=(0,0,0), res=101j, **kwargs):

            phi, theta = np.mgrid[0:np.pi:res, 0:2 * np.pi:res]

            x = r * np.sin(phi) * np.cos(theta)
            y = r * np.sin(phi) * np.sin(theta)
            z = r * np.cos(phi)

            mesh = mlab.mesh(x - pos[0], y - pos[1], z - pos[2], **kwargs)

            # mesh.actor.property.interpolation = 'phong'
            # mesh.actor.property.specular = 0.1
            # mesh.actor.property.specular_power = 5

            return mesh

        @staticmethod
        def draw_cylinder(x, y, **kwargs):

            x_ax = y[0] - x[0]
            y_ax = y[1] - x[1]
            z_ax = y[2] - x[2]

            cyl = visual.Cylinder(pos=(x[0], x[1], x[2]), 
                axis=(x_ax,y_ax,z_ax), radius=0.1, length=np.linalg.norm(x-y), **kwargs)
            return cyl

    # ------------------------------------------------------------------------------

        def volume_colormap(self, volume, cmap='plasma', vmin=0, vmax=1):

            values = np.linspace(vmin, vmax, 32)[:,np.newaxis]
            cc = plt.cm.get_cmap(cmap)(np.linspace(0.25, 1.0, 32))
            cc = np.concatenate([values, cc[:,:3]], axis=1)

            c = ctf.save_ctfs(volume._volume_property)
            c['rgb'] = cc
            # load the color transfer function to the volume
            ctf.load_ctfs(c, volume._volume_property)
            # signal for update
            volume.update_ctf = True


        def plot_voids_prepare_data(self, s, x, y, z, dist, cutoff: float):
            off = []

            off.append(s > cutoff)
            off.append(dist < 2)
            off.append(dist > 8)

            S = np.copy(s)

            S = np.abs(cutoff - S)

            for o in off:
                S[o] = 0
            return S

        def plot_volume_prepare_data(self, s, vmin: float, vmax: float):
            off = []

            S = np.copy(np.log(s+1))
            S -= np.min(S)
            S /= np.max(S)

            off.append(S > vmax)
            off.append(S < vmin)

            for o in off:
                S[o] = 0

            S -= np.min(S)
            S /= np.max(S)

            return S

        def plot_voids(self, s, x, y, z, dist, cutoff):
            S = self.plot_voids_prepare_data(s, x, y, z, dist, cutoff)

            print("Inverted density (voids):")
            self.statistics(S, nan=True, do_print=True)
            sf = mlab.pipeline.scalar_field(x, y, z, S)
            self.voids_volume = mlab.pipeline.volume(sf,
                color=black,
                # color=white,
                vmin=0, vmax=cutoff,
                figure=self.scene.mayavi_scene)
            self.voids_volume.mlab_source.print_traits()


        def plot_density_volume(self, s, x, y, z, dist, atom_type):

            vmin = self.volume_range[atom_type][0] * self.stats[atom_type]['mean']
            vmax = self.volume_range[atom_type][1] * self.stats[atom_type]['mean']

            S = self.plot_volume_prepare_data(s, vmin, vmax)

            sf = mlab.pipeline.scalar_field(x, y, z, s)

            if self.separate_atom_types is True:
                self.volume[atom_type] = mlab.pipeline.volume(sf,
                    color=self.colors[atom_type],
                    vmin=vmin, vmax=vmax,
                    figure=self.scene.mayavi_scene)
            else:
                self.volume['all'] = mlab.pipeline.volume(sf,
                    vmin=vmin, vmax=vmax,
                    figure=self.scene.mayavi_scene)

            # self.volume_colormap(volume, self.colormaps[atom_type], vmin=vmin, vmax=vmax)

            # volume = mlab.pipeline.volume(sf, color=black,
            #     vmin=100)

            # if atom_type in ['HW']:
            #     self.volume[atom_type] = mlab.points3d(x, y, z, s,
            #         color=self.colors[atom_type],
            #         vmin=vmin, vmax=vmax,
            #         transparent=True,
            #         mode='cube',
            #         figure=self.scene.mayavi_scene)

            # if atom_type in ['HW']:
            #     for ax in ['x_axes', 'y_axes', 'z_axes']:
            #         mlab.volume_slice(x, y, z, s,
            #             colormap='Greys',#self.colormaps[atom_type],
            #             vmin=self.stats[atom_type]['mean'],
            #             vmax=5*self.stats[atom_type]['mean'],
            #             plane_orientation=ax,
            #             #opacity=0.5,
            #             plane_opacity=0.5,
            #             transparent=True)


        def plot_density_contour(self, s, x, y, z, dist, atom_type):
            # mlab.contour3d(
            #     x, y, z, s, color=self.colors[atom_type],
            #     contours=[a*self.stats[atom_type]['mean'] for a in np.linspace(2,4,5)], opacity=0.025)

            # sf = mlab.pipeline.scalar_field(x, y, z, s)
            # volume = mlab.pipeline.volume(sf, color=self.colors[atom_type],
            #     vmin=3*self.stats[atom_type]['mean'], vmax=10*mean)

            for i, cnt in enumerate(self.contour_levels[atom_type]):
                if i == 0:
                    self.contour[atom_type] = []
                
                
                initial_value = cnt[0] * self.stats[atom_type]['mean']
                
                max_frac = self.stats[atom_type]['max'] / self.stats[atom_type]['mean']

                if initial_value > self.stats[atom_type]['max']:
                    logger.warning(f'Atom {atom_type}: Initial contour value of {cnt[0]} exceeds maximal value of {max_frac}')
                    initial_value = self.stats[atom_type]['max']
                    initial_frac = max_frac
                    # setattr(self, f'slider_{atom_type}', initial_frac)

                if len(cnt) < 3:
                    contours = [initial_value]
                else:
                    contours = [initial_value*(1-cnt[2]), initial_value, initial_value*(1+cnt[2])]

                self.contour[atom_type].append(mlab.contour3d(
                    x, y, z, s, color=self.colors[atom_type],
                    contours=contours,
                    opacity=cnt[1],
                    # transparent=True,
                    figure=self.scene.mayavi_scene))

            # if atom_type in ['OW']:
            #     mlab.volume_slice(x, y, z, s,
            #         colormap=self.colormaps[atom_type],
            #         vmin=self.stats[atom_type]['mean'],
            #         vmax=5*self.stats[atom_type]['mean'],
            #         transparent=True)

            # the following line will print you everything that you can modify on that object
            # self.contour.contour.print_traits()

            # s[s < 1*self.stats[atom_type]['mean']] = 0
            # s[s > 10*self.stats[atom_type]['mean']] = 0

            # sf = mlab.pipeline.scalar_field(x, y, z, s)
            # volume = mlab.pipeline.volume(sf,
            #     color=self.colors[atom_type],
            #     vmin=2*self.stats[atom_type]['mean'], vmax=3*self.stats[atom_type]['mean'])

    # ------------------------------------------------------------------------------

        def draw_centroids(self, exclude_centroids=[]):

            print(self.scene)
            self.scene.print_traits()
            self.scene.background = white
            self.scene.background = black
            self.scene.foreground = black
            self.scene.foreground = white

            # self.draw_sphere(r=4, color=self.blue, opacity=0.05)
            # self.draw_sphere(r=5.5, color=self.blue, opacity=0.05)

            print(self.CONFIG.folder_aligning)

            self.CONFIG.centroid_id = None
            
            # pathlist = Path(self.CONFIG.folder_aligning).glob(self.CONFIG.get_align_hash() + '_*.meta.npz')
            pathlist = Path(self.CONFIG.folder_centroids).glob("centroid_perturbed-" + self.CONFIG.get_align_hash() + '_*.npz')
            pathlist = Path(self.CONFIG.folder_centroids).glob("centroid-" + self.CONFIG.get_align_hash() + '_*.npz')
            pathlist = list(pathlist)
            NN = len(pathlist)

            for i in np.arange(10):
                self.add_trait(f"show_c{i}", Bool(True))

            structures = []

            for i, path in enumerate(pathlist):

                # m = re.match("[A-z0-9]*_([0-9]*).meta.npz", pathlist[i].parts[-1])
                m = re.match("centroid_perturbed-[A-z0-9]*_([0-9]*).npz", pathlist[i].parts[-1])
                m = re.match("centroid-[A-z0-9]*_([0-9]*).npz", pathlist[i].parts[-1])
                if m is not None:
                    shortname = m.group(1)
                else:
                    raise ValueError("unexpected name")

                if int(shortname) not in exclude_centroids:

                    print("Loading " + str(path))
                    tmp = np.load(str(path))

                    pos = np.asarray(tmp['centroid'], dtype=np.float32)
                    lab = np.asarray(tmp['centroid_lab'], dtype=np.uint8)

                    structures.append({
                        'positions': pos,
                        'labels': lab,
                        'shortname': shortname,
                        'name': "/".join(pathlist[i].parts[-2:]),
                        'path': pathlist[i],
                        })

            NN = len(structures)

            if NN == 0:
                raise ValueError("No centroid files found.")

            dist = np.zeros((NN, NN,))
            for i in np.arange(NN):
                for j in np.arange(i+1,NN):
                    d, _, = self.align_rotate(structures[j], structures[i])
                    dist[i,j] = d[0]
                    dist[j,i] = d[0]

            ii = np.argsort(np.sum(dist, axis=0))

            with np.printoptions(precision=1, suppress=True, linewidth=np.inf):
                print(10*(dist[ii,:])[:,ii])           

            reference = structures[ii[0]]
            sorted_structures = [structures[iii] for iii in ii]

            # self.align_all_and_draw(reference, sorted_structures)

            D = 6        
            x = np.linspace(-D*NN, D*NN, NN)
            y = np.linspace(-D*NN, D*NN, NN)
            xx, yy = np.meshgrid(x,y)
            coords = np.array((xx.ravel(), yy.ravel())).T

            # return

            for i in np.arange(NN):
                for j in np.arange(i+1,NN):
                    k = i*NN + j
                    #center = np.array([5*k, 5*k, 5*k])
                    center = np.array([coords[k][0], coords[k][1], 0])
                    # center = np.array([0, 0, 0])

                    self.align_all_and_draw(structures[i], [structures[i], structures[j]], center)


        def align_all_and_draw(self, reference, structures, center=np.array([0, 0, 0])):

            # CATION = self.CONFIG.CATION
            CATION = 'N'

            for structure in structures:
                dist, structure = self.align_rotate(reference, structure)


            for i, structure in enumerate(structures):

                kwargs = {
                    'opacity': 0.15,
                    'resolution': 20,
                    'figure': self.scene.mayavi_scene,
                    'color': self.colors[CATION],
                    }

                balls = []

                balls.append(self.draw_sphere(
                    pos=-center, # somwhow we need to put minus (?)
                    r=self.ionic_radius[CATION],                
                    **kwargs))

                n = structure['positions'].shape[0]

                for j in np.arange(n):
                    p = structure['positions'][j,:] + center
                    l = structure['labels'][j]

                    if i == 0:
                        kwargs['color'] = self.colors[decode_labels(l)]
                    else:
                        kwargs['color'] = self.colors_alt[decode_labels(l)]

                    balls.append(self.draw_sphere(
                        pos=-p, # somwhow we need to put minus (?)
                        # r=self.ionic_radius[decode_labels(l)]*(NN-i)/NN,
                        r=self.ionic_radius[decode_labels(l)],
                        **kwargs))

                self.centroid.append(balls)
                self.group_centroids[i].label = structure['name']

            desc = ":".join([structure['shortname'] for structure in structures])
            desc = f"{dist[0]*10:.1f} ({desc})" # dist *= 10 for display purposes
            mlab.text3d(*center, desc, figure=self.scene.mayavi_scene)


        def align_rotate(self, reference, structure):

            ref_pos =reference['positions']
            ref_lab = reference['labels']
            pos = np.array([structure['positions']])
            lab = np.array([structure['labels']])

            num_atoms = ref_pos.shape[0]

            perm_subsets_fun = lambda g, l: permutation_subsets_by_reference(g, l, ref_lab)
            perm_sets, perm_sets_sizes = perm_subsets_fun(ref_pos, ref_lab)
            perm_sets_labels = [ref_lab[ps] for ps in perm_sets]

            # logger.info('Aligning to: ' + str([decode_labels(ref_lab[i]) for i in np.concatenate(perm_sets)]))

            perm_A = np.asarray(np.concatenate(perm_sets))

            align = main_align_core

            N = 1

            _, _, _, _, dist, aligned_positions, _ = \
                align(pos, lab, N, [], ref_pos[perm_A, :],
                    perm_sets, perm_sets_sizes, perm_sets_labels, perm_subsets_fun,
                    return_distance=True,
                    return_positions=True,
                    return_labels=True,
                    return_permutations=True,
                    return_rotations=True,
                    return_centroid=True,
                    stride=1, n_cores=1)

            structure['positions'] = aligned_positions[0]

            dist = np.sqrt(dist)/(num_atoms-1)

            return dist, structure
            

        def main(self, plot_object='density'):

            histograms, edges, bin_volumes, num_frames = self.load_data()

            print(self.scene)
            self.scene.print_traits()
            self.scene.background = white
            #self.scene.background = black
            self.scene.foreground = black
            #self.scene.foreground = white

            self.spheres = {}

            if hasattr(self.CONFIG, 'CATION'):
                CATION = self.CONFIG.CATION

                self.spheres[CATION] = []
                self.spheres[CATION].append(self.draw_sphere(
                        r=self.ionic_radius[CATION],
                        color=self.colors[CATION],
                        opacity=1.0,
                        resolution=100,
                        figure=self.scene.mayavi_scene))

                self.balls['first_shell_cutoff'] = self.draw_sphere(
                        res=20j,            
                        representation='wireframe',
                        r=self.first_shell_cutoff[CATION],
                        color=convert_color((0, 0, 0)),
                        # color=self.colors[CATION],
                        opacity=0.1,
                        resolution=1,
                        figure=self.scene.mayavi_scene)

            # self.draw_sphere(r=4, color=self.blue, opacity=0.05)
            # self.draw_sphere(r=5.5, color=self.blue, opacity=0.05)

            plot_centroids = False
            plot_centroids = True

            if plot_centroids is True:
                tmp = np.load(self.CONFIG.file_centroid)
                # tmp = np.load(self.CONFIG.file_centroid_perturbed)
                # tmp = np.load(self.CONFIG.file_aligned_metadata)
                ref_pos = tmp['centroid']
                ref_lab = tmp['centroid_lab']

                for p, l in zip(ref_pos, ref_lab):

                    L = decode_labels(l)

                    if L not in self.spheres:
                        self.spheres[L] = []

                    self.spheres[L].append(self.draw_sphere(
                            pos=-p, # somwhow we need to put minus (?)
                            r=self.ionic_radius[L],
                            color=self.colors[L],
                            opacity=0.15,
                            resolution=100,
                            figure=self.scene.mayavi_scene))

                    sph_id = random_string()
                    self.spheres_props[sph_id] = {
                        'center': p,
                        'label': L,
                        'id': sph_id,
                    }

                    # self.draw_cylinder(-p, (0,0,0,), figure=self.scene.mayavi_scene)

            # tmp = np.load(self.CONFIG.file_centroid_new)
            # ref_pos = tmp['centroid']
            # ref_lab = tmp['centroid_lab']
            #
            # for p, l in zip(ref_pos, ref_lab):
            #     self.draw_sphere(
            #             pos=-p, # somwhow we need to put minus (?)
            #             r=self.ionic_radius[decode_labels(l)],
            #             color=self.colors[decode_labels(l)],
            #             opacity=0.15,
            #             resolution=100,
            #             figure=self.scene.mayavi_scene)

            for atom_type in histograms:

                # src = mlab.pipeline.scalar_field(s)
                # mlab.pipeline.iso_surface(src, contours=20, opacity=0.5, color=colors[atom_type])

                if atom_type not in self.exclude_atom_types:

                    self.displayed_atoms.add(atom_type)

                    s, x, y, z, dist = self.normalize_data(atom_type, histograms, edges, bin_volumes, num_frames)

                    print(f'{atom_type}:')

                    # Get correct estimates of density properties
                    s[dist > self.CONFIG.nhood_distance] = np.nan
                    self.stats[atom_type] = self.statistics(s, nan=True, do_print=True)

                    s[dist > self.CONFIG.nhood_distance] = 0

                    # plot
                    if 'void' in plot_object:
                        if atom_type in self.void_atom_types:
                            cutoff = self.stats[atom_type]['mean'] / 100.0
                            self.void_data_mean = self.stats[atom_type]['mean']
                            self.void_data = [s, x, y, z, dist]
                            self.plot_voids(s, x, y, z, dist, cutoff)

                    if 'density' in plot_object:

                        if self.plot_type == 'volume':
                            self.volume_data[atom_type] = [s, ]
                            self.plot_density_volume(s, x, y, z, dist, atom_type)

                        elif self.plot_type == 'contour':
                            self.contour_data[atom_type] = [s, x, y, z, dist, atom_type]

                            s = np.copy(s)
                            s[dist > default_crop_distance] = np.nan

                            self.plot_density_contour(s, x, y, z, dist, atom_type)
                            for cnt in self.contour_levels[atom_type]:
                                mean = self.stats[atom_type]['mean']
                                density_cutoff = cnt[0] * mean
                                self.whats_within_contour(density_cutoff, atom_type)
                        else:
                            raise(ValueError(f"Unknown plot_type {self.plot_type}"))

                    if 'density' not in plot_object and 'void' not in plot_object:
                        raise(ValueError(f"Unknown plot_object {plot_object}"))

            scale_by_atom_weight = False
            # plot_combined = True
            plot_combined = False
            if plot_combined is True:

                print(f'Combined plot:')

                for i, atom_type in enumerate(histograms):
                    s, x, y, z, dist = self.normalize_data(atom_type, histograms, edges, bin_volumes, num_frames)

                    if scale_by_atom_weight is True:
                        s *= self.atom_weight[atom_type]

                    if i == 0:
                        S = np.copy(s)
                    else:
                        S += s


                # Get correct estimates of density properties
                s[dist > self.CONFIG.nhood_distance] = np.nan
                self.stats['all'] = self.statistics(S, nan=True, do_print=True)

                s[dist > self.CONFIG.nhood_distance] = 0

                cutoff = self.stats['all']['mean'] / 3
                self.plot_voids(S, x, y, z, dist, cutoff)

            # https://github.com/enthought/mayavi/issues/491
            # self.scene.mayavi_scene.renderer.use_depth_peeling = True

            title = (self.CONFIG.cfg_short
                    + 1*" " + self.CONFIG.description
                    + 4*" " + "/".join(self.CONFIG.file_histogram.parts[-2:])
                    + 4*" " + f"#frames: {num_frames}")

            mlab.title(title, color=title_text_color, figure=self.scene.mayavi_scene, height=0.02, size=0.2)

            if False:
                mlab.show()

                    # if atom_type in ['OW', 'CL']:
                        # mlab.pipeline.volume(sf) #color=colors[atom_type],
                        # mlab.pipeline.volume(sf, #color=colors[atom_type],
                        #     vmin=5, vmax=50) # K
                            # vmin=10, vmax=200) # NA
                            # vmin=100, vmax=5000) # LI
                        # mlab.pipeline.volume(sf, color=my_red, #color=colors[atom_type],
                        #     vmin=100, vmax=200) # K
                            # vmin=10, vmax=200) # NA
                            # vmin=100, vmax=5000) # LI
                        # mlab.contour3d(
                        #     x, y, z, s, color=colors[atom_type],
                        #     contours=[500, 1000,], opacity=0.25)
                        #     # contours=[d, 2*d,], opacity=0.25)
                    # if atom_type == 'HW':
                    #     mlab.pipeline.volume(sf, #color=colors[atom_type],
                            # vmin=1.2, vmax=1.5)
                    #         vmin=1.5, vmax=1.8)
                    #         # vmin=2, vmax=4)
                    #         # vmin=5000, vmax=10000)
                    #     # mlab.contour3d(
                    #     #     x, y, z, s, color=colors[atom_type],
                    #     #     contours=list(np.asarray([1.6])*1e3), opacity=0.2)
                    # if atom_type == 'LI':
                    #     mlab.pipeline.volume(sf, #color=colors[atom_type],
                    #         )
                            # vmin=100, vmax=400)

                    # if CATION == 'LI':
                    #     mlab.contour3d(
                    #         x, y, z, s, color=colors[atom_type],
                    #         contours=[m + 0.1*d, ], opacity=0.01)
                    #     mlab.contour3d(
                    #         x, y, z, s, color=colors[atom_type],
                    #         contours=[m + 2*d, m + 3*d, m+4*d, ], opacity=0.1)
                    #     mlab.contour3d(
                    #         x, y, z, s, color=colors[atom_type],
                    #         contours=[m + 20*d, m + 200*d ], opacity=0.3)
                    # else:
                    #     mlab.contour3d(
                    #         x, y, z, s, color=colors[atom_type],
                    #         # contours=[0.005], opacity=0.01)
                    #         contours=[m + 0.1*d, ], opacity=0.01)
                    #
                    #     mlab.contour3d(
                    #         x, y, z, s, color=colors[atom_type],
                    #         # contours=[0.02, 0.1, 0.2, 0.3, 0.4], opacity=0.1)
                    #         contours=[m + 2*d, m + 3*d, m+4*d, ], opacity=0.1)
                    #
                    #     mlab.contour3d(
                    #         x, y, z, s, color=colors[atom_type],
                    #         # contours=[0.02, 0.1, 0.2, 0.3, 0.4], opacity=0.1)
                    #         # contours=[m + 2*d, m + 20*d, m + 200*d ], opacity=0.1)
                    #         contours=[m + 20*d, m + 200*d ], opacity=0.3)


                    # mlab.contour3d(s, opacity=0.05, contours=30, color=colors[atom_type]);
                    # mlab.pipeline.iso_surface(src, contours=[s.mean() + 2.0*s.std(), ], opacity=0.1, color=colors[atom_type])
                    # mlab.pipeline.iso_surface(src, contours=[s.mean() + 0.1*s.std(), ], opacity=0.01, color=colors[atom_type])
                # mlab.pipeline.iso_surface(src, contours=[s.min() + 0.1*s.ptp(), ], opacity=0.2, color=colors[atom_type])
                # mlab.pipeline.iso_surface(src, contours=[s.max() - 0.1*s.ptp(), ],)

    return ShellPlot3d

if __name__ == '__main__':

    main()
