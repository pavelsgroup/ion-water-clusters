# coding: utf-8
import numpy as np
import h5py
import scipy
import matplotlib
from tqdm import tqdm
import seaborn as sns
import pandas as pd
from pathlib import Path
import numba as nb

from IPython import get_ipython
if get_ipython() is not None:
    get_ipython().run_line_magic('load_ext', 'autoreload')
    get_ipython().run_line_magic('autoreload', '2')

    get_ipython().run_line_magic('matplotlib', 'inline')
    # matplotlib.use('Qt5Agg')

import matplotlib.pyplot as plt

from iwc.common.common import timer, encode_labels, decode_labels
import iwc.config as config

from iwc.shell_distances import ShellClasses

"""

conda install -c conda-forge numpy scipy matplotlib tqdm joblib pandas seaborn


conda install -c conda-forge ipykernel
python -m ipykernel install --user --name iwc

"""

# ------------------------------------------------------------------------------

@nb.njit
def rdf_core(pos, bin_edges, out=None):
    """
    - Much faster than simple numpy/python
    * uses numba to run the loops and continue/break when needed
    """
    for j in range(pos.shape[0]):
        r = np.linalg.norm(pos[j,:])
        if r < bin_edges[0]:
            continue
        for i in range(bin_edges.size -1):
            if r < bin_edges[i+1]:
                out[i] += 1
                break

@nb.njit
def rdf_core_alt(pos, lab, target_label, bin_edges_squared, out=None):
    """
    - Faster than rdf_core, but requires squared bin edges
    * uses numba to run the loops and continue/break when needed
    * uses square of the norm to save cpu ops
    * filters data based on label
    """
    for j in range(pos.shape[0]):
        if lab[j] == target_label:
            rs = 0
            for k in range(3):
                rs += pos[j,k] * pos[j,k]
            if rs < bin_edges_squared[0]:
                continue
            for i in range(bin_edges_squared.size -1):
                if rs < bin_edges_squared[i+1]:
                    out[i] += 1
                    break

@nb.njit
def rdf_core_alt_nocheck(pos, bin_edges_squared, out=None):
    """
    - Faster than rdf_core_alt if there is no need for checking label
    - Slower than rdf_core_alt if we check the label first using numpy
    * uses numba to run the loops and continue/break when needed
    * uses square of the norm to save cpu ops
    """
    for j in range(pos.shape[0]):
        rs = 0
        for k in range(3):
            rs += pos[j,k] * pos[j,k]
        if rs < bin_edges_squared[0]:
            continue
        for i in range(bin_edges_squared.size -1):
            if rs < bin_edges_squared[i+1]:
                out[i] += 1
                break

@timer
def rdf(positions, labels, target_label=None, num_bins=100, rdf_range=(0.0, 10.0)):

    bin_edges = np.linspace(rdf_range[0], rdf_range[1],
            num=num_bins+1, endpoint=True)
    bin_edges_sq = np.square(bin_edges)
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2
    bin_counts = np.zeros(num_bins, dtype=int)

    if target_label is not None:
        for pos, lab in zip(positions, labels):
            # i = lab == target_label
            # rdf_core(pos[i,:], bin_edges, out=bin_counts)
            # rdf_core_alt_nocheck(pos[i,:], bin_edges_sq, out=bin_counts)
            rdf_core_alt(pos, lab, target_label, bin_edges_sq, out=bin_counts)
    else:
        for pos, lab in zip(positions, labels):
            # rdf_core(pos, bin_edges, out=bin_counts)
            rdf_core_alt_nocheck(pos, bin_edges_sq, out=bin_counts)


    return bin_counts, bin_edges, bin_centers

def plot_rdf(bin_counts, bin_edges, bin_centers, N, x_shift=0, **kwargs):

    normalization = 1 / N

    plt.plot(bin_centers + x_shift, bin_counts * normalization, **kwargs)

def plot_rdf_norm(bin_counts, bin_edges, bin_centers, N, x_shift=0, **kwargs):

    normalization = 1 / N / bin_centers**2

    plt.plot(bin_centers + x_shift, bin_counts * normalization, **kwargs)

def main(CONFIG, do_plot=False, target_labels=None, rdf_range=None, plot_object='rdf', rdf_kwargs={}, plot_kwargs={}):

    # Data selection class
    sc = ShellClasses(CONFIG)

    # Select data by configuration
    ind, num_groups = sc.select(CONFIG.configuration_in_shells)

    # Load data
    pos, lab = sc.load_data(ind)

    # --------------------------------------------------------------------------

    if rdf_range is None:
        rdf_range=(0.0, CONFIG.nhood_distance)

    OUT = {}

    for target_label in target_labels:
        if target_label == 'all':
            L = None
        elif target_label is not None:
            L = encode_labels(target_label)[0]
        else:
            L = None

        bin_counts, bin_edges, bin_centers = rdf(pos, lab,
                target_label=L, rdf_range=rdf_range, **rdf_kwargs)
        N = pos.shape[0]

        if do_plot is True:
            if plot_object == 'rdf':
                plot_rdf(bin_counts, bin_edges, bin_centers, N, **plot_kwargs)
            elif plot_object == 'rdf_norm':
                plot_rdf_norm(bin_counts, bin_edges, bin_centers, N, **plot_kwargs)
            else:
                raise(ValueError(f"Unrecognised plot_object {plot_object}"))

        OUT[target_label] = {
            'bin_counts': bin_counts,
            'bin_edges': bin_edges,
            'bin_centers': bin_centers,
            'num_groups': N,
        }

    return OUT


if __name__ == '__main__':

    CONFIG = config.Config(stride=100, CATION='NA')
    main(CONFIG)
