# coding: utf-8
import os
import numpy as np
import h5py
import scipy
import matplotlib
import time
import tqdm
# from joblib import Parallel, delayed
from pathlib import Path
import pprint
from copy import copy, deepcopy

# from IPython import get_ipython
# if get_ipython() is not None:
#     get_ipython().run_line_magic('load_ext', 'autoreload')
#     get_ipython().run_line_magic('autoreload', '2')

#     get_ipython().run_line_magic('matplotlib', 'inline')
#     # matplotlib.use('Qt5Agg')

import matplotlib.pyplot as plt

from iwc.common.common import logger, notqdm, disp_title, timer, split, chunk_loader, encode_labels, decode_labels

try:
    from joblib import Parallel, delayed
except ModuleNotFoundError:
    # ImportWarning
    logger.warning("Module joblib not found, parallel processing will be disabled")

import iwc.config as config

disable_tqdm = bool(os.environ.get("DISABLE_TQDM"))
if disable_tqdm is True:
    print("encountered environment variable DISABLE_TQDM, disabling tqdm")
    tqdm = notqdm

pp = pprint.PrettyPrinter(indent=4)

"""

conda install -c conda-forge numpy scipy matplotlib tqdm joblib pandas seaborn numba hashlib


conda install -c conda-forge ipykernel
python -m ipykernel install --user --name iwc

"""

# ------------------------------------------------------------------------------

class NoDataFoundError(ValueError): pass


class HistogramAnalyser():
    def __init__(self, CONFIG, disable_tqdm=False):

        CONFIG.setup_output_files()

        self.file_aligned = CONFIG.file_aligned
        self.aligning_method = CONFIG.aligning_method

        self.configuration_in_shells = CONFIG.configuration_in_shells
        self.hist_range = CONFIG.hist_range
        self.num_bins = CONFIG.num_bins

        self.histogramdd_kwargs = {
            'bins': self.num_bins,
            'range': self.hist_range,
        }

        self.n_cores = CONFIG.n_cores

        self.disable_tqdm=disable_tqdm

        self.print_config()

    def print_config(self, indent=4):
        print("Selected config details:")
        for attr in ["configuration_in_shells", "aligning_method", "hist_range", "num_bins"]:
            print(" "*indent + "{}: {}".format(attr, getattr(self, attr)))

    @timer
    def load_data(self, j=0, k=None):

        logger.debug(f"Reading from {self.file_aligned}")

        if k is None:
            k = self.get_data_size()

        with h5py.File(self.file_aligned, 'r') as f:

            labels = f['labels'][j:k]
            #labels = np.concatenate(f['labels'][j:k], axis=0)

            if self.aligning_method == 'distance_intervals':
                shells = np.concatenate(f['shells'][j:k], axis=0)
            else:
                shells = None

            positions = np.copy(f['positions'][j:k])

            num_groups = positions.shape[0]

        ind = np.ones_like(labels, dtype=bool)

        for i, pos in enumerate(positions):
            if np.any(np.isnan(pos)):
                logger.critical(f"NaN encountered at position {i} -- applying patch")
                ind[i] = 0
            else:
                positions[i] = np.reshape(pos, (-1, 3))

        labels = np.concatenate(labels[ind], axis=0)
        positions = np.concatenate(positions[ind], axis=0)

        assert labels.dtype == 'uint8'

        if self.aligning_method == 'distance_intervals':
            assert shells.dtype == 'uint8'
        assert positions.dtype == 'float32'

        logger.debug(f"Loaded {num_groups} structures")

        return positions, labels, shells, num_groups

    def get_data_size(self):
        with h5py.File(self.file_aligned, 'r') as f:
            n = f['positions'].shape[0]
        return n

    def calculate_density(self, data, return_bin_volumes=False):

        num_atoms = data.shape[0]

        # print(self.histogramdd_kwargs)

        # histograms
        H, E = np.histogramdd(data, density=False, **self.histogramdd_kwargs)

        if return_bin_volumes is True:
            # we need to do normalization by ourselves
            bin_sizes = [np.diff(e) for e in E]
            X, Y, Z = np.meshgrid(*bin_sizes, indexing='ij')
            bin_volumes = X * Y * Z
        else:
            bin_volumes = None

        H = H.astype('int64')

        # if PLOT['3d_marginal_histograms']:
        #     for dim in [0, 1, 2]:
        #         print(atom_type)
        #         plot_3d_marginal_histograms(H, E, dim, cmap=colormaps[atom_type])

        return H, E, bin_volumes, num_atoms

    @timer
    def core_distance_intervals(self, positions, labels, bins, range, num_groups, str_labels_set, int_labels_set):

        print(" " * 4 + f"If the distribution was uniform, there would be "
            f"on average {num_groups / np.product(bins):.2g} entries per bin.")

        num_atoms = {}
        histograms = {}
        edges = {}
        bin_volumes = {}

        for atom_type, int_atom_type in zip(str_labels_set, int_labels_set):

            histograms[atom_type] = []
            edges[atom_type] = []
            bin_volumes[atom_type] = []
            num_atoms[atom_type] = []

            # select propper data
            data = positions[labels == int_atom_type]

            H, E, V, A = self.calculate_density(data, bins, range, num_groups)

            histograms[atom_type].append(H)
            edges[atom_type].append(E)
            bin_volumes[atom_type].append(V)
            num_atoms[atom_type].append(A)


            # histrograms separated by solvation shells
            if atom_type in shell_intervals:
                num_shells = len(shell_intervals[atom_type])
            else:
                shells[labels == int_atom_type] = 0
                num_shells = 1
            shell_ids = np.arange(num_shells)

            for s in shell_ids:

                # select propper data
                data = positions[np.logical_and(labels == int_atom_type, shells == s)]

                H, E, V, A = self.calculate_density(data, bins, range, num_groups)

                histograms[atom_type].append(H)
                edges[atom_type].append(E)
                bin_volumes[atom_type].append(V)
                num_atoms[atom_type].append(A)

        return histograms, edges, bin_volumes, num_atoms

    def data_reduction(self, data, r):

        if data is not None:
            histograms, edges, bin_volumes, num_atoms = data
        else:
            histograms, edges, bin_volumes, num_atoms = ({}, {}, {}, {},)

        N, H, E, V = r

        for atom_type in N:
            if atom_type not in num_atoms:
                num_atoms[atom_type] = N[atom_type]
                histograms[atom_type] = H[atom_type]
                edges[atom_type] = E[atom_type]
                bin_volumes[atom_type] = V[atom_type]
            else:
                num_atoms[atom_type] += N[atom_type]
                histograms[atom_type] += H[atom_type]
                
                for i in np.arange(len(E[atom_type])):
                    assert np.all(np.asarray(edges[atom_type][i]) == np.asarray(E[atom_type][i]))
                
                if V is not None:
                    assert(np.all(bin_volumes[atom_type] == V[atom_type]))
        
        return (histograms, edges, bin_volumes, num_atoms)

    def core_main_loop_parallel(self, myrange, prange):
        
        # AA = np.zeros(num_groups)

        # for ii, (jj, kk) in enumerate(zip(prange[:-1], prange[1:])):
        #     kk +=1
        #     for (j,k) in zip(myrange[jj:kk-1], myrange[jj+1:kk]):
        #         AA[j:k] += 1
        
        # if np.all(AA == 1):
        #     raise ValueError("success")
        # else:
        #     raise ValueError("error")
       
        data = None

        with Parallel(n_jobs=self.n_cores) as parallel:
            for ii, (jj, kk) in enumerate(zip(prange[:-1], prange[1:])):
                kk +=1
                results = parallel(delayed(self.data_processor)(j, k)
                    for (j,k) in zip(myrange[jj:kk-1], myrange[jj+1:kk]))
                
                for r in results:
                    data = self.data_reduction(data, r)

        return data


    def core_main_loop(self, myrange):

        num_atoms = {}
        histograms = {}
        edges = {}
        bin_volumes = {}

        for it, (j, k) in tqdm.tqdm(enumerate(zip(myrange[:-1], myrange[1:])), total=myrange.size-1, disable=self.disable_tqdm):

            positions, labels, shells, num_groups = self.load_data(j, k)

            str_labels_set = decode_labels(set(labels))
            int_labels_set = list(set(labels))

            for atom_type, int_atom_type in zip(str_labels_set, int_labels_set):

                data = positions[labels == int_atom_type]

                if it == 0:

                    histograms[atom_type], \
                    edges[atom_type], \
                    bin_volumes[atom_type], \
                    num_atoms[atom_type] = self.calculate_density(data)

                else:
                    # histograms, edges, bin_volumes
                    H, E, V, N = self.calculate_density(data)

                    histograms[atom_type] += H
                    num_atoms[atom_type] += N

                    for i in np.arange(len(E)):
                        assert np.all(np.asarray(edges[atom_type][i]) == np.asarray(E[i]))

                    if V is not None:
                        assert(np.all(bin_volumes[atom_type] == V))

        return histograms, edges, bin_volumes, num_atoms

    @timer
    def core(self):

        num_groups = self.get_data_size()

        if num_groups == 0:
            logger.critical(f"No data found in {self.file_aligned}")
            raise NoDataFoundError()

        logger.spam(" " * 4 + f"{num_groups} data found. If the distribution was uniform, there would be "
            f"on average {num_groups / np.product(self.num_bins):.2g} entries per bin.")

        # Load data
        myrange = chunk_loader(num_groups, max_chunk_size=32768)
        logger.spam(f"Processing in {len(myrange)-1} chunks of ~ {myrange[1]} items each")

        if self.n_cores == 1:
            results = self.core_main_loop(myrange)

        else:
            prange = chunk_loader(len(myrange)-1, chunk_size=self.n_cores-1)
            print(f"Processing in parallel using {self.n_cores} processes")    

            results = self.core_main_loop_parallel(myrange, prange)

        histograms, edges, bin_volumes, num_atoms = results

        return {
            'histograms': histograms, 
            'edges': edges, 
            'bin_volumes': bin_volumes, 
            'num_atoms': num_atoms,
            'num_groups': num_groups, }

    def data_processor(self, j, k):

        num_atoms = {}
        histograms = {}
        edges = {}
        bin_volumes = {}

        positions, labels, shells, num_groups = self.load_data(j, k)
        
        str_labels_set = decode_labels(set(labels))
        int_labels_set = list(set(labels))

        for atom_type, int_atom_type in zip(str_labels_set, int_labels_set):

            data = positions[labels == int_atom_type]

            histograms[atom_type], \
                edges[atom_type], \
                bin_volumes[atom_type], \
                num_atoms[atom_type] = self.calculate_density(data)

        return num_atoms, histograms, edges, bin_volumes

    def get_metadata(self):
        return {
            'configuration_in_shells': self.configuration_in_shells,
            'hist_range': self.hist_range,
            'num_bins': self.num_bins,}


def main_backup(CONFIG):

    print_config(CONFIG)

    PLOT = {
        '3d_marginal_histograms':           False,
    }

    # --------------------------------------------------------------------------

    if CONFIG.aligning_method == 'distance_intervals':
        shell_intervals = np.load(CONFIG.folder_hyd_shells / 'shell_intervals.npy').item()

        with np.load(CONFIG.file_distance_interval) as npzfile:
            distance_intervals = npzfile['distance_intervals'].item()
    else:
        shell_intervals = []
        distance_intervals = []

    # --------------------------------------------------------------------------

    bins = CONFIG.num_bins

    if CONFIG.hist_range == 'auto':
        raise(ValueError("Automatic histogram range is currently unsupported."))
        #range = [(positions[:, i].min(), positions[:, i].max()) for i in [0, 1, 2]]
        range = [(positions.min(), positions.max()) for i in [0, 1, 2]]
    else:
        range = CONFIG.hist_range

    if CONFIG.aligning_method == 'distance_intervals':
        histograms, edges, bin_volumes, num_atoms = core_distance_intervals(positions, labels, bins, range, num_groups, str_labels_set, int_labels_set)
    else:        
        histograms, edges, bin_volumes, num_atoms, num_groups = core(CONFIG)

    print(f"Saving results to\n{CONFIG.folder_histograms}")

    CONFIG.folder_histograms.mkdir(parents=False, exist_ok=True)

    np.savez(CONFIG.file_histogram,
        histograms=histograms,
        edges=edges,
        bin_volumes=bin_volumes,
        num_frames=num_groups,
        num_atoms=num_atoms,
        distance_intervals=distance_intervals)
    print(CONFIG.file_histogram)

    # --------------------------------------------------------------------------
    if CONFIG.aligning_method == 'distance_intervals':
        for atom_type, int_atom_type in zip(decode_labels(set(labels)), list(set(labels))):
            for i, (s, e, n) in enumerate(zip(histograms[atom_type], edges[atom_type], num_atoms[atom_type])):

                # when i = 0, the variables containt summary (overall) histograms
                if i > 0 and atom_type in distance_intervals.keys():
                    # distance_intervals[atom_type]
                    if i == 1:
                        r1 = 0
                        r2 = distance_intervals[atom_type][0][1]
                    if i == 2:
                        r1 = distance_intervals[atom_type][0][1]
                        r2 = CONFIG.nhood_distance
                    if i == 3:
                        print("Not yet implemented")
                else:
                    r1 = 0
                    r2 = CONFIG.nhood_distance

                vol = 4 / 3 * np.pi * (r2**3 - r1**3)

                s = s / s.sum() * n / vol * bins[0] * bins[1] * bins[2] * 8

                s = s[s > 0]

                s = s.reshape((-1,1))

                if __debug__ and False:
                    plt.figure()
                    plt.title(f"{atom_type} - shell {i}")
                    plt.hist(s, bins=100)
                    plt.show()


# @timer
def data_combiner(data, partial_data):
    
    if data is None:
        data = {
            'num_atoms': {},
            'histograms': {},
            'edges': {},
            'bin_volumes': {},
            'num_groups': 0,
        }
    
    data['num_groups'] += partial_data['num_groups']

    for at in partial_data['num_atoms']: # at = atom_type

        if at not in data['num_atoms']:
            for key in ['num_atoms', 'histograms', 'edges', 'bin_volumes']:
                data[key][at] = partial_data[key][at]
        else:
            for key in ['num_atoms', 'histograms']:
                data[key][at] += partial_data[key][at]
            
            for i in np.arange(len(partial_data['edges'][at])):
                assert np.all(np.asarray(data['edges'][at][i])
                    == np.asarray(partial_data['edges'][at][i]))

            if partial_data['bin_volumes'][at] is not None:
                assert np.all(data['bin_volumes'][at]
                    == partial_data['bin_volumes'][at])

    return data


@timer
def save_data(filepath, data, metadata):

    filepath.parent.mkdir(parents=False, exist_ok=True)

    np.savez(filepath, **data, **metadata,
        distance_intervals=[])

    logger.debug(f"Results saved to {filepath}")


def main(CONFIG):
   
    analyser = HistogramAnalyser(CONFIG)

    data = analyser.core()
    metadata = analyser.get_metadata()

    save_data(CONFIG.file_histogram, data, metadata)


def analyser_core_pfun(analyser):
    try:
        return analyser.core()
    except NoDataFoundError:
        logger.critical(f"Error encountered when processing {analyser.file_aligned}. Attempting to continue with other files.")
        return None

@timer
def main_multiple(CONFIG, attributes):

    data = None

    n_cores = CONFIG.n_cores

    file_histogram = CONFIG.file_histogram

    metadata = None

    analysers = []
    for attr in attributes:

        for key in attr:
            setattr(CONFIG, key, attr[key])

        CONFIG.n_cores = 1
        CONFIG.setup_output_files()
        analyser = deepcopy(HistogramAnalyser(CONFIG, disable_tqdm=True))
        analysers.append(analyser)
        print(analyser.file_aligned)

        if metadata is None:
            metadata = analyser.get_metadata()
        else:
            assert metadata == analyser.get_metadata()

    if n_cores == 1:
        logger.warning("Processing serially")
        for analyser in analysers:
            try:
                data = data_combiner(data, analyser.core())
            except NoDataFoundError:
                logger.critical(f"Error encountered when processing {CONFIG.file_aligned}. Attempting to continue with other files.")
                pass
    else:
        logger.warning(f"Processing in parallel using {n_cores} processes")

        results = Parallel(n_jobs=n_cores)(delayed(analyser_core_pfun)(analyser)
                    for analyser in analysers)

        for r in results:
            if r is not None:
                data = data_combiner(data, r)

        del results
    
    if data is not None:
        save_data(file_histogram, data, metadata)
        return data['num_groups']
    else:
        return 0


def main_wrapper(CONFIG):

    total_num_groups = 0

    k = 0
    while True:
        CONFIG.centroid_id = k
        CONFIG.setup_output_files()

        if os.path.exists(CONFIG.file_aligned):
            try:
                main(CONFIG)
            except NoDataFoundError:
                logger.critical(f"Error encountered when processing {CONFIG.file_aligned}. Attempting to continue with other files.")
                pass
        else:

            file_info = CONFIG.search_for_aligned_files(k)

            if not file_info:
                logger.warning(f"file {CONFIG.file_aligned} does not exist (this may be expected)" )
            else:
                logger.info("Found the following files:")
                pp.pprint(file_info)

                num_groups = main_multiple(deepcopy(CONFIG), file_info)
                total_num_groups += num_groups
        k += 1

        if k >= 10:
            break

    return total_num_groups


if __name__ == '__main__':

    STRIDE = 100
    CATION = 'LI'

    # ARR = np.asarray(['*', 2, 2, '*', '*', '*', '*', '*'])
    # ARR = np.asarray(['*', 3, 1, '*', '*', '*', '*', '*'])
    ARR = np.asarray(['*', 4, 0, '*', '*', '*', '*', '*'])

    CONFIG = config.Config(
            CATION=CATION,
            stride=STRIDE,
            configuration_in_shells=ARR)

    main(CONFIG)
