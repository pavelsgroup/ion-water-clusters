# coding: utf-8

from IPython import get_ipython
if get_ipython() is not None:
    get_ipython().run_line_magic('load_ext', 'autoreload')
    get_ipython().run_line_magic('autoreload', '2')

import numpy as np
from iwc.cluster_plot_3d import ShellPlot3d

import argparse

"""
"""

# ------------------------------------------------------------------------------
# CONFIGURATION

import iwc.config as config
# See config.py

def main(CONFIG):
    exclude_centroids = []
    # exclude_centroids = [1,2,4,5,6]
    # exclude_centroids = [1,3,4,5,6]
    # exclude_centroids = [1,2,3,4,5,6] # Li-3-2-5

    shellplot = ShellPlot3d(CONFIG,
            plot_type='centroid',)
    shellplot.draw_centroids(exclude_centroids=exclude_centroids)
    shellplot.configure_traits(view='view_centroids')

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("--cation", type=str, required=True,
                        help="")
    parser.add_argument("--stride", type=int, default=1,
                        help="")
    parser.add_argument("--configuration", nargs='+', type=int, default=None,
                        help="")
    args = parser.parse_args()

    if args.configuration is not None:
        configuration = { 'num_ow': args.configuration[0],
            'num_cl': args.configuration[1],
            'coord_num': args.configuration[2]}
    else:
        pass
        # configuration = {'num_ow': 3, 'num_cl': 0, 'coord_num': 3},

        # configuration = {'num_ow': 4, 'num_cl': 0, 'coord_num': 4},
        # configuration = {'num_ow': 3, 'num_cl': 1, 'coord_num': 4},
        # configuration = {'num_ow': 2, 'num_cl': 2, 'coord_num': 4},
        # configuration = {'num_ow': 1, 'num_cl': 3, 'coord_num': 4},
        # configuration = {'num_ow': 0, 'num_cl': 4, 'coord_num': 4},

        # configuration = {'num_ow': 5, 'num_cl': 0, 'coord_num': 5},
        # configuration = {'num_ow': 4, 'num_cl': 1, 'coord_num': 5},
        # configuration = {'num_ow': 3, 'num_cl': 2, 'coord_num': 5},
        # configuration = {'num_ow': 2, 'num_cl': 3, 'coord_num': 5},
        # configuration = {'num_ow': 1, 'num_cl': 4, 'coord_num': 5},
        # configuration = {'num_ow': 0, 'num_cl': 5, 'coord_num': 5},

        # configuration = {'num_ow': 6, 'num_cl': 0, 'coord_num': 6},
        # configuration = {'num_ow': 5, 'num_cl': 1, 'coord_num': 6},
        # configuration = {'num_ow': 4, 'num_cl': 2, 'coord_num': 6},
        # configuration = {'num_ow': 3, 'num_cl': 3, 'coord_num': 6},
        # configuration = {'num_ow': 2, 'num_cl': 4, 'coord_num': 6},
        # configuration = {'num_ow': 1, 'num_cl': 5, 'coord_num': 6},
        # configuration = {'num_ow': 0, 'num_cl': 6, 'coord_num': 6},

        # configuration = {'num_ow': 7, 'num_cl': 0, 'coord_num': 7},
        # configuration = {'num_ow': 6, 'num_cl': 1, 'coord_num': 7},
        # configuration = {'num_ow': 5, 'num_cl': 2, 'coord_num': 7},
        # configuration = {'num_ow': 4, 'num_cl': 3, 'coord_num': 7},
        # configuration = {'num_ow': 3, 'num_cl': 4, 'coord_num': 7},
        # configuration = {'num_ow': 2, 'num_cl': 5, 'coord_num': 7},
        # configuration = {'num_ow': 1, 'num_cl': 6, 'coord_num': 7},
        # configuration = {'num_ow': 0, 'num_cl': 7, 'coord_num': 7},

        # configuration = {'num_ow': 8, 'num_cl': 0, 'coord_num': 8},
        # configuration = {'num_ow': 7, 'num_cl': 1, 'coord_num': 8},
        # configuration = {'num_ow': 6, 'num_cl': 2, 'coord_num': 8},
        # configuration = {'num_ow': 5, 'num_cl': 3, 'coord_num': 8},
        # configuration = {'num_ow': 4, 'num_cl': 4, 'coord_num': 8},
        # configuration = {'num_ow': 3, 'num_cl': 5, 'coord_num': 8},
        # configuration = {'num_ow': 2, 'num_cl': 6, 'coord_num': 8},
        # configuration = {'num_ow': 1, 'num_cl': 7, 'coord_num': 8},
        # configuration = {'num_ow': 0, 'num_cl': 8, 'coord_num': 8},

        # configuration = {'num_ow': 9, 'num_cl': 0, 'coord_num': 9},
        # configuration = {'num_ow': 8, 'num_cl': 1, 'coord_num': 9},
        # configuration = {'num_ow': 7, 'num_cl': 2, 'coord_num': 9},
        # configuration = {'num_ow': 6, 'num_cl': 3, 'coord_num': 9},
        # configuration = {'num_ow': 5, 'num_cl': 4, 'coord_num': 9},
        # configuration = {'num_ow': 4, 'num_cl': 5, 'coord_num': 9},
        # configuration = {'num_ow': 3, 'num_cl': 6, 'coord_num': 9},
        # configuration = {'num_ow': 2, 'num_cl': 7, 'coord_num': 9},
        # configuration = {'num_ow': 1, 'num_cl': 8, 'coord_num': 9},

    CONFIG = config.Config(
        CATION=args.cation,
        stride=args.stride,
        # nhood_distance=5, # Angstrom
        # nhood_distance=8, # Angstrom
        nhood_distance=10, # Angstrom
        # nhood_distance=12, # Angstrom
        aligning_method='num_atoms',
        configuration_in_shells=configuration,
        )

    main(CONFIG)
