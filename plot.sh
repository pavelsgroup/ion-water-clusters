#!/bin/bash
for i in "$@"
do
case $i in
    --cation=*)
    CATION="${i#*=}"

    ;;
    --id-range=*)
    IDRANGE="${i#*=}"
    ;;
    *)
            # unknown option
    ;;
esac
done

#echo CATION = ${CATION}
#echo IDRANGE = ${IDRANGE}

for n in $(seq 0 $IDRANGE)
do
    #echo $n
    nohup python cluster_plot_3d_$CATION.py --centroid-id $n &
done

