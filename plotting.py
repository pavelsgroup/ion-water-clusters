# coding: utf-8
import os
import re
import numpy as np
import pandas as pd

from IPython import get_ipython
if get_ipython() is not None:
    get_ipython().run_line_magic('load_ext', 'autoreload')
    get_ipython().run_line_magic('autoreload', '2')

import argparse

from iwc.common.common import logger

import iwc.config as config

# ------------------------------------------------------------------------------

parser = argparse.ArgumentParser()

parser.add_argument("--cation", type=str, required=True,
                    help="")

parser.add_argument("--stride", type=int, default=1,
                    help="")

parser.add_argument("--list", action="store_true", default=False,
                    help="")

parser.add_argument("--configuration", nargs='+', type=int, default=None,
                    help="")

# parser.add_argument("--centroid-id", type=int, default=None,
#                     help="")

# parser.add_argument("--centroid-id-range", nargs='+', type=int, default=None,
#                     help="")

args = parser.parse_args()

"""
conda install -c conda-forge numpy scipy matplotlib tqdm joblib pandas seaborn numba ipython h5py MDAnalysis

screen -S iwc
cd ~/repo/ion-water-clusters/; goconda; conda activate iwc-mayavi;

EXAMPLE USAGE:

 * list datasets & print statistics

python plotting.py --cation K --stride 100 --list

 * plot selected configuration

python plotting.py --cation K --stride 100 --configuration 5 2 7

"""

# ==============================================================================

# ------------------------------------------------------------------------------

def execute(command):
    logger.debug("Executing command:")
    logger.debug(command)
    os.system(command)

CONFIG = config.Config(
    CATION=args.cation,
    stride=args.stride,
    #nhood_distance=args.nhood_distance,
    aligning_method='num_atoms',
    configuration_in_shells={})

parent = CONFIG.folder_histograms

logger.info(f"Using folder {str(parent)}")

df = pd.DataFrame()

i = 0
for f in parent.glob('*.npz'):
    with np.load(str(f)) as npzfile:
        # logger.info(f"Loading file {str(f.name)}")
        m = re.match("(\w+)_(\d)-(\w+).npz", str(f.name))
        if m is not None:
            conf_hash = m.group(1)
            centroid_id = m.group(2)
            hist_hash = m.group(3)

        # print(npzfile.files)
        n = npzfile['num_groups']
        C = npzfile['configuration_in_shells'].item()
        cc = tuple([C['num_ow'], C['num_cl'], C['coord_num']])

        df = df.append({
                'OW': C['num_ow'],  
                'CL': C['num_cl'],
                'N': C['coord_num'],
                'c_id': centroid_id,
                'num_groups': n,
                'fname': f.name,
                'conf_hash': conf_hash,
                'hist_hash': hist_hash,
                }, ignore_index=True)

        if args.configuration is not None:
            if np.any(tuple(args.configuration) != cc):
                # print("skipping")
                continue

        if args.list is False:
            logger.info(f"Plotting file {str(f.name)}")

            command = " ".join([
                f"nohup python iwc/density_map_3d.py",
                f"--cation {args.cation}",
                f"--stride {args.stride}",
                f"--centroid-id {centroid_id}",
                f"--configuration {C['num_ow']} {C['num_cl']} {C['coord_num']}",
                f"--hist-num-bins {npzfile['num_bins'][0]} {npzfile['num_bins'][1]} {npzfile['num_bins'][2]}"
                f"&"])

            execute(command)
            i += 1

if args.list is False:
    if args.configuration is not None:
        command = " ".join([
            f"nohup python iwc/centroid_plot_3d.py",
            f"--cation {args.cation}",
            f"--stride {args.stride}",
            f"--configuration {args.configuration[0]} {args.configuration[1]} {args.configuration[2]}",
            f"&"])

        execute(command)
        i += 1

    logger.info(f"We have opened {i} plotting windows")

else:
    
    df = df.astype({'OW': int, 'CL': int, 'N': int})

    sel = df.groupby(
            ['OW', 'CL', 'N', 'conf_hash']
        ).agg(
            {'num_groups': ['sum','count']}
        ).sort_values('N')

    with pd.option_context('display.max_rows', None, 'display.max_columns', None):

        print(70*"="+"\nAll structures")

        print(df.sort_values('N'))

        print(70*"="+"\nSummary")

        print(sel)

        print(70*"="+"\nInsignificant")

        print(df.query('num_groups < 500')) 

        print(70*"="+"\nCustom selection")

        print(df.query('OW == 5').query('CL == 4'))
# ------------------------------------------------------------------------------
