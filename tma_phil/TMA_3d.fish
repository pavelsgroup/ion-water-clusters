#!/bin/fish

rm nohup.out

for f in config_tma*.ini
    nohup python ../iwc/tma_phil/cluster_plot_3d_tma.py --ini-file=$f &
    # --overwrite-parent-dir=/qrk/phil_TMA
end
