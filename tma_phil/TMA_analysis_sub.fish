#!/bin/fish
#SBATCH -J TMA
##SBATCH --qos=backfill
#SBATCH --qos=normal
##SBATCH --partition=sbigmem,bigmem
#SBATCH --partition=scpu,cpu,bfill
##SBATCH --time=0-4:00
#SBATCH --time=0-12:00
##SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=36
##SBATCH --exclusive
##SBATCH --mem=766440
##SBATCH --mem=186120
#SBATCH --mem=82280
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=ondrej.tichacek@uochb.cas.cz

# USAGE
#
#   sbatch --export=INI_FILE="..." TMA_analysis_sub.fish
#

# set -l PARENT_DIR /home1/tichacek/phil_TMA/tip3p/
set -l PARENT_DIR /home1/tichacek/phil_TMA/
set -l IWC_DIR /home1/tichacek/ion-water-clusters

goconda
conda activate spsalign-tma

# python ../iwc/tma_phil/analysis.py --do-preprocess --ini-file=$INI_FILE --overwrite-parent-dir=$PARENT_DIR

python $IWC_DIR/iwc/tma_phil/analysis.py --n-cores=36 --do-align --do-separate-by-aligning --ini-file=$INI_FILE --overwrite-parent-dir=$PARENT_DIR

python $IWC_DIR/iwc/tma_phil/analysis.py --do-histograms --num-bins=64 --ini-file=$INI_FILE --overwrite-parent-dir=$PARENT_DIR
python $IWC_DIR/iwc/tma_phil/analysis.py --do-histograms --num-bins=100 --ini-file=$INI_FILE --overwrite-parent-dir=$PARENT_DIR
python $IWC_DIR/iwc/tma_phil/analysis.py --do-histograms --num-bins=128 --ini-file=$INI_FILE --overwrite-parent-dir=$PARENT_DIR
python $IWC_DIR/iwc/tma_phil/analysis.py --do-histograms --num-bins=168 --ini-file=$INI_FILE --overwrite-parent-dir=$PARENT_DIR

python $IWC_DIR/iwc/tma_phil/tma_cube.py --num-bins=64 --ini-file=$INI_FILE --overwrite-parent-dir=$PARENT_DIR
python $IWC_DIR/iwc/tma_phil/tma_cube.py --num-bins=100 --ini-file=$INI_FILE --overwrite-parent-dir=$PARENT_DIR
python $IWC_DIR/iwc/tma_phil/tma_cube.py --num-bins=128 --ini-file=$INI_FILE --overwrite-parent-dir=$PARENT_DIR
python $IWC_DIR/iwc/tma_phil/tma_cube.py --num-bins=168 --ini-file=$INI_FILE --overwrite-parent-dir=$PARENT_DIR

