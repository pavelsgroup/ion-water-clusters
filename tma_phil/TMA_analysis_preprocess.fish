#!/bin/fish

set -e

for f in configs/config_tma*.ini
    set -l ini_file (pwd)/$f
    echo $ini_file
    # python ../iwc/tma_phil/analysis.py --do-preprocess --ini-file=$ini_file
    nohup python ../iwc/tma_phil/analysis.py --do-preprocess --ini-file=$ini_file > $ini_file.preprocess.log &
end
