#!/bin/fish

for f in configs/config_tma*.ini
    set -l ini_file (pwd)/$f
    echo $ini_file
    
    python ../iwc/tma_phil/analysis.py --do-preprocess --ini-file=$ini_file
    
    python ../iwc/tma_phil/analysis.py --n-cores=14 --do-align --do-separate-by-aligning --ini-file=$ini_file
    
    python ../iwc/tma_phil/analysis.py --do-histograms --num-bins=64 --ini-file=$ini_file
    python ../iwc/tma_phil/analysis.py --do-histograms --num-bins=128 --ini-file=$ini_file

    python ../iwc/tma_phil/tma_cube.py --num-bins=64 --ini-file=$ini_file
    python ../iwc/tma_phil/tma_cube.py --num-bins=128 --ini-file=$ini_file
end
