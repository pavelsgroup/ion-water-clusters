#!/bin/fish

for f in configs/config_tma*.ini
# for f in configs/config_tma_charm36.ini
    set -l ini_file (pwd)/$f
    echo $ini_file
    sbatch -J $f --export=INI_FILE=$ini_file TMA_analysis_sub.fish
end
