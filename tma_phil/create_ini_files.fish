#!/bin/fish

cd /wrk/phil_TMA



for f in *
    if test -d $f
        set -l name "config_tma_"$f".ini"
        echo $name
        echo \
"[global]
parent_dir: /wrk/phil_TMA/

# relative to parent_dir
data_dir: "$f"
analysis_dir: "$f"/analysis

# relative to data_dir
trajectory_file: traj_comp.xtc
topology_file: conf.gro


heuristic_alignment: false

[alignment_num]
N: 0
C: 4
H: 0
OW: 0
CL: 0" > $name

    end
end
