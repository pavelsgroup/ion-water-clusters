```
conda create -n spsalign-tma python=3.9
conda activate spsalign-tma
conda install -c conda-forge numpy scipy numba tqdm joblib MDAnalysis=2.0.0 h5py coloredlogs verboselogs

pip install -e .
```


```
vmd -e ~/repo/ion-water-clusters/iwc/plot_volume.tcl  -args `ls *.cube | sort -V
```