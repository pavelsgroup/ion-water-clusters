#!/bin/bash
#SBATCH -J iwc-benzene
# SBATCH --time=12:00:00
#SBATCH --time=48:00:00
# SBATCH --nodes=2
#SBATCH --nodes=1
#SBATCH --exclusive
#SBATCH -p bigmem
#SBATCH --mem=754G
# SBATCH -p mem
# SBATCH -p bigmem

# SBATCH --export=DISABLE_TQDM=1

# view the output file using
# col -b < $1 | less

set -e

source /opt/uochb/soft/anaconda3/2019.03/etc/profile.d/conda.sh
conda activate $HOME/conda-envs/iwc

#source /opt/uochb/soft/spack/20190608/share/spack/setup-env.sh
#spack load parallel

#cd $(mktemp -d)

WRKDIR=$HOME/iwc-run-benzene-$SLURM_JOB_ID
mkdir -p $WRKDIR
cd $WRKDIR

set +e
cp $HOME/benzene_krystof/ion-water-clusters/* .
cp -r $HOME/benzene_krystof/ion-water-clusters/iwc .
cp -r $HOME/benzene_krystof/ion-water-clusters/iwc.egg-info .
set -e

WRKDIR=$(pwd)

# export DISABLE_TQDM=1

STRIDE=1

printf "\n\
======================================================================\n\
#\n\
#  This is script:     analysis_parallel.sh\n\
#\n\
#  WRKDIR = $WRKDIR\n\
#  STRIDE = $STRIDE\n\
#\n\
======================================================================\n"

#INI_FILE=config_benzene_water.ini
#INI_FILE=config_benzene_amonia.ini
INI_FILE=config_benzene_anion_amonia.ini

python iwc/krystof_analysis.py --n-cores 36 \
      --overwrite-parent $HOME/benzene_krystof/birch_reduction/ \
      --ini-file $INI_FILE \
      --stride $STRIDE \
      --nhood-distance 10 \
      --do-search \
      --do-separate \
      --do-histo \
| col -b

# ------------------------------------------------------------------------------

printf "\n\
======================================================================\n\
#\n\
#  Finished:     analysis_parallel.sh\n\
#\n\
#  You might want to delete the tmp directory\n\
#
#  $WRKDIR\n\
#\n\
======================================================================\n"


echo $WRKDIR
